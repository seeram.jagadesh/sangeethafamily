package com.sangeetha.employee.empAudit;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.multidex.BuildConfig;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.snackbar.Snackbar;
import com.sangeetha.employee.R;
import com.sangeetha.employee.customwidget.SearchableSpinner;
import com.sangeetha.employee.empAudit.model.DataItem;
import com.sangeetha.employee.empAudit.model.StartAuditResponse;
import com.sangeetha.employee.empLogin.loginModel.EmpData;
import com.sangeetha.employee.utils.AppPreference;
import com.sangeetha.employee.utils.LanguageUtil;
import com.sangeetha.employee.utils.MyContextWrapper;
import com.sangeetha.employee.utils.ValidationUtils;

import java.util.List;
import java.util.Locale;

import timber.log.Timber;

public class EmpAudit extends AppCompatActivity implements AdapterView.OnItemSelectedListener,
        EmpAuditFragment.OnFragmentInteractionListener, AuditView {
    //////////////////////
    private static final String TAG = EmpAudit.class.getSimpleName();
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    /**
     * Represents a geographical location.
     */
    protected Location mLastLocation;
    private RelativeLayout progressBar;
    private SearchableSpinner storeSpinner;
    private String latLong = null;
    String uToken;
    AppPreference appPreference;
    private String storeCode, storeName;
    private List<DataItem> data;
    /**
     * Provides the entry point to the Fused Location Provider API.
     */
    private FusedLocationProviderClient mFusedLocationClient;

//////////////////////////////////////////////////////


    @Override
    protected void attachBaseContext(Context newBase) {
        Locale languageType = LanguageUtil.getLanguageType(newBase);
        super.attachBaseContext(MyContextWrapper.wrap(newBase, languageType));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emp_audit);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        appPreference=new AppPreference(this);
        progressBar = findViewById(R.id.progressLinear);

        uToken=appPreference.getUserToken();

        AuditPresenter auditPresenter = new AuditPresenterImp(this);
        auditPresenter.getStoreList(uToken);
        storeSpinner = findViewById(R.id.store_spinner);
        storeSpinner.setOnItemSelectedListener(this);
        storeSpinner.setTitle("Select Store");
        storeSpinner.setPositiveButton("Done");
        Button auditSubmit = findViewById(R.id.audit_submit);
        auditSubmit.setOnClickListener(v -> {
            // Log.e(TAG,latLong);
            //setFragment(EmpAuditFragment.newInstance("",""));
            if (latLong != null) {
                Timber.e(latLong);
                if (ValidationUtils.isThereInternet(EmpAudit.this)) {
                    EmpData data = appPreference.getEmpData();

                    if (data.getEmployeeId() != null && storeCode != null) {
                        Timber.e(latLong);
                        auditPresenter.startAudit(uToken,data.getEmployeeId(), storeCode, latLong);
                    } else {
                        showMessage("Please select store.");
                    }

                } else {
                    showMessage("Please check internet connection");
                }
            } else {
                showMessage("No location detected. Make sure location is enabled on the device.");
            }


        });


    }

    @Override
    public void onStart() {
        super.onStart();


    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (!checkPermissions()) {
            requestPermissions();
        } else {
            getLastLocation();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
            return true;
        }
        return (super.onOptionsItemSelected(item));
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        storeCode = data.get(position).getStoreCode();
        storeName = data.get(position).getStoreName();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    protected void setFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =
                fragmentManager.beginTransaction();
        fragmentTransaction.replace(android.R.id.content, fragment);
        fragmentTransaction.addToBackStack(fragment.getTag());
        fragmentTransaction.commit();
    }

    @Override
    public void setStoreList(List<DataItem> data) {
        this.data = data;
        String[] stores = new String[data.size()];
        for (int i = 0; i < data.size(); i++) {
            stores[i] = data.get(i).getStoreName();
        }

        storeSpinner.setAdapter(new ArrayAdapter<>(EmpAudit.this, R.layout.custom_spinner_layout, stores));
    }

    @Override
    public void showMessage(String msg) {
        Toast.makeText(EmpAudit.this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void submitStore(StartAuditResponse startAuditResponse) {
        setFragment(EmpAuditFragment.newInstance(startAuditResponse, storeName));
        hideProgress();
    }

    /**
     * Provides a simple way of getting a device's location and is well suited for
     * applications that do not require a fine-grained location and that do not need location
     * updates. Gets the best and most recent location currently available, which may be null
     * in rare cases when a location is not available.
     * <p>
     * Note: this method should be called after location permission has been granted.
     */
    @SuppressWarnings("MissingPermission")
    private void getLastLocation() {
        mFusedLocationClient.getLastLocation()
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful() && task.getResult() != null) {
                        mLastLocation = task.getResult();
                        latLong = mLastLocation.getLatitude() + "," + mLastLocation.getLongitude();
                    } else {
                        Timber.tag(TAG).w(task.getException(), "getLastLocation:exception");
                        showSnackbar(getString(R.string.no_location_detected));
                    }
                });
    }

    /**
     * Shows a {@link Snackbar} using {@code text}.
     *
     * @param text The Snackbar text.
     */
    private void showSnackbar(final String text) {
        View container = findViewById(R.id.content);
        if (container != null) {
            Snackbar.make(container, text, Snackbar.LENGTH_LONG).show();
        }
    }

    /**
     * Shows a {@link Snackbar}.
     *
     * @param mainTextStringId The id for the string resource for the Snackbar text.
     * @param actionStringId   The text of the action item.
     * @param listener         The listener associated with the Snackbar action.
     */
    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }

    /**
     * Return the current state of the permissions needed.
     */
    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(EmpAudit.this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                REQUEST_PERMISSIONS_REQUEST_CODE);
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            // Log.i(TAG, "Displaying permission rationale to provide additional context.");

            showSnackbar(R.string.permission_rationale, android.R.string.ok,
                    view -> {
                        // Request permission
                        startLocationPermissionRequest();
                    });

        } else {
            //Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            startLocationPermissionRequest();
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Timber.i("onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Timber.i("User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                getLastLocation();
            } else {
                // Permission denied.

                // Notify the user via a SnackBar that they have rejected a core permission for the
                // app, which makes the Activity useless. In a real app, core permissions would
                // typically be best requested during a welcome-screen flow.

                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the user for permission (device policy or "Never ask
                // again" prompts). Therefore, a user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.
                showSnackbar(R.string.permission_denied_explanation, R.string.settings,
                        view -> {
                            // Build intent that displays the App settings screen.
                            Intent intent = new Intent();
                            intent.setAction(
                                    Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package",
                                    BuildConfig.APPLICATION_ID, null);
                            intent.setData(uri);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        });
            }
        }
    }


}