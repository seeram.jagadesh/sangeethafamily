package com.sangeetha.employee.callRecorder.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class LogInData {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("email")
    @Expose
    private Object email;
    @SerializedName("designation")
    @Expose
    private Object designation;
    @SerializedName("token")
    @Expose
    private String token;
}
