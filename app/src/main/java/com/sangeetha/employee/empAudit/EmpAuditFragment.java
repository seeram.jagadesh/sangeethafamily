package com.sangeetha.employee.empAudit;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.sangeetha.employee.R;
import com.sangeetha.employee.empAudit.model.DataItem;
import com.sangeetha.employee.empAudit.model.DesignationsItem;
import com.sangeetha.employee.empAudit.model.DropdownDetailsItem;
import com.sangeetha.employee.empAudit.model.EmpCategoryDataItem;
import com.sangeetha.employee.empAudit.model.EmpCategoryResponse;
import com.sangeetha.employee.empAudit.model.EmployeeDetailsItem;
import com.sangeetha.employee.empAudit.model.ProAuditModule;
import com.sangeetha.employee.empAudit.model.SearchEmpItem;
import com.sangeetha.employee.empAudit.model.StartAuditResponse;
import com.sangeetha.employee.empAudit.model.SubCategoryDataItem;
import com.sangeetha.employee.empAudit.model.SubCategoryResponse;
import com.sangeetha.employee.utils.AppPreference;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EmpAuditFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EmpAuditFragment extends Fragment implements EmpFragmentView {


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private StartAuditResponse mParam1;
    private String mParam2;
    String uToken;
    AppPreference appPreference;
    private OnFragmentInteractionListener mListener;
    private RelativeLayout progressBar;

    private AppCompatSpinner empBrand;
    private RecyclerView recyclerView;
    private AuditFormAdapter auditFormAdapter;
    private SearchEmpItem empList;

    private AppCompatSpinner empDesignation;
    private ArrayList<ProAuditModule> proData = new ArrayList<>();
    private AppCompatAutoCompleteTextView empID, empMobile;
    private AppCompatEditText empName;
    private AuditPresenter auditPresenter;
    private List<DesignationsItem> designations;

    private ArrayList<AppCompatAutoCompleteTextView> numberEditText = new ArrayList<>();
    private ArrayAdapter<String> empCategoryAdapter;
    private ArrayAdapter<String> subCategoryAdapter;
    private ArrayList<String> empCategoryList = new ArrayList<>();
    private ArrayList<String> empCategoryIdList = new ArrayList<>();
    private List<EmpCategoryDataItem> empCategoryData;
    private ArrayList<String> brand;
    private ArrayList<String> desg = new ArrayList<>();
    private TextWatcher mobileNumberSearch;

    public EmpAuditFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EmpAuditFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EmpAuditFragment newInstance(StartAuditResponse param1, String param2) {
        EmpAuditFragment fragment = new EmpAuditFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getParcelable(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_emp_audit, container, false);
        appPreference = new AppPreference(getActivity());
        progressBar = view.findViewById(R.id.progressLinear);
        uToken=appPreference.getUserToken();
        AppCompatTextView storeName = view.findViewById(R.id.store_name);
        storeName.setText(mParam2);
        AppCompatEditText storeCUG = view.findViewById(R.id.store_cug);
        AppCompatTextView auditorName = view.findViewById(R.id.auditor_name);
        auditorName.setText(appPreference.getEmpData().getEmpName());
        AppCompatTextView auditTime = view.findViewById(R.id.audit_time);
        auditTime.setText(mParam1.getStartTime());
        AppCompatSpinner typeSearch = view.findViewById(R.id.type_search);
        AppCompatImageButton moreNumber = view.findViewById(R.id.add_number);
        AppCompatTextView numbers = view.findViewById(R.id.more_no);
        AppCompatButton addMore = view.findViewById(R.id.add_more);
        LinearLayoutCompat numberLayout = view.findViewById(R.id.number_layout);
        LinearLayoutCompat addMoreLayout = view.findViewById(R.id.emp_layout);

        LinearLayoutCompat designationLayout = view.findViewById(R.id.designationLinear);
        LinearLayoutCompat employeeIdLinear = view.findViewById(R.id.employeeIdLinear);
        LinearLayoutCompat brandLayout = view.findViewById(R.id.brand_layout);
        AppCompatButton submitReport = view.findViewById(R.id.audit_submit);


        recyclerView = view.findViewById(R.id.audit_recycler);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        empCategoryAdapter = new ArrayAdapter<>(getActivity(), R.layout.custom_spinner_layout, empCategoryList);
        brand = new ArrayList<>();
        typeSearch.setAdapter(empCategoryAdapter);
        subCategoryAdapter = new ArrayAdapter<>(getActivity(), R.layout.custom_spinner_layout, brand);


        //////// add more emp or pro view ///////////////////
        ProAuditModule proAuditModule = new ProAuditModule();
        proData.add(proAuditModule);
        empID = view.findViewById(R.id.pro_id);

        empName = view.findViewById(R.id.emp_name);
        empMobile = view.findViewById(R.id.search_emp_mobile);
        empDesignation = view.findViewById(R.id.emp_designation);
        AppCompatSpinner empConfirm = view.findViewById(R.id.emp_confirm);
        empBrand = view.findViewById(R.id.emp_brand);
        empBrand.setAdapter(subCategoryAdapter);


        auditPresenter = new AuditPresenterImp(this);
        auditPresenter.getBrandList(uToken);
        auditPresenter.getEmpStatus(uToken);
        auditPresenter.getDesignation(uToken);
        auditPresenter.getEmpCategory(uToken);


        typeSearch.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(!empCategoryList.get(position).equalsIgnoreCase("select")) {
                    Log.e("category",empCategoryList.get(position));
                    proAuditModule.setEmpType(empCategoryList.get(position));
                    if (empCategoryList.get(position).equalsIgnoreCase("Sangeetha Employee")) {
//                        empMobile.addTextChangedListener(mobileNumberSearch);
                        employeeIdLinear.setVisibility(View.VISIBLE);
                        designationLayout.setVisibility(View.VISIBLE);
                        brandLayout.setVisibility(View.GONE);
                    } else {
//                        empMobile.removeTextChangedListener(mobileNumberSearch);
                        auditPresenter.getSubCategory(uToken, empCategoryIdList.get(position));
                        employeeIdLinear.setVisibility(View.GONE);
                        designationLayout.setVisibility(View.GONE);
                        brandLayout.setVisibility(View.VISIBLE);
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        empDesignation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                proAuditModule.setDesignation(desg.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        empConfirm.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                proAuditModule.setConfirm(getActivity().getResources().getStringArray(R.array.update_spinner)[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        empBrand.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                proAuditModule.setBrand(brand.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        empName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                proAuditModule.setEmpName(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        empID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                auditPresenter.getEmpDetail(uToken,s.toString());
                proAuditModule.setEmpId(s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        numberEditText.add(empMobile);

//        mobileNumberSearch = new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                auditPresenter.getEmpDetail(uToken,s.toString());
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        };


        empConfirm.setAdapter(new ArrayAdapter<>(getActivity(), R.layout.custom_spinner_layout, getActivity().getResources().getStringArray(R.array.update_spinner)));
        // empBrand.setAdapter(new ArrayAdapter<>(getActivity(),R.layout.custom_spinner_layout, getActivity().getResources().getStringArray(R.array.emp_confirm)));

        // empMobile.setThreshold(2);
        //empMobile.setAdapter(new ArrayAdapter<>(getActivity(),R.layout.custom_spinner_layout,fruits));


        addMore.setOnClickListener(v -> addMoreEmp(addMoreLayout));

        moreNumber.setOnClickListener(v -> {
            if (empMobile.getText().toString().isEmpty()) {
                showMessage("Please enter first mobile number.");

            } else if (proAuditModule.getEmpNumbers().size() < 5) {
                if (proAuditModule.getEmpNumbers().size() < 1) {
                    proAuditModule.getEmpNumbers().add(empMobile.getText().toString());
                }
                addNumber(numbers, getContext(), proAuditModule);
            } else {
                showMessage("Only 5 numbers.");
            }

        });


        submitReport.setOnClickListener(v -> {

            List<EmployeeDetailsItem> employeeDetailsItems = auditFormAdapter.getEmpDetails();
            Gson gson = new Gson();
            JsonElement element = gson.toJsonTree(employeeDetailsItems, new TypeToken<List<EmployeeDetailsItem>>() {
            }.getType());
            JsonArray jsonArray = element.getAsJsonArray();
            JsonObject emp_detail = new JsonObject();
            emp_detail.add("emp_detail", jsonArray);

            for (int i = 0; i < numberEditText.size(); i++) {
                if (proData.get(i).getEmpNumbers().size() < 1 && !numberEditText.get(i).getText().toString().isEmpty()) {
                    proData.get(i).getEmpNumbers().add(numberEditText.get(i).getText().toString());
                }

            }

            JsonElement element1 = gson.toJsonTree(proData, new TypeToken<List<ProAuditModule>>() {
            }.getType());
            JsonArray jsonArray1 = element1.getAsJsonArray();
            JsonObject pro_detail = new JsonObject();
            pro_detail.add("pro_detail", jsonArray1);
            Log.e(" emp_detail ", emp_detail.toString());
             Log.e(" pro_detail ", pro_detail.toString());

            auditPresenter.submitAuditReport(uToken,mParam1.getAuditId(), appPreference.getEmpData().getEmployeeId(), mParam1.getStoreCode(), emp_detail.toString(), pro_detail.toString());
        });

        return view;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void addMoreEmp(LinearLayoutCompat layoutCompat) {

        ProAuditModule proAuditModule = new ProAuditModule();
        proData.add(proAuditModule);
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.pro_audit_form, null);
        AppCompatAutoCompleteTextView mEmpID = view.findViewById(R.id.pro_id);

        AppCompatEditText empName = view.findViewById(R.id.emp_name);
        AppCompatAutoCompleteTextView empMobile = view.findViewById(R.id.search_more_mobile);
        numberEditText.add(empMobile);
        AppCompatImageButton moreNumber = view.findViewById(R.id.add_number);
        AppCompatTextView numbers = view.findViewById(R.id.more_no);
        LinearLayoutCompat numberLayout = view.findViewById(R.id.number_layout);

        LinearLayoutCompat employeeIdLayout = view.findViewById(R.id.employeeIdLayout);
        LinearLayoutCompat brandLayout = view.findViewById(R.id.brand_layout);
        LinearLayoutCompat designationLayout = view.findViewById(R.id.designationLayout);

        AppCompatSpinner empDesignation = view.findViewById(R.id.emp_designation);
        AppCompatSpinner moreBrand = view.findViewById(R.id.emp_brand);
        AppCompatSpinner empConfirm = view.findViewById(R.id.emp_confirm);

        empDesignation.setAdapter(new ArrayAdapter<>(getActivity(), R.layout.custom_spinner_layout, desg));
        brand = new ArrayList<>();
        brand.add(0,"Select");
        subCategoryAdapter = new ArrayAdapter<>(getActivity(), R.layout.custom_spinner_layout, brand);
        moreBrand.setAdapter(subCategoryAdapter);
        AppCompatSpinner typeSearch1 = view.findViewById(R.id.type_search);

        typeSearch1.setAdapter(empCategoryAdapter);
        typeSearch1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(!empCategoryList.get(position).equalsIgnoreCase("select")) {
                    proAuditModule.setEmpType(empCategoryList.get(position));
                    if (empCategoryList.get(position).equalsIgnoreCase("Sangeetha Employee")) {
//                        empMobile.addTextChangedListener(mobileNumberSearch);
                        proAuditModule.setBrand("");
                        employeeIdLayout.setVisibility(View.VISIBLE);
                        designationLayout.setVisibility(View.VISIBLE);
                        brandLayout.setVisibility(View.GONE);
                    } else {
//                        empMobile.removeTextChangedListener(mobileNumberSearch);
                        auditPresenter.getSubCategory(uToken, empCategoryIdList.get(position));
                        employeeIdLayout.setVisibility(View.GONE);
                        designationLayout.setVisibility(View.GONE);
                        brandLayout.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        empName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                proAuditModule.setEmpName(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        mEmpID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                auditPresenter.getEmpDetail(uToken,s.toString());
                proAuditModule.setEmpId(s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        empDesignation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                proAuditModule.setDesignation(desg.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        moreBrand.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String select=parent.getSelectedItem().toString();
                if(!select.equalsIgnoreCase("select"))
                    proAuditModule.setBrand(brand.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        empConfirm.setAdapter(new ArrayAdapter<>(getActivity(), R.layout.custom_spinner_layout, getActivity().getResources().getStringArray(R.array.update_spinner)));
        empConfirm.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                proAuditModule.setConfirm(getActivity().getResources().getStringArray(R.array.update_spinner)[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        empID = mEmpID;
        this.empMobile = empMobile;
        this.empName = empName;
        layoutCompat.addView(view);

        moreNumber.setOnClickListener(v -> {

            if (empMobile.getText().toString().isEmpty()) {
                showMessage("Please enter first mobile number.");

            } else if (proAuditModule.getEmpNumbers().size() < 5) {
                if (proAuditModule.getEmpNumbers().size() < 1) {
                    proAuditModule.getEmpNumbers().add(empMobile.getText().toString());
                }
                addNumber(numbers, getContext(), proAuditModule);
            } else {
                showMessage("Only 5 numbers.");
            }
            // addNumber(numbers, getContext(), proAuditModule);
        });


    }

    private void addNumber(AppCompatTextView mobileNumber, Context context, ProAuditModule proAuditModule) {
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(context);
        View mView = layoutInflaterAndroid.inflate(R.layout.add_no, null);
        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(context);
        alertDialogBuilderUserInput.setView(mView);
        final TextInputEditText userInputDialogEditText = mView.findViewById(R.id.more_no);
        alertDialogBuilderUserInput
                .setCancelable(false)
                .setPositiveButton("Done", (dialogBox, id) -> {

                    // ToDo get user input here
                    if (mobileNumber.getText().toString().isEmpty()) {
                        mobileNumber.setText(userInputDialogEditText.getText());
                        mobileNumber.setVisibility(View.VISIBLE);
                    } else {
                        String numbers = mobileNumber.getText().toString() + ", " + userInputDialogEditText.getText();
                        mobileNumber.setText(numbers);
                    }
                    mobileNumber.setSelected(true);
                    proAuditModule.getEmpNumbers().add(userInputDialogEditText.getText().toString());
                })

                .setNegativeButton("Cancel",
                        (dialogBox, id) -> dialogBox.cancel());

        AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
        alertDialogAndroid.show();
    }


    @Override
    public void setBrandList(List<DataItem> data) {


    }

    @Override
    public void showMessage(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void auditReportStatus(boolean status) {
        if (status) {
            showMessage("Audit Report Submitted Successfully.");
            getActivity().finish();
        }
    }

    @Override
    public void setEmpName(SearchEmpItem empList) {

        this.empList = empList;
//        String[] tmpList = new String[empList.size()];
//        String[] tmpMobile = new String[empList.size()];
//        for (int i = 0; i < empList.size(); i++) {
//            tmpList[i] = empList.get(i).getEmployeeId();
//            tmpMobile[i] = empList.get(i).getEmployeeMobile();
//        }
        empID.setThreshold(2);
        String mob = empList.getEmployeeMobile();
        empMobile.setText(mob);
        empName.setText(empList.getEmployeeName());
        Log.e("emp deg", empList.getDesignation());
        Log.e("emp deg", String.valueOf(desg.indexOf(empList.getDesignation())));

        empDesignation.setSelection(desg.indexOf(empList.getDesignation()));

//        ArrayAdapter<String> idAdapter = new ArrayAdapter<>(getActivity(), R.layout.custom_spinner_layout, tmpList);
//        empID.setAdapter(idAdapter);
//        ArrayAdapter<String> numberAdapter = new ArrayAdapter<>(getActivity(), R.layout.custom_spinner_layout, tmpMobile);
//        empMobile.setAdapter(numberAdapter);
//        empID.setOnItemClickListener((parent, view, position, id) -> {
//            String mob = empList.getEmployeeMobile();
//            empMobile.setText(mob);
//            empName.setText(empList.getEmployeeName());
//            Log.e("emp deg", empList.getDesignation());
//
//            empDesignation.setSelection(designations.indexOf(empList.getDesignation().toLowerCase()));
//        });

//        empMobile.setOnItemClickListener((parent, view, position, id) -> {
//
//            empID.setText(empList.getEmployeeId());
//            empName.setText(empList.getEmployeeName());
//            if (designations.contains(empList.getDesignation())) {
//
//                empDesignation.setSelection(designations.indexOf(empList.getDesignation()));
//            }
//
//
//        });


    }

    @Override
    public void empIdError() {

    }

    @Override
    public void setEmpStatus(List<DropdownDetailsItem> dropdownDetails) {
        auditFormAdapter = new AuditFormAdapter(mParam1.getEmployeeDetails(), dropdownDetails);
        recyclerView.setAdapter(auditFormAdapter);
    }

    @Override
    public void setDesignation(List<DesignationsItem> designations) {
//        empID.clearListSelection();

        this.designations = designations;

        for (DesignationsItem item : designations) {
            desg.add(item.getDesignationName());
        }

        empDesignation.setAdapter(new ArrayAdapter<>(getActivity(), R.layout.custom_spinner_layout, desg));
    }

    @Override
    public void setEmpCategory(EmpCategoryResponse empCategory) {
        if (empCategory.isStatus()) {
            empCategoryData = empCategory.getEmpCategoryData();
            empCategoryList.add(0,"Select");
            empCategoryIdList.add("99");
            for (EmpCategoryDataItem dataItem : empCategory.getEmpCategoryData()) {
                empCategoryList.add(dataItem.getCategoryName());
            }
            for (EmpCategoryDataItem dataItem : empCategory.getEmpCategoryData()) {
                empCategoryIdList.add(dataItem.getCategoryId());
            }
            empCategoryAdapter.notifyDataSetChanged();
        } else {
            showMessage(empCategory.getMessage());
        }

    }

    @Override
    public void setSubCategory(SubCategoryResponse subCategory) {
        if (subCategory.isStatus()) {
            brand.clear();
            brand.add(0,"Select");
            for (SubCategoryDataItem item : subCategory.getSubCategoryData()) {
                brand.add(item.getBrandName());
            }
            subCategoryAdapter.notifyDataSetChanged();

        } else {
            showMessage(subCategory.getMessage());
        }

    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
