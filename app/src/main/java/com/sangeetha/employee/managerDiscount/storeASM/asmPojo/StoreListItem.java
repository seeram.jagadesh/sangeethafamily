package com.sangeetha.employee.managerDiscount.storeASM.asmPojo;

import com.google.gson.annotations.SerializedName;

public class StoreListItem {

    @SerializedName("store_code")
    private String storeCode;

    @SerializedName("store_mobile")
    private String storeMobile;

    @SerializedName("store_name")
    private String storeName;

    @SerializedName("store_id")
    private String storeId;

    @SerializedName("address")
    private String storeAddress;

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getStoreMobile() {
        return storeMobile;
    }

    public void setStoreMobile(String storeMobile) {
        this.storeMobile = storeMobile;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreAddress() {
        return storeAddress;
    }

    public void setStoreAddress(String storeAddress) {
        this.storeAddress = storeAddress;
    }

    @Override
    public String toString() {
        return
                "StoreListItem{" +
                        "store_code = '" + storeCode + '\'' +
                        ",store_mobile = '" + storeMobile + '\'' +
                        ",store_name = '" + storeName + '\'' +
                        ",store_address = '" + storeAddress + '\'' +
                        "}";
    }
}