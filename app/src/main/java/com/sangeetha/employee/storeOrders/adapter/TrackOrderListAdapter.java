package com.sangeetha.employee.storeOrders.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.sangeetha.employee.R;
import com.sangeetha.employee.storeOrders.StoreOrderDetailsActivity;
import com.sangeetha.employee.storeOrders.model.OrderHistoryItem;
import com.sangeetha.employee.storeOrders.model.OrderListItemData;
import com.sangeetha.employee.utils.CalendarUtils;

import java.util.List;

public class TrackOrderListAdapter extends RecyclerView.Adapter<TrackOrderListAdapter.CouponHistoryItem> {

    private List<OrderHistoryItem> couponHistoryData;
    private Context context;

    public TrackOrderListAdapter(List<OrderHistoryItem> couponHistoryData, Context context) {
        this.couponHistoryData = couponHistoryData;
        this.context = context;
    }

    @NonNull
    @Override
    public CouponHistoryItem onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_track_layout, parent, false);
        return new CouponHistoryItem(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CouponHistoryItem holder, int position) {
        holder.order_status.setText(couponHistoryData.get(position).getCreated_at());
        holder.note.setText(Html.fromHtml(couponHistoryData.get(position).getDescription()));
        holder.date.setText(String.format("%s %s", couponHistoryData.get(position).getChange_by(), couponHistoryData.get(position).getProcessed_user()));
    }

    @Override
    public int getItemCount() {

        return couponHistoryData.size();
    }

    static class CouponHistoryItem extends RecyclerView.ViewHolder {
        AppCompatTextView order_status, note, date;

        CouponHistoryItem(@NonNull View itemView) {
            super(itemView);
            order_status = itemView.findViewById(R.id.order_status);
            note = itemView.findViewById(R.id.note);
            date = itemView.findViewById(R.id.date);

        }
    }
}
