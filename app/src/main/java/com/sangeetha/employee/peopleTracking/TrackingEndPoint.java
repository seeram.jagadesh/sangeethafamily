package com.sangeetha.employee.peopleTracking;

import com.sangeetha.employee.peopleTracking.model.StoreResponse;
import com.sangeetha.employee.peopleTracking.model.TrackingResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface TrackingEndPoint {

    @GET("api-fetch-store")
    Call<StoreResponse> getStores();

    @FormUrlEncoded
    @POST("api-tracking-submit")
    Call<TrackingResponse> startTracking(@Field("registration_id") String regId,
                                         @Field("store_id") String storeId,
                                         @Field("imei_number") String imei,
                                         @Field("type") String type,
                                         @Field("lat_value") String latValue,
                                         @Field("lng_value") String lagValue);

    @FormUrlEncoded
    @POST("api-tracking-submit")
    Call<TrackingResponse> stopTracking(@Field("registration_id") String regId,
                                        @Field("store_id") String storeId,
                                        @Field("imei_number") String imei,
                                        @Field("type") String type,
                                        @Field("lat_value") String latValue,
                                        @Field("lng_value") String lagValue,
                                        @Field("track_id") String trackingId);


}
