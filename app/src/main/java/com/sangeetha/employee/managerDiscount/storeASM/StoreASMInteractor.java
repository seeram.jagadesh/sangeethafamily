package com.sangeetha.employee.managerDiscount.storeASM;

import com.sangeetha.employee.CommonResponse;
import com.sangeetha.employee.checkStock.StockCheckResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.MakeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ProductTypeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.SubmitDiscountResponse;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.ApproveDiscountResponse;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.DiscountTypeResponse;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.PhoneDetailsResponse;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.RejectReasonResponse;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.StoreListResponse;
import com.sangeetha.employee.peopleTracking.model.StoreResponse;

public interface StoreASMInteractor {
    void getStoreList();
    void getStores(String token);

    void getMake(String userToken, String productId);

    void getModel(String userToken, String productId, String makeId);


    void getStocks(String token,String storeId,String productId,String makeId,String modelId);

    void getWaitingApproval(String empId);

    void getDiscountTypes(String token);

    void getProductType(String token, String paymentId);


    void getDiscountStatus(String token, String id);

    void getNotUsedApproval(String empId);

    void getUsedApproval(String empId);

    void discountApprove(String refId, String sourceFrom);

    void discountReject(String refId, String sourceFrom, String reason, String note);

    void getStoreList(String asmId);

    void getRejectionList(String token);

    void checkBargain(String regIs, String totalAmount, String empId);

    void submitBargain(String refId, String totalAmount, String empId, String bargainOption, String accessoryValue, String vasValue, String accessoryProduct, String vasProduct);

    void getPhoneDetailByRefId(String token,String refId);


    void checkEligibleException(String token);

    void salesCheckCalculate(String token, String paymentMode, String productType, String makeId, String modelId, String discountValue,String storeId);

    void sendExceptionRequest(String token, String paymentMode, String productType, String makeId,
                              String modelId, String discountValue, String storeId, String totalAmount,
                              String customerName, String customerMobile, String customerEmail,
                              String customerMobileVerified, String employeeId);


    interface StoreASMListener {
        void showMessage(String msg);

        void setStoreList();

        void setMake(MakeResponse managerResponse);

        void setModel(ModelResponse managerResponse);

        void setApproval(ApproveDiscountResponse approval);

        void setCheckStock(StockCheckResponse stockCheckResponse);

        void setDiscountTypes(DiscountTypeResponse discountTypes);

        void setDiscountStatus(ApproveDiscountResponse approveDiscountResponse);

        void setDiscountApproval(String couponCode);


        void setDiscountReject(String msg);

        void setDiscountBargain(String msg);

        void setProductTypes(ProductTypeResponse productTypeResponse);

        void setSubmitBargain(String msg);

        void setStoreList(StoreListResponse storeList);
        void setStores(StoreResponse storeList);

        void setRejectionList(RejectReasonResponse rejectionList);

        void setDiscountCheck(String jsonData);

        void setPhoneDetail(PhoneDetailsResponse phoneDetail);


        void getCheckEligibleException(CommonResponse commonResponse);

        void getSalesCheckCalculate(SubmitDiscountResponse commonResponse);

        void sendExceptionRequest(CommonResponse commonResponse);
    }
}
