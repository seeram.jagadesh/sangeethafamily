
package com.sangeetha.employee.appointments;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class AppointmentsData {

    @SerializedName("data")
    private List<Datum> mData;
    @SerializedName("iTotalDisplayRecords")
    private Long mITotalDisplayRecords;
    @SerializedName("iTotalRecords")
    private Long mITotalRecords;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("sColumns")
    private String mSColumns;
    @SerializedName("sEcho")
    private Long mSEcho;
    @SerializedName("status")
    private Boolean mStatus;

    public List<Datum> getData() {
        return mData;
    }

    public void setData(List<Datum> data) {
        mData = data;
    }

    public Long getITotalDisplayRecords() {
        return mITotalDisplayRecords;
    }

    public void setITotalDisplayRecords(Long iTotalDisplayRecords) {
        mITotalDisplayRecords = iTotalDisplayRecords;
    }

    public Long getITotalRecords() {
        return mITotalRecords;
    }

    public void setITotalRecords(Long iTotalRecords) {
        mITotalRecords = iTotalRecords;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getSColumns() {
        return mSColumns;
    }

    public void setSColumns(String sColumns) {
        mSColumns = sColumns;
    }

    public Long getSEcho() {
        return mSEcho;
    }

    public void setSEcho(Long sEcho) {
        mSEcho = sEcho;
    }

    public Boolean getStatus() {
        return mStatus;
    }

    public void setStatus(Boolean status) {
        mStatus = status;
    }

}
