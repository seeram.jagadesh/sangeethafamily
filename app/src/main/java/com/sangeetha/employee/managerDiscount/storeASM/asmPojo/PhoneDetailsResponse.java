package com.sangeetha.employee.managerDiscount.storeASM.asmPojo;

import com.google.gson.annotations.SerializedName;


public class PhoneDetailsResponse {

    @SerializedName("data")
    private DataItem data;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private boolean status;

    public DataItem getData() {
        return data;
    }

    public void setData(DataItem data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "ApproveDiscountResponse{" +
                        "data = '" + data + '\'' +
                        ",message = '" + message + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}
