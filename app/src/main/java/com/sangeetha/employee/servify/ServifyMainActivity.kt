package com.sangeetha.employee.servify

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.google.android.material.navigation.NavigationView
import com.sangeetha.employee.R
import com.sangeetha.employee.servify.model.data.ServifyHistoryData

class ServifyMainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, ServifyHistoryFragment.OnListFragmentInteractionListener {

    private var toolbar: Toolbar? = null
    private var drawerLayout: DrawerLayout? = null
    private var navigationView: NavigationView? = null
    var navController: NavController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_servify_main)
        bindViews()
        setUpToolbar()
        setUpNavigation()
        navigationView!!.setNavigationItemSelectedListener(this)
    }

    private fun bindViews() {
        toolbar = findViewById(R.id.servify_toolbar)
        drawerLayout = findViewById(R.id.servify_drawer_layout)
        navController = Navigation.findNavController(this, R.id.servify_main)
        navigationView = findViewById(R.id.servify_nav_view)

    }

    private fun setUpToolbar() {
        setSupportActionBar(toolbar)
        val supportActionBar = supportActionBar
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true)
            supportActionBar.setDisplayShowHomeEnabled(true)
        }
    }

    private fun setUpNavigation() {
        NavigationUI.setupActionBarWithNavController(this, navController!!, drawerLayout)
        NavigationUI.setupWithNavController(navigationView!!, navController!!)
    }

    override fun onSupportNavigateUp(): Boolean {
        return NavigationUI.navigateUp(navController!!, drawerLayout)
    }

    override fun onBackPressed() {
        if (drawerLayout!!.isDrawerOpen(GravityCompat.START)) {
            drawerLayout!!.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_servify_processing -> navController!!.navigate(R.id.action_global_servifyProcessFragment)
            R.id.nav_servify_history -> navController!!.navigate(R.id.action_global_servifyHistoryFragment)
          //  R.id.nav_servify_report -> navController!!.navigate(R.id.action_global_servifyReportFragment)
        }
        drawerLayout!!.closeDrawer(GravityCompat.START)
        return true
    }



    override fun onListFragmentInteraction(item: ServifyHistoryData?) {
        val bundle = Bundle()
        bundle.putParcelable("orderData", item)
        navController!!.navigate(R.id.action_global_servifyReportFragment, bundle)


    }
}
