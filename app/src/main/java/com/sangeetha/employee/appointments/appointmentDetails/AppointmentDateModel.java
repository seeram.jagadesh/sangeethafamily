package com.sangeetha.employee.appointments.appointmentDetails;

import com.google.gson.annotations.SerializedName;

public class AppointmentDateModel {

    @SerializedName("date")
    public String date;

    @SerializedName("date_visible_text")
    public String dateVisibleText;

    @SerializedName("clickable")
    public String clickable;

    @Override
    public String toString() {
        return "AppointmentDateModel{" +
                "date='" + date + '\'' +
                ", dateVisibleText='" + dateVisibleText + '\'' +
                ", clickable='" + clickable + '\'' +
                '}';
    }

    public String getClickable() {
        return clickable;
    }

    public void setClickable(String clickable) {
        this.clickable = clickable;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDateVisibleText() {
        return dateVisibleText;
    }

    public void setDateVisibleText(String dateVisibleText) {
        this.dateVisibleText = dateVisibleText;
    }
}
