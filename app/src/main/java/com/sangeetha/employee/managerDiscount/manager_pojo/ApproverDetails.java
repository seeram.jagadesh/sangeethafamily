package com.sangeetha.employee.managerDiscount.manager_pojo;

import com.google.gson.annotations.SerializedName;

public class ApproverDetails {

    @SerializedName("employee_id")
    String employeeId;

    @SerializedName("employee_name")
    String employeeName;

    @SerializedName("mobile")
    String mobile;

    @SerializedName("email")
    String email;

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
