package com.sangeetha.employee.managerDiscount.manager_pojo;

import com.google.gson.annotations.SerializedName;

public class Data {


    @SerializedName("vas_product")
    private String vasProduct;

    @SerializedName("discount_status_text")
    private String discountStatusText;

    @SerializedName("reference_id")
    private String referenceId;

    @SerializedName("discount_amount")
    private String discountAmount;

    @SerializedName("vas_value")
    private String vasValue;

    @SerializedName("discount_percentage")
    private String discountPercentage;

    @SerializedName("asm_discount_amount")
    private String asmDiscountAmount;

    @SerializedName("bargain_option")
    private String bargainOption;

    @SerializedName("fromdate")
    private String fromdate;

    @SerializedName("invoice_date")
    private String invoiceDate;

    @SerializedName("approver_type")
    private String approverType;

    @SerializedName("model_price")
    private String modelPrice;

    @SerializedName("todate")
    private String todate;

    @SerializedName("id")
    private String id;

    @SerializedName("accessory_product")
    private String accessoryProduct;

    @SerializedName("store_id")
    private String storeId;

    @SerializedName("approver")
    private String approver;

    @SerializedName("approval_code")
    private String approvalCode;

    @SerializedName("customer_mobile")
    private String customerMobile;

    @SerializedName("coupon_code")
    private String couponCode;

    @SerializedName("approver_name")
    private String approverName;

    @SerializedName("created")
    private String created;

    @SerializedName("invoice_no")
    private String invoiceNo;

    @SerializedName("employee_name")
    private String employeeName;

    @SerializedName("model_id")
    private String modelId;

    @SerializedName("reject_reason")
    private String rejectReason;

    @SerializedName("make_id")
    private String makeId;

    @SerializedName("asm_discount_percentage")
    private String asmDiscountPercentage;

    @SerializedName("total_amount")
    private String totalAmount;

    @SerializedName("reject_note")
    private String rejectNote;

    @SerializedName("accessory_value")
    private String accessoryValue;

    @SerializedName("manage_id")
    private String manageId;

    @SerializedName("internal_code")
    private String internalCode;

    @SerializedName("customer_email")
    private String customerEmail;

    @SerializedName("employee_id")
    private String employeeId;

    @SerializedName("customer_name")
    private String customerName;

    @SerializedName("coupon_status")
    private String status;

    public String getDiscountStatusText() {
        return discountStatusText;
    }

    public void setDiscountStatusText(String discountStatusText) {
        this.discountStatusText = discountStatusText;
    }

    public String getVasProduct() {
        return vasProduct;
    }

    public void setVasProduct(String vasProduct) {
        this.vasProduct = vasProduct;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getVasValue() {
        return vasValue;
    }

    public void setVasValue(String vasValue) {
        this.vasValue = vasValue;
    }

    public String getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(String discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public String getAsmDiscountAmount() {
        return asmDiscountAmount;
    }

    public void setAsmDiscountAmount(String asmDiscountAmount) {
        this.asmDiscountAmount = asmDiscountAmount;
    }

    public String getBargainOption() {
        return bargainOption;
    }

    public void setBargainOption(String bargainOption) {
        this.bargainOption = bargainOption;
    }

    public String getFromdate() {
        return fromdate;
    }

    public void setFromdate(String fromdate) {
        this.fromdate = fromdate;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getApproverType() {
        return approverType;
    }

    public void setApproverType(String approverType) {
        this.approverType = approverType;
    }

    public String getModelPrice() {
        return modelPrice;
    }

    public void setModelPrice(String modelPrice) {
        this.modelPrice = modelPrice;
    }

    public String getTodate() {
        return todate;
    }

    public void setTodate(String todate) {
        this.todate = todate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccessoryProduct() {
        return accessoryProduct;
    }

    public void setAccessoryProduct(String accessoryProduct) {
        this.accessoryProduct = accessoryProduct;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getApprover() {
        return approver;
    }

    public void setApprover(String approver) {
        this.approver = approver;
    }

    public String getApprovalCode() {
        return approvalCode;
    }

    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }

    public String getCustomerMobile() {
        return customerMobile;
    }

    public void setCustomerMobile(String customerMobile) {
        this.customerMobile = customerMobile;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getApproverName() {
        return approverName;
    }

    public void setApproverName(String approverName) {
        this.approverName = approverName;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getRejectReason() {
        return rejectReason;
    }

    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }

    public String getMakeId() {
        return makeId;
    }

    public void setMakeId(String makeId) {
        this.makeId = makeId;
    }

    public String getAsmDiscountPercentage() {
        return asmDiscountPercentage;
    }

    public void setAsmDiscountPercentage(String asmDiscountPercentage) {
        this.asmDiscountPercentage = asmDiscountPercentage;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getRejectNote() {
        return rejectNote;
    }

    public void setRejectNote(String rejectNote) {
        this.rejectNote = rejectNote;
    }

    public String getAccessoryValue() {
        return accessoryValue;
    }

    public void setAccessoryValue(String accessoryValue) {
        this.accessoryValue = accessoryValue;
    }

    public String getManageId() {
        return manageId;
    }

    public void setManageId(String manageId) {
        this.manageId = manageId;
    }

    public String getInternalCode() {
        return internalCode;
    }

    public void setInternalCode(String internalCode) {
        this.internalCode = internalCode;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "DataRe{" +
                        "vas_product = '" + vasProduct + '\'' +
                        ",reference_id = '" + referenceId + '\'' +
                        ",discount_amount = '" + discountAmount + '\'' +
                        ",vas_value = '" + vasValue + '\'' +
                        ",discount_percentage = '" + discountPercentage + '\'' +
                        ",asm_discount_amount = '" + asmDiscountAmount + '\'' +
                        ",bargain_option = '" + bargainOption + '\'' +
                        ",fromdate = '" + fromdate + '\'' +
                        ",invoice_date = '" + invoiceDate + '\'' +
                        ",approver_type = '" + approverType + '\'' +
                        ",model_price = '" + modelPrice + '\'' +
                        ",todate = '" + todate + '\'' +
                        ",id = '" + id + '\'' +
                        ",accessory_product = '" + accessoryProduct + '\'' +
                        ",store_id = '" + storeId + '\'' +
                        ",approver = '" + approver + '\'' +
                        ",approval_code = '" + approvalCode + '\'' +
                        ",customer_mobile = '" + customerMobile + '\'' +
                        ",coupon_code = '" + couponCode + '\'' +
                        ",approver_name = '" + approverName + '\'' +
                        ",created = '" + created + '\'' +
                        ",invoice_no = '" + invoiceNo + '\'' +
                        ",employee_name = '" + employeeName + '\'' +
                        ",model_id = '" + modelId + '\'' +
                        ",reject_reason = '" + rejectReason + '\'' +
                        ",make_id = '" + makeId + '\'' +
                        ",asm_discount_percentage = '" + asmDiscountPercentage + '\'' +
                        ",total_amount = '" + totalAmount + '\'' +
                        ",reject_note = '" + rejectNote + '\'' +
                        ",accessory_value = '" + accessoryValue + '\'' +
                        ",manage_id = '" + manageId + '\'' +
                        ",internal_code = '" + internalCode + '\'' +
                        ",customer_email = '" + customerEmail + '\'' +
                        ",employee_id = '" + employeeId + '\'' +
                        ",customer_name = '" + customerName + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}