package com.sangeetha.employee.onlineDelivery.model.orderDetails;

import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

import java.util.List;


public class Data{

	@SerializedName("imei_no")
	private String imeiNo;

	@SerializedName("assinged_date")
	private String assignedDate;

	@SerializedName("closed_date")
	private String closedDate;

	@SerializedName("customer_mobile_number")
	private String customerMobileNumber;

	@SerializedName("payment_type")
	private String paymentType;

	@SerializedName("amount_paid")
	private String amountPaid;

	@SerializedName("order_unique_id")
	private String orderUniqueId;

	@SerializedName("order_amount")
	private String orderAmount;

	@SerializedName("payment_menthod")
	private String paidBy;

	@SerializedName("image")
	private String proofImage;

	@SerializedName("payment_reference_number")
	private String referenceNumber;

	@SerializedName("user_address")
	private UserAddress userAddress;

	@SerializedName("order_deials")
	private List<OrderDeialsItem> orderDeials;

	public String getImeiNo() {
		return imeiNo;
	}

	public void setImeiNo(String imeiNo) {
		this.imeiNo = imeiNo;
	}

	public String getAssignedDate() {
		return assignedDate;
	}

	public void setAssignedDate(String assignedDate) {
		this.assignedDate = assignedDate;
	}

	public String getClosedDate() {
		return closedDate;
	}

	public void setClosedDate(String closedDate) {
		this.closedDate = closedDate;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	@SerializedName("customer_name")
	private String customerName;


	public String getPaidBy() {
		return paidBy;
	}

	public void setPaidBy(String paidBy) {
		this.paidBy = paidBy;
	}

	public String getProofImage() {
		return proofImage;
	}

	public void setProofImage(String proofImage) {
		this.proofImage = proofImage;
	}

	public void setCustomerMobileNumber(String customerMobileNumber){
		this.customerMobileNumber = customerMobileNumber;
	}

	public String getCustomerMobileNumber(){
		return customerMobileNumber;
	}

	public void setPaymentType(String paymentType){
		this.paymentType = paymentType;
	}

	public String getPaymentType(){
		return paymentType;
	}

	public void setAmountPaid(String amountPaid){
		this.amountPaid = amountPaid;
	}

	public String getAmountPaid(){
		return amountPaid;
	}

	public void setOrderUniqueId(String orderUniqueId){
		this.orderUniqueId = orderUniqueId;
	}

	public String getOrderUniqueId(){
		return orderUniqueId;
	}

	public void setOrderAmount(String orderAmount){
		this.orderAmount = orderAmount;
	}

	public String getOrderAmount(){
		return orderAmount;
	}

	public void setUserAddress(UserAddress userAddress){
		this.userAddress = userAddress;
	}

	public UserAddress getUserAddress(){
		return userAddress;
	}

	public void setOrderDeials(List<OrderDeialsItem> orderDeials){
		this.orderDeials = orderDeials;
	}

	public List<OrderDeialsItem> getOrderDeials(){
		return orderDeials;
	}

	public void setCustomerName(String customerName){
		this.customerName = customerName;
	}

	public String getCustomerName(){
		return customerName;
	}

	@NotNull
	@Override
 	public String toString(){
		return 
			"Data{" + 
			"customer_mobile_number = '" + customerMobileNumber + '\'' + 
			",payment_type = '" + paymentType + '\'' + 
			",amount_paid = '" + amountPaid + '\'' + 
			",order_unique_id = '" + orderUniqueId + '\'' + 
			",order_amount = '" + orderAmount + '\'' + 
			",user_address = '" + userAddress + '\'' + 
			",order_deials = '" + orderDeials + '\'' + 
			",customer_name = '" + customerName + '\'' + 
			"}";
		}
}