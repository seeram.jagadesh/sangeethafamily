package com.sangeetha.employee.callRecorder.receiver;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;

import com.sangeetha.employee.R;
import com.sangeetha.employee.callRecorder.MainActivity;
import com.sangeetha.employee.callRecorder.services.CallRecordService;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import timber.log.Timber;

public class CallRecordReceiver extends BroadcastReceiver {
    public static final String TAG = CallRecordReceiver.class.getSimpleName();

    private Bundle bundle;
    private String state;
    private MediaRecorder recorder;
    private boolean inOutCall = false;
    private File audiofile;
    private boolean recordStarted = false;
    private String name, phoneNumber, outOrIn, recordPath, time;
    private boolean wasRinging = false;
    public static final String ACTION_IN = "android.intent.action.PHONE_STATE";
    public static final String ACTION_OUT = "android.intent.action.NEW_OUTGOING_CALL";
    private SharedPreferences record;
    public static String CHANNEL_ID = "CallRecorder";
    public static int notificationId = 1;
    private long callStartTime;

    @Override
    public void onReceive(Context context, Intent intent) {
        Timber.d("onReceive = action = " + intent.getAction());
        // get Shared Preference for check if enable to record
        record = context.getSharedPreferences("Records", Context.MODE_PRIVATE);
        if (intent.getAction().equals(ACTION_OUT)) {
//            if ((bundle = intent.getExtras()) != null) {
            inOutCall = true;
            phoneNumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
            outOrIn = "0";
//                name = getName(phoneNumber, context);
            name = "Test";

//                if (record.getBoolean(name, false)) {
            startRecord();
//                }
//            }
        } else if (intent.getAction().equals(ACTION_IN)) {

            if ((bundle = intent.getExtras()) != null) {
                state = bundle.getString(TelephonyManager.EXTRA_STATE);
                Timber.d("onReceive : state = " + state);

                if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                    phoneNumber = bundle.getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
//                    name = getName(phoneNumber, context);
                    name = "Test";
                    outOrIn = "2";
                    wasRinging = true;
                } else if (state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                    if (bundle.containsKey(TelephonyManager.EXTRA_INCOMING_NUMBER)
                            && bundle.getString(TelephonyManager.EXTRA_INCOMING_NUMBER) != null) {
                        phoneNumber = bundle.getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
                    }

                    Timber.d("onReceive : state = " + state + "  wasRinging = " + wasRinging + " recordStarted = " + recordStarted + "  canRecord = " + record.getBoolean(name, false));
                    if (wasRinging && !recordStarted) {
                        outOrIn = "1";
//                        if (record.getBoolean(name, false))
                        startRecord();
                    } else if (!wasRinging && !recordStarted) {
                        //In Some devices action ACTION_OUT is not getting invoked. then it will be handled here.
                        outOrIn = "0"; // This will be considered as Outgoing call
//                        if (record.getBoolean(name, false))
                        startRecord();
                    }
                } else if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                    //For identifying the missed calls
                    if (wasRinging && !recordStarted) {
                        updateDataToServer(context, false);
                    }
                    wasRinging = false;
                    if (inOutCall && recordStarted) {
                        updateDataToServer(context, true);
                        try {
                            recorder.stop();
                        } catch (Exception stopException) {
                            stopException.printStackTrace();
                        }
                        recordPath = audiofile.getAbsolutePath();
                        inOutCall = false;
                        recordStarted = false;
                        setToDB();
                        callStartTime = -1;
//                        showNotification("Outgoing Call Record Saved",
//                                "With " + name + " at time " + time + "\n" +
//                                        "saved successfully, " +
//                                        "click to open record", context, name, time);
                    }
                    if (!inOutCall && recordStarted) {
                        updateDataToServer(context, true);
                        Timber.d("onReceive END");
                        try {
                            recorder.stop();

                            recorder.release();
                        } catch (Exception stopException) {
                            stopException.printStackTrace();
                            Timber.e(stopException);
                        }
                        recordPath = audiofile.getAbsolutePath();
                        recordStarted = false;
                        setToDB();
                        callStartTime = -1;
//                        showNotification("Incoming Call Record Saved",
//                                "With " + name + " at time " + time + "\n" +
//                                        "saved successfully, " +
//                                        "click to open record", context, name, time);
                    }
                }
            }
        }
    }


    private String getName(String phoneNumber, Context context) {
        String contactName = "Unknown";
        ContentResolver resolver = context.getContentResolver();
        Uri contactsTableUri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String selection = ContactsContract.CommonDataKinds.Phone.NUMBER + " LIKE ?";
        String[] selectionArgs = {"%" + phoneNumber.substring(1) + "%"};
        Cursor cursor = resolver.query(contactsTableUri, null, selection, selectionArgs, null);
        if (cursor.moveToFirst()) {
            contactName = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
        }
        return contactName;
    }


    private void setToDB() {
//        String sql = "INSERT INTO records(name, phone_number, in_out, record_ref, _id, remark) " +
//                "VALUES('" + name + "','" + phoneNumber + "','" + outOrIn + "','" + recordPath + "','" + time + "','');";
//        MainActivity.contactsDB.execSQL(sql);
    }

    private void updateDataToServer(Context context, boolean canUploadAudioFile) {
        Bundle bundle = new Bundle();
        String callType = null;
        if (outOrIn.equals("0")) {
            callType = CallRecordService.UpdatesReceiver.BUNDLE_CALL_TYPE_OUTGOING;
        } else if (outOrIn.equals("1")) {
            callType = CallRecordService.UpdatesReceiver.BUNDLE_CALL_TYPE_INCOMING;
        } else {
            callType = CallRecordService.UpdatesReceiver.BUNDLE_CALL_TYPE_MISSED;
        }
        long duration = 0;
        if (callStartTime > 0) {
            duration = System.currentTimeMillis() - callStartTime;
            duration = TimeUnit.MILLISECONDS.toSeconds(duration);
        }
        Timber.d("updateDataToServer : callTYpe = %s, phoneNumber = %s, duration = %s", callType, phoneNumber, duration + "");

        bundle.putString(CallRecordService.UpdatesReceiver.BUNDLE_CALL_TYPE, callType);
        bundle.putString(CallRecordService.UpdatesReceiver.BUNDLE_PHONE_NUMBER, phoneNumber);
        bundle.putString(CallRecordService.UpdatesReceiver.BUNDLE_DATE, time);
        bundle.putLong(CallRecordService.UpdatesReceiver.BUNDLE_DURATION, duration);
        if (canUploadAudioFile) {
            Timber.d("updateDataToServer = can upload = %s", canUploadAudioFile);
            bundle.putString(CallRecordService.UpdatesReceiver.BUNDLE_AUDIO_FILE_PATH, audiofile.getAbsolutePath());
        }
        Intent intent = new Intent(CallRecordService.ACTION_UPDATE_CALL);
        intent.putExtras(bundle);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    private void startRecord() {
        callStartTime = System.currentTimeMillis();
        Timber.d("startRecord");
        time = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.ENGLISH).format(new Date());
        String timeForPath = new SimpleDateFormat("dd-MM-yyyy hh-mm-ss", Locale.ENGLISH).format(new Date());
        File sampleDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "/CUG Caller");
        if (!sampleDir.exists()) {
            sampleDir.mkdirs();
        }
        try {
            audiofile = File.createTempFile("Record " + timeForPath, ".wav", sampleDir);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Timber.d("startRecord : audiofile = " + audiofile.getAbsolutePath());

        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_RECOGNITION);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.AMR_NB);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        recorder.setAudioSamplingRate(16000);
        recorder.setOutputFile(audiofile.getAbsolutePath());
        try {
            recorder.prepare();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        recorder.start();
        Timber.d("startRecord : recording started ");
        recordStarted = true;
    }


    public void showNotification(String notificationTitle, String notificationText,
                                 Context context, String name, String time) {
        Timber.d("showNotification : in showNotification");
        Intent intent = new Intent(context, MainActivity.class);
//        intent.putExtra("name", name);
//        intent.putExtra("_id", time);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_IMMUTABLE);

        // Build Notification with NotificationCompat.Builder
        // on Build.VERSION < Oreo the notification avoid the CHANEL_ID
        Notification notification = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_launcher)         //Set the icon
                .setContentTitle(notificationTitle)         //Set the title of notification
                .setContentText(notificationText)           //Set the text for notification
                .setContentIntent(pendingIntent)            // Starts Intent when notification clicked
                //.setOngoing(true)                         // stick notification
                .setAutoCancel(true)                        // close notification when clicked
                .build();
        NotificationManager notificationManager = context.getSystemService(NotificationManager.class);

        // Send the notification to the device Status bar.
        notificationManager.notify(notificationId, notification);

        notificationId++;  // for multiple(grouping) notifications on the same chanel
        Timber.d("showNotification : notificationId = " + Integer.toString(notificationId));
    }


}
