package com.sangeetha.employee.storeOrders.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.sangeetha.employee.R;
import com.sangeetha.employee.storeOrders.StoreOrderDetailsActivity;
import com.sangeetha.employee.storeOrders.model.OrderListItemData;
import com.sangeetha.employee.utils.CalendarUtils;

import java.util.List;

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.CouponHistoryItem> {

    private List<OrderListItemData> couponHistoryData;
    private Context context;

    public OrderListAdapter(List<OrderListItemData> couponHistoryData, Context context) {
        this.couponHistoryData = couponHistoryData;
        this.context = context;
    }

    @NonNull
    @Override
    public CouponHistoryItem onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.store_order_history_item, parent, false);
        return new CouponHistoryItem(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CouponHistoryItem holder, int position) {
        holder.dateTxt.setText(CalendarUtils.getDateInCustomFormat("dd-MMM-YYYY", couponHistoryData.get(position).getCreated_at()));

        holder.referenceTxt.setText(couponHistoryData.get(position).getOrder_unique_id());

        holder.storeCodeTxt.setText(couponHistoryData.get(position).getAllocated_store());

        holder.name.setText(couponHistoryData.get(position).getCustomer_name());

        holder.mobile_no.setText(couponHistoryData.get(position).getCustomer_mobile());

        if (couponHistoryData.get(position).getIs_app().equalsIgnoreCase("1")) {
            holder.order_from.setText("WEB");
        } else {
            holder.order_from.setText("APP");
        }

        holder.statusTxt.setText(String.format("%s%s", couponHistoryData.get(position).getOrder_status().substring(0, 1).toUpperCase(), couponHistoryData.get(position).getOrder_status().substring(1)));

        holder.referenceTxt.setOnClickListener(v -> {
            context.startActivity(new Intent(context, StoreOrderDetailsActivity.class).putExtra("orderId", holder.referenceTxt.getText().toString()));

        });

    }

    @Override
    public int getItemCount() {
        //  Log.e("store history", "" + couponHistoryData.size());
        return couponHistoryData.size();
    }

    static class CouponHistoryItem extends RecyclerView.ViewHolder {
        AppCompatTextView dateTxt, referenceTxt, storeCodeTxt, name, mobile_no, order_from, statusTxt;
        CardView discountCardView;
        LinearLayout itemLayout;

        CouponHistoryItem(@NonNull View itemView) {
            super(itemView);
            dateTxt = itemView.findViewById(R.id.order_date);
            referenceTxt = itemView.findViewById(R.id.order_no);
            storeCodeTxt = itemView.findViewById(R.id.store_code);
            name = itemView.findViewById(R.id.name);
            mobile_no = itemView.findViewById(R.id.mobile_no);
            order_from = itemView.findViewById(R.id.order_from);
            statusTxt = itemView.findViewById(R.id.status);
            discountCardView = itemView.findViewById(R.id.discountCardView);
            itemLayout = itemView.findViewById(R.id.itemLayout);
        }
    }
}
