package com.sangeetha.employee.claimManagement;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sangeetha.employee.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProceedClaimFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProceedClaimFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ProceedClaimFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProceedClaimFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProceedClaimFragment newInstance(String param1, String param2) {
        ProceedClaimFragment fragment = new ProceedClaimFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_proceed_claim, container, false);

        //customer details///////
        AppCompatTextView claimId = view.findViewById(R.id.claim_id_tv);
        AppCompatTextView claimType = view.findViewById(R.id.claim_type_tv);
        AppCompatTextView customerName = view.findViewById(R.id.name_tv);
        AppCompatTextView customerMobile = view.findViewById(R.id.mob_num_tv);
        AppCompatTextView customerEmail = view.findViewById(R.id.email_tv);
        AppCompatTextView customerDob = view.findViewById(R.id.dob_tv);
        AppCompatTextView anniversaryDate = view.findViewById(R.id.anniversary_date_tv);

        RecyclerView invoiceRV = view.findViewById(R.id.invoice_rv);
        invoiceRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        invoiceRV.setHasFixedSize(true);
        invoiceRV.setAdapter(new InvoiceVerificationAdapter());

        RecyclerView attachmentRV = view.findViewById(R.id.attachment_rv);
        attachmentRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        attachmentRV.setHasFixedSize(true);
        attachmentRV.setAdapter(new AttachmentVerificationAdapter());

        return view;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
