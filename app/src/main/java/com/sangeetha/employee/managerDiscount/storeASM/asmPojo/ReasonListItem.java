package com.sangeetha.employee.managerDiscount.storeASM.asmPojo;

import com.google.gson.annotations.SerializedName;

public class ReasonListItem {

    @SerializedName("reason")
    private String reason;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        return
                "ReasonListItem{" +
                        "reason = '" + reason + '\'' +
                        "}";
    }
}