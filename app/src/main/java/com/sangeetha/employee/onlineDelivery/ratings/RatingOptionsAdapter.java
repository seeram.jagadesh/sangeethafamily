package com.sangeetha.employee.onlineDelivery.ratings;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.recyclerview.widget.RecyclerView;


import com.sangeetha.employee.R;

import java.util.ArrayList;
import java.util.List;

public class RatingOptionsAdapter extends RecyclerView.Adapter<RatingOptionsAdapter.ExampleViewHolder> {
    private static SingleClickListener sClickListener;
    Context mContext;
    ArrayList<String> options;
    List<Integer> backgroundOptions;
    RatingClick listClick;
    int selectedPos = -1;

    public RatingOptionsAdapter(Context cxt, ArrayList<String> textRate, List<Integer> bgRate, RatingClick ratingClick) {
//        this.exampleList = exampleList;
        this.mContext = cxt;
        this.backgroundOptions = bgRate;
        this.options = textRate;
        this.listClick = ratingClick;
    }

    @NonNull
    @Override
    public ExampleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rating_text,
                parent, false);
        return new ExampleViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ExampleViewHolder holder, int position) {

        holder.chkPaymentOption.setText(options.get(position));
        holder.bgViewLayout.setBackgroundResource(backgroundOptions.get(position));

        if (selectedPos == position) {
            holder.paymentLayout.setBackground(mContext.getResources().getDrawable(R.drawable.round_bg));
        } else {
            holder.paymentLayout.setBackground(null);
        }
    }


    @Override
    public int getItemCount() {
        return options.size();
    }

    public void selectedItem() {
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(SingleClickListener clickListener) {
        sClickListener = clickListener;
    }

    public interface SingleClickListener {
        void onItemClickListener(int position, View view);
    }

    class ExampleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        TextView chkPaymentOption;
        LinearLayout bgViewLayout;

        AppCompatRadioButton checkBoxPay;
        RelativeLayout paymentLayout;

        ExampleViewHolder(View itemView) {
            super(itemView);
            chkPaymentOption = itemView.findViewById(R.id.txtRatingView);
            paymentLayout = itemView.findViewById(R.id.mainLayoutView);
            bgViewLayout = itemView.findViewById(R.id.bgViewLayout);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            selectedPos = getAdapterPosition();
            listClick.getSelected(options.get(selectedPos));
            sClickListener.onItemClickListener(getAdapterPosition(), v);
        }
    }
}