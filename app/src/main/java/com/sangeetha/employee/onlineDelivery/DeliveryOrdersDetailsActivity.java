package com.sangeetha.employee.onlineDelivery;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import com.sangeetha.employee.R;
import com.sangeetha.employee.onlineDelivery.model.OrderListResponse;
import com.sangeetha.employee.onlineDelivery.model.orderDetails.OrderDetailsResponse;
import com.sangeetha.employee.utils.AppPreference;
import com.sangeetha.employee.utils.LanguageUtil;
import com.sangeetha.employee.utils.MyContextWrapper;
import com.sangeetha.employee.utils.ValidationUtils;
import com.squareup.picasso.Picasso;

import java.util.Locale;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class DeliveryOrdersDetailsActivity extends AppCompatActivity implements OnlineDeliveryView {

    private AppCompatTextView orderNo, name, mobileNumber, amount, address1, address2,
            address3, paymentType, txtImeiNumber, paidBy, txtProduct, txtQuantity, referenceNumberText,assignedOn,deliveredOn;
    private ProgressBar progressBar;
    private AppCompatButton button;
    private String amountStr;
    LinearLayout paidByLayout, amountPaidLayout, referenceNumberLayout,assignedLayout,deliveredLayout;
    AppCompatImageView proofImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.delivery_orders_details_layout);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getResources().getString(R.string.order_details));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        orderNo = findViewById(R.id.orderNo);
        name = findViewById(R.id.name);
        mobileNumber = findViewById(R.id.mobile_no);
        amount = findViewById(R.id.amount);
        txtImeiNumber = findViewById(R.id.txtImeiNumber);
        address1 = findViewById(R.id.address1);
        address2 = findViewById(R.id.address2);
        address3 = findViewById(R.id.address3);
        paymentType = findViewById(R.id.payment_mode);
        progressBar = findViewById(R.id.progress_bar);
        button = findViewById(R.id.collect_cash);
        proofImage = findViewById(R.id.imageProof);
        paidBy = findViewById(R.id.txtPaidBy);
        referenceNumberText = findViewById(R.id.referenceNumberText);
        txtProduct = findViewById(R.id.txtProductName);
        txtQuantity = findViewById(R.id.txtQuantity);
        assignedOn=findViewById(R.id.assignedOn);
        deliveredOn=findViewById(R.id.DeliveredOn);
        paidByLayout = findViewById(R.id.paidByLayout);
        amountPaidLayout = findViewById(R.id.amountPaidlayout);
        assignedLayout = findViewById(R.id.assignedLayout);
        deliveredLayout = findViewById(R.id.deliveredLayout);
        referenceNumberLayout = findViewById(R.id.referenceNumberLayout);

        if (getIntent().getStringExtra("value").equalsIgnoreCase("pending")) {
            button.setVisibility(VISIBLE);

        } else {
            button.setVisibility(GONE);
        }


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(DeliveryOrdersDetailsActivity.this, OnlineDeliveryOTPActivity.class);
                i.putExtra("order_id", getIntent().getStringExtra("order_id"));
                i.putExtra("amount", amountStr);
                startActivityForResult(i, 1);
            }

        });

        AppPreference appPreference = new AppPreference(this);


        OnlineDeliverPresenter onlineDeliverPresenter = new OnlineOrderPresenterImpl(this);
        if (ValidationUtils.isThereInternet(this)) {
            onlineDeliverPresenter.orderDetails(appPreference.getEmpData().getUserId(), getIntent().getStringExtra("order_id"));
        } else {
            Toast.makeText(this, "There is no internet connection.Please connect to internet.", Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void showMessage(String msg) {
        Toast.makeText(DeliveryOrdersDetailsActivity.this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(GONE);
    }

    @Override
    public void getOrders(OrderListResponse orderListResponse) {

    }

    @Override
    public void getDeliveredOrders(OrderListResponse orderListResponse) {

    }

    @Override
    public void orderDetails(OrderDetailsResponse orderDetailsResponse) {
        orderNo.setText(orderDetailsResponse.getData().getOrderUniqueId());
        name.setText(orderDetailsResponse.getData().getCustomerName());
        txtImeiNumber.setText(orderDetailsResponse.getData().getImeiNo());
        mobileNumber.setText(orderDetailsResponse.getData().getCustomerMobileNumber());
        amountStr = orderDetailsResponse.getData().getOrderAmount();
        amount.setText(String.format("₹ %s", orderDetailsResponse.getData().getOrderAmount()));
        String product = orderDetailsResponse.getData().getOrderDeials().get(0).getProductName();
        String quantity = orderDetailsResponse.getData().getOrderDeials().get(0).getQuantity();

        String assignedOnDate=orderDetailsResponse.getData().getAssignedDate();
        String deliverdOnDate=orderDetailsResponse.getData().getClosedDate();
        if(!assignedOnDate.equalsIgnoreCase("")) {
            assignedLayout.setVisibility(VISIBLE);
            assignedOn.setText(assignedOnDate);
        }else {
            assignedLayout.setVisibility(GONE);
        }
        if(!deliverdOnDate.equalsIgnoreCase("")) {
            deliveredLayout.setVisibility(VISIBLE);
            deliveredOn.setText( deliverdOnDate);
        }else {
            deliveredLayout.setVisibility(GONE);
        }
        txtProduct.setText( product);
        txtQuantity.setText(quantity);

        address1.setText(orderDetailsResponse.getData().getUserAddress().getAddress());
        address2.setText(orderDetailsResponse.getData().getUserAddress().getCity());

        address3.setText(String.format("%s,%s", orderDetailsResponse.getData().getUserAddress().getState(), orderDetailsResponse.getData().getUserAddress().getPincode()));
        paymentType.setText(orderDetailsResponse.getData().getPaymentType());

//        if (!orderDetailsResponse.getData().getAmountPaid().equalsIgnoreCase("0")
//                && !orderDetailsResponse.getData().getAmountPaid().equalsIgnoreCase("")) {
//            amountPaidLayout.setVisibility(VISIBLE);
//            amount_paid.setText(String.format("₹ %s", orderDetailsResponse.getData().getAmountPaid()));
//        } else {
//            amountPaidLayout.setVisibility(GONE);
//        }

        if (!orderDetailsResponse.getData().getReferenceNumber().equalsIgnoreCase("0")
                && !orderDetailsResponse.getData().getReferenceNumber().equalsIgnoreCase("")) {
            referenceNumberLayout.setVisibility(VISIBLE);
            referenceNumberText.setText(orderDetailsResponse.getData().getReferenceNumber());
        } else {
            referenceNumberLayout.setVisibility(GONE);
        }

        //amount_paid.setText(String.format("₹ %s", orderDetailsResponse.getData().getAmountPaid()));

        if (!orderDetailsResponse.getData().getPaidBy().equalsIgnoreCase("")) {
            paidByLayout.setVisibility(VISIBLE);
            paidBy.setText(orderDetailsResponse.getData().getPaidBy());
        } else {
            paidByLayout.setVisibility(GONE);
        }
        if (!orderDetailsResponse.getData().getProofImage().equalsIgnoreCase("")) {
            proofImage.setVisibility(VISIBLE);
            Picasso.get()
                    .load(orderDetailsResponse.getData().getProofImage())
                    .placeholder(R.drawable.ic_camera).into(proofImage);
        } else {
            proofImage.setVisibility(GONE);
        }
        if (orderDetailsResponse.getData().getPaymentType().equalsIgnoreCase("cod")) {
            button.setText(getResources().getString(R.string.collect_cash));
        } else {
            button.setText(getResources().getString(R.string.deliver));
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                button.setVisibility(GONE);
            }
        }
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        Locale languageType = LanguageUtil.getLanguageType(newBase);
        super.attachBaseContext(MyContextWrapper.wrap(newBase, languageType));
    }
}
