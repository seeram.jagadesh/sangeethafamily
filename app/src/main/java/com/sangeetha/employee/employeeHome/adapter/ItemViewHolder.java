package com.sangeetha.employee.employeeHome.adapter;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.sangeetha.employee.R;


final class ItemViewHolder extends RecyclerView.ViewHolder {

    final View rootView;
    final AppCompatImageView tvItem;
    final AppCompatTextView tvSubItem;

    ItemViewHolder(@NonNull final View view) {
        super(view);

        rootView = view;
        tvItem = view.findViewById(R.id.item_icon);
        tvSubItem = view.findViewById(R.id.item_text);
    }
}
