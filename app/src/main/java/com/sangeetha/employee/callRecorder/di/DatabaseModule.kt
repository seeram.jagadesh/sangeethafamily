package com.sangeetha.employee.callRecorder.di

import android.content.Context
import androidx.room.Room
import com.sangeetha.employee.callRecorder.domain.room.CUGDao
import com.sangeetha.employee.callRecorder.domain.room.CUGDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
class DatabaseModule {

    @Provides
    fun provideCUGDatabase(@ApplicationContext context: Context): CUGDatabase =
        Room.databaseBuilder(
            context,
            CUGDatabase::class.java,
            CUGDatabase.DATABASE_NAME
        ).build()

    @Provides
    fun provideCUGDao(cocktailDatabase: CUGDatabase): CUGDao =
        cocktailDatabase.cugDao()
}