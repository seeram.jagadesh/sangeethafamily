package com.sangeetha.employee.managerDiscount.storeASM;

import android.text.TextUtils;

import com.sangeetha.employee.CommonResponse;
import com.sangeetha.employee.checkStock.StockCheckResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.MakeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ProductTypeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.SubmitDiscountResponse;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.ApproveDiscountResponse;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.DiscountTypeResponse;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.PhoneDetailsResponse;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.RejectReasonResponse;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.StoreListResponse;
import com.sangeetha.employee.peopleTracking.model.StoreResponse;

public class StoreASMPresenterImpl implements StoreASMPresenter, StoreASMInteractor.StoreASMListener {

    private ApprovalDiscountView approvalDiscountView;
    private StoreASMInteractor storeASMListener;
    private ApprovalDetailView approvalDetailView;
    private StoreASMView storeASMView;

    public StoreASMPresenterImpl(ApprovalDiscountView approvalDiscountView) {
        this.approvalDiscountView = approvalDiscountView;
        this.storeASMListener = new StoreASMInteractorImp(this);
    }

    public StoreASMPresenterImpl(ApprovalDetailView approvalDetailView) {
        this.approvalDetailView = approvalDetailView;
        this.storeASMListener = new StoreASMInteractorImp(this);
    }

    public StoreASMPresenterImpl(StoreASMView storeASMView) {
        this.storeASMView = storeASMView;
        this.storeASMListener = new StoreASMInteractorImp(this);
    }

    @Override
    public void getStoreList() {

    }

    @Override
    public void getMake(String userToken, String productId) {
        if (storeASMView != null) {
            storeASMListener.getMake(userToken,productId);
        }
    }

    @Override
    public void getModel(String userToken, String productId, String makeId) {
        if (storeASMView != null) {
            storeASMListener.getModel(userToken,productId,makeId);
        }
    }

    @Override
    public void getStores(String token) {
        if (storeASMView != null) {
            storeASMListener.getStores(token);
        }
    }

    @Override
    public void stockCheck(String token, String storeId, String productType, String makeId, String modelId) {
        if (storeASMView != null) {
            storeASMListener.getStocks(token,storeId,productType,makeId,modelId);
        }
    }

    @Override
    public void getWaitingApproval(String empId) {
        if (approvalDiscountView != null) {
            if (TextUtils.isEmpty(empId)) {
                approvalDiscountView.showMessage("Employee id is empty");
            } else {
                approvalDiscountView.showProgress();
                storeASMListener.getWaitingApproval(empId);
            }
        }

    }

    @Override
    public void getProductType(String token, String paymentId) {
        if (storeASMView != null) {
            storeASMListener.getProductType(token,paymentId);
        }
    }

    @Override
    public void getDiscounType(String token) {
        if (approvalDiscountView != null) {
            storeASMListener.getDiscountTypes(token);
        }
    }

    @Override
    public void getNotUsedApproval(String empId) {
        if (approvalDiscountView != null) {
            if (TextUtils.isEmpty(empId)) {
                approvalDiscountView.showMessage("Employee id is empty");
            } else {
                approvalDiscountView.showProgress();
                storeASMListener.getNotUsedApproval(empId);
            }
        }

    }

    @Override
    public void getUsedApproval(String empId) {
        if (approvalDiscountView != null) {
            if (TextUtils.isEmpty(empId)) {
                approvalDiscountView.showMessage("Employee id is empty");
            } else {
                approvalDiscountView.showProgress();
                storeASMListener.getUsedApproval(empId);
            }
        }

    }

    @Override
    public void getDiscountStatus(String token, String empId) {
        if (approvalDiscountView != null) {
            if (TextUtils.isEmpty(empId)) {
                approvalDiscountView.showMessage("Employee id is empty");
            } else {
                approvalDiscountView.showProgress();
                storeASMListener.getDiscountStatus(token, empId);
            }
        }

    }

    @Override
    public void discountApprove(String refId, String sourceFrom) {
        if (approvalDetailView != null) {
            if (TextUtils.isEmpty(refId)) {
                approvalDetailView.showMessage("Reference id is empty");
            } else {
                approvalDetailView.showProgress();
                storeASMListener.discountApprove(refId, sourceFrom);
            }
        }

    }

    @Override
    public void discountReject(String refId, String sourceFrom, String reason, String note) {
        if (approvalDetailView != null) {
            if (TextUtils.isEmpty(reason)) {
                approvalDetailView.showMessage("Please select reason.");
            } else {
                approvalDetailView.showProgress();
                storeASMListener.discountReject(refId, sourceFrom, reason, note);
            }
        }
    }

    @Override
    public void getStoreList(String asmId) {
        if (storeASMView != null) {
            if (TextUtils.isEmpty(asmId)) {
                storeASMView.showMessage("asm id is empty");
            } else {
                storeASMView.showProgress();
                storeASMListener.getStoreList(asmId);
            }
        }

    }

    @Override
    public void getRejectionList(String token) {
        if (approvalDetailView != null) {
            storeASMListener.getRejectionList(token);
        }

    }

    @Override
    public void checkBargain(String refId, String totalAmount, String empId) {
        if (approvalDetailView != null) {
            if (TextUtils.isEmpty(refId) && TextUtils.isEmpty(totalAmount)) {
                approvalDetailView.showMessage("Enter total amount");
            } else {
                storeASMListener.checkBargain(refId, totalAmount, empId);
            }

        }

    }

    @Override
    public void submitBargain(String refId, String totalAmount, String empId, String bargainOption, String accessoryValue, String vasValue, String accessoryProduct, String vasProduct) {
        if (approvalDetailView != null) {
            if (TextUtils.isEmpty(refId) && TextUtils.isEmpty(totalAmount)) {
                approvalDetailView.showMessage("Enter total amount");
            } else if (bargainOption.equalsIgnoreCase("correction with accessory") && accessoryProduct.isEmpty()) {
                approvalDetailView.showMessage("Enter Accessory Product");
            } else if (bargainOption.equalsIgnoreCase("correction with VAS") && vasProduct.isEmpty()) {
                approvalDetailView.showMessage("Enter VAS Product");
            } else if (bargainOption.equalsIgnoreCase("correction with accessory") && accessoryValue.isEmpty()) {
                approvalDetailView.showMessage("Enter price");
            } else if (bargainOption.equalsIgnoreCase("correction with VAS") && vasValue.isEmpty()) {
                approvalDetailView.showMessage("Enter price");
            } else {
                approvalDetailView.showProgress();
                storeASMListener.submitBargain(refId, totalAmount, empId, bargainOption, accessoryValue, vasValue, accessoryProduct, vasProduct);
            }

        }
    }


    @Override
    public void getPhoneDetailByRefId(String token, String refId) {
        if (approvalDetailView != null) {
            if (TextUtils.isEmpty(refId)) {
                approvalDetailView.showMessage("Some thing went wrong! Please login again.");
            } else {
                approvalDetailView.showProgress();
                storeASMListener.getPhoneDetailByRefId(token, refId);
            }

        }
    }

    @Override
    public void checkEligibleException(String token) {
        if (storeASMView != null) {
            storeASMView.showProgress();
            storeASMListener.checkEligibleException(token);
        }
    }

    @Override
    public void salesCheckCalculate(String token, String paymentMode, String productType, String makeId, String modelId, String discountValue,String storeId) {
        if (storeASMView != null) {
            if (TextUtils.isEmpty(discountValue) && TextUtils.isEmpty(discountValue)) {
                storeASMView.showMessage("Please enter discount value proceed.");
            } else {
                storeASMView.showProgress();
                storeASMListener.salesCheckCalculate(token,paymentMode,productType,makeId,modelId,discountValue,storeId);
            }
        }
    }

    @Override
    public void sendExceptionRequest(String token, String paymentMode, String productType,
                                     String makeId, String modelId, String discountValue,
                                     String storeId, String totalAmount, String customerName,
                                     String customerMobile, String customerEmail,
                                     String customerMobileVerified, String employeeId) {
        if (storeASMView != null) {
            if (TextUtils.isEmpty(customerName) && TextUtils.isEmpty(customerName)) {
                storeASMView.showMessage("Please enter customer name to proceed.");
            }else if (TextUtils.isEmpty(customerMobile) && TextUtils.isEmpty(customerMobile)) {
                storeASMView.showMessage("Please enter customer mobile to proceed.");
            }else if (TextUtils.isEmpty(customerEmail) && TextUtils.isEmpty(customerEmail)) {
                storeASMView.showMessage("Please enter customer email to proceed.");
            } else {
                storeASMView.showProgress();
                storeASMListener.sendExceptionRequest(token, paymentMode, productType, makeId, modelId, discountValue, storeId, totalAmount, customerName, customerMobile, customerEmail, "1", employeeId);
            }
        }
    }

    @Override
    public void showMessage(String msg) {
        if (approvalDiscountView != null) {
            approvalDiscountView.hideProgress();
            approvalDiscountView.showMessage(msg);
        }
    }

    @Override
    public void setStoreList() {

    }

    @Override
    public void setMake(MakeResponse managerResponse) {
        if (storeASMView != null) {
            storeASMView.setMake(managerResponse);
        }
    }

    @Override
    public void setModel(ModelResponse managerResponse) {
        if (storeASMView != null) {
            storeASMView.setModel(managerResponse);
        }
    }

    @Override
    public void setApproval(ApproveDiscountResponse approval) {
        if (approvalDiscountView != null) {
            approvalDiscountView.hideProgress();
            approvalDiscountView.setApproval(approval);
        }
    }

    @Override
    public void setCheckStock(StockCheckResponse stockCheckResponse) {
        if (storeASMView != null) {
            storeASMView.hideProgress();
            storeASMView.setStocks(stockCheckResponse);
        }
    }

    @Override
    public void setDiscountTypes(DiscountTypeResponse discountTypes) {
        if (approvalDiscountView != null) {
            approvalDiscountView.hideProgress();
            approvalDiscountView.setDiscountTypes(discountTypes);
        }
    }

    @Override
    public void setDiscountStatus(ApproveDiscountResponse approveDiscountResponse) {
        if (approvalDiscountView != null) {
            approvalDiscountView.hideProgress();
            approvalDiscountView.setDiscountStatus(approveDiscountResponse);
        }
    }

    @Override
    public void setDiscountApproval(String couponCode) {
        if (approvalDetailView != null) {
            approvalDetailView.hideProgress();
            approvalDetailView.setDiscountApproval(couponCode);
        }

    }

//    @Override
//    public void setDiscountTypes(String Token) {
//        if (approvalDiscountView != null) {
//            storeASMListener.getDiscountTypes(Token);
//        }
//    }

    @Override
    public void setDiscountReject(String msg) {
        if (approvalDetailView != null) {
            approvalDetailView.hideProgress();
            approvalDetailView.setDiscountReject(msg);
        }
    }

    @Override
    public void setDiscountBargain(String msg) {
        if (approvalDetailView != null) {
            approvalDetailView.hideProgress();
            approvalDetailView.setDiscountBargain(msg);
        }
    }

    @Override
    public void setProductTypes(ProductTypeResponse productTypeResponse) {
        if (storeASMView != null) {
            storeASMView.setProductTypes(productTypeResponse);
        }
    }

    @Override
    public void setSubmitBargain(String msg) {
        if (approvalDetailView != null) {
            approvalDetailView.hideProgress();
            approvalDetailView.setSubmitBargain(msg);
        }

    }

    @Override
    public void setStoreList(StoreListResponse storeList) {
        if (storeASMView != null) {
            storeASMView.hideProgress();
            storeASMView.setStoreList(storeList);
        }
    }

    @Override
    public void setStores(StoreResponse storeList) {
        if (storeASMView != null) {
            storeASMView.hideProgress();
            storeASMView.setStores(storeList);
        }
    }

    @Override
    public void setRejectionList(RejectReasonResponse rejectionList) {
        if (approvalDetailView != null) {
            approvalDetailView.setRejectionList(rejectionList);
        }
    }

    @Override
    public void setDiscountCheck(String jsonData) {

        if (approvalDetailView != null && jsonData != null) {
            approvalDetailView.setDiscountCheck(jsonData);
        }

    }

    @Override
    public void setPhoneDetail(PhoneDetailsResponse approval) {
        if (approvalDetailView != null) {
            approvalDetailView.hideProgress();
            approvalDetailView.setPhoneDetail(approval);
        }
    }

    @Override
    public void getCheckEligibleException(CommonResponse commonResponse) {
        if (storeASMView != null) {
            storeASMView.hideProgress();
            storeASMView.setCheckEligibleException(commonResponse);
        }
    }

    @Override
    public void getSalesCheckCalculate(SubmitDiscountResponse commonResponse) {
        if (storeASMView != null) {
            storeASMView.hideProgress();
            storeASMView.setSalesCheckCalculate(commonResponse);
        }
    }

    @Override
    public void sendExceptionRequest(CommonResponse commonResponse) {
        if (storeASMView != null) {
            storeASMView.hideProgress();
            storeASMView.setSendExceptionRequest(commonResponse);
        }
    }
}
