package com.sangeetha.employee.servify.model.data

import com.google.gson.annotations.SerializedName

data class ReferenceCodeData(
    @SerializedName("sang_offer")
    val sangOffers: SangOffersData,
    @SerializedName("offer")
    val Offers: OffersData,
    @SerializedName("servify_amount")
    val amount: Int,
    @SerializedName("retailer_amount")
    val retailerAmount: Int,
    @SerializedName("currency_code")
    val currency_code: String,
    @SerializedName("imei")
    val imei: String,
    @SerializedName("logistics_reference_id")
    val logistics_reference_id: String,
    @SerializedName("trade_model")
    val trade_model: String,
    @SerializedName("trade_mode")
    val trade_mode: String,
    @SerializedName("model")
    val model: String,
    @SerializedName("customer_name")
    val customer_name: String,
    @SerializedName("customer_mobile")
    val customer_mobile: String,
    @SerializedName("trade_reference_id")
    val trade_reference_id: String,
    @SerializedName("trade_status")
    val trade_status: String
)
