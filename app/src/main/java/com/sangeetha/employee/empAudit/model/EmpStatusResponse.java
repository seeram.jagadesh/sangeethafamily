package com.sangeetha.employee.empAudit.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EmpStatusResponse {

    @SerializedName("dropdown_details")
    private List<DropdownDetailsItem> dropdownDetails;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private boolean status;

    public List<DropdownDetailsItem> getDropdownDetails() {
        return dropdownDetails;
    }

    public void setDropdownDetails(List<DropdownDetailsItem> dropdownDetails) {
        this.dropdownDetails = dropdownDetails;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "EmpStatusResponse{" +
                        "dropdown_details = '" + dropdownDetails + '\'' +
                        ",message = '" + message + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}