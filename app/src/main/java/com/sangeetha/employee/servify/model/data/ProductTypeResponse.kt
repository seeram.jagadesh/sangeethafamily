package com.sangeetha.employee.servify.model.data

import com.google.gson.annotations.SerializedName

data class ProductTypeResponse(
        @SerializedName("status")
        val status: Boolean,
        @SerializedName("data")
        val dataList: List<ProductTypeData>,
        @SerializedName("message")
        val message: String
)