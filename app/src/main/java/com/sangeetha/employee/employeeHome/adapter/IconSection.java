package com.sangeetha.employee.employeeHome.adapter;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sangeetha.employee.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import io.github.luizgrp.sectionedrecyclerviewadapter.Section;
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters;

public final class IconSection extends Section {

    private final String title;
    private final List<PortalIconValues> list;
    private final ClickListener clickListener;

    public IconSection(@NonNull final String title, @NonNull final List<PortalIconValues> list,
                       @NonNull final ClickListener clickListener) {
        super(SectionParameters.builder()
                .itemResourceId(R.layout.employee_dashboard_item)
                .headerResourceId(R.layout.home_item_header)
                .build());

        this.title = title;
        this.list = list;
        this.clickListener = clickListener;
    }

    @Override
    public int getContentItemsTotal() {
        return list.size();
    }

    @Override
    public RecyclerView.ViewHolder getItemViewHolder(final View view) {
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindItemViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final ItemViewHolder itemHolder = (ItemViewHolder) holder;

        final PortalIconValues movie = list.get(position);

        Picasso.get().load(movie.image).placeholder(R.mipmap.ic_launcher_round).into(itemHolder.tvItem);
        itemHolder.tvSubItem.setText(movie.name);

        itemHolder.rootView.setOnClickListener(v ->
                clickListener.onItemRootViewClicked(this, itemHolder.getAdapterPosition(),list.get(position),title)
        );
    }

    @Override
    public RecyclerView.ViewHolder getHeaderViewHolder(final View view) {
        return new HeaderViewHolder(view);
    }

    @Override
    public void onBindHeaderViewHolder(final RecyclerView.ViewHolder holder) {
        final HeaderViewHolder headerHolder = (HeaderViewHolder) holder;

        headerHolder.tvTitle.setText(title);
    }

    public interface ClickListener {

        void onHeaderMoreButtonClicked(@NonNull final IconSection section, int itemAdapterPosition);

        void onItemRootViewClicked(@NonNull final IconSection section, final int itemAdapterPosition,PortalIconValues iconValues,String title);
    }
}
