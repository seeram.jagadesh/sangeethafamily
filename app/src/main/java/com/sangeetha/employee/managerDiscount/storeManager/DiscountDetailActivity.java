package com.sangeetha.employee.managerDiscount.storeManager;

import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.sangeetha.employee.R;
import com.sangeetha.employee.managerDiscount.manager_pojo.Data;
import com.sangeetha.employee.managerDiscount.manager_pojo.DiscountDetailsResponse;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.DataItem;
import com.sangeetha.employee.utils.AppPreference;
import com.sangeetha.employee.utils.LanguageUtil;
import com.sangeetha.employee.utils.MyContextWrapper;

import java.util.ArrayList;
import java.util.Locale;

public class DiscountDetailActivity extends AppCompatActivity implements DiscountDetailView {

    private AppCompatTextView make, model, price, sangeethaPrice,
            discountPrice, totalPrice, mobileNo, email, customerName,
            employeeId, name, approverName, dateTxt, referenceNoTxt, coupon,
            discountPercentage, statusTxt, rejectReason, rejectNote,
            bargainType, bargainProduct, bargainValue, approvalDetails,
            asmCurrentPrice, asmDiscountPercentage, asmDiscountAmount, asmTotalAmount;
    private ProgressBar progressBar;
    private ManagerDiscountPresenter managerDiscountPresenter;
    private FloatingActionButton sync;
    private LinearLayout cancelLayout, approveRejectLayout;
    private LinearLayout couponLayout;
    private LinearLayout reasonLayout;
    private LinearLayout reasonNoteLayout;
    private LinearLayout bargainLayout;
    private LinearLayout asmDiscountLayout;
    AppPreference appPreference;
    String userToken, title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.store_coupon_detail);
        String refId = getIntent().getStringExtra("reference_id");
        LinearLayout mainLayout = findViewById(R.id.main_layout);

        appPreference = new AppPreference(this);

        sync = findViewById(R.id.floatingActionButton);
        make = findViewById(R.id.make);
        model = findViewById(R.id.model);
        price = findViewById(R.id.price);
        cancelLayout = findViewById(R.id.cancelLayout);
        approveRejectLayout = findViewById(R.id.approveRejectLayout);
        couponLayout = findViewById(R.id.couponLayout);
        sangeethaPrice = findViewById(R.id.sangeethaPrice);
        discountPrice = findViewById(R.id.discountPrice);
        totalPrice = findViewById(R.id.totalPrice);
        mobileNo = findViewById(R.id.mobileNo);
        email = findViewById(R.id.email);
        employeeId = findViewById(R.id.employeeId);
        name = findViewById(R.id.name);
        userToken = appPreference.getUserToken();
        approverName = findViewById(R.id.approverName);
        dateTxt = findViewById(R.id.dateTxt);
        referenceNoTxt = findViewById(R.id.referenceNoTxt);
        statusTxt = findViewById(R.id.statusTxt);
        coupon = findViewById(R.id.coupon);
        progressBar = findViewById(R.id.coupon_detail_progressBar);
        managerDiscountPresenter = new ManagerDiscountPresenterImp(this);
        managerDiscountPresenter.discountInfo(userToken, refId);
        AppCompatButton cancel = findViewById(R.id.cancel);
        cancel.setOnClickListener(v -> managerDiscountPresenter.cancelCoupon(refId));
        customerName = findViewById(R.id.customer_name);
        discountPercentage = findViewById(R.id.discount_percentage);
        sync.setOnClickListener(v -> managerDiscountPresenter.discountInfo(userToken, refId));

        reasonLayout = findViewById(R.id.reason_layout);
        reasonNoteLayout = findViewById(R.id.reason_note_layout);
        rejectReason = findViewById(R.id.reject_reasonTxt);
        rejectNote = findViewById(R.id.reject_noteTxt);

        bargainLayout = findViewById(R.id.bargain_layout);
        bargainType = findViewById(R.id.bargain_type);
        bargainProduct = findViewById(R.id.bargain_product);
        bargainValue = findViewById(R.id.bargain_amount);
        approvalDetails = findViewById(R.id.approval_details);
        asmDiscountLayout = findViewById(R.id.asm_discount_layout);
        asmCurrentPrice = findViewById(R.id.asm_current_price);
        asmDiscountPercentage = findViewById(R.id.asm_discount_percentage);
        asmDiscountAmount = findViewById(R.id.asm_discount_amount);
        asmTotalAmount = findViewById(R.id.asm_total_price);

        title = appPreference.getTitle();
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        Locale languageType = LanguageUtil.getLanguageType(newBase);
        super.attachBaseContext(MyContextWrapper.wrap(newBase, languageType));
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showCouponMessage(String msg) {
        Toast.makeText(DiscountDetailActivity.this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setDiscountDetails(DiscountDetailsResponse discountDetails) {

    }

    @Override
    public void setDiscountInfo(DiscountDetailsResponse discountDetails) {
        Data data = discountDetails.getData();
        make.setText(data.getMakeId());
        model.setText(data.getModelId());
        price.setText(data.getModelPrice());
        sangeethaPrice.setText(data.getModelPrice());
        discountPercentage.setText("Discount value");
        discountPrice.setText(data.getDiscountAmount());
        totalPrice.setText(String.valueOf(Float.parseFloat(data.getModelPrice()) - Float.parseFloat(data.getDiscountAmount())));
        mobileNo.setText(data.getCustomerMobile());
        email.setText(data.getCustomerEmail());
        employeeId.setText(data.getEmployeeId());
        name.setText(data.getEmployeeName());
        approverName.setText(data.getApproverName());
        dateTxt.setText(data.getCreated());
        referenceNoTxt.setText(data.getReferenceId());
        if (data.getStatus()==null ||data.getStatus().equalsIgnoreCase("")) {
            statusTxt.setText(data.getDiscountStatusText());
        } else {
            statusTxt.setText(data.getStatus());
        }
        if(data.getCouponCode().equalsIgnoreCase("")){
            couponLayout.setVisibility(View.GONE);
        }else{
            couponLayout.setVisibility(View.VISIBLE);
            coupon.setText(data.getCouponCode());
        }

        customerName.setText(data.getCustomerName());
//
        if (title.equalsIgnoreCase("store")) {
            if (discountDetails.getData().getStatus() != null
                    && discountDetails.getData().getStatus().equalsIgnoreCase("Not Used")
            ) {
                cancelLayout.setVisibility(View.VISIBLE);
                approveRejectLayout.setVisibility(View.GONE);

                // couponLayout.setVisibility(View.GONE);
            } else {
                cancelLayout.setVisibility(View.GONE);
                approveRejectLayout.setVisibility(View.GONE);
                //couponLayout.setVisibility(View.VISIBLE);
            }
        }else{
            approveRejectLayout.setVisibility(View.GONE);
            if (discountDetails.getData().getStatus() != null
                    && discountDetails.getData().getStatus().equalsIgnoreCase("Not Used")
            ) {
                cancelLayout.setVisibility(View.VISIBLE);
                approveRejectLayout.setVisibility(View.GONE);

                // couponLayout.setVisibility(View.GONE);
            } else {
                cancelLayout.setVisibility(View.GONE);
                approveRejectLayout.setVisibility(View.GONE);
                //couponLayout.setVisibility(View.VISIBLE);
            }
        }
        if (discountDetails.getData().getStatus() != null && discountDetails.getData().getStatus().equalsIgnoreCase("Rejected")) {
            rejectReason.setText(discountDetails.getData().getRejectReason());
            if (discountDetails.getData().getRejectNote() != null) {
                rejectNote.setText(discountDetails.getData().getRejectNote());
                reasonNoteLayout.setVisibility(View.VISIBLE);
            } else {
                reasonNoteLayout.setVisibility(View.GONE);
            }
            reasonLayout.setVisibility(View.VISIBLE);
        } else {
            reasonLayout.setVisibility(View.GONE);
        }
        if (!data.getBargainOption().isEmpty()) {
            approvalDetails.setText("Approval Details (Bargain)");
            if (data.getBargainOption().equalsIgnoreCase("correction with reduced discount")) {
                asmDiscountLayout.setVisibility(View.VISIBLE);
                bargainLayout.setVisibility(View.GONE);
                asmCurrentPrice.setText(data.getModelPrice());
                asmDiscountPercentage.setText(String.format("Discount @ %s%% :", data.getAsmDiscountPercentage()));
                asmDiscountAmount.setText(data.getAsmDiscountAmount());
                if (!data.getAsmDiscountAmount().isEmpty())
                    asmTotalAmount.setText(String.valueOf(Integer.parseInt(data.getModelPrice()) - Integer.parseInt(data.getAsmDiscountAmount())));
            } else {
                asmDiscountLayout.setVisibility(View.GONE);
                bargainLayout.setVisibility(View.VISIBLE);
                bargainType.setText(data.getBargainOption());
                if (!data.getAccessoryProduct().isEmpty()) {
                    bargainProduct.setText(data.getAccessoryProduct());
                    bargainValue.setText(data.getAccessoryValue());
                } else {
                    bargainProduct.setText(data.getVasProduct());
                    bargainValue.setText(data.getVasValue());
                }
            }

        } else {
            bargainLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void couponCancel(boolean status, String msg) {
        if (status) {
            showCouponMessage(msg);
            finish();
        } else {
            showCouponMessage(msg);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
            return true;
        }
        return (super.onOptionsItemSelected(item));
    }
}
