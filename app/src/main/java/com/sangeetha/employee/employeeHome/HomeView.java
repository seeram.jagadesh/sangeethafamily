package com.sangeetha.employee.employeeHome;

import org.json.JSONObject;

public interface HomeView {

    void showMessage(String message);

    void showProgress();

    void showProgress(String message);

    void hideProgress();

    void getPortalIcons(JSONObject data);
}
