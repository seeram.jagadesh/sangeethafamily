package com.sangeetha.employee.callRecorder.domain.room.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "CUGMessage")
data class CUGMessage(
    @PrimaryKey val id: Int,
    @ColumnInfo(name = "message") val message: String,
    @ColumnInfo(name = "address") val address: String,
    @ColumnInfo(name = "type") val type: String,
    @ColumnInfo(name = "date") val date: String,
)