package com.sangeetha.employee.storeOrders;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.sangeetha.employee.R;
import com.sangeetha.employee.SangeethaCareApiClient;
import com.sangeetha.employee.employeeHome.EmployeeLanding;
import com.sangeetha.employee.mining.ApiCallable;
import com.sangeetha.employee.storeOrders.adapter.OrderListAdapter;
import com.sangeetha.employee.storeOrders.model.OrderListItemData;
import com.sangeetha.employee.storeOrders.model.OrderListResponse;
import com.sangeetha.employee.utils.AppPreference;

import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StoreOrdersActivity extends AppCompatActivity {


    RecyclerView recyclerView;
    ProgressDialog progressDialog;

    String storeCode, status;
    AppPreference appPreference;
    OrderListAdapter orderListAdapter;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(StoreOrdersActivity.this, EmployeeLanding.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_orders);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        Objects.requireNonNull(getSupportActionBar()).setTitle("Orders List");

        appPreference = new AppPreference(this);

        storeCode = getIntent().getStringExtra("store_code");
        status = getIntent().getStringExtra("status");

        recyclerView = findViewById(R.id.OrderDetailsRecycler);

        if (isNetworkAvailable(StoreOrdersActivity.this)) {
            showProgress();
            getOrderList();
        }
    }

    private void getOrderList() {
        ApiCallable apiCallable = SangeethaCareApiClient.getClient().create(ApiCallable.class);
        apiCallable.getStoreOrdersList(appPreference.getUserToken(), status, storeCode).enqueue(new Callback<OrderListResponse>() {
            @Override
            public void onResponse(Call<OrderListResponse> call, Response<OrderListResponse> response) {
                hideProgress();
                if (response.isSuccessful()) {
                    OrderListResponse orderListResponse=response.body();
                    if(orderListResponse.isStatus()){
                        List<OrderListItemData> data=orderListResponse.getData();
                        orderListAdapter=new OrderListAdapter(data,StoreOrdersActivity.this);
                        recyclerView.setAdapter(orderListAdapter);
                    }else{
                        showMessage(orderListResponse.getMessage());
                    }
                } else {
                    showMessage("error while getting order list.");
                }
            }

            @Override
            public void onFailure(Call<OrderListResponse> call, Throwable t) {
                hideProgress();
                showMessage("Failed while getting order list.");
            }
        });
    }

    public static boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            //   Log.e("Network Testing", "***Available***");
            return true;
        } else {
            //  Log.e("Network Testing", "***Not Available***");
            Toast.makeText(mContext, "No network available, please check your WiFi or EmpData connection", Toast.LENGTH_LONG).show();
            return false;
        }

    }


    public void showProgress() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading..");
        progressDialog.show();
    }


    public void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }


    public void showMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }


}