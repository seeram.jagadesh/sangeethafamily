package com.sangeetha.employee.empLogin.loginModel;

import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

public class OnlineDeliveryData {

    @SerializedName("user_id")
    private String user_id;

    @SerializedName("employee_name")
    private String employee_name;

    @SerializedName("email")
    private String email;

    @SerializedName("mobile")
    private String mobile;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getEmployee_name() {
        return employee_name;
    }

    public void setEmployee_name(String employee_name) {
        this.employee_name = employee_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @NotNull
    @Override
    public String toString() {
        return "OnlineDeliveryData{" +
                "user_id='" + user_id + '\'' +
                ", employee_name='" + employee_name + '\'' +
                ", email='" + email + '\'' +
                ", mobile='" + mobile + '\'' +
                '}';
    }
}