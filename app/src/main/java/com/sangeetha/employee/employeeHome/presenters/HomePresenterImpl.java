package com.sangeetha.employee.employeeHome.presenters;

import android.text.TextUtils;

import com.sangeetha.employee.employeeHome.HomeView;
import com.sangeetha.employee.employeeHome.interactors.HomeInteractor;
import com.sangeetha.employee.employeeHome.interactors.HomeInteractorImpl;

import org.json.JSONObject;

public class HomePresenterImpl implements HomePresenter, HomeInteractor.HomeListener {

    HomeView homeView;
    HomeInteractor homeInteractor;

    public HomePresenterImpl(HomeView homeView) {
        this.homeView = homeView;
        this.homeInteractor = new HomeInteractorImpl(this);
    }

    @Override
    public void showMessage(String message) {
        if (homeView != null) {
            homeView.hideProgress();
            homeView.showMessage(message);
        }
    }

    @Override
    public void setPortalIcons(JSONObject data) {
        if (homeView != null) {
            homeView.getPortalIcons(data);
        }
    }

    @Override
    public void portalIcons(String token) {
        if (homeView != null) {
            homeView.showProgress();
            homeInteractor.getPortalIcons(token);
        }
    }
}
