package com.sangeetha.employee.appointments.appointmentDetails;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nex3z.togglebuttongroup.SingleSelectToggleGroup;
import com.nex3z.togglebuttongroup.button.LabelToggle;
import com.sangeetha.employee.CommonEndPoint;
import com.sangeetha.employee.CommonResponse;
import com.sangeetha.employee.R;
import com.sangeetha.employee.SangeethaCareApiClient;
import com.sangeetha.employee.appointments.AppointmentsListActivity;
import com.sangeetha.employee.appointments.AppointmentsListResponse;
import com.sangeetha.employee.appointments.ListInterface;
import com.sangeetha.employee.appointments.adapter.MorningSlotAdapter;
import com.sangeetha.employee.appointments.adapter.TimeItemClick;
import com.sangeetha.employee.customwidget.SearchableSpinner;
import com.sangeetha.employee.utils.AppConstants;
import com.sangeetha.employee.utils.AppPreference;
import com.sangeetha.employee.utils.CalendarUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppointmentsDetailsActivity extends AppCompatActivity implements TimeItemClick {

    String appointmentId = "";
    AppPreference appPreference;
    private CountDownTimer otpTimer;
    TimeItemClick timeItemClick;
    LinearLayout dateTimeLayout;
    private boolean isTimerRunning;
    private long timeRemaining;
    LinearLayout numberLayout, dateLayout;
    RecyclerView OptionsRecycler;
    CardView appointmentConfirmationLayout;
    String selectedDate = "";
    List<AppointmentDateModel> appointmentDateModel = new ArrayList<>();
    private ArrayList<String> dates = new ArrayList<>();
    SearchableSpinner dateSpinner;
    TimeModel timeModel;
    SingleSelectToggleGroup groupChoices;
    ArrayList<HashMap<String, String>> appointmentStatusList;
    private long durationOfOtp = 120000;
    private ArrayAdapter<String> dateAdapter;
    AppCompatEditText edtInvoiceDate, edtInvoiceNumber;
    AppCompatButton btnSendOTP, otpSubmitBtn, btnSubmitData;
    AppCompatTextView txtAppointmentID, txtAppointmentType, otpExpireTime,
            txtEmail, txtAppointmentDate, txtTime, txtStatus, txtCustomerName, txtMobileNo, txtBrandName, txtModelName;

    AppCompatTextView txtAppointmentStatus,txtSaleStatus,txtInvoiceNumber,txtInvoiceDate,txtAppointmentNotes;
    LinearLayout txtInvoiceNolayout,txtInvoiceDateLayout;
    CardView appointmentHistoryLayout;
    AppCompatEditText edtOTP;
    LinearLayout otpLayout;
    String loadsearchkeyID = "";
    LinearLayout spinStatusLayout;
    AppCompatEditText edtNotes;
    Spinner spinStatus, spinAppointmentStatus;


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void runTimer() {
        TextView otpWillExpireTV = findViewById(R.id.otp_will_expire_tv);
        otpExpireTime.setVisibility(View.VISIBLE);
        otpSubmitBtn.setText("Submit OTP");
        otpWillExpireTV.setText("Your OTP will expire in ");
        otpExpireTime.setVisibility(View.VISIBLE);
        otpTimer = new CountDownTimer(120000, 1000) {

            public void onTick(long millisUntilFinished) {

                long seconds = (millisUntilFinished / 1000) % 60;
                long minutes = ((millisUntilFinished - seconds) / 1000) / 60;
                if (seconds > 9) {
                    otpExpireTime.setText("0" +
                            "" + minutes + ":" + seconds);
                } else {
                    otpExpireTime.setText("0" + minutes + ":" + "0" + seconds);
                }
            }

            public void onFinish() {
                otpExpireTime.setText("00:00");
                otpExpireTime.setText("Your otp expired please use Resend OTP to continue!");
                otpExpireTime.setVisibility(View.GONE);
                otpSubmitBtn.setText("Resend OTP");
                if (otpTimer != null) {
                    otpTimer.cancel();
                    otpTimer = null;
                }
            }
        }.start();
    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointments_details);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        appPreference = new AppPreference(this);

        Intent g = getIntent();
        if (g.hasExtra("id")) {
            appointmentId = g.getStringExtra("id");
        }
        findViews();
        appointmentStatusList = new ArrayList<>();
        getAppointmentDetails(appointmentId);

        HashMap<String, String> dataHash = new HashMap<>();
        dataHash.put("0", "Select");
        dataHash.put("1", "Visited");
        dataHash.put("2", "Not-Visited");
        dataHash.put("3", "Cancelled");
        dataHash.put("4", "Postponed");

        timeItemClick = this;
        appointmentStatusList.add(dataHash);
        Log.e("HASH STA", appointmentStatusList.toString());
        Log.e("HASH STA", appointmentStatusList.toString());


        dates.add(0, "--- Select Date ---");
        dateAdapter = new ArrayAdapter<String>(this, R.layout.custom_spinner_layout, dates);
        dateAdapter.setDropDownViewResource(R.layout.custom_spinner_white_layout);
        dateSpinner.setAdapter(dateAdapter);


        dateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String select = adapterView.getSelectedItem().toString();
                if (!select.equalsIgnoreCase("--- Select Date ---")) {
                    String apM = appointmentDateModel.get(i - 1).getDate();
                    selectedDate = appointmentDateModel.get(i - 1).getDate();
                    getTimeSlot(apM, txtAppointmentType.getText().toString());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btnSendOTP.setOnClickListener(v -> {
            ListInterface listInterface = SangeethaCareApiClient.getClient().create(ListInterface.class);
            listInterface.generateOTP(appPreference.getToken(), appointmentId).enqueue(new Callback<CommonResponse>() {
                @Override
                public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                    if (response.isSuccessful()) {
                        CommonResponse commonResponse = response.body();
                        if (commonResponse.getStatus()) {
                            showMessage(commonResponse.getMessage());
                            otpLayout.setVisibility(View.VISIBLE);
                            btnSendOTP.setVisibility(View.GONE);
                            runTimer();
                        } else {
                            showMessage(commonResponse.getMessage());
                            otpLayout.setVisibility(View.GONE);
                            btnSendOTP.setVisibility(View.VISIBLE);
                        }
                    } else {
                        showMessage("Error while generating OTP");
                        otpLayout.setVisibility(View.GONE);
                        btnSendOTP.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onFailure(Call<CommonResponse> call, Throwable t) {
                    showMessage("Failed to generate OTP");
                    otpLayout.setVisibility(View.GONE);
                    btnSendOTP.setVisibility(View.VISIBLE);

                }
            });
        });

        otpSubmitBtn.setOnClickListener(v -> {
            if (edtOTP.getText().toString().equalsIgnoreCase("")) {
                showMessage("Please enter OTP.");
            } else {
                ListInterface listInterface = SangeethaCareApiClient.getClient().create(ListInterface.class);
                listInterface.verifyOTP(appPreference.getToken(), appointmentId, edtOTP.getText().toString()).enqueue(new Callback<CommonResponse>() {
                    @Override
                    public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                        if (response.isSuccessful()) {
                            CommonResponse commonResponse = response.body();
                            if (commonResponse.getStatus()) {
                                showMessage(commonResponse.getMessage());
                                otpLayout.setVisibility(View.GONE);
                                btnSendOTP.setVisibility(View.GONE);
                                appointmentConfirmationLayout.setVisibility(View.VISIBLE);
                            } else {
                                showMessage(commonResponse.getMessage());
                                otpLayout.setVisibility(View.VISIBLE);
                            }
                        } else {
                            showMessage("Error while submitting OTP");
                            otpLayout.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onFailure(Call<CommonResponse> call, Throwable t) {
                        otpLayout.setVisibility(View.VISIBLE);
                    }
                });
            }
        });
        List<StringWithTag> itemList = new ArrayList<StringWithTag>();


        for (Map.Entry<String, String> entry : dataHash.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            itemList.add(new StringWithTag(value, key));
        }

        ArrayAdapter<StringWithTag> spinnerAdapter = new ArrayAdapter<StringWithTag>(this, android.R.layout.simple_spinner_item, itemList);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinAppointmentStatus.setAdapter(spinnerAdapter);


        spinAppointmentStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                StringWithTag swt = (StringWithTag) adapterView.getItemAtPosition(i);
                loadsearchkeyID = (String) swt.tag;
                if (loadsearchkeyID.equalsIgnoreCase("1")) {
                    spinStatusLayout.setVisibility(View.VISIBLE);
                    numberLayout.setVisibility(View.GONE);
                    dateTimeLayout.setVisibility(View.GONE);
                    dateLayout.setVisibility(View.GONE);
                } else if (loadsearchkeyID.equalsIgnoreCase("4")) {
                    dateTimeLayout.setVisibility(View.VISIBLE);
                    getDates(txtAppointmentType.getText().toString());
                } else {
                    spinStatusLayout.setVisibility(View.GONE);
                    dateTimeLayout.setVisibility(View.GONE);
                    numberLayout.setVisibility(View.GONE);
                    dateLayout.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selected = adapterView.getSelectedItem().toString();
                if (selected.equalsIgnoreCase("sale")) {
                    numberLayout.setVisibility(View.VISIBLE);
                    dateLayout.setVisibility(View.VISIBLE);
                } else {
                    numberLayout.setVisibility(View.GONE);
                    dateLayout.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        groupChoices.setOnCheckedChangeListener(new SingleSelectToggleGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SingleSelectToggleGroup group, int checkedId) {
                String name = nameList.get(checkedId);

                for (int f = 0; f < dataListModel.size(); f++) {
                    HashMap<String, List<TimeModel>> model = dataListModel.get(f);
                    for (String key : model.keySet()) {
                        System.out.println(key);
                        if (name.equalsIgnoreCase(key)) {
                            List<TimeModel> timeModelList = model.get(key);
                            morningSlotAdapter = new MorningSlotAdapter(AppointmentsDetailsActivity.this, AppointmentsDetailsActivity.this, timeModelList);
                            OptionsRecycler.setAdapter(morningSlotAdapter);
                        }
                    }
                }
            }
        });

        edtInvoiceDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEvent.ACTION_UP == event.getAction()) {
                    showCalendarForDatePicking();
                }
                return true;
            }
        });

        btnSubmitData.setOnClickListener(v -> {
            String appointment = spinAppointmentStatus.getSelectedItem().toString();
            String status = spinStatus.getSelectedItem().toString();
            String notes = edtNotes.getText().toString();
            String invoiceNo = edtInvoiceNumber.getText().toString();
            String invoicedate = edtInvoiceDate.getText().toString();

            if (appointment.equalsIgnoreCase("select")) {
                showMessage("Please select a appointment status.");
            } else if (status.equalsIgnoreCase("select") && loadsearchkeyID.equalsIgnoreCase("1")) {
                showMessage("Please select a status");
            } else if (invoiceNo.equalsIgnoreCase("") && status.equalsIgnoreCase("sale")) {
                showMessage("Please enter invoice number");
            } else if (invoicedate.equalsIgnoreCase("") && status.equalsIgnoreCase("sale")) {
                showMessage("Please enter invoice date");
            } else if (notes.equalsIgnoreCase("")) {
                showMessage("Please enter notes.");
            } else {
                if (loadsearchkeyID.equalsIgnoreCase("1")) {
                    if (status.equalsIgnoreCase("sale")) {
                        submitData(loadsearchkeyID, status.toLowerCase(), notes, "", "", invoiceNo, invoicedate);
                    } else if (status.equalsIgnoreCase("enquiry") || status.equalsIgnoreCase("lost sales")
                            || status.equalsIgnoreCase("complaint")) {
                        submitData(loadsearchkeyID, status.toLowerCase(), notes, "", "", "", "");
                    }
                } else if (loadsearchkeyID.equalsIgnoreCase("4")) {
                    submitData(loadsearchkeyID, status.toLowerCase(), notes, selectedDate, timeModel.getTimeSlot(), "", "");
                } else {
                    submitData(loadsearchkeyID, status.toLowerCase(), notes, "", "", "", "");
                }
            }
        });
    }

    MorningSlotAdapter morningSlotAdapter;

    ArrayList<String> nameList = new ArrayList<>();
    ArrayList<HashMap<String, List<TimeModel>>> dataListModel = new ArrayList<>();

//    ArrayList<String> nameList = new ArrayList<>();
//    ArrayList<HashMap<String, List<TimeModel>>> dataListModel = new ArrayList<>();

    ArrayList<List<TimeModel>> dataTimeList = new ArrayList<>();

    private void getTimeSlot(String apM, String appointmentType) {
        ListInterface listInterface = SangeethaCareApiClient.getClient().create(ListInterface.class);
        listInterface.getTimeSlots(appPreference.getToken(), appointmentId, "app", selectedDate).enqueue(new Callback<AppointmentSlotsResponse>() {
            @Override
            public void onResponse(Call<AppointmentSlotsResponse> call, Response<AppointmentSlotsResponse> response) {
                if (response.isSuccessful()) {
                    AppointmentSlotsResponse appointmentSlotsResponse = response.body();
                    if (appointmentSlotsResponse.isStatus()) {
                        List<Object> data = appointmentSlotsResponse.getDataArray();

                        Gson gson = new Gson();
                        String json = gson.toJson(data);
                        try {
                            JSONArray array = new JSONArray(json);
                            if (array.length() > 0) {

                                if (!dataTimeList.isEmpty()) {
                                    dataTimeList.clear();
                                }
                                if (groupChoices != null) {
                                    groupChoices.removeAllViews();
                                    nameList.clear();
                                    dataListModel.clear();
                                }
                                for (int f = 0; f < array.length(); f++) {
                                    HashMap<String, List<TimeModel>> listHashMap = new HashMap<>();
                                    View myVi = getLayoutInflater().inflate(R.layout.time_slot_layout, null);
                                    RecyclerView eveRecycler = myVi.findViewById(R.id.eveRecycler);
                                    eveRecycler.setTag(f);
                                    JSONObject jsonObject = (JSONObject) array.get(f);
                                    String slotName = jsonObject.get("slot_name_without_slug").toString();
                                    String slotSlug = jsonObject.get("slot_name").toString();
                                    LabelToggle labelToggle = new LabelToggle(AppointmentsDetailsActivity.this);
                                    labelToggle.setPadding(2, 2, 2, 2);
                                    labelToggle.setMarkerColor(getResources().getColor(R.color.colorPrimaryApp));
                                    labelToggle.setChecked(false);
                                    nameList.add(slotSlug);
                                    labelToggle.setText(slotName.replace(" Slot",""));
                                    labelToggle.setId(f);
                                    groupChoices.addView(labelToggle, f);
                                    JSONArray jsonArray = jsonObject.getJSONArray(slotSlug);

                                    List<TimeModel> timeModelList = new ArrayList<>();

                                    for (int d = 0; d < jsonArray.length(); d++) {
                                        Gson gs = new Gson();
                                        TimeModel timeModel = gs.fromJson(jsonArray.getJSONObject(d).toString(), TimeModel.class);
                                        timeModelList.add(timeModel);
                                    }

                                    listHashMap.put(slotSlug, timeModelList);

                                    dataListModel.add(listHashMap);
                                    morningSlotAdapter = new MorningSlotAdapter(AppointmentsDetailsActivity.this, AppointmentsDetailsActivity.this, timeModelList);
                                    eveRecycler.setAdapter(morningSlotAdapter);

                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        showMessage(appointmentSlotsResponse.getMessage());
                    }
                } else {
                    showMessage("Error while getting time slots.");
                }
            }

            @Override
            public void onFailure(Call<AppointmentSlotsResponse> call, Throwable t) {
                showMessage("Failed to get time slots.");
            }
        });
    }

    private void getDates(String appointmentType) {
        ListInterface listInterface = SangeethaCareApiClient.getClient().create(ListInterface.class);
        listInterface.getDateList(appPreference.getToken(), appointmentType).enqueue(new Callback<AppointmentDateResponse>() {
            @Override
            public void onResponse(Call<AppointmentDateResponse> call, Response<AppointmentDateResponse> response) {

                if (response.isSuccessful()) {
                    AppointmentDateResponse appointmentDetailsResponse = response.body();
                    if (appointmentDetailsResponse.isStatus()) {
                        appointmentDateModel = appointmentDetailsResponse.getAppointmentDateModelList();
                        if (dates != null) {
                            dates.clear();
                        }
                        dates.add(0, "--- Select Date ---");
                        for (AppointmentDateModel apModel : appointmentDateModel) {
                            dates.add(apModel.getDateVisibleText());
                        }
                        dateAdapter.notifyDataSetChanged();
                    } else {
                        showMessage(appointmentDetailsResponse.getMessage());
                    }
                } else {
                    showMessage("Error while getting dates.");
                }
            }

            @Override
            public void onFailure(Call<AppointmentDateResponse> call, Throwable t) {
                showMessage("Failed while getting dates.");

            }
        });
    }

    private void submitData(String appointment, String status, String notes, String date, String time, String invoiceNo, String invoicedate) {
        ListInterface listInterface = SangeethaCareApiClient.getClient().create(ListInterface.class);
        listInterface.submitData(appPreference.getToken(),
                appointmentId, appointment, status, date, time, invoiceNo,
                invoicedate, notes).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (response.isSuccessful()) {
                    CommonResponse commonResponse = response.body();
                    if (commonResponse.getStatus()) {
                        showMessage(commonResponse.getMessage());
                        startActivity(new Intent(AppointmentsDetailsActivity.this, AppointmentsListActivity.class));
                    } else {
                        showMessage(commonResponse.getMessage());
                    }
                } else {
                    showMessage("Error while submitting data.");
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                showMessage("Failed while submitting data.");
            }
        });
    }

    String withYearorNot = "";

    private void showCalendarForDatePicking() {

//            fin.setVisibility(View.VISIBLE);
        withYearorNot = "";

        Calendar calendar = Calendar.getInstance();
        Date d = calendar.getTime();
        int yearD = calendar.get(Calendar.YEAR);

        String currentYear = String.valueOf(yearD);

        int yearDefault = d.getYear();
        final int monthDefault = d.getMonth();

        final int dateDefault = d.getDate();
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                (view, year, monthOfYear, dayOfMonth) -> {
                    Calendar datePickedFromCalendar = Calendar.getInstance();
                    datePickedFromCalendar.set(year, monthOfYear, dayOfMonth);
                    String dateForView = CalendarUtils.getDateInCustomFormat(datePickedFromCalendar.getTime(), "dd-MM-yyyy");
//                    Toast.makeText(this, monthDefault+"", Toast.LENGTH_SHORT).show();
                    String acade = AppConstants.getAcademicYear(dateForView);
                    edtInvoiceDate.setText(acade);
                    edtInvoiceDate.setText(dateForView);
                }, yearD, monthDefault, dateDefault);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        //datePickerDialog.getDatePicker().setCalendarViewShown(false);
        datePickerDialog.show();


    }

    private void findViews() {
        OptionsRecycler = findViewById(R.id.OptionsRecycler);
        appointmentConfirmationLayout = findViewById(R.id.appointmentConfirmationLayout);
        txtAppointmentID = findViewById(R.id.txtAppointmentID);
        txtAppointmentType = findViewById(R.id.txtAppointmentType);
        txtEmail = findViewById(R.id.txtEmail);
        groupChoices=findViewById(R.id.group_choices);
        dateTimeLayout=findViewById(R.id.dateTimeLayout);
        txtAppointmentDate = findViewById(R.id.txtAppointmentDate);
        txtTime = findViewById(R.id.txtTime);
        txtStatus = findViewById(R.id.txtStatus);
        txtCustomerName = findViewById(R.id.txtCustomerName);
        txtMobileNo = findViewById(R.id.txtMobileNo);
        txtBrandName = findViewById(R.id.txtBrandName);
        txtModelName = findViewById(R.id.txtModelName);
        btnSendOTP = findViewById(R.id.btnSendOTP);
        otpLayout = findViewById(R.id.otpLayout);
        otpSubmitBtn = findViewById(R.id.otp_submit_btn);
        btnSubmitData = findViewById(R.id.btnSubmitData);
        edtOTP = findViewById(R.id.otp_ed);
        edtNotes = findViewById(R.id.edtNotes);
        otpExpireTime = findViewById(R.id.otp_expire_time_tv);
        spinStatus = findViewById(R.id.spinStatus);
        spinAppointmentStatus = findViewById(R.id.spinAppointmentStatus);
        spinStatusLayout = findViewById(R.id.spinStatusLayout);
        numberLayout = findViewById(R.id.invoiceNumberLayout);
        dateLayout = findViewById(R.id.invoiceDateLayout);
        edtInvoiceDate = findViewById(R.id.edtInvoiceDate);
        edtInvoiceNumber = findViewById(R.id.edtInvoiceNumber);
        dateSpinner = findViewById(R.id.dateSpinner);

        txtAppointmentStatus= findViewById(R.id.txtAppointmentStatus);
        txtSaleStatus= findViewById(R.id.txtSaleStatus);
        txtInvoiceNumber= findViewById(R.id.txtInvoiceNumber);
        txtInvoiceDate= findViewById(R.id.txtInvoiceDate);
        txtAppointmentNotes= findViewById(R.id.txtAppointmentNotes);
        txtInvoiceNolayout= findViewById(R.id.txtInvoiceNolayout);
        txtInvoiceDateLayout= findViewById(R.id.txtInvoiceDateLayout);
        appointmentHistoryLayout= findViewById(R.id.appointmentHistoryLayout);
    }

    private void getAppointmentDetails(String appointmentId) {
        ListInterface commonEndPoint = SangeethaCareApiClient.getClient().create(ListInterface.class);
        commonEndPoint.getAppointmentDetails(appPreference.getToken(), appointmentId, "").enqueue(new Callback<AppointmentDetailsResponse>() {
            @Override
            public void onResponse(Call<AppointmentDetailsResponse> call, Response<AppointmentDetailsResponse> response) {
                if (response.isSuccessful()) {
                    AppointmentDetailsResponse appointmentDetailsResponse = response.body();
                    if (appointmentDetailsResponse.isStatus()) {
                        setViews(appointmentDetailsResponse.getAppointmentDetails().getAppointmentDetails());
                    } else {
                        showMessage(appointmentDetailsResponse.getMessage());
                    }
                } else {
                    showMessage("Error while getting details.");
                }
            }

            @Override
            public void onFailure(Call<AppointmentDetailsResponse> call, Throwable t) {

            }
        });
    }

    private void setViews(AppointmentDetails appointmentDetails) {
        txtAppointmentID.setText(appointmentDetails.getUniqueId());
        txtAppointmentType.setText(appointmentDetails.getAppointmentType());
        txtEmail.setText(appointmentDetails.getCustomerDetails().getEmail());
        txtAppointmentDate.setText(appointmentDetails.getDate());
        txtTime.setText(appointmentDetails.getTime());
        txtStatus.setText(appointmentDetails.getStatus());
        txtCustomerName.setText(appointmentDetails.getCustomerDetails().getName());
        txtMobileNo.setText(appointmentDetails.getCustomerDetails().getMobile());
        txtBrandName.setText(appointmentDetails.getModelDetailsList().get(0).getBrandName());
        txtModelName.setText(appointmentDetails.getModelDetailsList().get(0).getModelName());

        Log.e("DATA",appointmentDetails.getAppointmentDescription().toString());
        Log.e("DATA",appointmentDetails.getAppointmentDescription().toString());
        if (appointmentDetails.getStatus().equalsIgnoreCase("pending")) {
            btnSendOTP.setVisibility(View.VISIBLE);
            appointmentHistoryLayout.setVisibility(View.GONE);
        }else{
            appointmentHistoryLayout.setVisibility(View.VISIBLE);
            try {
                if(appointmentDetails.getAppointmentDescription().toString().equalsIgnoreCase("")){
                    txtAppointmentStatus.setText(appointmentDetails.getStatus());
                    txtSaleStatus.setText(appointmentDetails.getStatus());
                    txtInvoiceNolayout.setVisibility(View.GONE);
                    txtInvoiceDateLayout.setVisibility(View.GONE);
                }else {

                    String jsonInString = new Gson().toJson(appointmentDetails.getAppointmentDescription());
                    JSONObject jsonObject = new JSONObject(jsonInString);
                    String appStatus = jsonObject.getString("appointment_status_1");
                    String invoiceNo = jsonObject.getString("invoice_no");
                    String invoiceDate = jsonObject.getString("invoice_date");

                    txtAppointmentStatus.setText(appointmentDetails.getStatus());
                    txtSaleStatus.setText(appStatus);
                    if (invoiceNo.equalsIgnoreCase("")) {
                        txtInvoiceNolayout.setVisibility(View.GONE);
                        txtInvoiceDateLayout.setVisibility(View.GONE);
                    } else {
                        txtInvoiceNolayout.setVisibility(View.VISIBLE);
                        txtInvoiceDateLayout.setVisibility(View.VISIBLE);
                        txtInvoiceDate.setText(invoiceDate);
                        txtInvoiceNumber.setText(invoiceNo);
                    }

                }
                txtAppointmentNotes.setText(appointmentDetails.getAppointmentNotes());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getClickedItem(TimeModel item) {
        timeModel = item;
    }
}

class StringWithTag {
    public String string;
    public Object tag;

    public StringWithTag(String string, Object tag) {
        this.string = string;
        this.tag = tag;
    }

    @Override
    public String toString() {
        return string;
    }
}