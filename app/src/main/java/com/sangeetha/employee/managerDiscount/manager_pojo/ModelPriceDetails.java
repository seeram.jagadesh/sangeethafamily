package com.sangeetha.employee.managerDiscount.manager_pojo;

import com.google.gson.annotations.SerializedName;

public class ModelPriceDetails {

    @SerializedName("model_name")
    private String modelName;

    @SerializedName("model_code")
    private String internalCode;

    @SerializedName("model_price")
    private String itemPrice;

    @SerializedName("created")
    private String created;

    @SerializedName("modified")
    private String modified;

    @SerializedName("id")
    private String id;

    @SerializedName("model_id")
    private String modelId;

    @SerializedName("make_id")
    private String makeId;

    @SerializedName("status")
    private String status;

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getInternalCode() {
        return internalCode;
    }

    public void setInternalCode(String internalCode) {
        this.internalCode = internalCode;
    }

    public String getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(String itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getMakeId() {
        return makeId;
    }

    public void setMakeId(String makeId) {
        this.makeId = makeId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "ModelPriceDetails{" +
                        "model_name = '" + modelName + '\'' +
                        ",internal_code = '" + internalCode + '\'' +
                        ",item_price = '" + itemPrice + '\'' +
                        ",created = '" + created + '\'' +
                        ",modified = '" + modified + '\'' +
                        ",id = '" + id + '\'' +
                        ",model_id = '" + modelId + '\'' +
                        ",make_id = '" + makeId + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}