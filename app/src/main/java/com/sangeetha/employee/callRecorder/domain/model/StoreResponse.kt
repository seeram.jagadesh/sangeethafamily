package com.sangeetha.employee.callRecorder.domain.model

import com.google.gson.annotations.SerializedName

data class StoreResponse(
    @SerializedName("status")
    val status: String,

    @SerializedName("data")
    val data: List<Store>,

    )

data class Store(
    @SerializedName("id")
    val id: String,
    @SerializedName("store_code")
    val store_code: String,
    @SerializedName("store_name")
    val store_name: String,
)