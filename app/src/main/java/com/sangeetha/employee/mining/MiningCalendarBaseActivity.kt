package com.sangeetha.employee.mining

import android.annotation.SuppressLint
import android.graphics.RectF
import android.os.Bundle
import android.text.Html
import android.util.TypedValue
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.ViewModelProvider
import com.sangeetha.employee.R
import com.sangeetha.employee.calendarView.*
import com.sangeetha.employee.databinding.ActivityMiningBinding
import com.sangeetha.employee.databinding.ActivityMiningCalendarBinding
import com.sangeetha.employee.mining.response.CalendarResponse
import com.sangeetha.employee.utils.AppPreference
import com.sangeetha.employee.utils.CalendarUtils

import timber.log.Timber
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

//import android.text.format.DateFormat
/**
 * This is a base activity which contains week view and all the codes necessary to initialize the
 * week view.
 */
class MiningCalendarBaseActivity : AppCompatActivity(), WeekView.EventClickListener, MonthLoader.MonthChangeListener, WeekView.EventLongPressListener, WeekView.EmptyViewLongPressListener, WeekView.EmptyViewClickListener{
    private var mWeekViewType = TYPE_THREE_DAY_VIEW
    private lateinit var shortDateFormat: DateFormat
    private lateinit var timeFormat: DateFormat
    var uniqueId: Long = 0
    fun getUniqueId(): String = uniqueId++.toString()
    var appPreference: AppPreference? = null
    var response: CalendarResponse? = null
    lateinit var events: MutableList<WeekViewEvent>
    var matchedEvents: MutableList<WeekViewEvent>? = null
    var calendarViewModel: CalendarViewModel? = null

    private lateinit var binding: ActivityMiningCalendarBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMiningCalendarBinding.inflate(layoutInflater)
        shortDateFormat = WeekViewUtil.getWeekdayWithNumericDayAndMonthFormat(this, true)
        timeFormat = android.text.format.DateFormat.getTimeFormat(this)
                ?: SimpleDateFormat("HH:mm", Locale.getDefault())
        setContentView(binding.root)
        val toolbar = findViewById<Toolbar>(R.id.miningCalendarToolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = resources.getString(R.string.calendar)


        // Get a reference for the week view in the layout.
        // Show a toast message about the touched event.
        binding.contentMin.weekView.eventClickListener = this

        // The week view has infinite scrolling horizontally. We have to provide the events of a
        // month every time the month changes on the week view.
        binding.contentMin.weekView.monthChangeListener = this

        // Set long press listener for events.
        binding.contentMin.weekView.eventLongPressListener = this

        // Set long press listener for empty view
        binding.contentMin.weekView.emptyViewLongPressListener = this

        // Set EmptyView Click Listener
        binding.contentMin.weekView.emptyViewClickListener = this

        // Set AddEvent Click Listener
//        weekView.addEventClickListener = this



        // Set minDate
        /*Calendar minDate = Calendar.getInstance();
        minDate.set(Calendar.DAY_OF_MONTH, 1);
        minDate.add(Calendar.MONTH, 1);
        mWeekView.setMinDate(minDate);

        // Set maxDate
        Calendar maxDate = Calendar.getInstance();
        maxDate.add(Calendar.MONTH, 1);
        maxDate.set(Calendar.DAY_OF_MONTH, 10);
        mWeekView.setMaxDate(maxDate);

        Calendar calendar = (Calendar) maxDate.clone();
        calendar.add(Calendar.DATE, -2);
        mWeekView.goToDate(calendar);*/

        //mWeekView.setAutoLimitTime(true);
        //mWeekView.setLimitTime(4, 16);

        //mWeekView.setMinTime(10);
        //mWeekView.setMaxTime(20);

        // Set up a date time interpreter to interpret how the date and time will be formatted in
        // the week view. This is optional.
        setupDateTimeInterpreter(true)
        binding.contentMin.weekView.typeface = ResourcesCompat.getFont(this, R.font.tiltium)
        appPreference = AppPreference(this)
        val userToken: String? =appPreference?.userToken
        calendarViewModel = ViewModelProvider(this).get(CalendarViewModel::class.java)
        calendarViewModel!!.calendarTasks(userToken).observe(this, androidx.lifecycle.Observer { calendarResponse: CalendarResponse ->
            if (calendarResponse.status) {
                Timber.e(calendarResponse.data.toString())
                response = calendarResponse
                onResume()
            } else {
                Toast.makeText(this, calendarResponse.message, Toast.LENGTH_SHORT).show()
            }
        })
        binding.contentMin.weekView.notifyDataSetChanged()
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.calendar_options, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            R.id.action_today -> {
                binding.contentMin.weekView.goToToday()
                return true
            }
            R.id.action_day_view -> {
                if (!item.isChecked) {
                    item.isChecked = true
                    setDayViewType(TYPE_DAY_VIEW)
                }
                return true
            }
            R.id.action_three_day_view -> {
                if (!item.isChecked) {
                    item.isChecked = true
                    setDayViewType(TYPE_THREE_DAY_VIEW)
                }
                return true
            }
            R.id.action_week_view -> {
                if (!item.isChecked) {
                    item.isChecked = true
                    setDayViewType(TYPE_WEEK_VIEW)
                }
                return true
            }
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun setDayViewType(dayViewType: Int) {
        setupDateTimeInterpreter(dayViewType == TYPE_WEEK_VIEW)

        when (dayViewType) {
            TYPE_DAY_VIEW -> {
                mWeekViewType = TYPE_DAY_VIEW
                binding.contentMin.weekView.numberOfVisibleDays = 1
                // Lets change some dimensions to best fit the view.
                binding.contentMin.weekView.columnGap = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8f, resources.displayMetrics).toInt()
                binding.contentMin.weekView.textSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12f, resources.displayMetrics)
                binding.contentMin.weekView.eventTextSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12f, resources.displayMetrics)
            }
            TYPE_THREE_DAY_VIEW -> {
                mWeekViewType = TYPE_THREE_DAY_VIEW
                binding.contentMin.weekView.numberOfVisibleDays = 3
                // Lets change some dimensions to best fit the view.
                binding.contentMin.weekView.columnGap = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8f, resources.displayMetrics).toInt()
                binding.contentMin.weekView.textSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12f, resources.displayMetrics)
                binding.contentMin.weekView.eventTextSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12f, resources.displayMetrics)
            }
            TYPE_WEEK_VIEW -> {
                mWeekViewType = TYPE_WEEK_VIEW
                binding.contentMin.weekView.numberOfVisibleDays = 7
                // Lets change some dimensions to best fit the view.
                binding.contentMin.weekView.columnGap = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2f, resources.displayMetrics).toInt()
                binding.contentMin.weekView.textSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 10f, resources.displayMetrics)
                binding.contentMin.weekView.eventTextSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 10f, resources.displayMetrics)
            }
        }
    }

    /**
     * Set up a date time interpreter which will show short date values when in week view and long
     * date values otherwise.
     *
     * @param shortDate True if the date values should be short.
     */
    protected open fun setupDateTimeInterpreter(shortDate: Boolean) {
        val calendar = Calendar.getInstance().apply {
            set(Calendar.MINUTE, 0)
            set(Calendar.SECOND, 0)
            set(Calendar.MILLISECOND, 0)
        }
        val normalDateFormat = WeekViewUtil.getWeekdayWithNumericDayAndMonthFormat(this@MiningCalendarBaseActivity, false)
        binding.contentMin.weekView.dateTimeInterpreter = object : DateTimeInterpreter {
            override fun getFormattedTimeOfDay(hour: Int, minutes: Int): String {
                calendar.set(Calendar.HOUR_OF_DAY, hour)
                calendar.set(Calendar.MINUTE, minutes)
                return timeFormat.format(calendar.time)
            }

            override fun getFormattedWeekDayTitle(date: Calendar): String {
                return if (shortDate) shortDateFormat.format(date.time) else normalDateFormat.format(date.time)
            }
        }
    }

    protected fun getEventTitle(startCal: Calendar, endCal: Calendar? = null, allDay: Boolean = false): String {
        val startDate = startCal.time
        val endDate = endCal?.time
        return when {
            allDay -> {
                if (endCal == null || WeekViewUtil.isSameDay(startCal, endCal))
                    shortDateFormat.format(startDate)
                else "${shortDateFormat.format(startDate)}..${shortDateFormat.format(endDate)}"
            }
            endCal == null -> "${shortDateFormat.format(startDate)} ${timeFormat.format(startDate)}"
            WeekViewUtil.isSameDay(startCal, endCal) -> "${shortDateFormat.format(startDate)} ${timeFormat.format(startDate)}..${timeFormat.format(endDate)}"
            else -> "${shortDateFormat.format(startDate)} ${timeFormat.format(startDate)}..${shortDateFormat.format(endDate)} ${timeFormat.format(endDate)}"
        }
    }

    protected fun getEventTitle(time: Calendar, title: String): String? {
        return String.format("$title at %02d:%02d %s/%d/%d", time[Calendar.HOUR_OF_DAY], time[Calendar.MINUTE], time[Calendar.MONTH] + 1, time[Calendar.DAY_OF_MONTH], time[Calendar.YEAR])
    }

    override fun onEventClick(event: WeekViewEvent, eventRect: RectF) {
//        Toast.makeText(this, "Clicked " + event.name, Toast.LENGTH_SHORT).show()
        showEventInfo(event)
    }

    override fun onEventLongPress(event: WeekViewEvent, eventRect: RectF) {
        //Toast.makeText(this, "Long pressed event: " + event.name, Toast.LENGTH_SHORT).show()
        showEventInfo(event)
    }

    override fun onEmptyViewLongPress(time: Calendar) {
        //  Toast.makeText(this, "Empty view long pressed: " + getEventTitle(time), Toast.LENGTH_SHORT).show()
    }

    override fun onEmptyViewClicked(date: Calendar) {
        // Toast.makeText(this, "Empty view" + " clicked: " + getEventTitle(date), Toast.LENGTH_SHORT).show()
    }

//    override fun onAddEventClicked(startTime: Calendar, endTime: Calendar) {
//        //Toast.makeText(this, "Add event clicked.", Toast.LENGTH_SHORT).show()
//    }
//    override fun onDrop(view: View, date: Calendar) {
//        //Toast.makeText(this, "View dropped to " + date.toString(), Toast.LENGTH_SHORT).show()
//    }

    companion object {
        const val TYPE_DAY_VIEW = 1
        const val TYPE_THREE_DAY_VIEW = 2
        const val TYPE_WEEK_VIEW = 3
    }


    override fun onResume() {
        super.onResume()
        binding.contentMin.weekView.notifyDataSetChanged()
    }

    override fun onMonthChange(newYear: Int, newMonth: Int): MutableList<out WeekViewEvent>? {
        // Populate the week view with some events.

        //Toast.makeText(applicationContext,"Month : $newMonth",Toast.LENGTH_SHORT).show()
        // Populate the week view with some events.
        events = ArrayList()
        matchedEvents = ArrayList()


        if (response != null && response!!.data.size > 0) {
            showEvents(newMonth, newYear)
        }


        return matchedEvents as ArrayList<WeekViewEvent>

    }

    private fun eventMatches(event: WeekViewEvent, year: Int, month: Int): Boolean {
        return event.startTime[Calendar.YEAR] == year && event.startTime[Calendar.MONTH] == month || event.endTime[Calendar.YEAR] == year && event.endTime[Calendar.MONTH] == month
    }

    private fun showEvents(month: Int, year: Int) {
        val idset = 0
        val c = 0
        var startCal: Calendar? = null
        var endCal: Calendar? = null


        for (i in response!!.data.indices) {
            startCal = Calendar.getInstance()
            endCal = startCal.clone() as Calendar
            val date = response!!.data[i].startDate
            val title = response!!.data[i].title
            val formattedDate = CalendarUtils.getDateFormat(date)
            val myDate = CalendarUtils.getDateFromString("dd/MM/yyyy hh:mm", formattedDate)
            val start = Calendar.getInstance()
            start[Calendar.DATE] = myDate.date
            start[Calendar.HOUR_OF_DAY] = myDate.hours
            start[Calendar.MINUTE] = myDate.minutes
            start[Calendar.MONTH] = myDate.month
            start[Calendar.YEAR] = year
            val end = start.clone() as Calendar
            end.add(Calendar.HOUR, 1)
            var eventAPI: WeekViewEvent? = WeekViewEvent(i.toString(), title, start, end)
            eventAPI!!.color = resources.getColor(R.color.radio5)
            eventAPI.name = getEventTitle(start,title)

//            events.add(eventAPI)
            if (!events.contains(eventAPI)) {
                events.add(eventAPI)
            }
            for (we in events) {
                if (eventMatches(we, year, month)) {
                    if (!matchedEvents?.contains(we)!!) {
                        matchedEvents!!.add(we)
                    }
                }
            }
            startCal = null
            endCal = null
            eventAPI = null
        }
    }

    @SuppressLint("RestrictedApi")
    private fun showEventInfo(event: WeekViewEvent) {
        val builder = AlertDialog.Builder(this, R.style.Theme_Dialog_event)
        builder.setTitle("Event Info")
        val dpi = this.resources.displayMetrics.density
        val input = TextView(this)
        input.setTextColor(resources.getColor(R.color.black))
        input.textSize = 16f
        builder.setView(input, (25 * dpi).toInt(), (8 * dpi).toInt(), (14 * dpi).toInt(), (8 * dpi).toInt())
        var sdate = event.startTime[Calendar.DAY_OF_MONTH].toString()
        val aMonth=event.startTime[Calendar.MONTH]+1
        var smonth =aMonth.toString()
        val syear = event.startTime[Calendar.YEAR].toString()
        var shour = event.startTime[Calendar.HOUR_OF_DAY].toString()
        var sminute = event.startTime[Calendar.MINUTE].toString()
        var edate = event.endTime[Calendar.DAY_OF_MONTH].toString()
        val eMonth=event.endTime[Calendar.MONTH]+1
        var emonth =eMonth.toString()
        val eyear = event.endTime[Calendar.YEAR].toString()
        var ehour = event.endTime[Calendar.HOUR_OF_DAY].toString()
        var eminute = event.endTime[Calendar.MINUTE].toString()
        if (sdate.length < 2) {
            sdate = "0$sdate"
        }
        if (edate.length < 2) {
            edate = "0$edate"
        }
        if (smonth.length < 2) {
            smonth = "0$smonth"
        }
        if (emonth.length < 2) {
            emonth = "0$emonth"
        }
        if (shour.length < 2) {
            shour = "0$shour"
        }
        if (ehour.length < 2) {
            ehour = "0$ehour"
        }
        if (sminute.length < 2) {
            sminute = "0$sminute"
        }
        if (eminute.length < 2) {
            eminute = "0$eminute"
        }
        val start = "$sdate-${smonth}-$syear $shour:$sminute"
        val end = "$edate-$emonth-$eyear $ehour:$eminute"
        input.text = """
            Title: ${event.name}
            Start Time: $start
            End Time: $end
            """.trimIndent()
        builder.setPositiveButton(Html.fromHtml("<font color='#f97b08'>Yes</font>")) { dialog, id -> //TODO
            dialog.dismiss()
        }
        val dialog = builder.create()
        dialog.show()
    }

}
