package com.sangeetha.employee.onlineDelivery.model;

import com.google.gson.annotations.SerializedName;

public class ReviewValueResponse {
    @SerializedName("status")
    public boolean status;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public ReviewModel data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ReviewModel getData() {
        return data;
    }

    public void setData(ReviewModel data) {
        this.data = data;
    }
}
