package com.sangeetha.employee.servify

import android.annotation.SuppressLint
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.sangeetha.employee.R
import com.sangeetha.employee.databinding.FragmentServifyProcessBinding
import com.sangeetha.employee.employee.servify.viewmodel.ServifyViewModel
import com.sangeetha.employee.managerDiscount.manager_pojo.DataItem
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelItem
import com.sangeetha.employee.servify.model.data.ProductTypeData
import com.sangeetha.employee.utils.AppPreference

class ServifyProcessFragment : Fragment(), View.OnClickListener,
    AdapterView.OnItemSelectedListener {

    companion object {
        fun newInstance() = ServifyProcessFragment()
    }

    private lateinit var viewModel: ServifyViewModel
    private var isReferalIdValid = false
    private lateinit var makeAdapter: ArrayAdapter<String>
    private lateinit var productTypeAdapter: ArrayAdapter<String>
    private lateinit var modelAdapter: ArrayAdapter<String>
    private lateinit var makeDataList: ArrayList<DataItem>
    private lateinit var productTypeDataList: ArrayList<ProductTypeData>
    private lateinit var modelDataList: ArrayList<ModelItem>
    private lateinit var makes: ArrayList<String>
    private lateinit var models: ArrayList<String>
    private lateinit var productType: ArrayList<String>
    private var selectedMake: String? = null
    var appPreference: AppPreference? = null
    private var selectedModel: String? = null
    private var selectedProductType: String = ""
    private var storeId = "560"
    var empId = ""
    var empName = ""
    private var userToken: String = ""

    private lateinit var binding: FragmentServifyProcessBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentServifyProcessBinding.inflate(inflater, container, false )
        makeDataList = ArrayList()
        modelDataList = ArrayList()
        productTypeDataList = ArrayList()
        makes = ArrayList()
        models = ArrayList()
        productType = ArrayList()
        appPreference = AppPreference(activity)
        binding.submitRefId.setOnClickListener(this)
        binding.refContinue.setOnClickListener(this)
        binding.submitInfo.setOnClickListener(this)
        binding.infoBack.setOnClickListener(this)
        binding.copyCoupon.setOnClickListener(this)
        binding.infoContinue.setOnClickListener(this)

        userToken = appPreference?.userToken.toString()
        Log.e("token", userToken)
        makes.add(0, "---- Select Make ----")
        models.add(0, "---- Select Model ----")
        productType.add(0, "---- Select Product Type ----")
        makeAdapter = context?.let { ArrayAdapter(it, R.layout.custom_spinner_layout, makes) }!!
        productTypeAdapter =
            context?.let { ArrayAdapter(it, R.layout.custom_spinner_layout, productType) }!!
        modelAdapter = context?.let { ArrayAdapter(it, R.layout.custom_spinner_layout, models) }!!
        binding.servifyMakeSpinner.adapter = makeAdapter
        binding.servifyProductTypeSpinner.adapter = productTypeAdapter
        binding.servifyModelSpinner.adapter = modelAdapter
        binding.servifyMakeSpinner.onItemSelectedListener = this
        binding.servifyProductTypeSpinner.onItemSelectedListener = this
        binding.servifyModelSpinner.onItemSelectedListener = this
        binding.servifyEmpId.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s?.length!! > 3) {
                    viewModel.findEmp(userToken, s.toString())
                        .observe(viewLifecycleOwner, Observer {
                            if (it.status) {
                                empId = s.toString()
                                empName = it.employeeName
                                binding.servifyEmpName.setText(it.employeeName)
                            }
                        })
                }
            }

        })
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ServifyViewModel::class.java)

//        viewModel.getProductType(userToken).observe(viewLifecycleOwner, Observer {
//            if (it.status) {
//                productTypeDataList.addAll(it.dataList)
//                for (Product in productTypeDataList) {
//                    productType.add(Product.productName)
//                }
//                productTypeAdapter.notifyDataSetChanged()
//            }
//        })
    }

    private fun showMessage(msg: String) {
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show()
    }

    @SuppressLint("LogNotTimber")
    override fun onClick(v: View?) {
        when (v) {
            binding.submitRefId -> {
                if (TextUtils.isEmpty(binding.servifyRefId.text)) {
                    binding.servifyRefId.error = "Enter reference id"
                } else {
                    binding.submitRefId.visibility = View.INVISIBLE
                    binding.servifyloding.visibility = VISIBLE
                    binding.servifyloding.show()
                    viewModel.checkReferenceCode(userToken, binding.servifyRefId.text.toString())
                        .observe(viewLifecycleOwner, Observer { it ->
                            binding.servifyloding.hide()
                            binding.submitRefId.visibility = VISIBLE
                            if (it.status) {
                                isReferalIdValid = true
                                binding.servifyloding.visibility = GONE
                                binding.refContinue.visibility = VISIBLE
                                binding.servifyTradeid.text = it.referenceCodeData.trade_reference_id
                                binding.servifyModel.text = it.referenceCodeData.trade_model
                                binding.servifyIMEI.text = it.referenceCodeData.imei
                                binding.servifyAmount.text = it.referenceCodeData.amount.toString()

                                binding.offerNameLayout.visibility = GONE
                                binding.offerValueLayout.visibility = GONE
                                binding.servifyOfferNameLayout.visibility = GONE
                                binding.servifyOfferValueLayout.visibility = GONE
                                //if(it.referenceCodeData.sangOffers.offerName!=null) {
                                it.referenceCodeData.takeIf {
                                    it.sangOffers != null
                                }?.let {
                                    binding.txtofferName.text = it.sangOffers.offerName
                                    binding.txtofferValue.text = it.sangOffers.offerValue

                                    if (!it.sangOffers.offerName.equals("", true)) {
                                        binding.offerNameLayout.visibility = VISIBLE
                                        binding.offerValueLayout.visibility = VISIBLE
                                    } else {

                                        binding.offerNameLayout.visibility = GONE
                                        binding.offerValueLayout.visibility = GONE
                                    }
                                }
                                it.referenceCodeData.takeIf {
                                    it.Offers != null
                                }?.let {
                                    binding.servifyOfferName.text = it.Offers.offerName
                                    binding.servifyOfferValue.text = it.Offers.offerValue

                                    if (!it.Offers.offerName.equals("", true)) {
                                        binding.servifyOfferNameLayout.visibility = VISIBLE
                                        binding.servifyOfferValueLayout.visibility = VISIBLE
                                    } else {
                                        binding.servifyOfferNameLayout.visibility = GONE
                                        binding.servifyOfferValueLayout.visibility = GONE
                                    }
                                }

                                binding.servifyRetailerAmount.text =
                                    it.referenceCodeData.retailerAmount.toString()
                                if (TextUtils.isEmpty(it.referenceCodeData.logistics_reference_id)) {
                                    binding.servifyLgid.text = "---"
                                } else {
                                    binding.servifyLgid.text = it.referenceCodeData.logistics_reference_id
                                }
                                binding.servifyStatus.text = it.referenceCodeData.trade_status

                                viewModel.getProductType(userToken)
                                    .observe(viewLifecycleOwner, Observer {
                                        if (it.status) {
                                            productTypeDataList.addAll(it.dataList)
                                            for (Product in productTypeDataList) {
                                                productType.add(Product.productName)
                                            }
                                            productTypeAdapter.notifyDataSetChanged()
                                        }
                                    })


                            } else {
                                showMessage(it.message)
                            }
                        })

                }
            }
            binding.refContinue -> {
                if (isReferalIdValid) {
                    binding.createCouponLayout.expand(true)
                    binding.infoBack.visibility = VISIBLE
                }

            }

            binding.submitInfo -> {
                when {
                    TextUtils.isEmpty(binding.servifyCustomerName.text) -> {
                        binding.servifyCustomerName.error = "Enter Customer Name"
                        binding.servifyCustomerName.requestFocus()
                    }
                    TextUtils.isEmpty(binding.servifyCustomerNo.text) -> {
                        binding.servifyCustomerNo.error = "Enter Customer No"
                        binding.servifyCustomerNo.requestFocus()
                    }
                    TextUtils.isEmpty(binding.servifyCustomerEmail.text) -> {
                        binding.servifyCustomerEmail.error = "Enter Customer Email"
                        binding.servifyCustomerEmail.requestFocus()
                    }
                    selectedMake == null -> {
                        showMessage("Select Mobile Brand")
                    }
                    selectedModel == null -> {
                        showMessage("Select Model")
                    }
                    TextUtils.isEmpty(selectedProductType) -> {
                        showMessage("Select Product Type")
                    }

                    TextUtils.isEmpty(binding.servifyEmpId.text) -> {
                        binding.servifyEmpId.error = "Enter Employee Id"
                        binding.servifyEmpId.requestFocus()
                    }
                    TextUtils.isEmpty(binding.servifyEmpName.text) -> {
                        binding.servifyEmpId.error = "Enter Valid Employee Id"
                        showMessage("Enter Valid Employee Id")
                        binding.servifyEmpId.requestFocus()
                    }
                    !binding.servifyCheckbox.isChecked -> {
                        showMessage("Please Check declaration")
                        binding.servifyCheckbox.requestFocus()
                    }
                    else -> {
                        binding.infoLoding.visibility = VISIBLE
                        binding.infoLoding.show()
                        binding.submitInfo.visibility = View.INVISIBLE

                        viewModel.getCoupon(
                            userToken,
                            binding.servifyRefId.text.toString(),
                            binding.servifyAmount.text.toString(),
                            binding.servifyModel.text.toString(),
                            binding.servifyIMEI.text.toString(),
                            binding.servifyStatus.text.toString(),
                            binding.servifyCustomerName.text.toString(),
                            binding.servifyCustomerNo.text.toString(),
                            binding.servifyCustomerEmail.text.toString(),
                            selectedMake!!,
                            selectedModel!!,
                            empId,
                            empName,
                            "agreed",
                            selectedProductType,
                            binding.servifyRetailerAmount.text.toString(),
                            binding.servifyOfferName.text.toString(),
                            binding.servifyOfferValue.text.toString(),
                            binding.txtofferName.text.toString(),
                            binding.txtofferValue.text.toString()
                        ).observe(viewLifecycleOwner, Observer {
                            binding.infoLoding.hide()
                            binding.submitInfo.visibility = VISIBLE
                            if (it.status) {
                                binding.servifyCouponCode.text = it.apxCouponCode
                                binding.couponLayout.expand(true)

                            } else {
                                showMessage(it.message)
                            }
                        })
                    }
                }
            }
            binding.infoBack -> {
                binding.createCouponLayout.expand(false)
                binding.referenceLayout.expand(true)
            }
            binding.copyCoupon -> {
                copyCode(binding.servifyCouponCode.text.toString())
//                val clipBoard = context?.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
//                val clipData = ClipData.newPlainText("Coupon", servifyCouponCode.text.toString())
//                clipBoard.setPrimaryClip(clipData)
            }


        }
    }

    fun copyCode(coupon: String) {
        val clipBoard = activity?.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clipData = ClipData.newPlainText("Coupon", coupon)
        clipBoard.setPrimaryClip(clipData)
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        if (position > 0) {
            when (parent?.id) {
                R.id.servifyProductTypeSpinner -> {
                    selectedProductType = productTypeDataList[position - 1].id
                    setMake(productTypeDataList[position - 1].id)
                    makes.clear()
                    makes.add(0, "---- Select Make ----")
                    makeAdapter.notifyDataSetChanged()
                    models.clear()
                    models.add(0, "---- Select Model ----")
                    modelAdapter.notifyDataSetChanged()
                }
                R.id.servifyMakeSpinner -> {
                    selectedMake = makeDataList[position - 1].id
                    setModel(makeDataList[position - 1].id, selectedProductType)
                    models.clear()
                    models.add(0, "---- Select Model ----")
                    modelAdapter.notifyDataSetChanged()
                }
                R.id.servifyModelSpinner -> {
                    selectedModel = modelDataList[position - 1].id
                }
            }
        }

    }

    private fun setModel(makeId: String, productId: String) {
        models.clear()
        modelDataList.clear()
        models.add(0, "---- Select Model ----")
        viewModel.getModel(userToken, makeId, productId).observe(viewLifecycleOwner, Observer {
            if (it.isStatus) {
                modelDataList.addAll(it.data)
                for (model in it.data) {
                    models.add(model.modelName)
                }
                modelAdapter.notifyDataSetChanged()
            }
        })
    }

    private fun setMake(productType: String) {
        makes.clear()
        makeDataList.clear()
        makes.add(0, "---- Select Make ----")
        viewModel.getMake(userToken, productType).observe(viewLifecycleOwner, Observer {
            if (it.isStatus) {
                makeDataList.addAll(it.data)
                for (makeData in makeDataList) {
                    makes.add(makeData.makeName)
                }
                makeAdapter.notifyDataSetChanged()
            }
        })
    }

}
