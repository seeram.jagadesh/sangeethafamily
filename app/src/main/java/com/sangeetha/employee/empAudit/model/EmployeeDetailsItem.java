package com.sangeetha.employee.empAudit.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class EmployeeDetailsItem implements Parcelable {

    public static final Creator<EmployeeDetailsItem> CREATOR = new Creator<EmployeeDetailsItem>() {
        @Override
        public EmployeeDetailsItem createFromParcel(Parcel in) {
            return new EmployeeDetailsItem(in);
        }

        @Override
        public EmployeeDetailsItem[] newArray(int size) {
            return new EmployeeDetailsItem[size];
        }
    };
    @SerializedName("division")
    private String division;
    @SerializedName("admin_user_id")
    private String adminUserId;
    @SerializedName("created")
    private String created;
    @SerializedName("employee_id")
    private String employeeId;
    @SerializedName("mobile")
    private String mobile;
    @SerializedName("modified")
    private Object modified;
    @SerializedName("employee_name")
    private String employeeName;
    @SerializedName("id")
    private String id;
    @SerializedName("designation")
    private String designation;
    @SerializedName("department")
    private String department;
    @SerializedName("region")
    private String region;
    @SerializedName("email")
    private String email;
    private String update;
    private String additionalInfo;
    private ArrayList<String> numbers;

    protected EmployeeDetailsItem(Parcel in) {
        division = in.readString();
        adminUserId = in.readString();
        created = in.readString();
        employeeId = in.readString();
        mobile = in.readString();
        employeeName = in.readString();
        id = in.readString();
        designation = in.readString();
        department = in.readString();
        region = in.readString();
        email = in.readString();
        update = in.readString();
        additionalInfo = in.readString();
        numbers = in.createStringArrayList();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(division);
        dest.writeString(adminUserId);
        dest.writeString(created);
        dest.writeString(employeeId);
        dest.writeString(mobile);
        dest.writeString(employeeName);
        dest.writeString(id);
        dest.writeString(designation);
        dest.writeString(department);
        dest.writeString(region);
        dest.writeString(email);
        dest.writeString(update);
        dest.writeString(additionalInfo);
        dest.writeStringList(numbers);
    }


    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getAdminUserId() {
        return adminUserId;
    }

    public void setAdminUserId(String adminUserId) {
        this.adminUserId = adminUserId;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Object getModified() {
        return modified;
    }

    public void setModified(Object modified) {
        this.modified = modified;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUpdate() {
        return update;
    }

    public void setUpdate(String update) {
        this.update = update;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public ArrayList<String> getNumbers() {
        numbers = new ArrayList<>();
        return numbers;
    }

    public void setNumbers(ArrayList<String> numbers) {
        this.numbers = numbers;
    }
}