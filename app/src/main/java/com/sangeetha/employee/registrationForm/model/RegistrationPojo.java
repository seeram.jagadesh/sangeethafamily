package com.sangeetha.employee.registrationForm.model;


import com.google.gson.annotations.SerializedName;


public class RegistrationPojo {

    @SerializedName("data")
    public Data data;

    @SerializedName("message")
    public String message;

    @SerializedName("status")
    public boolean status;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "RegistrationPojo{" +
                        "data = '" + data + '\'' +
                        ",message = '" + message + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}