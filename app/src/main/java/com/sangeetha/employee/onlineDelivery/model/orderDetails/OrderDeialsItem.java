package com.sangeetha.employee.onlineDelivery.model.orderDetails;


import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;


public class OrderDeialsItem{

	@SerializedName("quantity")
	private String quantity;

	@SerializedName("product_name")
	private String productName;

	public void setQuantity(String quantity){
		this.quantity = quantity;
	}

	public String getQuantity(){
		return quantity;
	}

	public void setProductName(String productName){
		this.productName = productName;
	}

	public String getProductName(){
		return productName;
	}

	@NotNull
	@Override
 	public String toString(){
		return 
			"OrderDeialsItem{" + 
			"quantity = '" + quantity + '\'' + 
			",product_name = '" + productName + '\'' + 
			"}";
		}
}