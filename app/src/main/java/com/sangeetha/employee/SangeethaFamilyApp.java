package com.sangeetha.employee;

import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.util.Log;

import androidx.multidex.MultiDex;
import cafe.adriel.androidaudioconverter.AndroidAudioConverter;
import cafe.adriel.androidaudioconverter.callback.ILoadCallback;
import dagger.hilt.android.HiltAndroidApp;
import timber.log.Timber;
import timber.log.Timber.DebugTree;

import com.arthenica.mobileffmpeg.Config;
import com.arthenica.mobileffmpeg.LogCallback;
import com.arthenica.mobileffmpeg.LogMessage;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.sangeetha.employee.utils.AppPreference;
import com.sangeetha.employee.utils.LanguageUtil;

import java.util.Locale;

@HiltAndroidApp
public class SangeethaFamilyApp extends Application {


    private static Context mContext;

    public static Context getContext() {
        return mContext;
    }

    @Override
    public void onCreate() {

        super.onCreate();

        Fresco.initialize(this);
//        Timber.plant(new DebugTree());
        Timber.plant(new Timber.DebugTree());

        mContext = this;
        Config.enableLogCallback(new LogCallback() {
            public void apply(LogMessage message) {
                Log.d(Config.TAG, message.getText());
            }
        });
        //region UIL
//        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
//                .resetViewBeforeLoading(true)
//                .cacheOnDisk(true)
//                .cacheInMemory(true)
//                .imageScaleType(ImageScaleType.EXACTLY)
//                .displayer(new FadeInBitmapDisplayer(300))
//                .build();
//
//        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
//                .defaultDisplayImageOptions(defaultOptions)
//                .memoryCache(new WeakMemoryCache())
//                .diskCacheSize(100 * 1024 * 1024)
//                .build();
//
//        ImageLoader.getInstance().init(config);
        //endregion
        String localLanguageType = AppPreference.getLanguage(mContext);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if ("hi".equals(localLanguageType)) {
                LanguageUtil.changeLanguageType(mContext, Locale.forLanguageTag("hi"));
            } else {
                LanguageUtil.changeLanguageType(mContext, Locale.ENGLISH);
            }
        } else {
            AppPreference.setLanguage(AppPreference.getLanguage(mContext), mContext);
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

}
