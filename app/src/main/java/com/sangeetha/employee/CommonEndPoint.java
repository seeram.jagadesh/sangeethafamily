package com.sangeetha.employee;


import com.sangeetha.employee.CommonResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface CommonEndPoint {

    @FormUrlEncoded
    @POST("api-login")
    Call<ResponseBody> empLogin(@Field("username") String username,
                                @Field("password") String password,
                                @Field("imei_number") String imei_number);
    @FormUrlEncoded
    @POST("delivery-app-login")
    Call<ResponseBody> delLogin(@Field("username") String username,
                                @Field("password") String password);

    @POST("api-portal-icon")
    Call<ResponseBody> portalIcons(@Header("sangeetha_token") String token);

    @FormUrlEncoded
    @POST("api-admin-token-submit")
    Call<CommonResponse> addNotificationToken(@Field("employee_id") String employeeId,
                                              @Field("token") String token);
}
