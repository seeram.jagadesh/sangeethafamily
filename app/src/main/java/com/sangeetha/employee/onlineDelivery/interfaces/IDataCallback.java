package com.sangeetha.employee.onlineDelivery.interfaces;

import java.util.ArrayList;

public interface IDataCallback {

    void onFragmentCreated( ArrayList<String> listData);
}
