package com.sangeetha.employee.onlineDelivery;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.sangeetha.employee.CommonResponse;
import com.sangeetha.employee.R;
import com.sangeetha.employee.RetrofitServiceGenerator;
import com.sangeetha.employee.SangeethaCareApiClient;
import com.sangeetha.employee.onlineDelivery.model.ReviewValueResponse;
import com.sangeetha.employee.onlineDelivery.model.otpVerification.OtpVerification;
import com.sangeetha.employee.onlineDelivery.ratings.RatingClick;
import com.sangeetha.employee.onlineDelivery.ratings.RatingOptionsAdapter;
import com.sangeetha.employee.utils.AppConstants;
import com.sangeetha.employee.utils.AppPreference;
import com.sangeetha.employee.utils.ImageUtility;
import com.sangeetha.employee.utils.LanguageUtil;
import com.sangeetha.employee.utils.MyContextWrapper;
import com.sangeetha.employee.utils.UploadImagePresenter;

import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OnlineDeliveryOTPActivity extends AppCompatActivity implements UploadImagePresenter.uploadImage,
        RatingClick, RatingOptionsAdapter.SingleClickListener {

    private AppCompatEditText otpEditText;

    String paymentType, amount;
    int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    Bitmap bitmapImage;
    private String accessoriesImageTxt = "";
    private String accessoriesImageID = "";
    ImageUtility imageUtility;
    private File photoFile = null;
    String imageUploadedId = "";
    private String rating = "";
    String amountPaid = "";
    private String ratingNo = "";
    String fromOrder;
    AppCompatTextView ratingText;
    RatingClick clickRating;
    RatingOptionsAdapter ratingsAdapter;
    UploadImagePresenter uploadImagePresenter;
    private Uri uri;
    ProgressDialog uploadProgress;
    AppCompatImageView imageView;
    private static final int CAMERA_ACCESS_REQ_CODE = 88;

    ReviewValueResponse reviewValueResponse;

    private String[] permissionsReq = new String[]{
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    OnlineDeliveryEndPoint onlineDeliveryEndPoint;
    private String orderId;

    String info = "";
    private AppPreference appPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_delivery_otp);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getResources().getString(R.string.verify_otp));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        otpEditText = findViewById(R.id.otp_ed);

        imageUtility = new ImageUtility(this);
        AppCompatButton verifyOtp = findViewById(R.id.otp_submit_btn);
        AppCompatButton resendOtp = findViewById(R.id.resend_otp);
        onlineDeliveryEndPoint = SangeethaCareApiClient.getClient1().create(OnlineDeliveryEndPoint.class);
        OnlineDeliveryEndPoint getval = SangeethaCareApiClient.getEcomClient().create(OnlineDeliveryEndPoint.class);
        orderId = getIntent().getStringExtra("order_id");
        amount = getIntent().getStringExtra("amount");
        amountPaid = getIntent().getStringExtra("amount_paid");

        Intent g = getIntent();
        if (g.hasExtra("info")) {
            info = g.getStringExtra("info");
        }
        appPreference = new AppPreference(this);
        uploadImagePresenter = new UploadImagePresenter(getApplicationContext(), OnlineDeliveryOTPActivity.this);

        clickRating = this;
        verifyOtp.setOnClickListener(v -> {

            if (!TextUtils.isEmpty(Objects.requireNonNull(otpEditText.getText()).toString())) {
                verificationOtp(otpEditText.getText().toString());
            } else {
                Toast.makeText(OnlineDeliveryOTPActivity.this, "Please enter OTP.", Toast.LENGTH_SHORT).show();
            }

        });


        resendOtp.setOnClickListener(v -> generateOtp());


        if (info.equalsIgnoreCase("rating")) {
            showRateCard();
        } else if (info.equalsIgnoreCase("employee")) {
            getEmployeeInfo();
        }

        getval.getValueReviews().enqueue(new Callback<ReviewValueResponse>() {
            @Override
            public void onResponse(@NotNull Call<ReviewValueResponse> call, @NotNull Response<ReviewValueResponse> response) {
                if (response.isSuccessful()) {
                    reviewValueResponse = response.body();
                } else {
                    showMsg("Error while receiving review messages");
                }
            }

            @Override
            public void onFailure(@NotNull Call<ReviewValueResponse> call, @NotNull Throwable t) {
                showMsg("Failed to get review messages");
            }
        });
    }

    private void verificationOtp(String otp) {
        onlineDeliveryEndPoint.verifyOtp(appPreference.getEmpData().getUserId(), orderId, otp).enqueue(new Callback<OtpVerification>() {
            @Override
            public void onResponse(@NotNull Call<OtpVerification> call, @NotNull Response<OtpVerification> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    if (response.body().isStatus()) {
                        OtpVerification otpVerification = response.body();
                        amountPaid=otpVerification.getAmountPaid();
                        Toast.makeText(OnlineDeliveryOTPActivity.this, otpVerification.getMessage(), Toast.LENGTH_SHORT).show();
//                        if (otpVerification.getAmountPaid().equals("1")) {
//                            showProgress("Updating data");
//                            sendOrderStatus("", "", "", "");
                        getEmployeeInfo();
//                        }
//                        else {
////                            showDialogue();
//                            getEmployeeInfo();
//                        }
                    } else {

                        Toast.makeText(OnlineDeliveryOTPActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {

                    Toast.makeText(OnlineDeliveryOTPActivity.this, "Server returned an error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<OtpVerification> call, @NotNull Throwable t) {

                Toast.makeText(OnlineDeliveryOTPActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void sendOrderStatus(String paymentType, String referenceNo, String attachment_id, String paidBy) {
        onlineDeliveryEndPoint.verifyOrder(appPreference.getEmpData().getUserId(), orderId, paymentType, referenceNo, attachment_id, paidBy).enqueue(new Callback<OtpVerification>() {
            @Override
            public void onResponse(@NotNull Call<OtpVerification> call, @NotNull Response<OtpVerification> response) {
                if (response.isSuccessful()) {
                    hideProgress();
                    assert response.body() != null;
                    if (response.body().isStatus()) {
                        OtpVerification otpVerification = response.body();
                        Toast.makeText(OnlineDeliveryOTPActivity.this, otpVerification.getMessage(), Toast.LENGTH_SHORT).show();
                        if (otpVerification.getReview_added_status().equalsIgnoreCase("1")) {
                            Intent intent = new Intent();
                            // intent.putExtra("editTextValue", "value_here")
                            setResult(RESULT_OK, intent);
                            finish();
                        } else {
                            showRateCard();
                        }
                    } else {
                        Toast.makeText(OnlineDeliveryOTPActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    hideProgress();
                    Toast.makeText(OnlineDeliveryOTPActivity.this, "Server returned an error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<OtpVerification> call, @NotNull Throwable t) {
                hideProgress();
                Toast.makeText(OnlineDeliveryOTPActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getEmployeeInfo() {
        final BottomSheetDialog empBottom = new BottomSheetDialog(OnlineDeliveryOTPActivity.this);
        View v = getLayoutInflater().inflate(R.layout.vehicle_info, null);

        TextInputEditText edtEmpId = v.findViewById(R.id.empId);
        TextInputEditText edtempVehicleNo = v.findViewById(R.id.empVehicleNo);
        TextInputEditText edtempDistance = v.findViewById(R.id.empDistance);
        AppCompatButton addData = v.findViewById(R.id.btnAddData);

        addData.setOnClickListener(c -> {

            if (Objects.requireNonNull(edtEmpId.getText()).toString().equalsIgnoreCase("")) {
                showMsg("Please enter employee ID to proceed.");
            } else if (Objects.requireNonNull(edtempVehicleNo.getText()).toString().equalsIgnoreCase("")) {
                showMsg("Please enter vehicle number to proceed.");
            } else if (Objects.requireNonNull(edtempDistance.getText()).toString().equalsIgnoreCase("")) {
                showMsg("Please enter distance you travelled.");
            } else {
                OnlineDeliveryEndPoint deliveryEndPoint = RetrofitServiceGenerator.getDeliveryClient(AppConstants.adminBackEndBaseUrl).create(OnlineDeliveryEndPoint.class);
                deliveryEndPoint.submitEmpInfo(orderId, edtEmpId.getText().toString(), edtempVehicleNo.getText().toString(), edtempDistance.getText().toString()).enqueue(new Callback<CommonResponse>() {
                    @Override
                    public void onResponse(@NotNull Call<CommonResponse> call, @NotNull Response<CommonResponse> response) {
                        if (response.isSuccessful()) {
                            CommonResponse res = response.body();
                            assert res != null;
                            if (res.getStatus()) {
                                showMsg(res.getMessage());
                                showRateCard();
                            } else {
                                showMsg(res.getMessage());
                            }
                        } else {
                            showMsg("Error while submitting your data");
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<CommonResponse> call, @NotNull Throwable t) {
                        showMsg("Failed to submit your data");
                    }
                });
            }
            empBottom.dismiss();

        });
        empBottom.setContentView(v);
        empBottom.setCancelable(false);
        empBottom.setCanceledOnTouchOutside(false);
        empBottom.show();
    }

    private void showRateCard() {
        final BottomSheetDialog dialog = new BottomSheetDialog(OnlineDeliveryOTPActivity.this);
        View view = getLayoutInflater().inflate(R.layout.order_product_rating, null);
        AppCompatRatingBar compatRatingBar = view.findViewById(R.id.ratingBar);
        ratingText = view.findViewById(R.id.ratingText);
        TextInputEditText edtComment = view.findViewById(R.id.comment);
        AppCompatButton submit = view.findViewById(R.id.submit);
        AppCompatButton cancel = view.findViewById(R.id.cancel);
        RecyclerView ratingRecycler = view.findViewById(R.id.ratingRecycler);

        ArrayList<String> textRate = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.rating_count)));

        int[] drawableItems = {R.drawable.plainround_bg, R.drawable.rating1_bg, R.drawable.rating2_bg,
                R.drawable.rating3_bg, R.drawable.rating4_bg, R.drawable.rating5_bg};
        List<Integer> bgRate = new ArrayList<Integer>(drawableItems.length);
        for (int f : drawableItems) {
            bgRate.add(f);
        }
        ratingsAdapter = new RatingOptionsAdapter(this, textRate, bgRate, clickRating);
        ratingsAdapter.setOnItemClickListener(this);

        ratingRecycler.setAdapter(ratingsAdapter);
//        compatRatingBar.setOnRatingBarChangeListener((ratingBar, v, b) -> {
//
//            switch ((int) ratingBar.getRating()) {
//                case 1:
//                    rating = reviewValueResponse.getData().getOne();
//                    ratingText.setText(rating);
//                    break;
//                case 2:
//                    rating = reviewValueResponse.getData().getTwo();
//                    ratingText.setText(rating);
//                    break;
//                case 3:
//                    rating = reviewValueResponse.getData().getThree();
//                    ratingText.setText(rating);
//                    break;
//                case 4:
//                    rating = reviewValueResponse.getData().getFour();
//                    ratingText.setText(rating);
//                    break;
//                case 5:
//                    rating = reviewValueResponse.getData().getFive();
//                    ratingText.setText(rating);
//                    break;
//                default:
//                    ratingText.setText("");
//            }
//        });
        cancel.setOnClickListener(v -> {
            dialog.hide();
        });

        submit.setOnClickListener(v -> {
            onlineDeliveryEndPoint.submitOrderReview(orderId, ratingNo,
                    Objects.requireNonNull(edtComment.getText()).toString()).enqueue(new Callback<CommonResponse>() {
                @Override
                public void onResponse(@NotNull Call<CommonResponse> call, @NotNull Response<CommonResponse> response) {
                    CommonResponse commonResponse = response.body();
                    if (response.isSuccessful()) {
                        assert commonResponse != null;
                        if (commonResponse.getStatus()) {
                            if (dialog.isShowing()) {
                                dialog.hide();
                            }
                            showMsg(commonResponse.getMessage());
//                            Intent intent = new Intent();
//                            setResult(RESULT_OK, intent);
//                            finish();
                            if (amountPaid.equals("1")) {
                                showProgress("Updating data");
                                sendOrderStatus("", "", "", "");
                            } else {
                                showDialogue();
                            }
                        } else {
                            showMsg(commonResponse.getMessage());
                        }
                    } else {
                        showMsg("Error while submitting review data.");
                    }

                }

                @Override
                public void onFailure(@NotNull Call<CommonResponse> call, @NotNull Throwable t) {
                    showMsg("Failed to submit review data.");
                }
            });
        });
        dialog.setContentView(view);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    private void generateOtp() {
        onlineDeliveryEndPoint.generateOtp(appPreference.getEmpData().getUserId(), orderId).enqueue(new Callback<OtpVerification>() {
            @Override
            public void onResponse(@NotNull Call<OtpVerification> call, @NotNull Response<OtpVerification> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    if (response.body().isStatus()) {
                        OtpVerification otpVerification = response.body();
                        Toast.makeText(OnlineDeliveryOTPActivity.this, otpVerification.getMessage(), Toast.LENGTH_SHORT).show();
                    } else {

                        Toast.makeText(OnlineDeliveryOTPActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {

                    Toast.makeText(OnlineDeliveryOTPActivity.this, "Server returned an error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<OtpVerification> call, @NotNull Throwable t) {

                Toast.makeText(OnlineDeliveryOTPActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showDialogue() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.cash_collected_pop_up);
        dialog.setCancelable(true);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(Objects.requireNonNull(dialog.getWindow()).getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        AppCompatTextView textView = dialog.findViewById(R.id.textView);
        imageView = dialog.findViewById(R.id.cameraAddImage);
        textView.setText(String.format("Are you sure ₹ %s is collected?", amount));
        RadioGroup radioGroup = dialog.findViewById(R.id.radioGroup);
        TextInputLayout referenceNoLayout = dialog.findViewById(R.id.referenceNoLayout);
        TextInputLayout paymentMethodLayout = dialog.findViewById(R.id.paymentMethodLayuot);
        LinearLayout addImageLayout = dialog.findViewById(R.id.addImageLayout);
        TextInputEditText referenceNoTxt = dialog.findViewById(R.id.referenceNoTxt);
        TextInputEditText paymentMethod = dialog.findViewById(R.id.txtPaymentMethod);

        dialog.findViewById(R.id.yes_btn).setOnClickListener(v -> {

            switch (paymentType) {
                case "1":
                    dialog.dismiss();
                    showProgress("Updating data");
//                    getEmployeeInfo();
                    sendOrderStatus(paymentType, "", "", "");
                    break;
                case "2":
                    if (TextUtils.isEmpty(Objects.requireNonNull(referenceNoTxt.getText()).toString()))
                        showMsg("Please enter reference number");
                    else if (TextUtils.isEmpty(imageUploadedId))
                        showMsg("Please upload customer proof of payment");
                    else {
                        showProgress("Updating data");
//                        getEmployeeInfo();
                        sendOrderStatus(paymentType, referenceNoTxt.getText().toString(), imageUploadedId, "");
                    }
                    break;
                case "3":
                    if (TextUtils.isEmpty(Objects.requireNonNull(paymentMethod.getText().toString())))
                        showMsg("Please enter payment method");
                    else if (TextUtils.isEmpty(Objects.requireNonNull(referenceNoTxt.getText()).toString()))
                        showMsg("Please enter reference number");
                    else if (TextUtils.isEmpty(imageUploadedId))
                        showMsg("Please upload customer proof of payment");
                    else {
//                        getEmployeeInfo();
                        showProgress("Updating data");
                        sendOrderStatus(paymentType, referenceNoTxt.getText().toString(), imageUploadedId, paymentMethod.getText().toString());
                    }
                    break;
            }
        });

        imageView.setOnClickListener(v -> {
            showImagePickerActionPopUp();
        });
        dialog.findViewById(R.id.no_btn).setOnClickListener(v -> dialog.dismiss());


        radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.radio1:
                    paymentType = "1";
                    referenceNoLayout.setVisibility(View.GONE);
                    addImageLayout.setVisibility(View.GONE);
                    paymentMethodLayout.setVisibility(View.GONE);

                    break;
                case R.id.radio2:
                    paymentType = "2";
                    referenceNoLayout.setVisibility(View.VISIBLE);
                    paymentMethodLayout.setVisibility(View.GONE);
                    addImageLayout.setVisibility(View.VISIBLE);
                    break;
                case R.id.radio3:
                    paymentType = "3";
                    referenceNoLayout.setVisibility(View.VISIBLE);
                    paymentMethodLayout.setVisibility(View.VISIBLE);
                    addImageLayout.setVisibility(View.VISIBLE);
                    break;
            }
        });


    }

    private boolean checkForCameraPermission() {

        if (permissionsReq != null) {
            for (String permission : permissionsReq) {
                if (ActivityCompat.checkSelfPermission(this, permission)
                        != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private void reqCameraAccess() {
        ActivityCompat.requestPermissions(this,
                permissionsReq, CAMERA_ACCESS_REQ_CODE);
    }

    private void setUpCameraPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (!checkForCameraPermission()) {
                reqCameraAccess();
            } else
                setUpCamera();
        } else
            setUpCamera();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case -1:
                break;

            case CAMERA_ACCESS_REQ_CODE:
                if (grantResults.length > 0) {

                    if ((grantResults[0] == PackageManager.PERMISSION_GRANTED)
                            && (grantResults[1] == PackageManager.PERMISSION_GRANTED))
                        setUpCamera();
                    else
                        reqCameraAccess();
                } else {
                    // Toast.makeText(DPClaimActivity.this, "Please allow camera access", Toast.LENGTH_SHORT).show();
                    reqCameraAccess();
                }
                break;
        }
    }

    private void showImagePickerActionPopUp() {
        final BottomSheetDialog dialog = new BottomSheetDialog(OnlineDeliveryOTPActivity.this);
        View view = getLayoutInflater().inflate(R.layout.image_pick_sheet, null);
        view.findViewById(R.id.gallery_pick_id).setOnClickListener(view1 -> {
            dialog.dismiss();
            openGallery();
        });

        view.findViewById(R.id.camera_pick_id).setOnClickListener(view12 -> {
            dialog.dismiss();
            setUpCameraPermission();
            // setUpCamera();

        });

        dialog.setContentView(view);
        dialog.show();
    }

    private void setUpCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            try {
                photoFile = imageUtility.createImageFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            Log.e("photo", photoFile.getAbsolutePath() + "  ");
            // Continue only if the File was successfully created
            if (photoFile != null) {
                uri = FileProvider.getUriForFile(OnlineDeliveryOTPActivity.this,
                        "com.sangeetha.employee",
                        photoFile);
                takePictureIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                takePictureIntent.setClipData(ClipData.newRawUri(null, uri));
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                startActivityForResult(takePictureIntent, REQUEST_CAMERA);
            }
        }
    }

    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setDataAndType(uri, "image/*");
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            if (requestCode == REQUEST_CAMERA) {
                imageView.setImageBitmap(imageUtility.compressImage(photoFile.getAbsolutePath(), photoFile));
                //imageView.setImageBitmap(ImageUtility.loadResizedBitmap(photoFile.getAbsolutePath(), 500, 700, false));
            } else if (requestCode == SELECT_FILE) {
                {
                    try {
                        bitmapImage = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
                        imageView.setImageBitmap(imageUtility.createDirectoryAndSaveFile(bitmapImage));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            getBase64(imageView);
        }
    }

    public byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        return stream.toByteArray();
    }

    private void getBase64(AppCompatImageView imageView) {
        Bitmap bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        accessoriesImageTxt = imageUtility.imageString(((BitmapDrawable) imageView.getDrawable()).getBitmap());

        showProgress("Uploading payment proof");

//        applyClaimProgress.setMessage("Uploading your image");
        //showProgress("UPloading your proof image");
        uploadImagePresenter.imageUpload(orderId, getBytesFromBitmap(bmp));
    }


    private void showProgress(String msg) {
        if (uploadProgress != null)
            uploadProgress = null;

        uploadProgress = new ProgressDialog(OnlineDeliveryOTPActivity.this);
        uploadProgress.setCancelable(false);
        uploadProgress.setCanceledOnTouchOutside(false);
        uploadProgress.setMessage(msg);
        uploadProgress.show();
    }

    void hideProgress() {
        if (uploadProgress != null && uploadProgress.isShowing())
            uploadProgress.dismiss();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        Locale languageType = LanguageUtil.getLanguageType(newBase);
        super.attachBaseContext(MyContextWrapper.wrap(newBase, languageType));
    }

    @Override
    public void uploadSuccess(String response) {
        hideProgress();
        imageUploadedId = response;
    }

    @Override
    public void uploadError(String response) {
        hideProgress();
        showMsg(response);
    }

    @Override
    public void uploadFail(String response) {
        hideProgress();
        showMsg(response);
    }

    public void showMsg(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getSelected(String number) {
        ratingNo = number;
//        rating = number;
        ratingText.setVisibility(View.VISIBLE);
        switch ((int) Integer.parseInt(number)) {
            case 1:
//                rating = reviewValueResponse.getData().getOne();
                ratingText.setText(reviewValueResponse.getData().getOne());
                break;
            case 2:
//                rating = reviewValueResponse.getData().getTwo();
                ratingText.setText(reviewValueResponse.getData().getTwo());
                break;
            case 3:
//                rating = reviewValueResponse.getData().getThree();
                ratingText.setText(reviewValueResponse.getData().getThree());
                break;
            case 4:
//                rating = reviewValueResponse.getData().getFour();
                ratingText.setText(reviewValueResponse.getData().getFour());
                break;
            case 5:
//                rating = reviewValueResponse.getData().getFive();
                ratingText.setText(reviewValueResponse.getData().getFive());
                break;
            default:
                ratingText.setText("");
        }
//        ratingText.setText(number);

    }

    @Override
    public void onItemClickListener(int position, View view) {
        ratingsAdapter.selectedItem();

    }
}
