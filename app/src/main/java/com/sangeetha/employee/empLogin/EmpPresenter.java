package com.sangeetha.employee.empLogin;

public interface EmpPresenter {

    /**
     * @param empId
     * @param pass
     * @param IMEI
     */
    void login(String empId, String pass, String IMEI);

    void delLogin(String id,String pass);
}
