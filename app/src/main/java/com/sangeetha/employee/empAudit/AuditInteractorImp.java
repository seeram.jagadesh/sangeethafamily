package com.sangeetha.employee.empAudit;

import androidx.annotation.NonNull;

import com.sangeetha.employee.SangeethaCareApiClient;
import com.sangeetha.employee.empAudit.model.AuditReportResponse;
import com.sangeetha.employee.empAudit.model.BrandResponse;
import com.sangeetha.employee.empAudit.model.DesignationResponse;
import com.sangeetha.employee.empAudit.model.EmpCategoryResponse;
import com.sangeetha.employee.empAudit.model.EmpStatusResponse;
import com.sangeetha.employee.empAudit.model.SearchEmpResponse;
import com.sangeetha.employee.empAudit.model.StartAuditResponse;
import com.sangeetha.employee.empAudit.model.StoreResponse;
import com.sangeetha.employee.empAudit.model.SubCategoryResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuditInteractorImp implements AuditInteractor {

    private AuditListener auditListener;
    private AuditEndPoint auditEndPoint;

    AuditInteractorImp(AuditListener auditListener) {
        this.auditListener = auditListener;
        auditEndPoint = SangeethaCareApiClient.getClient().create(AuditEndPoint.class);
    }

    @Override
    public void getStoreList(String token) {
        auditEndPoint.getStores(token).enqueue(new Callback<StoreResponse>() {
            @Override
            public void onResponse(@NonNull Call<StoreResponse> call, @NonNull Response<StoreResponse> response) {
                if (response.isSuccessful()) {
                    StoreResponse storeResponse = response.body();
                    assert storeResponse != null;
                    if (storeResponse.isStatus()) {
                        auditListener.setStoreList(storeResponse.getData());
                    } else {
                        auditListener.showMessage(storeResponse.getMessage());
                    }
                } else {
                    auditListener.showMessage(response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<StoreResponse> call, @NonNull Throwable t) {
                auditListener.showMessage(t.getMessage());
            }
        });

    }

    @Override
    public void getBrandList(String token) {
        auditEndPoint.getBrands(token).enqueue(new Callback<BrandResponse>() {
            @Override
            public void onResponse(@NonNull Call<BrandResponse> call, @NonNull Response<BrandResponse> response) {
                if (response.isSuccessful()) {
                    BrandResponse brandResponse = response.body();
                    assert brandResponse != null;
                    if (brandResponse.isStatus()) {
                        auditListener.setBrandList(brandResponse.getData());
                    }
                } else {
                    auditListener.showMessage(response.message());
                }

            }

            @Override
            public void onFailure(@NonNull Call<BrandResponse> call, @NonNull Throwable t) {
                auditListener.showMessage(t.getMessage());
            }
        });

    }

    @Override
    public void getEmpStatus(String token) {
        auditEndPoint.getEmpStatus(token).enqueue(new Callback<EmpStatusResponse>() {
            @Override
            public void onResponse(@NonNull Call<EmpStatusResponse> call, @NonNull Response<EmpStatusResponse> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    if (response.body().isStatus()) {
                        auditListener.setEmpStatus(response.body().getDropdownDetails());
                    } else {
                        auditListener.showMessage(response.body().getMessage());
                    }
                } else {
                    auditListener.showMessage(response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<EmpStatusResponse> call, @NonNull Throwable t) {
                auditListener.showMessage(t.getMessage());
            }
        });

    }

    @Override
    public void getEmployeeDetails(String token,String empId) {
        auditEndPoint.getEmployeeDetail(token,empId).enqueue(new Callback<SearchEmpResponse>() {
            @Override
            public void onResponse(@NonNull Call<SearchEmpResponse> call, @NonNull Response<SearchEmpResponse> response) {
                if (response.isSuccessful()) {
                    SearchEmpResponse fetchEmployeeResponse = response.body();
                    assert fetchEmployeeResponse != null;
                    if (fetchEmployeeResponse.isStatus()) {
                        auditListener.setEmpName(fetchEmployeeResponse.getEmployeeDetails());
                    } else {

                        auditListener.showMessage(fetchEmployeeResponse.getMessage());
                    }
                } else {
                    auditListener.showMessage(response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<SearchEmpResponse> call, @NonNull Throwable t) {
                auditListener.showMessage(t.getMessage());
            }
        });

    }

    @Override
    public void startAudit(String token,String empId, String storeCode, String latLon) {
        //Log.e("lat_long ----",latLon);
        auditEndPoint.startAudit(token,empId, storeCode, latLon).enqueue(new Callback<StartAuditResponse>() {
            @Override
            public void onResponse(@NonNull Call<StartAuditResponse> call, @NonNull Response<StartAuditResponse> response) {
                if (response.isSuccessful()) {
                    StartAuditResponse startAuditResponse = response.body();
                    assert startAuditResponse != null;
                    if (startAuditResponse.isStatus()) {
                        auditListener.submitStore(startAuditResponse);

                    } else {
                        auditListener.showMessage(startAuditResponse.getMessage());
                    }
                } else {
                    auditListener.showMessage(response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<StartAuditResponse> call, @NonNull Throwable t) {
                auditListener.showMessage(t.getMessage());
            }
        });


    }

    @Override
    public void submitAuditReport(String token,String auditId, String empId, String storeCode, String empDetail, String proDetail) {
        //AuditEndPoint apiCallable = SangeethaCareApiClient.getClient().create(AuditEndPoint.class);
        auditEndPoint.submitAudit(token,auditId, empId, storeCode, empDetail, proDetail).enqueue(new Callback<AuditReportResponse>() {
            @Override
            public void onResponse(@NonNull Call<AuditReportResponse> call, @NonNull Response<AuditReportResponse> response) {
                if (response.isSuccessful()) {
                    AuditReportResponse auditReportResponse = response.body();
                    assert auditReportResponse != null;
                    if (auditReportResponse.isStatus()) {
                        assert response.body() != null;
                        auditListener.auditReportStatus(response.body().isStatus());
                    } else {
                        assert response.body() != null;
                        auditListener.showMessage(response.body().getMessage());
                    }

                } else {
                    auditListener.showMessage(response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<AuditReportResponse> call, @NonNull Throwable t) {
                auditListener.showMessage(t.getMessage());
            }
        });

    }

    @Override
    public void getDesignation(String token) {
        auditEndPoint.getDesignation(token).enqueue(new Callback<DesignationResponse>() {
            @Override
            public void onResponse(@NonNull Call<DesignationResponse> call, @NonNull Response<DesignationResponse> response) {
                if (response.isSuccessful()) {

                    assert response.body() != null;
                    if (response.body().isStatus()) {
                        auditListener.setDesignation(response.body().getDesignations());
                    } else {
                        auditListener.showMessage(response.body().getMessage());
                    }
                } else {
                    auditListener.showMessage(response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<DesignationResponse> call, @NonNull Throwable t) {
                auditListener.showMessage(t.getMessage());
            }
        });

    }

    @Override
    public void getEmpCategory(String token) {
        auditEndPoint.getEmpCategory(token).enqueue(new Callback<EmpCategoryResponse>() {
            @Override
            public void onResponse(@NonNull Call<EmpCategoryResponse> call, @NonNull Response<EmpCategoryResponse> response) {
                if (response.isSuccessful()) {
                    auditListener.setEmpCategory(response.body());
                } else {
                    assert response.body() != null;
                    auditListener.showMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(@NonNull Call<EmpCategoryResponse> call, @NonNull Throwable t) {
                auditListener.showMessage(t.getMessage());
            }
        });

    }

    @Override
    public void getSubCategory(String token,String categoryId) {
        auditEndPoint.getSubCategory(token,categoryId).enqueue(new Callback<SubCategoryResponse>() {
            @Override
            public void onResponse(@NonNull Call<SubCategoryResponse> call, @NonNull Response<SubCategoryResponse> response) {
                if (response.isSuccessful()) {
                    auditListener.setSubCategory(response.body());
                } else {
                    auditListener.showMessage(response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<SubCategoryResponse> call, @NonNull Throwable t) {
                auditListener.showMessage(t.getMessage());
            }
        });

    }
}
