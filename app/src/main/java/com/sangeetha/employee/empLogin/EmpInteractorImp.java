package com.sangeetha.employee.empLogin;

import com.google.gson.Gson;
import com.sangeetha.employee.CommonEndPoint;
import com.sangeetha.employee.RetrofitServiceGenerator;
import com.sangeetha.employee.empLogin.loginModel.EmpData;
import com.sangeetha.employee.utils.AppConstants;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmpInteractorImp implements EmpInteractor {

    private EmpInteractor.LoginProgressListener loginProgressListener;

    public EmpInteractorImp(LoginProgressListener loginProgressListener) {
        this.loginProgressListener = loginProgressListener;
    }

    @Override
    public void login(String empId, String pass, String IMEI) {
        CommonEndPoint commonEndPoint = RetrofitServiceGenerator.getClient(AppConstants.zapStoreBackEndBaseUrl).create(CommonEndPoint.class);
        commonEndPoint.empLogin(empId, pass, IMEI).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        assert response.body() != null;
                        String res = response.body().string();
                        JSONObject responseJson = new JSONObject(res);
                        if (responseJson.optBoolean("status")) {
                            Gson gson = new Gson();
//                            if (responseJson.getString("group").equalsIgnoreCase("store")) {
//                                StoreData storeData = gson.fromJson(responseJson.getString("data"), StoreData.class);
//                                loginProgressListener.setStoreData(storeData);
//
//                            } else if (responseJson.getString("group").equalsIgnoreCase("asm")) {
//                                ASMData asmData = gson.fromJson(responseJson.getString("data"), ASMData.class);
//                                loginProgressListener.setASMData(asmData);
//
//                            } else if (responseJson.getString("group").equalsIgnoreCase("admin")) {
                            EmpData empData = gson.fromJson(responseJson.getString("data"), EmpData.class);
                            loginProgressListener.setToken(responseJson.getString("token"));
                            loginProgressListener.setCallRecordingEnable(responseJson.getBoolean("permission"));
                            loginProgressListener.loginSuccesses(empData);
//                            }
//
//                            else if (responseJson.getString("group").equalsIgnoreCase("delivery")) {
//                                OnlineDeliveryData onlineDeliveryData = gson.fromJson(responseJson.getString("data"), OnlineDeliveryData.class);
//                                loginProgressListener.setOnlineDeliveryData(onlineDeliveryData);
//                            }
                        } else {
                            loginProgressListener.showMessage(responseJson.optString("message"));
                        }

                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    loginProgressListener.showMessage(response.message());
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                loginProgressListener.showMessage(t.getMessage());
            }
        });

    }

    @Override
    public void delLogin(String id, String pass) {
        CommonEndPoint commonEndPoint = RetrofitServiceGenerator.getDeliveryClient(AppConstants.adminBackEndBaseUrl).create(CommonEndPoint.class);
        commonEndPoint.delLogin(id, pass).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        assert response.body() != null;
                        String res = response.body().string();
                        JSONObject responseJson = new JSONObject(res);
                        if (responseJson.optBoolean("status")) {
                            Gson gson = new Gson();
//                            if (responseJson.getString("group").equalsIgnoreCase("store")) {
//                                StoreData storeData = gson.fromJson(responseJson.getString("data"), StoreData.class);
//                                loginProgressListener.setStoreData(storeData);
//
//                            } else if (responseJson.getString("group").equalsIgnoreCase("asm")) {
//                                ASMData asmData = gson.fromJson(responseJson.getString("data"), ASMData.class);
//                                loginProgressListener.setASMData(asmData);
//
//                            } else if (responseJson.getString("group").equalsIgnoreCase("admin")) {
                            EmpData empData = gson.fromJson(responseJson.getString("data"), EmpData.class);
                            empData.setGroup(responseJson.getString("group"));
                            loginProgressListener.setToken(responseJson.getString("token"));
                            loginProgressListener.delLogin(empData);
//                            }
//
//                            else if (responseJson.getString("group").equalsIgnoreCase("delivery")) {
//                                OnlineDeliveryData onlineDeliveryData = gson.fromJson(responseJson.getString("data"), OnlineDeliveryData.class);
//                                loginProgressListener.setOnlineDeliveryData(onlineDeliveryData);
//                            }
                        } else {
                            loginProgressListener.showMessage(responseJson.optString("message"));
                        }

                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    loginProgressListener.showMessage(response.message());
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                loginProgressListener.showMessage(t.getMessage());
            }
        });
    }
}
