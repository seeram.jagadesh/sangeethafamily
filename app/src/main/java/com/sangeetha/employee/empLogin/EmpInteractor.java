package com.sangeetha.employee.empLogin;


import com.sangeetha.employee.empLogin.loginModel.ASMData;
import com.sangeetha.employee.empLogin.loginModel.EmpData;
import com.sangeetha.employee.empLogin.loginModel.OnlineDeliveryData;
import com.sangeetha.employee.empLogin.loginModel.StoreData;

public interface EmpInteractor {


    void login(String empId, String pass, String IMEI);

    void delLogin(String id,String pass);

    interface LoginProgressListener {
        void showMessage(String message);

        void setToken(String token);

        void setCallRecordingEnable(Boolean isEnable);

        void loginSuccesses(EmpData data);

        void delLogin(EmpData data);

        void setStoreData(StoreData storeData);

        void setASMData(ASMData asmData);

        void setOnlineDeliveryData(OnlineDeliveryData onlineDeliveryData);

    }
}
