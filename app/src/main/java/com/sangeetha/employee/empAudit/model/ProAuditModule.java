package com.sangeetha.employee.empAudit.model;

import java.util.ArrayList;

public class ProAuditModule {

    private String empId;
    private String empName;
    private ArrayList<String> empNumbers = new ArrayList<>();
    private String designation;
    private String brand;
    private String confirm;
    private String empType;

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public ArrayList<String> getEmpNumbers() {
        return empNumbers;
    }

    public void setEmpNumbers(ArrayList<String> empNumbers) {
        this.empNumbers = empNumbers;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getConfirm() {
        return confirm;
    }

    public void setConfirm(String confirm) {
        this.confirm = confirm;
    }

    public String getEmpType() {
        return empType;
    }

    public void setEmpType(String empType) {
        this.empType = empType;
    }
}
