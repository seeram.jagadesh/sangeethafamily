package com.sangeetha.employee.managerDiscount.storeASM.asmPojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class DataItem implements Parcelable {

    public static final Creator<DataItem> CREATOR = new Creator<DataItem>() {
        @Override
        public DataItem createFromParcel(Parcel in) {
            return new DataItem(in);
        }

        @Override
        public DataItem[] newArray(int size) {
            return new DataItem[size];
        }
    };
    @SerializedName("reference_id")
    private String referenceId;
    @SerializedName("discount_amount")
    private String discountAmount;
    @SerializedName("used_invoice_no")
    private String usedInvoiceNo;
    @SerializedName("discount_percentage")
    private String discountPercentage;
    @SerializedName("fromdate")
    private String fromdate;
    @SerializedName("invoice_date")
    private String invoiceDate;
    @SerializedName("model_price")
    private String modelPrice;
    @SerializedName("store_submit")
    private String storeSubmit;
    @SerializedName("discount_status")
    private String discountStatus;
    @SerializedName("todate")
    private String todate;
    @SerializedName("modified")
    private String modified;
    @SerializedName("id")
    private String id;
    @SerializedName("store_id")
    private String storeId;
    @SerializedName("approver")
    private String approver;
    @SerializedName("source_from")
    private String sourceFrom;
    @SerializedName("approval_code")
    private String approvalCode;
    @SerializedName("customer_mobile")
    private String customerMobile;
    @SerializedName("amount")
    private String amount;
    @SerializedName("coupon_code")
    private String couponCode;
    @SerializedName("customer_otp")
    private String customerOtp;
    @SerializedName("created")
    private String created;
    @SerializedName("model_id")
    private String modelId;
    @SerializedName("make_id")
    private String makeId;

    @SerializedName("customer_is_verified")
    private String customerIsVerified;
    @SerializedName("coupon_status")
    private String couponStatus;
    @SerializedName("total_amount")
    private String totalAmount;
    @SerializedName("manage_id")
    private String manageId;
    @SerializedName("internal_code")
    private String internalCode;
    @SerializedName("customer_email")
    private String customerEmail;
    @SerializedName("employee_id")
    private String employeeId;
    @SerializedName("customer_name")
    private String customerName;
    @SerializedName("approved_submit")
    private String approvedSubmit;
    @SerializedName("status")
    private String status;

    protected DataItem(Parcel in) {
        referenceId = in.readString();
        discountAmount = in.readString();
        usedInvoiceNo = in.readString();
        discountPercentage = in.readString();
        fromdate = in.readString();
        invoiceDate = in.readString();
        modelPrice = in.readString();
        storeSubmit = in.readString();
        discountStatus = in.readString();
        todate = in.readString();
        modified = in.readString();
        id = in.readString();
        storeId = in.readString();
        approver = in.readString();
        sourceFrom = in.readString();
        approvalCode = in.readString();
        customerMobile = in.readString();
        amount = in.readString();
        couponCode = in.readString();
        customerOtp = in.readString();
        created = in.readString();
        makeId = in.readString();
        modelId = in.readString();
        customerIsVerified = in.readString();
        couponStatus = in.readString();
        totalAmount = in.readString();
        manageId = in.readString();
        internalCode = in.readString();
        customerEmail = in.readString();
        employeeId = in.readString();
        customerName = in.readString();
        approvedSubmit = in.readString();
        status = in.readString();
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getUsedInvoiceNo() {
        return usedInvoiceNo;
    }

    public void setUsedInvoiceNo(String usedInvoiceNo) {
        this.usedInvoiceNo = usedInvoiceNo;
    }

    public String getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(String discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public String getFromdate() {
        return fromdate;
    }

    public void setFromdate(String fromdate) {
        this.fromdate = fromdate;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getModelPrice() {
        return modelPrice;
    }

    public void setModelPrice(String modelPrice) {
        this.modelPrice = modelPrice;
    }

    public String getStoreSubmit() {
        return storeSubmit;
    }

    public void setStoreSubmit(String storeSubmit) {
        this.storeSubmit = storeSubmit;
    }

    public String getDiscountStatus() {
        return discountStatus;
    }

    public void setDiscountStatus(String discountStatus) {
        this.discountStatus = discountStatus;
    }

    public String getTodate() {
        return todate;
    }

    public void setTodate(String todate) {
        this.todate = todate;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getApprover() {
        return approver;
    }

    public void setApprover(String approver) {
        this.approver = approver;
    }

    public String getSourceFrom() {
        return sourceFrom;
    }

    public void setSourceFrom(String sourceFrom) {
        this.sourceFrom = sourceFrom;
    }

    public String getApprovalCode() {
        return approvalCode;
    }

    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }

    public String getCustomerMobile() {
        return customerMobile;
    }

    public void setCustomerMobile(String customerMobile) {
        this.customerMobile = customerMobile;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getCustomerOtp() {
        return customerOtp;
    }

    public void setCustomerOtp(String customerOtp) {
        this.customerOtp = customerOtp;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getCustomerIsVerified() {
        return customerIsVerified;
    }

    public void setCustomerIsVerified(String customerIsVerified) {
        this.customerIsVerified = customerIsVerified;
    }

    public String getCouponStatus() {
        return couponStatus;
    }

    public void setCouponStatus(String couponStatus) {
        this.couponStatus = couponStatus;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getManageId() {
        return manageId;
    }

    public void setManageId(String manageId) {
        this.manageId = manageId;
    }

    public String getInternalCode() {
        return internalCode;
    }

    public void setInternalCode(String internalCode) {
        this.internalCode = internalCode;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getApprovedSubmit() {
        return approvedSubmit;
    }

    public void setApprovedSubmit(String approvedSubmit) {
        this.approvedSubmit = approvedSubmit;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMakeId() {
        return makeId;
    }

    public void setMakeId(String makeId) {
        this.makeId = makeId;
    }

    @Override
    public String toString() {
        return
                "DataItem{" +
                        "reference_id = '" + referenceId + '\'' +
                        ",discount_amount = '" + discountAmount + '\'' +
                        ",used_invoice_no = '" + usedInvoiceNo + '\'' +
                        ",discount_percentage = '" + discountPercentage + '\'' +
                        ",fromdate = '" + fromdate + '\'' +
                        ",invoice_date = '" + invoiceDate + '\'' +
                        ",model_price = '" + modelPrice + '\'' +
                        ",store_submit = '" + storeSubmit + '\'' +
                        ",discount_status = '" + discountStatus + '\'' +
                        ",todate = '" + todate + '\'' +
                        ",modified = '" + modified + '\'' +
                        ",id = '" + id + '\'' +
                        ",store_id = '" + storeId + '\'' +
                        ",approver = '" + approver + '\'' +
                        ",source_from = '" + sourceFrom + '\'' +
                        ",approval_code = '" + approvalCode + '\'' +
                        ",customer_mobile = '" + customerMobile + '\'' +
                        ",amount = '" + amount + '\'' +
                        ",coupon_code = '" + couponCode + '\'' +
                        ",customer_otp = '" + customerOtp + '\'' +
                        ",created = '" + created + '\'' +
                        ",model_id = '" + modelId + '\'' +
                        ",customer_is_verified = '" + customerIsVerified + '\'' +
                        ",coupon_status = '" + couponStatus + '\'' +
                        ",total_amount = '" + totalAmount + '\'' +
                        ",manage_id = '" + manageId + '\'' +
                        ",internal_code = '" + internalCode + '\'' +
                        ",customer_email = '" + customerEmail + '\'' +
                        ",employee_id = '" + employeeId + '\'' +
                        ",customer_name = '" + customerName + '\'' +
                        ",approved_submit = '" + approvedSubmit + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(referenceId);
        dest.writeString(discountAmount);
        dest.writeString(usedInvoiceNo);
        dest.writeString(discountPercentage);
        dest.writeString(fromdate);
        dest.writeString(invoiceDate);
        dest.writeString(modelPrice);
        dest.writeString(storeSubmit);
        dest.writeString(discountStatus);
        dest.writeString(todate);
        dest.writeString(modified);
        dest.writeString(id);
        dest.writeString(storeId);
        dest.writeString(approver);
        dest.writeString(sourceFrom);
        dest.writeString(approvalCode);
        dest.writeString(customerMobile);
        dest.writeString(amount);
        dest.writeString(couponCode);
        dest.writeString(customerOtp);
        dest.writeString(created);
        dest.writeString(makeId);
        dest.writeString(modelId);
        dest.writeString(customerIsVerified);
        dest.writeString(couponStatus);
        dest.writeString(totalAmount);
        dest.writeString(manageId);
        dest.writeString(internalCode);
        dest.writeString(customerEmail);
        dest.writeString(employeeId);
        dest.writeString(customerName);
        dest.writeString(approvedSubmit);
        dest.writeString(status);
    }
}