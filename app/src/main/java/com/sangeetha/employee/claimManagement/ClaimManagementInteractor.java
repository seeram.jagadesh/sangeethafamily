package com.sangeetha.employee.claimManagement;

public interface ClaimManagementInteractor {

    void getClaimDetails(String claimId);

    void proceedClaim();

    void submitClaim();

    interface ClaimManagementListener {
        void showMessage(String msg);

        void setClaimDetails();

    }


}
