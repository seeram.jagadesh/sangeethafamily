package com.sangeetha.employee.empAudit;

public interface AuditPresenter {

    void getStoreList(String token);

    void getBrandList(String token);

    void getEmpStatus(String token);

    void getEmpDetail(String token,String empId);

    void startAudit(String token,String empId, String storeCode, String latLon);

    void submitAuditReport(String token,String auditId, String empId, String storeCode, String empDetail, String proDetail);

    void getDesignation(String token);

    void getEmpCategory(String token);

    void getSubCategory(String token,String categoryId);

}
