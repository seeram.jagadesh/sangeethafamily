package com.sangeetha.employee.registrationForm.model;


import com.google.gson.annotations.SerializedName;

import java.math.BigInteger;


public class Data {

    @SerializedName("imeinumber")
    public BigInteger imeinumber;

    @SerializedName("gender")
    public String gender;

    @SerializedName("surname")
    public String surname;

    @SerializedName("created")
    public String created;

    @SerializedName("customerEmail")
    public String customerEmail;

    @SerializedName("registrationId")
    public String registrationId;

    @SerializedName("invoiceNumber")
    public String invoiceNumber;

    @SerializedName("customerDOB")
    public String customerDOB;

    @SerializedName("customerNumber")
    public BigInteger customerNumber;

    @SerializedName("updated")
    public String updated;

    @SerializedName("customerAnniversaryDate")
    public String customerAnniversaryDate;

    @SerializedName("customerName")
    public String customerName;

    @SerializedName("make")
    public String make;

    @SerializedName("modelNo")
    public String model;

    @SerializedName("invoiceTotal")
    public String total;

    @SerializedName("password")
    public String password;

    @SerializedName("verification")
    public Boolean verification;

    public Boolean getVerification() {
        return verification;
    }

    public void setVerification(Boolean verification) {
        this.verification = verification;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }


    public BigInteger getImeinumber() {
        return imeinumber;
    }

    public void setImeinumber(BigInteger imeinumber) {
        this.imeinumber = imeinumber;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }


    public BigInteger getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(BigInteger customerNumber) {
        this.customerNumber = customerNumber;
    }


    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {

        this.updated = updated;
    }

    public String getCustomerDOB() {
        return customerDOB;
    }

    public void setCustomerDOB(String customerDOB) {
        this.customerDOB = customerDOB;
    }

    public String getCustomerAnniversaryDate() {
        return customerAnniversaryDate;
    }

    public void setCustomerAnniversaryDate(String customerAnniversaryDate) {
        this.customerAnniversaryDate = customerAnniversaryDate;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    @Override
    public String toString() {
        return
                "EmpData{" +
                        "imeinumber = '" + imeinumber + '\'' +
                        ",gender = '" + gender + '\'' +
                        ",surname = '" + surname + '\'' +
                        ",created = '" + created + '\'' +
                        ",customerEmail = '" + customerEmail + '\'' +
                        ",registrationId = '" + registrationId + '\'' +
                        ",invoiceNumber = '" + invoiceNumber + '\'' +
                        ",customerDOB = '" + customerDOB + '\'' +
                        ",customerNumber = '" + customerNumber + '\'' +
                        ",updated = '" + updated + '\'' +
                        ",customerAnniversaryDate = '" + customerAnniversaryDate + '\'' +
                        ",customerName = '" + customerName + '\'' +
                        "}";
    }
}