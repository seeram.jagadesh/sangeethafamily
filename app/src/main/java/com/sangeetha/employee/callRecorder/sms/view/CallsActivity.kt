package com.sangeetha.employee.callRecorder.sms.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.sangeetha.employee.callRecorder.extensions.show
import com.sangeetha.employee.callRecorder.sms.adapters.MessagesAdapter
import com.sangeetha.employee.callRecorder.sms.viewmodel.SMSViewModel
import com.sangeetha.employee.databinding.ActivityCallsBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class CallsActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModel: SMSViewModel

    private lateinit var binding: ActivityCallsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCallsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setUpObservers()
    }

    private fun setUpObservers() {
        val messagesAdapter = MessagesAdapter {
        }.apply {
            addLoadStateListener {
                if (it.append is LoadState.NotLoading && it.append.endOfPaginationReached &&
                    itemCount < 1
                ) {
                    binding.textNoMessages.show()
                }
            }
        }

        binding.recyclerviewMessages.apply {
            layoutManager =
                LinearLayoutManager(this@CallsActivity, LinearLayoutManager.VERTICAL, false)
            adapter = messagesAdapter
        }

        viewModel.messagesLiveData.observe(this) {
            lifecycleScope.launch { messagesAdapter.submitData(it) }
        }
    }
}