package com.sangeetha.employee.calendarView

import androidx.annotation.ColorInt

interface TextColorPicker {

    @ColorInt
    fun getTextColor(event: WeekViewEvent): Int

}
