package com.sangeetha.employee.callRecorder.domain

import android.content.SharedPreferences
import androidx.core.content.edit

class SharedPreferencesSmsStorage constructor(private val sharedPreferences: SharedPreferences) {

    companion object {

        const val CHECK_IN_VALUE = 1
        const val CHECK_OUT_VALUE = 2

        private const val LAST_SMS_PARSED = "last_sms_parsed"
        private const val DEFAULT_SMS_PARSED_VALUE = -1

        private const val APP_TOKEN = "app_token"

        private const val SMS_TRACKING = "sms_tracking"
        private const val LOCATION_TRACKING = "location_tracking"
        private const val CALL_TRACKING = "call_tracking"
        private const val USER_INFO = "user_info"
        private const val CHECK_IN = "check_in"
        private const val INTERVAL_TIME = "interval_time"
        private const val IS_CALL_RECORDING_ENABLE = "is_call_recording_enable"
    }

    fun updateLastSmsIntercepted(smsId: Int) {
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.putInt(LAST_SMS_PARSED, smsId)
        editor.apply()
    }

    fun getLastSmsIntercepted(): Int {
        return sharedPreferences.getInt(
            LAST_SMS_PARSED,
            DEFAULT_SMS_PARSED_VALUE
        )
    }

    fun isFirstSmsIntercepted(): Boolean {
        return getLastSmsIntercepted() == DEFAULT_SMS_PARSED_VALUE
    }

    fun setToken(token: String) {
        sharedPreferences.edit(commit = true) {
            putString(APP_TOKEN, token)
        }
    }

    fun getToken(): String? {
        return sharedPreferences.getString(APP_TOKEN, null)
    }

    fun setCallRecordingFeatureEnable(isEnable: Boolean) {
        sharedPreferences.edit(commit = true) {
            putBoolean(IS_CALL_RECORDING_ENABLE, isEnable)
        }
    }

    fun isCallRecordingFeatureEnable(): Boolean {
        return sharedPreferences.getBoolean(IS_CALL_RECORDING_ENABLE, true)
    }

    fun setIntervalTime(time: Int) {
        sharedPreferences.edit(commit = true) {
            putInt(INTERVAL_TIME, time)
        }
    }

    fun getIntervalTime(): Int {
        return sharedPreferences.getInt(INTERVAL_TIME, 10000)
    }

    fun setUserInfo(userInfo: String) {
        sharedPreferences.edit(commit = true) {
            putString(USER_INFO, userInfo)
        }
    }

    fun getUserInfo(): String? {
        return sharedPreferences.getString(USER_INFO, null)
    }

    fun setSmsTrackingEnable(isEnable: Boolean) {
        sharedPreferences.edit(commit = true) {
            putBoolean(SMS_TRACKING, isEnable)
        }
    }

    fun isSmsTrackingEnable(): Boolean {
        return sharedPreferences.getBoolean(SMS_TRACKING, true)
    }

    fun setCallTrackingEnable(isEnable: Boolean) {
        sharedPreferences.edit(commit = true) {
            putBoolean(CALL_TRACKING, isEnable)
        }
    }

    fun isCallTrackingEnable(): Boolean {
        return sharedPreferences.getBoolean(CALL_TRACKING, true)
    }

    fun setLocationTrackingEnable(isEnable: Boolean) {
        sharedPreferences.edit(commit = true) {
            putBoolean(LOCATION_TRACKING, isEnable)
        }
    }

    fun isLocationTrackingEnable(): Boolean {
        return sharedPreferences.getBoolean(LOCATION_TRACKING, true)
    }

    fun setCheckIn(checkInValue: Int) {
        sharedPreferences.edit(commit = true) {
            putInt(CHECK_IN, checkInValue)
        }
    }

    fun isCheckIn(): Int {
        return sharedPreferences.getInt(CHECK_IN, CHECK_IN_VALUE)
    }

    fun clear() {
        sharedPreferences.edit {
            clear()
        }
    }
}