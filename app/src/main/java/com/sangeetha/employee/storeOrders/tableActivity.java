package com.sangeetha.employee.storeOrders;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;

import com.sangeetha.employee.R;
import com.sangeetha.employee.SangeethaCareApiClient;
import com.sangeetha.employee.employeeHome.EmployeeLanding;
import com.sangeetha.employee.mining.ApiCallable;
import com.sangeetha.employee.utils.AppPreference;
import com.sangeetha.employee.utils.HorizontalScrollViewListener;
import com.sangeetha.employee.utils.ObservableHorizontalScrollView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class tableActivity extends AppCompatActivity implements HorizontalScrollViewListener {

    private TableLayout frozenHeaderTable;
    private TableLayout contentHeaderTable;
    private TableLayout frozenTable;
    private TableLayout contentTable;

    AppPreference appPreference;
    Typeface font;
    float fontSize;
    int cellWidthFactor;
    ArrayList<String> stores = new ArrayList<>();
    JSONObject resultJSON;

    ObservableHorizontalScrollView headerScrollView;
    ObservableHorizontalScrollView contentScrollView;

    JSONObject store;
    JSONArray storeArray;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(tableActivity.this, EmployeeLanding.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table);

//        font = Typeface.createFromAsset(getAssets(), "fonts/consola.ttf");

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        Objects.requireNonNull(getSupportActionBar()).setTitle("Store Orders");


        fontSize = 15; // Actually this is dynamic in my application, but that code is removed for clarity
        final float scale = getBaseContext().getResources().getDisplayMetrics().density;
        cellWidthFactor = (int) Math.ceil(fontSize * scale * (fontSize < 12 ? 0.5 : 0.7));

        appPreference = new AppPreference(this);

        frozenTable = (TableLayout) findViewById(R.id.frozenTable);
        contentTable = (TableLayout) findViewById(R.id.contentTable);
        frozenHeaderTable = (TableLayout) findViewById(R.id.frozenTableHeader);
        contentHeaderTable = (TableLayout) findViewById(R.id.contentTableHeader);
        headerScrollView = (ObservableHorizontalScrollView) findViewById(R.id.contentTableHeaderHorizontalScrollView);
        headerScrollView.setScrollViewListener(this);
        contentScrollView = (ObservableHorizontalScrollView) findViewById(R.id.contentTableHorizontalScrollView);
        contentScrollView.setScrollViewListener(this);
        contentScrollView.setHorizontalScrollBarEnabled(false); // Only show the scroll bar on the header table (so that there aren't two)

        if (getIntent().hasExtra("JSON")) {
            try {
                resultJSON = new JSONObject(getIntent().getStringExtra("JSON"));
//                resultJSON = new JSONObject(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        getStatusType();


//        InitializeInitialData();
    }

    private void getStatusType() {
        ApiCallable apiCallable = SangeethaCareApiClient.getClient().create(ApiCallable.class);
        apiCallable.getOrderStatusType(appPreference.getUserToken()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    JSONObject data = null;
//                    ResponseBody responseBody=response.body();
                    String responseBody = null;
                    try {
                        responseBody = response.body().string();
                        data = new JSONObject(responseBody);
                        boolean status = data.getBoolean("status");
                        JSONArray array = data.getJSONArray("reason_list");

                        String myData;
                        ArrayList<String> datas = new ArrayList<>();
                        JSONArray dataArray = new JSONArray();
                        for (int g = 0; g < array.length(); g++) {
                            JSONObject st = array.getJSONObject(g);
                            Iterator keys = st.keys();
                            datas.clear();

                            while (keys.hasNext()) {
                                String storeKey = (String) keys.next();
                                myData = st.getString(storeKey);
                                datas.add(storeKey);
                                dataArray.put(myData);
                            }
                        }
                        if (status) {
                            if (resultJSON != null) {
                                try {
                                    parseStoreDetails(resultJSON, dataArray);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(tableActivity.this, "Error while processing request.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(tableActivity.this, "Failed to process your request", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void parseStoreDetails(JSONObject storeDetails, JSONArray date) throws JSONException {
        JSONObject jsonStore = new JSONObject(storeDetails.toString());

        Iterator keys = jsonStore.keys();

        stores.clear();
        storeArray = new JSONArray();

        while (keys.hasNext()) {
            String storeKey = (String) keys.next();
            store = jsonStore.getJSONObject(storeKey);
            stores.add(storeKey);
            storeArray.put(store);
        }

        PopulateMainTable(stores, storeArray, date);
    }


    protected void PopulateMainTable(ArrayList<String> content, JSONArray date, JSONArray listArray) throws JSONException {


        for (int i = -1; i < content.size(); i++) {
            TableRow frozenRow = new TableRow(this);

            View header = getLayoutInflater().inflate(R.layout.item_store, null);


            if (i == -1) {
                View headerRoot = getLayoutInflater().inflate(R.layout.header_item_store, null);
                AppCompatTextView storeHead = headerRoot.findViewById(R.id.txtFilterStore);

                storeHead.setWidth(290);
                storeHead.setMaxWidth(320);
                storeHead.setGravity(Gravity.CENTER);

                frozenRow.addView(headerRoot);

            } else {
                AppCompatTextView txtFilterStore = header.findViewById(R.id.txtFilterStore);
                txtFilterStore.setText(content.get(i));
                txtFilterStore.setGravity(Gravity.CENTER);
                txtFilterStore.setHeight(100);
                frozenRow.addView(header);
            }

            // The rest of them
            TableRow row = new TableRow(this);
            for (int j = 0; j < listArray.length(); j++) {


                JSONObject myObject = null;
                if (i == -1) {

                    View v = getLayoutInflater().inflate(R.layout.store_recycler, null);
                    AppCompatTextView txtDate = v.findViewById(R.id.txtDate);
                    txtDate.setText(listArray.getString(j));
                    txtDate.setTypeface(txtDate.getTypeface(), Typeface.BOLD);
                    row.addView(v);
                } else {
                    View itemView = getLayoutInflater().inflate(R.layout.item_data_total, null);

                    if (storeArray.getJSONObject(i).has(listArray.getString(j).toLowerCase())) {
                        String count = storeArray.getJSONObject(i).getString(listArray.getString(j).toLowerCase());
                        AppCompatTextView store = itemView.findViewById(R.id.filterStore);
                        store.setHeight(100);
                        store.setText(count);
                        if (Integer.parseInt(count) > 0) {
                            store.setTextColor(getResources().getColor(R.color.colorPrimaryApp));
                            store.setTypeface(store.getTypeface(), Typeface.BOLD);
                            int finalI = i;
                            int finalJ = j;
                            store.setOnClickListener(v -> {
                                try {
                                    startActivity(new Intent(tableActivity.this, StoreOrdersActivity.class)
                                            .putExtra("status", listArray.getString(finalJ).toLowerCase())
                                            .putExtra("store_code", content.get(finalI))
                                            .putExtra("JSON", resultJSON.toString()));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            });
                        }
                    }


                    row.addView(itemView);
                }

            }

            if (i == -1) {
                frozenHeaderTable.addView(frozenRow);
                contentHeaderTable.addView(row);
            } else {
                frozenTable.addView(frozenRow);
                contentTable.addView(row);
            }
        }


    }


    public void onScrollChanged(ObservableHorizontalScrollView scrollView, int x, int y, int oldX, int oldY) {
        if (scrollView == headerScrollView) {
            contentScrollView.scrollTo(x, y);
        } else if (scrollView == contentScrollView) {
            headerScrollView.scrollTo(x, y);
        }
    }
}