package com.sangeetha.employee.managerDiscount.storeManager;

import androidx.annotation.NonNull;

import com.sangeetha.employee.CommonResponse;
import com.sangeetha.employee.RetrofitServiceGenerator;
import com.sangeetha.employee.managerDiscount.couponCode.CheckCouponCode;
import com.sangeetha.employee.managerDiscount.manager_pojo.AlternativeMethodResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.CouponHistoryResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.DiscountDetailsResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.FindEmployeeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.MakeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelDiscountResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelPriceResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.PaymentModeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ProductTypeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.SubmitDiscountResponse;
import com.sangeetha.employee.utils.AppConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MangerDiscountInteractorImp implements ManagerDiscountInteractor {

    private ManagerDiscountListener managerDiscountListener;
    private ManagerDiscountEndPoint managerDiscountEndPoint;


    MangerDiscountInteractorImp(ManagerDiscountListener managerDiscountListener) {
        this.managerDiscountListener = managerDiscountListener;
        managerDiscountEndPoint = RetrofitServiceGenerator.getClient(AppConstants.zapStoreBackEndBaseUrl).create(ManagerDiscountEndPoint.class);

    }

    @Override
    public void getMake(String userToken,String productId) {
        managerDiscountEndPoint.getMakeDetail(userToken,productId).enqueue(new Callback<MakeResponse>() {
            @Override
            public void onResponse(@NonNull Call<MakeResponse> call, @NonNull Response<MakeResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().isStatus()) {
                        MakeResponse managerResponse = response.body();
                        managerDiscountListener.setMake(managerResponse);

                    } else {
                        managerDiscountListener.showMessage(response.body().getMessage());
                    }
                } else {
                    managerDiscountListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(@NonNull Call<MakeResponse> call, @NonNull Throwable t) {
                managerDiscountListener.showMessage(t.getMessage());
            }
        });

    }

    @Override
    public void getModule(String userToken,String productId,String makeId) {
        managerDiscountEndPoint.getModel(userToken, productId, makeId).enqueue(new Callback<ModelResponse>() {
            @Override
            public void onResponse(@NonNull Call<ModelResponse> call, @NonNull Response<ModelResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().isStatus()) {
                        ModelResponse modelResponse = response.body();
                        managerDiscountListener.setModel(modelResponse);
                    } else {
                        managerDiscountListener.showMessage(response.body().getMessage());
                    }
                } else {
                    managerDiscountListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(@NonNull Call<ModelResponse> call, Throwable t) {
                managerDiscountListener.showMessage(t.getMessage());
            }
        });

    }

    @Override
    public void getProductType(String token, String paymentId) {
        managerDiscountEndPoint.getProductTypesList(token).enqueue(new Callback<ProductTypeResponse>() {
            @Override
            public void onResponse(@NonNull Call<ProductTypeResponse> call, @NonNull Response<ProductTypeResponse> response) {
                if (response.isSuccessful()) {
                    managerDiscountListener.setProductTypes(response.body());
                } else {
                    managerDiscountListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(@NonNull Call<ProductTypeResponse> call, @NonNull Throwable t) {
                managerDiscountListener.showMessage(t.getMessage());
            }
        });

    }

    @Override
    public void generateOTP(String token,String mobileNumber) {
        managerDiscountEndPoint.generateOtp(token,mobileNumber).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String res = response.body().string();
                        JSONObject responseJson = new JSONObject(res);
                        managerDiscountListener.setCustomerVerification(responseJson.getBoolean("status"), responseJson.optString("message"), 1, responseJson.optString("type"));
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    managerDiscountListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                managerDiscountListener.showMessage(t.getMessage());
            }
        });

    }

    @Override
    public void validateOTP(String token,String mobileNumber, String otp) {

        managerDiscountEndPoint.verifyOTP(token,mobileNumber, otp).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String res = response.body().string();
                        JSONObject responseJson = new JSONObject(res);
                        if (responseJson.getBoolean("status")) {
                            managerDiscountListener.setOtpVerification(responseJson.getBoolean("status"));
                            managerDiscountListener.showMessage(responseJson.optString("message"));
                        } else {
                            managerDiscountListener.showMessage(responseJson.optString("message"));
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    managerDiscountListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, Throwable t) {
                managerDiscountListener.showMessage(t.getMessage());
            }
        });

    }

    @Override
    public void resendOtp(String token,String mobileNumber) {
        managerDiscountEndPoint.resendOtp(token,mobileNumber).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String res = response.body().string();
                        JSONObject responseJson = new JSONObject(res);
                        managerDiscountListener.setCustomerVerification(responseJson.getBoolean("status"), responseJson.optString("message"), 2, responseJson.optString("type"));
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    managerDiscountListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                managerDiscountListener.showMessage(t.getMessage());
            }
        });

    }

    @Override
    public void getProductPrice(String token, String productType, String brandId
            , String modelId, String paymentMode) {
        managerDiscountEndPoint.getModelPriceDetail(token, productType, brandId, modelId, paymentMode).enqueue(new Callback<ModelPriceResponse>() {
            @Override
            public void onResponse(@NonNull Call<ModelPriceResponse> call, @NonNull Response<ModelPriceResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().isStatus()) {
                        ModelPriceResponse managerResponse = response.body();
                        managerDiscountListener.setProductPrice(managerResponse);
                    } else {
                        managerDiscountListener.showMessage(response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ModelPriceResponse> call, @NonNull Throwable t) {
                managerDiscountListener.showMessage(t.getMessage());
            }
        });

    }

    @Override
    public void checkModelDiscount(String modelId, String storeId) {
        managerDiscountEndPoint.checkModelDiscount(modelId, storeId).enqueue(new Callback<ModelDiscountResponse>() {
            @Override
            public void onResponse(@NonNull Call<ModelDiscountResponse> call, @NonNull Response<ModelDiscountResponse> response) {
                if (response.isSuccessful()) {
                    ModelDiscountResponse modelDiscountResponse = response.body();
                    managerDiscountListener.setDiscountMessage(modelDiscountResponse);

                } else {
                    managerDiscountListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(@NonNull Call<ModelDiscountResponse> call, @NonNull Throwable t) {
                managerDiscountListener.showMessage(t.getMessage());
            }
        });

    }

    @Override
    public void submitDiscount() {

    }

    @Override
    public void getApproval() {

    }

    @Override
    public void findEmployeeName(String token,String empId) {
        managerDiscountEndPoint.findEmployeeName(token,empId).enqueue(new Callback<FindEmployeeResponse>() {
            @Override
            public void onResponse(@NonNull Call<FindEmployeeResponse> call, @NonNull Response<FindEmployeeResponse> response) {
                if (response.isSuccessful()) {
                    managerDiscountListener.setEmployeeName(response.body());
                } else {
                    managerDiscountListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(@NonNull Call<FindEmployeeResponse> call, @NonNull Throwable t) {
                managerDiscountListener.showMessage(t.getMessage());
            }
        });

    }

    @Override
    public void sendRequest(String managerId, String storeId, String employeeId, String modelId, String discountPer, String discountAmount, String totalAmount, String customerMobile, boolean mobileVerified, String emil, String customerName, String customerOtp, String approver) {
        managerDiscountEndPoint.sendRequest(managerId, storeId, employeeId, modelId, discountPer, discountAmount, totalAmount, customerMobile, mobileVerified, emil, customerName, customerOtp, approver).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String res = response.body().string();
                        JSONObject responseJson = new JSONObject(res);
                        if (responseJson.getBoolean("status")) {
                            managerDiscountListener.setApprovalRequest(responseJson.getBoolean("status"), responseJson.getString("reference_id"));
                        } else {
                            managerDiscountListener.setApprovalRequest(responseJson.getBoolean("status"), responseJson.optString("message"));
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    managerDiscountListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                managerDiscountListener.showMessage(t.getMessage());
            }
        });
    }

    @Override
    public void sendPaymentRequest(String token,String managerId,  String employeeId, String modelId, String discountPer,
                                    String totalAmount, String customerMobile,
                                   boolean mobileVerified, String emil, String customerName, String customerOtp,
                                   String approver, String paymentID,
                                   String productType,
                                   String makeId) {
        managerDiscountEndPoint.sendPaymentRequest(token,managerId, employeeId, modelId, discountPer,
                totalAmount, customerMobile, mobileVerified, emil, customerName, customerOtp, approver, paymentID,productType,makeId).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String res = response.body().string();
                        JSONObject responseJson = new JSONObject(res);
                        if (responseJson.getBoolean("status")) {
                            managerDiscountListener.setApprovalRequest(responseJson.getBoolean("status"), responseJson.getString("reference_id"));
                        } else {
                            managerDiscountListener.setApprovalRequest(responseJson.getBoolean("status"), responseJson.optString("message"));
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    managerDiscountListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                managerDiscountListener.showMessage(t.getMessage());
            }
        });
    }

    @Override
    public void alternativeMethod(String token,String approvalCode, String sourceFrom) {
        managerDiscountEndPoint.alternativeMethod(token,approvalCode, sourceFrom).enqueue(new Callback<AlternativeMethodResponse>() {
            @Override
            public void onResponse(@NonNull Call<AlternativeMethodResponse> call, @NonNull Response<AlternativeMethodResponse> response) {
                if (response.isSuccessful()) {
                    managerDiscountListener.setAlternativeMethod(response.body());
                } else {
                    managerDiscountListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(@NonNull Call<AlternativeMethodResponse> call, @NonNull Throwable t) {
                managerDiscountListener.showMessage(t.getMessage());
            }
        });

    }

    @Override
    public void couponHistory(String storeId) {
        managerDiscountEndPoint.getCouponHistory(storeId).enqueue(new Callback<CouponHistoryResponse>() {
            @Override
            public void onResponse(@NonNull Call<CouponHistoryResponse> call, @NonNull Response<CouponHistoryResponse> response) {
                if (response.isSuccessful()) {
                    managerDiscountListener.setCouponHistory(response.body());
                } else {
                    managerDiscountListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(@NonNull Call<CouponHistoryResponse> call, @NonNull Throwable t) {
                managerDiscountListener.showMessage(t.getMessage());
            }
        });

    }

    @Override
    public void discountDetails(String token,String refId) {
        managerDiscountEndPoint.getDiscountDetails(token,refId).enqueue(new Callback<DiscountDetailsResponse>() {
            @Override
            public void onResponse(@NonNull Call<DiscountDetailsResponse> call, @NonNull Response<DiscountDetailsResponse> response) {
                if (response.isSuccessful()) {
                    managerDiscountListener.setDiscountDetails(response.body());
                } else {
                    managerDiscountListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(@NonNull Call<DiscountDetailsResponse> call, @NonNull Throwable t) {
                managerDiscountListener.showMessage(t.getMessage());
            }
        });

    }
    @Override
    public void discountInfo(String token,String refId) {
        managerDiscountEndPoint.getDiscountInfo(token,refId).enqueue(new Callback<DiscountDetailsResponse>() {
            @Override
            public void onResponse(@NonNull Call<DiscountDetailsResponse> call, @NonNull Response<DiscountDetailsResponse> response) {
                if (response.isSuccessful()) {
                    managerDiscountListener.setDiscountInfo(response.body());
                } else {
                    managerDiscountListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(@NonNull Call<DiscountDetailsResponse> call, @NonNull Throwable t) {
                managerDiscountListener.showMessage(t.getMessage());
            }
        });

    }

    @Override
    public void cancelCoupon(String refId) {
        managerDiscountEndPoint.cancelCoupon(refId).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String res = response.body().string();
                        JSONObject responseJson = new JSONObject(res);
                        managerDiscountListener.couponCancel(responseJson.optBoolean("status"), responseJson.optString("message"));
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    managerDiscountListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

    }

    @Override
    public void percentageBaseDiscount(String token,
                                       String productType,  String brandId,
                                       String modelId, String paymentMode,
                                       String manageId, String discountId) {
        managerDiscountEndPoint.percentageBaseDiscount(token, productType, brandId, modelId, paymentMode, manageId, discountId).enqueue(new Callback<SubmitDiscountResponse>() {
            @Override
            public void onResponse(@NonNull Call<SubmitDiscountResponse> call, @NonNull Response<SubmitDiscountResponse> response) {
                if (response.isSuccessful()) {
                    managerDiscountListener.setPercentageBasedDiscount(response.body());
                } else {
                    managerDiscountListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(@NonNull Call<SubmitDiscountResponse> call, @NonNull Throwable t) {
                managerDiscountListener.showMessage(t.getMessage());
            }
        });

    }

    @Override
    public void amountBasedDiscount(String storeId, String modelId, String manageId, String discountValue, String totalValue) {
        managerDiscountEndPoint.amountBasedDiscount(storeId, modelId, manageId, discountValue, totalValue).enqueue(new Callback<SubmitDiscountResponse>() {
            @Override
            public void onResponse(@NonNull Call<SubmitDiscountResponse> call, @NonNull Response<SubmitDiscountResponse> response) {
                if (response.isSuccessful()) {
                    managerDiscountListener.setAmountBasedDiscount(response.body());
                } else {
                    managerDiscountListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(@NonNull Call<SubmitDiscountResponse> call, @NonNull Throwable t) {
                managerDiscountListener.showMessage(t.getMessage());
            }
        });

    }

    @Override
    public void checkCouponCode(String regId) {
        managerDiscountEndPoint.checkCouponCode(regId).enqueue(new Callback<CheckCouponCode>() {
            @Override
            public void onResponse(@NonNull Call<CheckCouponCode> call, @NonNull Response<CheckCouponCode> response) {
                if (response.isSuccessful()) {
                    managerDiscountListener.setCheckCouponCode(response.body());
                } else {
                    managerDiscountListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(@NonNull Call<CheckCouponCode> call, Throwable t) {
                managerDiscountListener.showMessage(t.getMessage());
            }
        });

    }

    @Override
    public void couponCodeCancel(String regId) {
        managerDiscountEndPoint.couponCodeCancel(regId).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(@NonNull Call<CommonResponse> call, @NonNull Response<CommonResponse> response) {
                if (response.isSuccessful()) {
                    managerDiscountListener.setCouponCodeCancel(response.body());
                } else {
                    managerDiscountListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                managerDiscountListener.showMessage(t.getMessage());
            }
        });

    }

    @Override
    public void getPayments(String token) {
        managerDiscountEndPoint.getPaymentModeList(token).enqueue(new Callback<PaymentModeResponse>() {
            @Override
            public void onResponse(@NonNull Call<PaymentModeResponse> call, @NonNull Response<PaymentModeResponse> response) {
                if (response.isSuccessful()) {
                    managerDiscountListener.setPaymentModes(response.body());
                } else {
                    managerDiscountListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(@NonNull Call<PaymentModeResponse> call, @NonNull Throwable t) {
                managerDiscountListener.showMessage(t.getMessage());
            }
        });

    }

    @Override
    public void checkModelPaymentDiscount(String token, String productType, String brandId
            , String modelId, String paymentMode) {
//checkModelPaymentDiscount
        managerDiscountEndPoint.checkModelPaymentDiscount(token, productType, brandId, modelId, paymentMode).enqueue(new Callback<ModelDiscountResponse>() {
            @Override
            public void onResponse(@NonNull Call<ModelDiscountResponse> call, @NonNull Response<ModelDiscountResponse> response) {
                if (response.isSuccessful()) {
                    ModelDiscountResponse modelDiscountResponse = response.body();
                    managerDiscountListener.setDiscountMessage(modelDiscountResponse);

                } else {
                    managerDiscountListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(@NonNull Call<ModelDiscountResponse> call, @NonNull Throwable t) {
                managerDiscountListener.showMessage(t.getMessage());
            }
        });
    }
}
