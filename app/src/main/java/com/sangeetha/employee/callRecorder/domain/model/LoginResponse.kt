package com.sangeetha.employee.callRecorder.domain.model

import com.google.gson.annotations.SerializedName

data class LoginResponse(
    @SerializedName("id")
    val id: Int,
    @SerializedName("username")
    val username: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("designation")
    val designation: String,
    @SerializedName("token")
    val token: String
)