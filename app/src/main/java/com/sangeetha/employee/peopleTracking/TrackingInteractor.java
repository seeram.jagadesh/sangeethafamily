package com.sangeetha.employee.peopleTracking;

import com.sangeetha.employee.peopleTracking.model.StoreResponse;
import com.sangeetha.employee.peopleTracking.model.TrackingResponse;

public interface TrackingInteractor {
    void getStoreList();

    void startTracking(String regId, String storeId, String imei, String type, String latValue, String lngValue);

    void stopTracking(String regId, String storeId, String imei, String type, String latValue, String lngValue, String trackingId);

    interface TrackingListener {
        void setMessage(String msg);

        void setStoreList(StoreResponse storeList);

        void setTracking(TrackingResponse trackingResponse);
    }
}
