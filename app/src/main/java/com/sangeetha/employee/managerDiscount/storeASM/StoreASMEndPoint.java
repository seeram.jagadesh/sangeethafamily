package com.sangeetha.employee.managerDiscount.storeASM;

import com.sangeetha.employee.CommonResponse;
import com.sangeetha.employee.checkStock.StockCheckResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.MakeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ProductTypeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.SubmitDiscountResponse;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.ApproveDiscountResponse;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.DiscountTypeResponse;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.PhoneDetailsResponse;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.RejectReasonResponse;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.StoreListResponse;
import com.sangeetha.employee.peopleTracking.model.StoreResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface StoreASMEndPoint {


    @POST("api-stock-product-type")
    Call<ProductTypeResponse> getProductTypesList(@Header("sangeetha_token") String token);

    @FormUrlEncoded
    @POST("api-stock-get-brand")
    Call<MakeResponse> getMake(@Header("sangeetha_token") String token, @Field("product_value") String productId);

    @FormUrlEncoded
    @POST("api-stock-get-model")
    Call<ModelResponse> getModel(@Header("sangeetha_token") String token, @Field("product_value") String productId, @Field("brand_value") String makeId);


    @FormUrlEncoded
    @POST("api-asm-waiting-approval")
    Call<ApproveDiscountResponse> waitingApproval(@Field("employee_id") String empId);

    @FormUrlEncoded
    @POST("api-manager-asm-discount-details")
    Call<ApproveDiscountResponse> getApprovalType(@Header("sangeetha_token") String token, @Field("approval_type") String empId);


    @GET("api-manager-discount-type")
    Call<DiscountTypeResponse> getDiscountTypeList(@Header("sangeetha_token") String token);


    @GET("api-manager-sales-eligible")
    Call<CommonResponse> checkExceptionEligible(@Header("sangeetha_token") String token);

    @FormUrlEncoded
    @POST("api-manager-sales-check-calculate")
    Call<SubmitDiscountResponse> salesCheckCalculate(@Header("sangeetha_token") String token, @Field("payment_mode") String paymentMode, @Field("product_type") String productType,
                                                     @Field("make_id") String makeId, @Field("model_id") String modelId, @Field("discount_value") String discountValue, @Field("store_id") String storeId);

    @FormUrlEncoded
    @POST("api-check-stock")
    Call<StockCheckResponse> checkStock(@Header("sangeetha_token") String token,
                                        @Field("product_id") String productType,
                                        @Field("brand_id") String makeId,
                                        @Field("model_id") String modelId,
                                        @Field("store_id") String storeId);

    @FormUrlEncoded
    @POST("api-manager-sales-send-request")
    Call<CommonResponse> exceptionSendRequest(@Header("sangeetha_token") String token, @Field("payment_mode") String paymentMode,
                                              @Field("product_type") String productType, @Field("make_id") String makeId,
                                              @Field("model_id") String modelId, @Field("discount_value") String discountValue,
                                              @Field("store_id") String storeId, @Field("total_amount") String totalAmount,
                                              @Field("customer_name") String customerName, @Field("customer_mobile") String customerMobile,
                                              @Field("customer_email") String customerEmail, @Field("customer_mobile_verified") String customerMobileVerified,
                                              @Field("employee_id") String employeeId);

    @FormUrlEncoded
    @POST("api-asm-approved-not-used")
    Call<ApproveDiscountResponse> notUsedApproval(@Field("employee_id") String empId);

    @FormUrlEncoded
    @POST("api-asm-approved-used")
    Call<ApproveDiscountResponse> usedApproval(@Field("employee_id") String empId);

    @FormUrlEncoded
    @POST("api-manager-phone-details")
    Call<PhoneDetailsResponse> phoneDetailByRefId(@Header("sangeetha_token") String token, @Field("reference_id") String refId);

    @FormUrlEncoded
    @POST("api-manager-asm-approval-discount")
    Call<ResponseBody> discountApprove(@Header("sangeetha_token") String token, @Field("reference_id") String refId);

    @FormUrlEncoded
    @POST("api-manager-asm-rejected-discount")
    Call<ResponseBody> discountReject(@Header("sangeetha_token") String token,
                                      @Field("reference_id") String refId,
                                      @Field("reason") String reason,
                                      @Field("note") String note);

    @GET("api-manager-asm-store-list")
    Call<StoreListResponse> getStoreList(@Header("sangeetha_token") String token);


    @GET("api-fetch-store")
    Call<StoreResponse> getStores(@Header("sangeetha_token") String token);

    @GET("api-manager-asm-reject-reason-list")
    Call<RejectReasonResponse> getRejectionList(@Header("sangeetha_token") String token);

    @FormUrlEncoded
    @POST("api-asm-check-discount-bargain")
    Call<ResponseBody> checkBargain(@Field("reference_id") String refId,
                                    @Field("total_amount") String totalAmount,
                                    @Field("asm_employee_id") String empId);

    @FormUrlEncoded
    @POST("api-asm-discount-bargain")
    Call<ResponseBody> submitBargain(@Field("reference_id") String refId,
                                     @Field("total_amount") String totalAmount,
                                     @Field("asm_employee_id") String empId,
                                     @Field("bargain_option") String bargainOption,
                                     @Field("accessory_value") String accessoryValue,
                                     @Field("vas_value") String vasValue,
                                     @Field("accessory_product") String accessoryProduct,
                                     @Field("vas_product") String vasProduct);


}
