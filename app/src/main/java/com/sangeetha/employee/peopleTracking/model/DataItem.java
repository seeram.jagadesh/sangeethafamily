package com.sangeetha.employee.peopleTracking.model;

import com.google.gson.annotations.SerializedName;

public class DataItem {

    @SerializedName("store_id")
    private String storeId;

    @SerializedName("id")
    private String id;

    @SerializedName("store_code")
    private String storeCode;

    @SerializedName("store_name")
    private String storeName;

    @SerializedName("make_id")
    private String makeID;

    @SerializedName("make_name")
    private String makeName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getMakeID() {
        return makeID;
    }

    public void setMakeID(String makeID) {
        this.makeID = makeID;
    }

    public String getMakeName() {
        return makeName;
    }

    public void setMakeName(String makeName) {
        this.makeName = makeName;
    }

    @Override
    public String toString() {
        return
                "DataItem{" +
                        "store_id = '" + storeId + '\'' +
                        ",store_code = '" + storeCode + '\'' +
                        ",store_name = '" + storeName + '\'' +
                        "}";
    }
}