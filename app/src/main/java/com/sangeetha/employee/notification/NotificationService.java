package com.sangeetha.employee.notification;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.sangeetha.employee.utils.AppPreference;
import com.sangeetha.employee.utils.NotificationHelper;
import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

import timber.log.Timber;


public class NotificationService extends FirebaseMessagingService {
    private static final String TAG = "NOTIFICATION";
    NotificationHelper notificationHelper;

    @Override
    public void onMessageReceived(@NotNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        notificationHelper = new NotificationHelper(this);
        showNotification(remoteMessage);

    }

    private void showNotification(RemoteMessage remoteMessage) {
        NotificationChannel notificationChannel = null;
        Timber.tag("remote message").e("%s", remoteMessage.getFrom());
        if (remoteMessage.getData().size() > 0) {
            String image = remoteMessage.getData().get("image");
            String title = remoteMessage.getData().get("title");
            String msg = remoteMessage.getData().get("message");
            String action = remoteMessage.getData().get("action");
            String displayName = remoteMessage.getData().get("displayName");
            String actionDestination = remoteMessage.getData().get("action_destination");
            //   Log.e("ACTION", action + "," + remoteMessage.getData().get("message"));
            if ( remoteMessage.getData().containsKey("action") && Objects.requireNonNull(action).equalsIgnoreCase("activity")) {
                String msgData = remoteMessage.getData().get("message");
                try {
                    assert msgData != null;
                    JSONObject jsonObject = new JSONObject(msgData);
                    if (jsonObject.getString("action").equalsIgnoreCase("manager_discount")) {
                        if (Build.VERSION.SDK_INT >= 26) {
                            notificationChannel = notificationHelper.getNotificationChannel(NotificationHelper.NOTIFICATION_WITH_ACTION_CHANNEL,
                                    "ASM", NotificationManager.IMPORTANCE_HIGH, "ASM Discount");
                        }
                        String msgText = jsonObject.getString("message");
                        String refId = jsonObject.getString("coupon_code");
                        notificationHelper.showASMNotification("Manager Discount", msgText, refId, notificationChannel);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else  {
                if (Build.VERSION.SDK_INT >= 26) {
                    notificationChannel = notificationHelper.getNotificationChannel(NotificationHelper.NOTIFICATION_CHANNEL2,
                            "Notifications", NotificationManager.IMPORTANCE_DEFAULT, "Sangeethamobiles Notifications");
                }
                notificationHelper.createNotification(title, msg, notificationChannel);
            }
        }




    }

    @Override
    public void onNewToken(@NotNull String token) {
        super.onNewToken(token);
        AppPreference appPreference = new AppPreference(this);
        appPreference.saveToken(token);
        Timber.tag("firebase id").e("%s", token);
    }
}
