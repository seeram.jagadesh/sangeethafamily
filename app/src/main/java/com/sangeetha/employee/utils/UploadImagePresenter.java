package com.sangeetha.employee.utils;

import android.content.Context;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class UploadImagePresenter {
    private Context context;
    private uploadImage uploadImagePost;

    public UploadImagePresenter(Context context, uploadImage acPost) {
        this.context = context;
        this.uploadImagePost = acPost;
    }

    public interface uploadImage{
        void uploadSuccess(String response);
        void uploadError(String response);
        void uploadFail(String response);
    }

        public void imageUpload(final String orderId,byte[] attachment) {


        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, AppConstants.onlineDelivery + "upload-image-app", new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                Log.e("Response",resultResponse);
                try {
                    JSONObject reader = new JSONObject(resultResponse);
                    Log.e("JSON",reader.toString());
                    boolean status = reader.getBoolean("status");
                    try {
                        if(status){
                            String image=reader.getString("image_id");
                            uploadImagePost.uploadSuccess(image);
                        }else {
                            uploadImagePost.uploadError(reader.getString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    uploadImagePost.uploadFail("Something went wrong. Please try after some time.");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");

                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message+" Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message+ " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message+" Internal Server Error";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                uploadImagePost.uploadFail(errorMessage);
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("order_unique_id",orderId);


                Log.e("Params input",params.toString());
                Log.e("Params input",params.toString());
                return params;
            }
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                String imgname=orderId+"-"+imagename;
                if(orderId.length()!=0)
                params.put("attachment", new DataPart(imgname+".jpg", attachment));
                Log.e("Params omage input",attachment.toString());
                return params;
            }
        };

        VolleySingleton.getInstance(context).addToRequestQueue(multipartRequest);
    }
}