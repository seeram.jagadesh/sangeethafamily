package com.sangeetha.employee.mining.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sangeetha.employee.mining.model.FetchMiningData;


public class FetchMiningResponse {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private FetchMiningData data;

    @Override
    public String toString() {
        return "FetchMiningResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public FetchMiningData getData() {
        return data;
    }

    public void setData(FetchMiningData data) {
        this.data = data;
    }
}
