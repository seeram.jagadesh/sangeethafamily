package com.sangeetha.employee.onlineDelivery.model;

import com.google.gson.annotations.SerializedName;

public class ReviewModel {
    @SerializedName("1")
    public String one;
    @SerializedName("2")
    public String two;
    @SerializedName("3")
    public String three;
    @SerializedName("4")
    public String four;
    @SerializedName("5")
    public String five;

    public String getOne() {
        return one;
    }

    public void setOne(String one) {
        this.one = one;
    }

    public String getTwo() {
        return two;
    }

    public void setTwo(String two) {
        this.two = two;
    }

    public String getThree() {
        return three;
    }

    public void setThree(String three) {
        this.three = three;
    }

    public String getFour() {
        return four;
    }

    public void setFour(String four) {
        this.four = four;
    }

    public String getFive() {
        return five;
    }

    public void setFive(String five) {
        this.five = five;
    }
}
