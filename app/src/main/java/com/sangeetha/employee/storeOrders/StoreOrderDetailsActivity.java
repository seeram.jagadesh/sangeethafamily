package com.sangeetha.employee.storeOrders;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.sangeetha.employee.CommonResponse;
import com.sangeetha.employee.R;
import com.sangeetha.employee.SangeethaCareApiClient;
import com.sangeetha.employee.mining.ApiCallable;
import com.sangeetha.employee.storeOrders.adapter.TrackOrderListAdapter;
import com.sangeetha.employee.storeOrders.model.OrderDetailsResponse;
import com.sangeetha.employee.storeOrders.model.OrderHistoryItem;
import com.sangeetha.employee.utils.AppConstants;
import com.sangeetha.employee.utils.AppPreference;
import com.sangeetha.employee.utils.CalendarUtils;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StoreOrderDetailsActivity extends AppCompatActivity {

    AppCompatTextView status, date, referenceNoTxt, customerName, customerMobile, product_code, productname, txtQuantity, Price, special_price, txtGSTPercent, txtGSTAmount, txtTotal;
    AppCompatTextView payment_gateway, transactionId, total_price;
    AppCompatTextView store_code, store_cug, store_email, store_name, asm_id, asm_name, asm_number, payment_status, titleOrderHistory;
    RecyclerView trackRecycler;
    LinearLayout paymentStatusLayout;
    ProgressBar progressBar;

    LinearLayout otpLayout, deliveryData;
    Spinner spinStatus, spinShipping, spinWarehouse, spinSelf, spinDelivery, spinConfirmed;
    String deliverOption = "";
    AppCompatEditText edtReference, otp_ed;
    AppCompatButton submitSelf, submitOthers, submitWare, submitDelivery, otp_submit_btn, resend_otp, delivery_boy;
    AppCompatButton submit;

    String empId = "";
    AppCompatEditText edtEmpID, WareedtEmpID, edtBrand, edtname, edtMobile, edtMobileOther, edtWayBillNo, edtReferenceNo, edtOtherEmpID;
    LinearLayout changeStatusLayout, shippingLayout, uploadSection;
    MaterialCardView invoiceCard;
    AppCompatTextView nameAddress, shipping_address, shipping_email_tv, shipping_phone_tv, storeProcess, txtEmployeeId, txtWareEmployeeId, txtOtherEmployeeId;
    AppCompatTextView billingName, billing_address, billing_address_tv, billing_email, store_code_invoice, invoice_date, txtIMEI, invoice_store;

    TextInputEditText invoice_num;
    LinearLayout shippingDetails;

    AppCompatTextView shipment_type, courier_name, courier_reference_no, pod_image_view;

    AppCompatButton findIMEI;
    int REQUEST_CAMERA = 0, SELECT_FILE = 1;

    String selectedFilePath = "";
    String spinSelfSelect = "", spinShipSelect = "";
    String orderId = "";

    AppCompatTextView uploadedFile;
    LinearLayout shippingSelf, shippingOthers, shippingWare;
    ApiCallable apiCallable;

    AppPreference appPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_order_details);

        appPreference = new AppPreference(this);
        orderId = getIntent().getStringExtra("orderId");
        findViews();
        getDetails(orderId);

        submit.setOnClickListener(v -> {
            String selectValue = spinStatus.getSelectedItem().toString();
            if (selectValue.equalsIgnoreCase("select order status")) {
                showMsg("Please select a status to proceed..");
            } else {
                String status = "";
                if (selectValue.equalsIgnoreCase("accept")) {
                    status = "1";
                } else {
                    status = "2";
                }
                changeStatus(status);
            }
        });

        findIMEI.setOnClickListener(v -> {
            if (invoice_num.getText().toString().equalsIgnoreCase("")) {
                showMsg("Please enter invoice number to proceed.");
            } else if (invoice_date.getText().toString().equalsIgnoreCase("")) {
                showMsg("Please select invoice date to proceed.");
            } else {
                String btnText = findIMEI.getText().toString();
                if (btnText.equalsIgnoreCase("Find the IMEI Number")) {
                    String invoice = invoice_store.getText().toString() + "/" + store_code_invoice.getText().toString() + invoice_num.getText().toString();
                    findIMEINo(invoice, "");
                } else {
                    String invoice = invoice_store.getText().toString() + "/" + store_code_invoice.getText().toString() + invoice_num.getText().toString();
                    findIMEINo(invoice, txtIMEI.getText().toString());
                }
            }
        });

        otp_submit_btn.setOnClickListener(v -> {
            String otp = otp_ed.getText().toString();
            if (otp.equalsIgnoreCase("")) {
                showMsg("Please enter valid OTP to proceed..");
            } else {
                validateOTP(otp);
            }
        });
        resend_otp.setOnClickListener(v -> {
            ApiCallable apiCallable = SangeethaCareApiClient.getClient().create(ApiCallable.class);
            apiCallable.resendOTP(appPreference.getUserToken(), orderId).enqueue(new Callback<CommonResponse>() {
                @Override
                public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                    if (response.isSuccessful()) {
                        hideProgress();
                        CommonResponse commonResponse = response.body();
                        if (commonResponse.getStatus()) {
                            showMsg(commonResponse.getMessage());
                        } else {
                            showMsg(commonResponse.getMessage());
                        }
                    } else {
                        hideProgress();
                        showMsg("Error while resending otp data.");
                    }
                }

                @Override
                public void onFailure(Call<CommonResponse> call, Throwable t) {

                    hideProgress();
                    showMsg("Failed to resend otp data..");
                }
            });
        });
        delivery_boy.setOnClickListener(v -> {
            ApiCallable apiCallable = SangeethaCareApiClient.getClient().create(ApiCallable.class);
            apiCallable.sendDeliveryBoyDetails(appPreference.getUserToken(), orderId).enqueue(new Callback<CommonResponse>() {
                @Override
                public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                    if (response.isSuccessful()) {
                        hideProgress();
                        CommonResponse commonResponse = response.body();
                        if (commonResponse.getStatus()) {
                            showMsg(commonResponse.getMessage());
                        } else {
                            showMsg(commonResponse.getMessage());
                        }
                    } else {
                        hideProgress();
                        showMsg("Error while resending otp data.");
                    }
                }

                @Override
                public void onFailure(Call<CommonResponse> call, Throwable t) {

                    hideProgress();
                    showMsg("Failed to resend otp data..");
                }
            });
        });

        uploadSection.setOnClickListener(v -> {
            openGallery();
        });
    }

    private Uri uri;

    private void openGallery() {

        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setDataAndType(uri, "image/*,application/pdf,application/msword,text/*");
//        intent.setType("image/*,application/pdf,application/msword,text/*");
//        String[] mimetypes = {"image/*", "application/pdf", "application/msword", "text/*"};
//        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            if (requestCode == SELECT_FILE) {
                {
                    try {
//                        bitmapImage = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
//                        imageView.setImageBitmap(imageUtility.createDirectoryAndSaveFile(bitmapImage));

                        Uri uriD = data.getData();
//                        File myFile=new File(uriD.getPath());
//                        selectedFilePath=myFile.getAbsolutePath();
//                        Log.e("file path",selectedFilePath);
                        selectedFilePath = getRealPathFromURI(StoreOrderDetailsActivity.this, uriD);
                        Log.e("file path", selectedFilePath);

                        String filename = selectedFilePath.substring(selectedFilePath.lastIndexOf("/") + 1);
                        if (filename.contains(".mp4") || filename.contains(".webm") || filename.contains(".3gp") || filename.contains(".3gpp") || filename.contains(".avi")
                                || filename.contains(".mpeg4")) {//avi

                            showMsg("Please select only images,pdf and doc files to upload.");
                            selectedFilePath="";
                        } else {
                            uploadedFile.setText(String.format("Selected File Name : %s", filename));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    }

    private void validateOTP(String otp) {
        ApiCallable apiCallable = SangeethaCareApiClient.getClient().create(ApiCallable.class);
        apiCallable.validateOTP(appPreference.getUserToken(), orderId, otp).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (response.isSuccessful()) {
                    hideProgress();
                    CommonResponse commonResponse = response.body();
                    if (commonResponse.getStatus()) {
                        showMsg(commonResponse.getMessage());
                        recreate();
                    } else {
                        showMsg(commonResponse.getMessage());
                    }
                } else {
                    hideProgress();
                    showMsg("Error while submitting otp data.");
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {

                hideProgress();
                showMsg("Failed to submit otp data..");
            }
        });
    }

    private void findIMEINo(String invoice, String imei) {
        showProgress();
        ApiCallable apiCallable = SangeethaCareApiClient.getClient().create(ApiCallable.class);
        apiCallable.updateOrderStatusStep2(appPreference.getUserToken(), orderId, spinConfirmed.getSelectedItem().toString().toLowerCase(), invoice, invoice_date.getText().toString(), imei).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(@NonNull Call<CommonResponse> call, @NonNull Response<CommonResponse> response) {
                if (response.isSuccessful()) {
                    hideProgress();
                    CommonResponse commonResponse = response.body();
                    if (commonResponse.getStatus()) {
                        showMsg(commonResponse.getMessage());
                        txtIMEI.setText(commonResponse.getMessageData());
                        if (findIMEI.getText().toString().equalsIgnoreCase("Find the IMEI Number")) {
                            findIMEI.setText("Submit");
                        } else {
                            recreate();
                        }
                    } else {
                        showMsg(commonResponse.getMessage());
                    }
                } else {
                    hideProgress();
                    showMsg("Error while getting data.");
                }
            }

            @Override
            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {

                hideProgress();
                showMsg("Failed to get data..");
            }
        });
    }

    private void changeStatus(String status) {
        showProgress();
        ApiCallable apiCallable = SangeethaCareApiClient.getClient().create(ApiCallable.class);
        apiCallable.updateOrderStatusStep1(appPreference.getUserToken(), orderId, status).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(@NonNull Call<CommonResponse> call, @NonNull Response<CommonResponse> response) {
                if (response.isSuccessful()) {
                    CommonResponse commonResponse = response.body();
                    assert commonResponse != null;
                    hideProgress();
                    if (commonResponse.getStatus()) {

                        showMsg(commonResponse.getMessage());
                        recreate();
                    } else {
                        showMsg(commonResponse.getMessage());
                    }
                } else {
                    hideProgress();
                    showMsg("Error while updating data");
                }
            }

            @Override
            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                hideProgress();
                showMsg("Failed to update data.");
            }
        });

    }

    private void showMsg(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    private boolean isConnectingToInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    void findViews() {

        status = findViewById(R.id.status);
        shippingDetails = findViewById(R.id.shippingDetails);
        uploadedFile = findViewById(R.id.uploadedFile);
        spinConfirmed = findViewById(R.id.spinConfirmed);
        storeProcess = findViewById(R.id.storeProcess);
        date = findViewById(R.id.date);
        referenceNoTxt = findViewById(R.id.referenceNoTxt);
        customerName = findViewById(R.id.customerName);
        titleOrderHistory = findViewById(R.id.titleOrderHistory);
        customerMobile = findViewById(R.id.customerMobile);
        product_code = findViewById(R.id.product_code);
        special_price = findViewById(R.id.special_price);
        productname = findViewById(R.id.productname);
        txtQuantity = findViewById(R.id.txtQuantity);
        Price = findViewById(R.id.Price);
        payment_gateway = findViewById(R.id.payment_gateway);
        paymentStatusLayout = findViewById(R.id.paymentStatusLayout);
        transactionId = findViewById(R.id.transactionId);
        total_price = findViewById(R.id.total_price);
        trackRecycler = findViewById(R.id.trackRecycler);
        findIMEI = findViewById(R.id.findIMEI);

        asm_number = findViewById(R.id.asm_number);
        payment_status = findViewById(R.id.payment_status);
        asm_name = findViewById(R.id.asm_name);
        asm_id = findViewById(R.id.asm_id);
        store_name = findViewById(R.id.store_name);
        store_email = findViewById(R.id.store_email);
        store_cug = findViewById(R.id.store_cug);
        store_code = findViewById(R.id.store_code);

        txtGSTPercent = findViewById(R.id.txtGSTPercent);
        txtGSTAmount = findViewById(R.id.txtGSTAmount);
        txtTotal = findViewById(R.id.txtTotal);

        nameAddress = findViewById(R.id.nameAddress);
        shipping_address = findViewById(R.id.shipping_address);
        shipping_email_tv = findViewById(R.id.shipping_email_tv);
        shipping_phone_tv = findViewById(R.id.shipping_phone_tv);
        billingName = findViewById(R.id.billingName);
        billing_address = findViewById(R.id.billing_address);
        billing_address_tv = findViewById(R.id.billing_address_tv);
        billing_email = findViewById(R.id.billing_email);
        changeStatusLayout = findViewById(R.id.statusLayout);
        shippingLayout = findViewById(R.id.shippingLayout);
        uploadSection = findViewById(R.id.uploadSection);
        spinStatus = findViewById(R.id.spinStatus);
        submit = findViewById(R.id.statusSubmit);
        otpLayout = findViewById(R.id.otpLayout);
        deliveryData = findViewById(R.id.deliveryData);


        invoice_num = findViewById(R.id.invoice_num);
        txtIMEI = findViewById(R.id.txtIMEI);
        store_code_invoice = findViewById(R.id.store_code_invoice);
        invoice_date = findViewById(R.id.invoice_date);
        invoiceCard = findViewById(R.id.invoiceCard);
        invoice_store = findViewById(R.id.invoice_store);
        spinShipping = findViewById(R.id.spinShipping);
        spinWarehouse = findViewById(R.id.spinWarehouse);
        spinSelf = findViewById(R.id.spinSelf);
        spinDelivery = findViewById(R.id.spinDelivery);
        edtReference = findViewById(R.id.referenceNo);

        shippingSelf = findViewById(R.id.shipping_self);
        submitSelf = findViewById(R.id.submitSelf);
        shippingOthers = findViewById(R.id.shipping_others);
        submitOthers = findViewById(R.id.submitOthers);
        shippingWare = findViewById(R.id.shipping_ware);
        submitWare = findViewById(R.id.submitWare);
        submitDelivery = findViewById(R.id.submitDelivery);

        //edtEmpID,edtBrand,edtname,edtMobile
        edtEmpID = findViewById(R.id.edtEmpID);
        WareedtEmpID = findViewById(R.id.WareedtEmpID);
        edtBrand = findViewById(R.id.edtBrand);
        edtname = findViewById(R.id.edtname);
        edtMobile = findViewById(R.id.edtMobile);
        edtMobileOther = findViewById(R.id.edtMobileOther);
        txtEmployeeId = findViewById(R.id.txtEmployeeId);
        edtOtherEmpID = findViewById(R.id.edtOtherEmpID);
        txtWareEmployeeId = findViewById(R.id.txtWareEmployeeId);
        txtOtherEmployeeId = findViewById(R.id.txtOtherEmployeeId);
        //edtWayBillNo,edtReferenceNo
        edtReferenceNo = findViewById(R.id.edtReferenceNo);
        edtWayBillNo = findViewById(R.id.edtWayBillNo);

        otp_ed = findViewById(R.id.otp_ed);
        //otp_submit_btn,resend_otp,delivery_boy
        delivery_boy = findViewById(R.id.delivery_boy);
        otp_submit_btn = findViewById(R.id.otp_submit_btn);
        resend_otp = findViewById(R.id.resend_otp);

        //shipment_type,courier_name,courier_reference_no,pod_image_view
        pod_image_view = findViewById(R.id.pod_image_view);
        courier_reference_no = findViewById(R.id.courier_reference_no);
        courier_name = findViewById(R.id.courier_name);
        shipment_type = findViewById(R.id.shipment_type);

    }

    ProgressDialog progressDialog;

    public void showProgress() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait..");
        progressDialog.show();
    }

    public void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    String shipmentType = "";

    private void getDetails(String orderId) {
        showProgress();
        apiCallable = SangeethaCareApiClient.getClient().create(ApiCallable.class);
        apiCallable.getOrderDetails(appPreference.getUserToken(), orderId).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    hideProgress();
                    String res = null;
                    try {
                        res = response.body().string();

                        JSONObject jsonObject = new JSONObject(res);
                        String ship = jsonObject.getJSONObject("data").getString("order_shipment");
                        if (!ship.equalsIgnoreCase("")) {
                            JSONObject shipment = new JSONObject(ship);
                            if (shipment.has("shippment_type")) {
                                Log.e("data", ship.toString());
                                shipmentType = shipment.getString("shippment_type");
                                if (shipmentType.equalsIgnoreCase("2")) {
                                    shippingDetails.setVisibility(View.VISIBLE);
                                    //shipment_type,courier_name,courier_reference_no,pod_image_view
                                    shipment_type.setText("Other");
                                    storeProcess.setText("Shipment Details");
                                    courier_name.setText(shipment.getString("courier_name"));
                                    courier_reference_no.setText(shipment.getString("waybill_no"));
                                    if (shipment.getString("ship_label").equalsIgnoreCase("")) {
                                        pod_image_view.setText("-");
                                    } else {
                                        pod_image_view.setOnClickListener(v -> {

                                            if (isConnectingToInternet()) {
                                                try {
//                                                new DownloadTask(StoreOrderDetailsActivity.this, pod_image_view, shipment.getString("ship_label"));
                                                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(shipment.getString("ship_label")));
                                                    startActivity(browserIntent);
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }

                                            } else
                                                Toast.makeText(StoreOrderDetailsActivity.this, "Oops!! There is no internet connection. Please enable internet connection and try again.", Toast.LENGTH_SHORT).show();
                                        });
                                    }
                                }
                            } else {
                                Log.e("empy data", "data is empty");
                            }
                        } else {
                            Log.e("ship", "emoty");
                        }
                        Gson gson = new Gson();
                        OrderDetailsResponse orderDetailsResponse = gson.fromJson(String.valueOf(jsonObject), OrderDetailsResponse.class);

//                    OrderDetailsResponse orderDetailsResponse = response.body();
                        if (orderDetailsResponse.isStatus()) {
                            status.setText(orderDetailsResponse.getOrderDetailsData().getOrder_status());
                            date.setText(orderDetailsResponse.getOrderDetailsData().getOrderSummary().getCreated_at());
                            referenceNoTxt.setText(orderDetailsResponse.getOrderDetailsData().getOrderNo());
                            customerName.setText(orderDetailsResponse.getOrderDetailsData().getOrderSummary().getCustomer_name());
                            customerMobile.setText(orderDetailsResponse.getOrderDetailsData().getOrderSummary().getCustomer_mobile());

                            product_code.setText(orderDetailsResponse.getOrderDetailsData().getOrderDetailsItemList().get(0).getProduct_code());
                            special_price.setText(String.format("%s %s", getResources().getString(R.string.rs), orderDetailsResponse.getOrderDetailsData().getOrderDetailsItemList().get(0).getProduct_special_price()));
                            txtGSTPercent.setText(String.format("%s %%", orderDetailsResponse.getOrderDetailsData().getOrderDetailsItemList().get(0).getGst()));
                            txtGSTAmount.setText(String.format("%s %s", getResources().getString(R.string.rs), getGSTAMount(orderDetailsResponse.getOrderDetailsData().getOrderDetailsItemList().get(0).getGst(), orderDetailsResponse.getOrderDetailsData().getOrderDetailsItemList().get(0).getProduct_price())));
                            txtTotal.setText(String.format("%s %s", getResources().getString(R.string.rs), getTotal(orderDetailsResponse.getOrderDetailsData().getOrderDetailsItemList().get(0).getPrice(),
                                    getGSTAMount(orderDetailsResponse.getOrderDetailsData().getOrderDetailsItemList().get(0).getGst(), orderDetailsResponse.getOrderDetailsData().getOrderDetailsItemList().get(0).getProduct_price()))));
                            payment_status.setText(orderDetailsResponse.getOrderDetailsData().getOrderSummary().getPayment_status());
                            productname.setText(orderDetailsResponse.getOrderDetailsData().getOrderDetailsItemList().get(0).getProduct_apx_name());
                            txtQuantity.setText(orderDetailsResponse.getOrderDetailsData().getOrderDetailsItemList().get(0).getQuantity());

                            store_code.setText(orderDetailsResponse.getOrderDetailsData().getOrderStoreDetails().getStore_code());
                            store_cug.setText(orderDetailsResponse.getOrderDetailsData().getOrderStoreDetails().getSto_mobile());
                            store_name.setText(orderDetailsResponse.getOrderDetailsData().getOrderStoreDetails().getStore_name());
                            store_email.setText(orderDetailsResponse.getOrderDetailsData().getOrderStoreDetails().getEmp_email());
                            asm_id.setText(orderDetailsResponse.getOrderDetailsData().getOrderStoreDetails().getAsm_emp_id());
                            asm_name.setText(orderDetailsResponse.getOrderDetailsData().getOrderStoreDetails().getEmployee_name());
                            asm_number.setText(orderDetailsResponse.getOrderDetailsData().getOrderStoreDetails().getEmp_mobile());


                            if (orderDetailsResponse.getOrderDetailsData().getOrder_status().equalsIgnoreCase("placed")) {
                                paymentStatusLayout.setVisibility(View.GONE);
                            } else {
                                paymentStatusLayout.setVisibility(View.VISIBLE);
                            }

                            Price.setText(String.format("%s %s", getResources().getString(R.string.rs), orderDetailsResponse.getOrderDetailsData().getOrderDetailsItemList().get(0).getProduct_price()));

                            payment_gateway.setText(orderDetailsResponse.getOrderDetailsData().getOrderSummary().getPayment_gateway_name());
                            transactionId.setText(orderDetailsResponse.getOrderDetailsData().getOrderSummary().getTransaction_id());
                            total_price.setText(String.format("%s %s", getResources().getString(R.string.rs), orderDetailsResponse.getOrderDetailsData().getOrderSummary().getTotal_price()));

                            List<OrderHistoryItem> historyItemList = orderDetailsResponse.getOrderDetailsData().getOrderHistoryItemList();

                            if (historyItemList.size() > 0) {
                                titleOrderHistory.setVisibility(View.VISIBLE);
                                trackRecycler.setVisibility(View.VISIBLE);
                                trackRecycler.setAdapter(new TrackOrderListAdapter(historyItemList, StoreOrderDetailsActivity.this));
                            } else {
                                trackRecycler.setVisibility(View.GONE);
                                titleOrderHistory.setVisibility(View.GONE);
                            }

                            if (orderDetailsResponse.getOrderDetailsData().getOrderSummary().getOrder_store_status().equalsIgnoreCase("0")) {
                                changeStatusLayout.setVisibility(View.VISIBLE);
                                otpLayout.setVisibility(View.GONE);
                                deliveryData.setVisibility(View.GONE);
                                shippingLayout.setVisibility(View.GONE);
                                invoiceCard.setVisibility(View.GONE);
                            } else if (orderDetailsResponse.getOrderDetailsData().getOrderSummary().getOrder_store_status().equalsIgnoreCase("1")
                                    && (orderDetailsResponse.getOrderDetailsData().getOrderSummary().getImei_no().equalsIgnoreCase("") ||
                                    !orderDetailsResponse.getOrderDetailsData().getOrderSummary().getImei_no().equalsIgnoreCase(""))
                                    && orderDetailsResponse.getOrderDetailsData().getOrderSummary().getOrder_status().equalsIgnoreCase("cancelled")) {
                                changeStatusLayout.setVisibility(View.GONE);
                                otpLayout.setVisibility(View.GONE);
                                deliveryData.setVisibility(View.GONE);
                                findViewById(R.id.storeProcess).setVisibility(View.GONE);
//                                findViewById(R.id.changeStatusLayout).setVisibility(View.GONE);
                                invoiceCard.setVisibility(View.GONE);
                                shippingLayout.setVisibility(View.GONE);
                            } else if (orderDetailsResponse.getOrderDetailsData().getOrderSummary().getOrder_store_status().equalsIgnoreCase("1")
                                    && orderDetailsResponse.getOrderDetailsData().getOrderSummary().getImei_no().equalsIgnoreCase("")) {
                                changeStatusLayout.setVisibility(View.GONE);
                                otpLayout.setVisibility(View.GONE);
                                deliveryData.setVisibility(View.GONE);
                                invoiceCard.setVisibility(View.VISIBLE);
                                findViewById(R.id.changeStatusLayout).setVisibility(View.VISIBLE);
                                shippingLayout.setVisibility(View.GONE);
                            } else if (orderDetailsResponse.getOrderDetailsData().getOrderSummary().getOrder_store_status().equalsIgnoreCase("1")
                                    && !orderDetailsResponse.getOrderDetailsData().getOrderSummary().getImei_no().equalsIgnoreCase("")
                                    && orderDetailsResponse.getOrderDetailsData().getOrderSummary().getOrder_status().equalsIgnoreCase("shipped")
                                    && shipmentType.equalsIgnoreCase("1")
                            ) {

                                deliveryData.setVisibility(View.GONE);
                                otpLayout.setVisibility(View.VISIBLE);
                                shippingLayout.setVisibility(View.GONE);
                                changeStatusLayout.setVisibility(View.GONE);
                                invoiceCard.setVisibility(View.GONE);
//                                findViewById(R.id.changeStatusLayout).setVisibility(View.GONE);
                            } else if (orderDetailsResponse.getOrderDetailsData().getOrderSummary().getOrder_store_status().equalsIgnoreCase("1")
                                    && !orderDetailsResponse.getOrderDetailsData().getOrderSummary().getImei_no().equalsIgnoreCase("")
                                    && orderDetailsResponse.getOrderDetailsData().getOrderSummary().getOrder_status().equalsIgnoreCase("shipped")) {
                                deliveryData.setVisibility(View.VISIBLE);
                                otpLayout.setVisibility(View.GONE);
                                shippingLayout.setVisibility(View.GONE);
                                changeStatusLayout.setVisibility(View.GONE);
                                invoiceCard.setVisibility(View.GONE);
//                                findViewById(R.id.changeStatusLayout).setVisibility(View.GONE);
                            } else if (orderDetailsResponse.getOrderDetailsData().getOrderSummary().getOrder_status().equalsIgnoreCase("delivered")) {
                                changeStatusLayout.setVisibility(View.GONE);
                                invoiceCard.setVisibility(View.GONE);
                                otpLayout.setVisibility(View.GONE);
                                deliveryData.setVisibility(View.GONE);
//                                findViewById(R.id.changeStatusLayout).setVisibility(View.GONE);
//                            getShippingCouriers();
                                shippingLayout.setVisibility(View.GONE);
                            } else if (orderDetailsResponse.getOrderDetailsData().getOrderSummary().getOrder_store_status().equalsIgnoreCase("1")
                                    && !orderDetailsResponse.getOrderDetailsData().getOrderSummary().getImei_no().equalsIgnoreCase("")
                                    && orderDetailsResponse.getOrderDetailsData().getOrderSummary().getOrder_status().equalsIgnoreCase("confirmed")) {
                                changeStatusLayout.setVisibility(View.GONE);
                                invoiceCard.setVisibility(View.GONE);
                                otpLayout.setVisibility(View.GONE);
                                deliveryData.setVisibility(View.GONE);
//                                findViewById(R.id.changeStatusLayout).setVisibility(View.GONE);
                                getShippingCouriers();
                                shippingLayout.setVisibility(View.VISIBLE);
                            }
                            String address = orderDetailsResponse.getOrderDetailsData().getOrderSummary().getShipping_address() + "," + orderDetailsResponse.getOrderDetailsData().getOrderSummary().getShipping_landmark() + "," + orderDetailsResponse.getOrderDetailsData().getOrderSummary().getShipping_pincode();
                            nameAddress.setText(orderDetailsResponse.getOrderDetailsData().getOrderSummary().getShipping_name());
                            shipping_address.setText(address);
                            shipping_email_tv.setText(orderDetailsResponse.getOrderDetailsData().getOrderSummary().getShipping_email());
                            shipping_phone_tv.setText(orderDetailsResponse.getOrderDetailsData().getOrderSummary().getShipping_mobile_number());

                            String bAddress = orderDetailsResponse.getOrderDetailsData().getOrderSummary().getBilling_address() + "," + orderDetailsResponse.getOrderDetailsData().getOrderSummary().getBilling_landmark() + "," + orderDetailsResponse.getOrderDetailsData().getOrderSummary().getBilling_pincode();
                            billingName.setText(orderDetailsResponse.getOrderDetailsData().getOrderSummary().getBilling_name());
                            billing_address.setText(bAddress);
                            billing_email.setText(orderDetailsResponse.getOrderDetailsData().getOrderSummary().getBilling_email());
                            billing_address_tv.setText(orderDetailsResponse.getOrderDetailsData().getOrderSummary().getBilling_mobile_number());

                            store_code_invoice.setText(orderDetailsResponse.getOrderDetailsData().getOrderStoreDetails().getStore_code());
                            invoice_date.setOnClickListener(v -> {
                                showCalendarForDatePicking();
                            });

                            spinShipping.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    txtEmployeeId.setText("");
                                    txtOtherEmployeeId.setText("");
                                    txtWareEmployeeId.setText("");
                                    String selected = parent.getSelectedItem().toString();
                                    spinShipSelect = data.get(position);
                                    if (selected.equalsIgnoreCase("self")) {
                                        getSubShippingCouriers("self");
                                    } else if (selected.equalsIgnoreCase("WareIQ Courier")) {
                                        getSubShippingCouriers("wareIQ_courier");
                                    } else if (!selected.equalsIgnoreCase("select shipping method")) {

                                        shippingOthers.setVisibility(View.VISIBLE);
                                        shippingSelf.setVisibility(View.GONE);
                                        shippingWare.setVisibility(View.GONE);

                                        if (selected.equalsIgnoreCase("bluedart")) {
                                            edtReferenceNo.setVisibility(View.GONE);
                                            edtMobileOther.setVisibility(View.GONE);
                                        } else {
                                            edtReferenceNo.setVisibility(View.VISIBLE);
                                            edtWayBillNo.setVisibility(View.GONE);
                                            edtMobileOther.setVisibility(View.GONE);
                                        }
                                        submitOthers.requestFocus();
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });

                            spinSelf.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    String select = parent.getSelectedItem().toString();
                                    if (select.equalsIgnoreCase("promoters")) {
                                        edtBrand.setVisibility(View.VISIBLE);
                                        edtname.setVisibility(View.VISIBLE);
                                        edtMobile.setVisibility(View.VISIBLE);
                                        edtEmpID.setVisibility(View.GONE);
                                        spinSelfSelect = dataSub.get(position).toString();
                                        txtEmployeeId.setVisibility(View.GONE);
                                    } else if (select.equalsIgnoreCase("sangeetha employee")) {
                                        txtEmployeeId.setVisibility(View.GONE);
                                        spinSelfSelect = dataSub.get(position).toString();
                                        edtEmpID.setVisibility(View.VISIBLE);
                                        edtBrand.setVisibility(View.GONE);
                                        edtname.setVisibility(View.GONE);
                                        edtMobile.setVisibility(View.GONE);
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                            spinDelivery.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    deliverOption = parent.getSelectedItem().toString();
                                    if (deliverOption.equalsIgnoreCase("cash")) {
                                        edtReference.setHint(getResources().getString(R.string.amount));
                                    } else {
                                        edtReference.setHint(getResources().getString(R.string.reference_no));
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                            spinWarehouse.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    String select = parent.getSelectedItem().toString();
                                    if (!select.equalsIgnoreCase("select warehouse")) {
//                                    edtBrand.setVisibility(View.VISIBLE);
//                                    edtname.setVisibility(View.VISIBLE);
//                                    edtMobile.setVisibility(View.VISIBLE);
//                                    edtEmpID.setVisibility(View.GONE);
                                        spinSelfSelect = dataSub.get(position).toString();

                                    }

                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });

                            edtEmpID.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                }

                                @Override
                                public void onTextChanged(CharSequence s, int start, int before, int count) {

                                }

                                @Override
                                public void afterTextChanged(Editable s) {
                                    if (s.length() == 4) {
                                        getEmpDetails(s.toString());
                                    }
                                }
                            });
                            WareedtEmpID.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                }

                                @Override
                                public void onTextChanged(CharSequence s, int start, int before, int count) {

                                }

                                @Override
                                public void afterTextChanged(Editable s) {
                                    if (s.length() == 4) {
                                        getEmpDetails(s.toString());
                                    }
                                }
                            });
                            edtOtherEmpID.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                }

                                @Override
                                public void onTextChanged(CharSequence s, int start, int before, int count) {

                                }

                                @Override
                                public void afterTextChanged(Editable s) {
                                    if (s.length() == 4) {
                                        getEmpDetails(s.toString());
                                    }
                                }
                            });

                            submitSelf.setOnClickListener(v -> {
                            Log.e("self",spinSelf.getSelectedItem().toString());
                                if(spinSelfSelect.equalsIgnoreCase("promoters")){
                                 if(edtname.getText().toString().equalsIgnoreCase("")){
                                     showMsg("Please enter promoter name to proceed.");
                                 }else if(edtBrand.getText().toString().equalsIgnoreCase("")){
                                     showMsg("Please enter promoter brand to proceed.");
                                 }else if(edtMobile.getText().toString().equalsIgnoreCase("")){
                                     showMsg("Please enter promoter mobile number to proceed.");
                                 }else{
                                     sendData("self", spinSelfSelect, edtBrand.getText().toString(), edtname.getText().toString(), edtMobile.getText().toString(), "");
                                 }
                                }else if(spinSelfSelect.equalsIgnoreCase("sangeetha employee")) {
                                    Log.e("self",spinSelfSelect);
                                    sendData("self", spinSelfSelect, "", "", "", "");
                                }
                            });

                            submitOthers.setOnClickListener(v -> {

                                if (spinShipSelect.equalsIgnoreCase("select shipping method")) {
                                    showMsg("Please select a shipping partner.");
                                } else {

                                    if (spinShipSelect.equalsIgnoreCase("bluedart")) {
//                                    sendData(spinShipSelect, spinShipSelect, "", "", "", edtWayBillNo.getText().toString());
                                        if (edtWayBillNo.getText().toString().equalsIgnoreCase("")) {
                                            showMsg("Please enter waybill number to proceed..");
                                        } else if (empId.equalsIgnoreCase("")) {
                                            showMsg("Please enter employee ID to proceed..");
                                        } else {
                                            sendDataWithImage(spinShipSelect, spinShipSelect, "", "", "", edtWayBillNo.getText().toString(), selectedFilePath);
                                        }
                                    } else if (!spinShipSelect.equalsIgnoreCase("WareIQ Courier")) {
//                                    sendData(spinShipSelect, spinShipSelect, "", "", "", edtReferenceNo.getText().toString());
                                        if (edtReferenceNo.getText().toString().equalsIgnoreCase("")) {
                                            showMsg("Please enter reference number to proceed..");
                                        } else if (empId.equalsIgnoreCase("")) {
                                            showMsg("Please enter employee ID to proceed..");
                                        } else {
                                            sendDataWithImage(spinShipSelect, spinShipSelect, "", "", "", edtReferenceNo.getText().toString(), selectedFilePath);
                                        }
                                    }
                                }
                            });
                            submitWare.setOnClickListener(v -> {
                                sendData(spinShipSelect, spinSelfSelect, "", "", "", "");
                            });

                            submitDelivery.setOnClickListener(v -> {

                                if (spinDelivery.getSelectedItem().toString().equalsIgnoreCase("select payment mode")) {
                                    showMsg("Please select a payment mode to proceed..");
                                } else if (edtReference.getText().toString().equalsIgnoreCase("")) {
                                    showMsg("Please enter reference number to proceed.");
                                } else {
                                    ApiCallable apiCallable = SangeethaCareApiClient.getClient().create(ApiCallable.class);
                                    apiCallable.orderDeliver(appPreference.getUserToken(), orderId, "Delivered", deliverOption, edtReference.getText().toString()).enqueue(new Callback<CommonResponse>() {
                                        @Override
                                        public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                                            if (response.isSuccessful()) {
                                                CommonResponse commonResponse = response.body();
                                                assert commonResponse != null;
                                                hideProgress();
                                                if (commonResponse.getStatus()) {

                                                    showMsg(commonResponse.getMessage());
                                                    recreate();
                                                } else {
                                                    showMsg(commonResponse.getMessage());
                                                }
                                            } else {
                                                hideProgress();
                                                showMsg("Error while updating data");
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<CommonResponse> call, Throwable t) {
                                            hideProgress();
                                            showMsg("Failed to update data.");

                                        }
                                    });
                                }
                            });

                        } else {
                            Toast.makeText(StoreOrderDetailsActivity.this, orderDetailsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                hideProgress();
                Log.e("error", t.getMessage());
                Log.e("error", t.getMessage());
                Toast.makeText(StoreOrderDetailsActivity.this, "Failed to get data.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void sendData(String shipMethid, String shipBy, String promoterBrand, String promoName, String mobile, String refNo) {
        ApiCallable apiCallable = SangeethaCareApiClient.getClient().create(ApiCallable.class);
        apiCallable.updateOrderShipStatus(appPreference.getUserToken(), "shipped", orderId, shipMethid, shipBy, promoterBrand, promoName, mobile, empId, refNo).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(@NonNull Call<CommonResponse> call, @NonNull Response<CommonResponse> response) {
                if (response.isSuccessful()) {
                    CommonResponse commonResponse = response.body();
                    if (commonResponse.getStatus()) {
                        hideProgress();
                        recreate();
                    } else {
                        hideProgress();
                        showMsg(commonResponse.getMessage());
                    }
                } else {
                    hideProgress();
                    showMsg("Error while updating data.");
                }
            }

            @Override
            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                hideProgress();
                showMsg("Failed to update data.");
            }
        });
    }

    private void sendDataWithImage(String shipMethid, String shipBy, String promoterBrand, String promoName, String mobile, String refNo, String selectedImage) {
        ApiCallable apiCallable = SangeethaCareApiClient.getClient().create(ApiCallable.class);
        if (shipMethid == null) {
            shipMethid = "";
        }
        if (shipBy == null) {
            shipBy = "";
        }
        if (promoterBrand == null) {
            promoterBrand = "";
        }
        if (promoName == null) {
            promoName = "";
        }
        if (mobile == null) {
            mobile = "";
        }
        if (refNo == null) {
            refNo = "";
        }
        if (selectedImage == null) {
            selectedImage = "";
        }


        File audioFile;
        MultipartBody.Part body;
        if (!selectedFilePath.equalsIgnoreCase("")) {

            audioFile = new File(selectedFilePath);
            RequestBody requestFile = RequestBody.create(MediaType.parse("*/*"), audioFile);
            body = MultipartBody.Part.createFormData("pod_image", audioFile.getName(), requestFile);
        } else {

            RequestBody requestFile = RequestBody.create(MediaType.parse("*/*"), "");
            body = MultipartBody.Part.createFormData("pod_image", "document", requestFile);

        }

        RequestBody ordeStatus = RequestBody.create(MediaType.parse("multipart/form-data"), "shipped");
        RequestBody oId = RequestBody.create(MediaType.parse("multipart/form-data"), orderId);
        RequestBody shipMethod = RequestBody.create(MediaType.parse("multipart/form-data"), shipMethid);
        RequestBody shipby = RequestBody.create(MediaType.parse("multipart/form-data"), shipBy);
        RequestBody brand = RequestBody.create(MediaType.parse("multipart/form-data"), promoterBrand);
        RequestBody name = RequestBody.create(MediaType.parse("multipart/form-data"), promoName);
        RequestBody mobileNo = RequestBody.create(MediaType.parse("multipart/form-data"), mobile);
        RequestBody employeeId = RequestBody.create(MediaType.parse("multipart/form-data"), empId);
        RequestBody referenceNo = RequestBody.create(MediaType.parse("multipart/form-data"), refNo);


        apiCallable.submitOrderShipStatus(appPreference.getUserToken(), ordeStatus, oId, shipMethod, shipby, brand, name, mobileNo, employeeId, referenceNo, body).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(@NonNull Call<CommonResponse> call, @NonNull Response<CommonResponse> response) {
                if (response.isSuccessful()) {
                    CommonResponse commonResponse = response.body();
                    if (commonResponse.getStatus()) {
                        hideProgress();
                        showMsg(commonResponse.getMessage());
                        recreate();
                    } else {
                        hideProgress();
                        showMsg(commonResponse.getMessage());
                    }
                } else {
                    hideProgress();
                    showMsg("Error while updating data.");
                }
            }

            @Override
            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                hideProgress();
                Log.e("error", t.getMessage());
                Log.e("error", t.getLocalizedMessage());
                showMsg("Failed to update data.");
            }
        });
    }

    private String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } catch (Exception e) {
            Log.e("TAG", "getRealPathFromURI Exception : " + e.toString());
            return "";
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private void getEmpDetails(String emp) {
        showProgress();
        ApiCallable apiCallable = SangeethaCareApiClient.getClient().create(ApiCallable.class);
        apiCallable.getOrderEmpDetails(appPreference.getUserToken(), emp).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(@NonNull Call<CommonResponse> call, @NonNull Response<CommonResponse> response) {
                if (response.isSuccessful()) {
                    CommonResponse commonResponse = response.body();
                    if (commonResponse.getStatus()) {
                        hideProgress();
                        empId = emp;
                        txtEmployeeId.setVisibility(View.VISIBLE);
                        txtEmployeeId.setText(commonResponse.getEmployee_name());
                        txtWareEmployeeId.setText(commonResponse.getEmployee_name());
                        txtOtherEmployeeId.setText(commonResponse.getEmployee_name());
                    } else {
                        hideProgress();
                        txtEmployeeId.setText("");
                        txtWareEmployeeId.setText("");
                        txtOtherEmployeeId.setText("");
                        showMsg(commonResponse.getMessage());
                    }
                } else {
                    hideProgress();
                    showMsg("Error while getting employee data.");
                }
            }

            @Override
            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                hideProgress();
                showMsg("Failed to get employee data.");
            }
        });
    }

    ArrayList<String> data = new ArrayList<>();
    ArrayList<String> key = new ArrayList<>();


    private void getShippingCouriers() {
        ApiCallable apiCallable = SangeethaCareApiClient.getClient().create(ApiCallable.class);
        apiCallable.getShipmentCourier(appPreference.getUserToken()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String res = response.body().string();

                        JSONObject jsonObject = new JSONObject(res);

                        boolean status = jsonObject.getBoolean("status");
                        String message = jsonObject.getString("message");
                        if (status) {
                            hideProgress();
                            JSONObject dataJSON = jsonObject.getJSONObject("data");
                            Iterator keys = dataJSON.keys();
                            data.clear();
                            key.clear();

                            data.add("Select Shipping method");
                            key.add("Select Shipping method");
                            String myData;
                            JSONArray dataArray = new JSONArray();
                            while (keys.hasNext()) {
                                String storeKey = (String) keys.next();
                                try {
                                    myData = dataJSON.getString(storeKey);
                                    data.add(storeKey);
                                    dataArray.put(myData);
                                    key.add(myData);
                                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(StoreOrderDetailsActivity.this, android.R.layout.simple_spinner_item, key);
                                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    spinShipping.setAdapter(adapter);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            hideProgress();
                            showMsg(message);
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                hideProgress();
                showMsg("Failed to get shipment data.");
            }
        });
    }


    ArrayList<String> dataSub = new ArrayList<>();
    ArrayList<String> keySub = new ArrayList<>();

    private void getSubShippingCouriers(String courier) {

//        ArrayList<String> data = new ArrayList<>();
//        ArrayList<String> key = new ArrayList<>();
        ApiCallable apiCallable = SangeethaCareApiClient.getClient().create(ApiCallable.class);
        apiCallable.getSubShipmentCourier(appPreference.getUserToken(), courier).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String res = response.body().string();

                        JSONObject jsonObject = new JSONObject(res);

                        boolean status = jsonObject.getBoolean("status");
                        String message = jsonObject.getString("message");
                        if (status) {
                            hideProgress();
                            JSONObject dataJSON = jsonObject.getJSONObject("data");
                            Iterator keys = dataJSON.keys();
                            dataSub.clear();
                            keySub.clear();
                            if (courier.equalsIgnoreCase("self")) {
                                dataSub.add("Select Shipping By");
                                keySub.add("Select Shipping By");
                            } else {
                                dataSub.add("Select Warehouse");
                                keySub.add("Select Warehouse");
                            }

                            String myData;
                            JSONArray dataArray = new JSONArray();
                            while (keys.hasNext()) {
                                String storeKey = (String) keys.next();
                                try {
                                    myData = dataJSON.getString(storeKey);
                                    dataSub.add(storeKey);
                                    dataArray.put(myData);
                                    keySub.add(myData);
                                    if (courier.equalsIgnoreCase("self")) {
                                        shippingWare.setVisibility(View.GONE);
                                        shippingOthers.setVisibility(View.GONE);
                                        shippingSelf.setVisibility(View.VISIBLE);
                                        submitSelf.requestFocus();
                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(StoreOrderDetailsActivity.this, android.R.layout.simple_spinner_item, keySub);
                                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        spinSelf.setAdapter(adapter);
                                    } else {
                                        shippingWare.setVisibility(View.VISIBLE);
                                        shippingSelf.setVisibility(View.GONE);
                                        submitWare.requestFocus();
                                        shippingOthers.setVisibility(View.GONE);
                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(StoreOrderDetailsActivity.this, android.R.layout.simple_spinner_item, keySub);
                                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        spinWarehouse.setAdapter(adapter);
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            hideProgress();
                            showMsg(message);
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                hideProgress();
                showMsg("Failed to get shipment data.");
            }
        });
    }

    private String getTotal(String product_price, String toString) {
        return String.valueOf(Double.parseDouble(product_price) + Double.parseDouble(toString));
    }

    private String getGSTAMount(String gst, String product_price) {
        return String.valueOf(Double.parseDouble(product_price) * Integer.parseInt(gst) / 100);
    }

    String withYearorNot = "";

    private void showCalendarForDatePicking() {

//            fin.setVisibility(View.VISIBLE);
        withYearorNot = "";

        Calendar calendar = Calendar.getInstance();
        Date d = calendar.getTime();
        int yearD = calendar.get(Calendar.YEAR);

        String currentYear = String.valueOf(yearD);

        int yearDefault = d.getYear();
        final int monthDefault = d.getMonth();

        final int dateDefault = d.getDate();
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                (view, year, monthOfYear, dayOfMonth) -> {
                    Calendar datePickedFromCalendar = Calendar.getInstance();
                    datePickedFromCalendar.set(year, monthOfYear, dayOfMonth);
                    String dateForView = CalendarUtils.getDateInCustomFormat(datePickedFromCalendar.getTime(), "dd-MM-yyyy");
//                    Toast.makeText(this, monthDefault+"", Toast.LENGTH_SHORT).show();
                    String acade = AppConstants.getAcademicYear(dateForView);
                    invoice_date.setText(acade);
                    invoice_date.setText(dateForView);
                }, yearD, monthDefault, dateDefault);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        //datePickerDialog.getDatePicker().setCalendarViewShown(false);
        datePickerDialog.show();


    }
}