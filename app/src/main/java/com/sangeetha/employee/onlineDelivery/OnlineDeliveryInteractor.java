package com.sangeetha.employee.onlineDelivery;

import com.sangeetha.employee.onlineDelivery.model.OrderListResponse;
import com.sangeetha.employee.onlineDelivery.model.orderDetails.OrderDetailsResponse;

public interface OnlineDeliveryInteractor {

    void getOrders(String userId);

    void getDeliveredOrders(String userId);

    void orderDetails(String id, String orderId);

    interface OnlineDeliveryListener {

        void showMessage(String msg);

        void getOrders(OrderListResponse orderListResponse);

        void getDeliveredOrders(OrderListResponse orderListResponse);

        void orderDetails(OrderDetailsResponse orderDetailsResponse);


    }
}
