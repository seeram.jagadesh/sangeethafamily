package com.sangeetha.employee.managerDiscount.newStoreProcess;

import com.sangeetha.employee.CommonResponse;
import com.sangeetha.employee.managerDiscount.couponCode.CheckCouponCode;
import com.sangeetha.employee.managerDiscount.manager_pojo.AlternativeMethodResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.DiscountDetailsResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.FindEmployeeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.MakeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelDiscountResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelPriceResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.PaymentModeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ProductTypeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.SubmitDiscountResponse;

public interface StoreView {

    void showProgress();

    void hideProgress();

    void showMessage(String msg);

    void setMake(MakeResponse managerResponse);

    void setProductTypes(ProductTypeResponse productTypes);

    void setPaymentModes(PaymentModeResponse managerResponse);

    void setModel(ModelResponse managerResponse);

    void setProductPrice(ModelPriceResponse managerResponse);

    void setDiscountMessage(ModelDiscountResponse discountMessage);

    void setOtpVerification(boolean verification);

    void setCustomerVerification(boolean statue, String msg, int type,String otpType);

    void setEmployeeName(FindEmployeeResponse employeeName);

    void setAlternativeMethod(AlternativeMethodResponse alternativeMethod);

    void setApprovalRequest(boolean status, String refId);

    void setPercentageBasedDiscount(SubmitDiscountResponse percentageBasedDiscount);

    void setAmountBasedDiscount(SubmitDiscountResponse amountBasedDiscount);

    void setDiscountDetails(DiscountDetailsResponse discountDetails);

    void setCheckCouponCode(CheckCouponCode checkCouponCode);

    void setCouponCodeCancel(CommonResponse commonResponse);

}
