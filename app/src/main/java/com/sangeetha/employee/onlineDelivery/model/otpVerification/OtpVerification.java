package com.sangeetha.employee.onlineDelivery.model.otpVerification;


import com.google.gson.annotations.SerializedName;


public class OtpVerification{

	@SerializedName("amount_paid")
	private String amountPaid;

	@SerializedName("review_added_status")
	private String review_added_status;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private boolean status;

	public String getReview_added_status() {
		return review_added_status;
	}

	public void setReview_added_status(String review_added_status) {
		this.review_added_status = review_added_status;
	}

	public void setAmountPaid(String amountPaid){
		this.amountPaid = amountPaid;
	}

	public String getAmountPaid(){
		return amountPaid;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}


	@Override
	public String toString() {
		return "OtpVerification{" +
				"amountPaid='" + amountPaid + '\'' +
				", review_added_status='" + review_added_status + '\'' +
				", message='" + message + '\'' +
				", status=" + status +
				'}';
	}
}