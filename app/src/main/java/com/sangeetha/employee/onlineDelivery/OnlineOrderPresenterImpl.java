package com.sangeetha.employee.onlineDelivery;


import com.sangeetha.employee.onlineDelivery.model.OrderListResponse;
import com.sangeetha.employee.onlineDelivery.model.orderDetails.OrderDetailsResponse;

public class OnlineOrderPresenterImpl implements OnlineDeliverPresenter, OnlineDeliveryInteractor.OnlineDeliveryListener {

    private OnlineDeliveryView onlineDeliveryView;
    private OnlineDeliveryInteractor onlineDeliveryInteractor;

    public OnlineOrderPresenterImpl(OnlineDeliveryView onlineDeliveryView) {
        this.onlineDeliveryView = onlineDeliveryView;
        onlineDeliveryInteractor = new OnlineDeliveryInteractorImpl(this);
    }


    @Override
    public void showMessage(String msg) {
        if (onlineDeliveryView != null) {
            onlineDeliveryView.hideProgress();
            onlineDeliveryView.showMessage(msg);
        }

    }

    @Override
    public void getOrders(OrderListResponse orderListResponse) {
        if (onlineDeliveryView != null) {
            onlineDeliveryView.hideProgress();
            onlineDeliveryView.getOrders(orderListResponse);
        }
    }

    @Override
    public void getDeliveredOrders(OrderListResponse orderListResponse) {
        if (onlineDeliveryView != null) {
            onlineDeliveryView.hideProgress();
            onlineDeliveryView.getDeliveredOrders(orderListResponse);
        }
    }

    @Override
    public void orderDetails(OrderDetailsResponse orderListResponse) {
        if (onlineDeliveryView != null) {
            onlineDeliveryView.hideProgress();
            onlineDeliveryView.orderDetails(orderListResponse);
        }
    }


    @Override
    public void getOrders(String userId) {
        if (onlineDeliveryView != null) {
            onlineDeliveryView.showProgress();
            onlineDeliveryInteractor.getOrders(userId);
        }
    }

    @Override
    public void getDeliverOrders(String userId) {
        if (onlineDeliveryView != null) {
            onlineDeliveryView.showProgress();
            onlineDeliveryInteractor.getDeliveredOrders(userId);
        }
    }

    @Override
    public void orderDetails(String userId,String orderId) {
        if (onlineDeliveryView != null) {
            onlineDeliveryView.showProgress();
            onlineDeliveryInteractor.orderDetails(userId,orderId);
        }
    }
}
