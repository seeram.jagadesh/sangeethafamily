package com.sangeetha.employee.forgotPassword.model;

import com.sangeetha.employee.CommonResponse;
import com.sangeetha.employee.forgotPassword.GenerateOtpResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface OtpCallable {
    @POST("api-get-mobile-generate-otp")
    @FormUrlEncoded
    Call<GenerateOtpResponse> generateOtp(
            @Field("registration_id") String regID,
            @Field("customer_mobile") String mob
    );

    @POST("api-get-mobile-verified-otp")
    @FormUrlEncoded
    Call<CommonResponse> validateOtp(
            @Field("registration_id") String regID,
            @Field("customer_mobile") String mob,
            @Field("otpvalue") String otp
    );
}
