
package com.sangeetha.employee.appointments;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class AppointmentsListResponse {

    @SerializedName("data")
    private AppointmentsData mAppointmentsData;
    @SerializedName("message")
    private String mMessage;
    @SerializedName("status")
    private Boolean mStatus;

    public AppointmentsData getData() {
        return mAppointmentsData;
    }

    public void setData(AppointmentsData appointmentsData) {
        mAppointmentsData = appointmentsData;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public Boolean getStatus() {
        return mStatus;
    }

    public void setStatus(Boolean status) {
        mStatus = status;
    }

}
