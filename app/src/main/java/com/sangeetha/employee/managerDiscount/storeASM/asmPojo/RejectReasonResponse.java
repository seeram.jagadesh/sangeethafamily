package com.sangeetha.employee.managerDiscount.storeASM.asmPojo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RejectReasonResponse {

    @SerializedName("reason_list")
    private List reasonList;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private boolean status;

    public List getReasonList() {
        return reasonList;
    }

    public void setReasonList(List reasonList) {
        this.reasonList = reasonList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "RejectReasonResponse{" +
                        "reason_list = '" + reasonList + '\'' +
                        ",message = '" + message + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}