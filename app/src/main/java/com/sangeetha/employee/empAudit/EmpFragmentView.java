package com.sangeetha.employee.empAudit;


import com.sangeetha.employee.empAudit.model.DataItem;
import com.sangeetha.employee.empAudit.model.DesignationsItem;
import com.sangeetha.employee.empAudit.model.DropdownDetailsItem;
import com.sangeetha.employee.empAudit.model.EmpCategoryResponse;
import com.sangeetha.employee.empAudit.model.SearchEmpItem;
import com.sangeetha.employee.empAudit.model.SubCategoryResponse;

import java.util.List;

public interface EmpFragmentView {

    void setBrandList(List<DataItem> data);

    void showMessage(String msg);

    void showProgress();

    void hideProgress();

    void auditReportStatus(boolean status);

    void setEmpName(SearchEmpItem empList);

    void empIdError();

    void setEmpStatus(List<DropdownDetailsItem> dropdownDetails);

    void setDesignation(List<DesignationsItem> designations);

    void setEmpCategory(EmpCategoryResponse empCategory);

    void setSubCategory(SubCategoryResponse subCategory);

}
