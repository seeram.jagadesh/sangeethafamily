package com.sangeetha.employee.callRecorder.utils

import com.sangeetha.employee.callRecorder.domain.model.CUGResult
import retrofit2.Response

suspend fun <T> callApi(block: suspend () -> Response<T>): CUGResult<T> {
    val response = block()
    return try {
        if (response.isSuccessful) {
            response.body()?.let {
                CUGResult.Success(it, response.code())
            } ?: CUGResult.Error(response.message())
        } else {
            CUGResult.Error(response.message())
        }
    } catch (expected: Exception) {
        CUGResult.Error(expected.message ?: "Oops!! Something went wrong.")
    }
}