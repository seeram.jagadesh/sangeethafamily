package com.sangeetha.employee.servify.model.data


import com.google.gson.annotations.SerializedName

data class ModelData(
    @SerializedName("id")
    val id: String,
    @SerializedName("model_name")
    val modelName: String
)