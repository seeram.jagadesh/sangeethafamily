package com.sangeetha.employee.empAudit.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchEmpResponse {

    @SerializedName("employee_details")
    private SearchEmpItem employeeDetails;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private boolean status;

    public SearchEmpItem getEmployeeDetails() {
        return employeeDetails;
    }

    public void setEmployeeDetails(SearchEmpItem employeeDetails) {
        this.employeeDetails = employeeDetails;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "SearchEmpResponse{" +
                        "employee_details = '" + employeeDetails + '\'' +
                        ",message = '" + message + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}