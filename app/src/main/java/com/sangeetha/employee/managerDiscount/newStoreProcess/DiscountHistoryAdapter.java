package com.sangeetha.employee.managerDiscount.newStoreProcess;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.sangeetha.employee.R;
import com.sangeetha.employee.managerDiscount.manager_pojo.CouponHistoryData;

import java.util.ArrayList;

public class DiscountHistoryAdapter extends RecyclerView.Adapter<DiscountHistoryAdapter.CouponHistoryItem> {

    private ArrayList<CouponHistoryData> couponHistoryData;
    private Context context;

    public DiscountHistoryAdapter(ArrayList<CouponHistoryData> couponHistoryData, Context context) {
        this.couponHistoryData = couponHistoryData;
        this.context = context;
    }

    @NonNull
    @Override
    public CouponHistoryItem onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.store_history_item, parent, false);
        return new CouponHistoryItem(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CouponHistoryItem holder, int position) {
        holder.dateTxt.setText(couponHistoryData.get(position).getStoreSubmit());
        if (couponHistoryData.get(position).getCouponCode().equalsIgnoreCase("")||couponHistoryData.get(position).getCouponCode()==null) {
            holder.couponCodelayout.setVisibility(View.GONE);
        } else {
            holder.couponCodelayout.setVisibility(View.VISIBLE);
            holder.apxCodeTxt.setText(couponHistoryData.get(position).getCouponCode());
        }
        if (couponHistoryData.get(position).getDiscountAmount()==null) {
            holder.couponAmountLayout.setVisibility(View.GONE);
        } else {
            holder.couponAmountLayout.setVisibility(View.VISIBLE);
            holder.amountTxt.setText(couponHistoryData.get(position).getDiscountAmount());
        }
        holder.mobileNoTxt.setText(couponHistoryData.get(position).getCustomerMobile());
        holder.modelTxt.setText(couponHistoryData.get(position).getModelId());
        holder.referenceTxt.setText(couponHistoryData.get(position).getReferenceId());
        if (couponHistoryData.get(position).getStatus()==null ||couponHistoryData.get(position).getStatus().equalsIgnoreCase("")) {
            holder.statusTxt.setText(couponHistoryData.get(position).getDiscountStatus());
        } else {
            holder.statusTxt.setText(couponHistoryData.get(position).getStatus());
        }
        holder.discountCardView.setOnClickListener(v -> {
            Intent intent = new Intent(context, DiscountDetailActivity.class);
            intent.putExtra("reference_id", holder.referenceTxt.getText().toString());

            context.startActivity(intent);
        });

    }

    @Override
    public int getItemCount() {
        //  Log.e("store history", "" + couponHistoryData.size());
        return couponHistoryData.size();
    }

    class CouponHistoryItem extends RecyclerView.ViewHolder {
        AppCompatTextView dateTxt, referenceTxt, mobileNoTxt, modelTxt, apxCodeTxt, amountTxt, statusTxt;
        CardView discountCardView;
        LinearLayout couponCodelayout, couponAmountLayout, couponStatusLayout;

        CouponHistoryItem(@NonNull View itemView) {
            super(itemView);
            dateTxt = itemView.findViewById(R.id.dateTxt);
            referenceTxt = itemView.findViewById(R.id.referenceTxt);
            mobileNoTxt = itemView.findViewById(R.id.mobileNoTxt);
            modelTxt = itemView.findViewById(R.id.modelTxt);
            apxCodeTxt = itemView.findViewById(R.id.apxCodeTxt);
            amountTxt = itemView.findViewById(R.id.amountTxt);
            statusTxt = itemView.findViewById(R.id.statusTxt);
            discountCardView = itemView.findViewById(R.id.discountCardView);
            couponCodelayout = itemView.findViewById(R.id.couponLayout);
            couponAmountLayout = itemView.findViewById(R.id.couponAmountLayout);
            couponStatusLayout = itemView.findViewById(R.id.couponStatusLayout);
        }
    }
}
