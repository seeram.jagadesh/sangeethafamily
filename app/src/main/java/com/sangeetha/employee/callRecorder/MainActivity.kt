package com.sangeetha.employee.callRecorder

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.PeriodicWorkRequest
import androidx.work.WorkManager
import com.sangeetha.employee.callRecorder.domain.SharedPreferencesSmsStorage
import com.sangeetha.employee.callRecorder.domain.model.MetaDataResponse
import com.sangeetha.employee.callRecorder.extensions.getPreferences
import com.sangeetha.employee.callRecorder.network.CUGApiClient
import com.sangeetha.employee.callRecorder.services.ServiceWorker
import com.sangeetha.employee.callRecorder.sms.view.CallsActivity
import com.sangeetha.employee.callRecorder.sms.view.SMSActivity
import com.sangeetha.employee.callRecorder.utils.AppUtils
import com.fondesa.kpermissions.allGranted
import com.fondesa.kpermissions.extension.permissionsBuilder
import com.fondesa.kpermissions.extension.send
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.sangeetha.employee.R
import com.sangeetha.employee.callRecorder.services.CallRecordService
import com.sangeetha.employee.callRecorder.services.CallRecorderAccessibilityService
import com.sangeetha.employee.databinding.ActivityCugMainBinding
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.util.concurrent.TimeUnit


class MainActivity : AppCompatActivity() {

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var binding: ActivityCugMainBinding

    private val permissionsArr = arrayOf(
        Manifest.permission.RECEIVE_SMS,
        Manifest.permission.READ_SMS,
        Manifest.permission.READ_CONTACTS,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.RECORD_AUDIO,
        Manifest.permission.READ_PHONE_STATE,
        Manifest.permission.READ_CALL_LOG,
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION
    )

    private var drawOverOtherAppsLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                requestAdditionalPermissions()
            }

        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCugMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        Timber.d("onCreate")
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        getInfo()
        initUi()
    }

    override fun onResume() {
        super.onResume()
        val preferences = getPreferences()?.let { SharedPreferencesSmsStorage(it) }
        if (preferences?.isCallRecordingFeatureEnable() == true) {
            checkAllPermissions { }
        }
    }

    private fun startServiceViaWorker() {
        Timber.d("startServiceViaWorker called")
        val UNIQUE_WORK_NAME = "CUG StartMyServiceViaWorker"
        val workManager = WorkManager.getInstance(this)

        // As per Documentation: The minimum repeat interval that can be defined is 15 minutes
        // (same as the JobScheduler API), but in practice 15 doesn't work. Using 16 here
        val request = PeriodicWorkRequest.Builder(
            ServiceWorker::class.java,
            16,
            TimeUnit.MINUTES
        ).build()

        // to schedule a unique work, no matter how many times app is opened i.e. startServiceViaWorker gets called
        // do check for AutoStart permission
        workManager.enqueueUniquePeriodicWork(
            UNIQUE_WORK_NAME,
            ExistingPeriodicWorkPolicy.KEEP,
            request
        )
    }

    private fun initUi() {
        val preferences = getPreferences()?.let { SharedPreferencesSmsStorage(it) }
        with(binding) {
            checkAllPermissions {

            }

            btnGrantPermissions.setOnClickListener {
                requestPermissions()
            }

            btnStartService.setOnClickListener {
                checkAllPermissions {
                    startServiceViaWorker()
                    startForegroundService(Intent(this@MainActivity, CallRecordService::class.java))
                }
            }

            switchCallTracking.setOnCheckedChangeListener { _, b ->
                preferences?.setCallTrackingEnable(b)
            }

            switchLocationTracking.setOnCheckedChangeListener { _, b ->
                preferences?.setLocationTrackingEnable(b)
            }

            switchSmsTracking.setOnCheckedChangeListener { _, b ->
                preferences?.setSmsTrackingEnable(b)
            }

            textManageCall.setOnClickListener {
                startActivity(Intent(this@MainActivity, CallsActivity::class.java))
            }

            textManageSms.setOnClickListener {
                startActivity(Intent(this@MainActivity, SMSActivity::class.java))
            }

            getPreferences()?.let {
                if (SharedPreferencesSmsStorage(it).isCheckIn() == SharedPreferencesSmsStorage.CHECK_IN_VALUE) {
                    btnCheckIn.text = getString(R.string.check_in)
                } else {
                    btnCheckIn.text = getString(R.string.check_out)
                }
            }

            btnCheckIn.setOnClickListener {
                if (checkLocationPermission()) {
                    fusedLocationClient.lastLocation
                        .addOnSuccessListener { location: Location? ->
                            // Got last known location. In some rare situations this can be null.
                            getPreferences()?.let {
                                val sharedPreferencesSmsStorage = SharedPreferencesSmsStorage(it)
                                if (btnCheckIn.text.toString() == getString(R.string.check_in)) {
                                    btnCheckIn.text = getString(R.string.check_out)
                                    sharedPreferencesSmsStorage.setCheckIn(
                                        SharedPreferencesSmsStorage.CHECK_OUT_VALUE
                                    )
                                    postCheck("check_in", location)
                                } else {
                                    btnCheckIn.text = getString(R.string.check_in)
                                    sharedPreferencesSmsStorage.setCheckIn(
                                        SharedPreferencesSmsStorage.CHECK_IN_VALUE
                                    )
                                    postCheck("check_out", location)
                                }
                            }
                        }
                } else {
                    permissionsBuilder(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ).build()
                        .send { result ->
                            Timber.d("requestPermissions : result = $result")
                            if (result.allGranted()) {
                                Timber.d("requestPermissions : all permissions are granted.")
                            } else {
                            }
                        }
                }
            }

            btnLogout.setOnClickListener {
                preferences?.clear()
                finish()
            }
        }
    }

    private fun showLoading() {
        binding.containerProgress.visibility = View.VISIBLE
    }

    private fun dismissLoading() {
        binding.containerProgress.visibility = View.GONE
    }

    private fun postCheck(value: String, location: Location?) {
        val dataArray = JSONArray()
        try {
            val callJsonObj = JSONObject()
            callJsonObj.put("lat", location?.latitude)
            callJsonObj.put("long", location?.longitude)
            dataArray.put(callJsonObj)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        getPreferences()?.let {
            showLoading()
            val sharedPreferencesSmsStorage = SharedPreferencesSmsStorage(it)
            var infoObject: JSONObject? = null
            try {
                infoObject = JSONObject(sharedPreferencesSmsStorage.getUserInfo())
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            val token = sharedPreferencesSmsStorage.getToken()
            CUGApiClient.service.postCheckIn(
                "Bearer $token",
                value,
                dataArray,
                infoObject
            ).enqueue(object : Callback<ResponseBody> {
                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    dismissLoading()
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    dismissLoading()
                }
            })
        }
    }

    private fun getInfo() {
        showLoading()
        CUGApiClient.service.getInfo().enqueue(object : Callback<MetaDataResponse> {
            override fun onResponse(
                call: Call<MetaDataResponse>,
                response: Response<MetaDataResponse>
            ) {
                dismissLoading()
                getPreferences()?.let {
                    val metaDataResponse = response.body()
                    SharedPreferencesSmsStorage(it).setIntervalTime(
                        metaDataResponse?.interval_second?.toInt()?.times(1000) ?: 10000
                    )
                }
            }

            override fun onFailure(call: Call<MetaDataResponse>, t: Throwable) {
                dismissLoading()
            }
        })
    }

    private fun checkAllPermissions(block: () -> Unit) {
        if (!checkAllPermissionsAccepted()) {
            binding.tvMessage.text = getString(R.string.grant_message)
            binding.btnStartService.visibility = View.INVISIBLE
            binding.btnGrantPermissions.visibility = View.VISIBLE
        } else {
            binding.tvMessage.text = getString(R.string.grant_success_message)
            binding.btnStartService.visibility = View.VISIBLE
            binding.btnGrantPermissions.visibility = View.INVISIBLE
            block()
        }
    }

    private fun requestPermissions() {
        permissionsBuilder(
            Manifest.permission.RECEIVE_SMS,
            Manifest.permission.READ_SMS,
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.READ_CALL_LOG,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        ).build()
            .send { result ->
                Timber.d("requestPermissions : result = $result")
                if (result.allGranted()) {
                    Timber.d("requestPermissions : all permissions are granted.")
                    binding.tvMessage.visibility = View.GONE
                } else {
                    binding.tvMessage.visibility = View.VISIBLE
                    binding.tvMessage.text = "Please accept all the permissions"
                }
                requestAdditionalPermissions()
            }
    }

    private fun checkLocationPermission(): Boolean {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }

    private fun checkAllPermissionsAccepted(): Boolean {
        for (permission in permissionsArr) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    permission
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return false
            }
        }
        if (!AppUtils.canDrawOverlays(this)) {
            return false
        } else if (!AppUtils.isAccessibilityServiceEnabled(
                this,
                CallRecorderAccessibilityService::class.java
            )
        ) {
            return false
        }
        return true
    }

    private fun requestAdditionalPermissions() {
        Timber.d("requestAdditionalPermissions")
        if (!AppUtils.canDrawOverlays(this)) {
            requestDrawOverlayPermission()
        } else if (!AppUtils.isAccessibilityServiceEnabled(
                this,
                CallRecorderAccessibilityService::class.java
            )
        ) {
            val openSettings = Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS)
            openSettings.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_HISTORY)
            startActivity(openSettings)
        }
    }

    private fun requestDrawOverlayPermission() {
        val intent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION)
        intent.data = Uri.parse("package:$packageName")
        drawOverOtherAppsLauncher.launch(intent)
    }

    fun showAppSettings() {
        val intent = Intent(
            Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
            Uri.parse("package:$packageName")
        )
        startActivity(intent)
    }
}