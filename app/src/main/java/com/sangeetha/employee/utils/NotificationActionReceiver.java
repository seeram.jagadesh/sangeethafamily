package com.sangeetha.employee.utils;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class NotificationActionReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getStringExtra("action");

        int id = intent.getIntExtra("ID", 0);
        NotificationManager notifyMgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        assert notifyMgr != null;
        notifyMgr.cancel(id);

        String claimID = intent.getStringExtra("CLAIM_ID");

        if (action.equalsIgnoreCase("activity")) {
            goToClaimRatingScreen(context, claimID);
        }
    }

    private void goToClaimRatingScreen(Context context, String claimID) {
//        Intent intent = new Intent(context, ClaimFeedbackActivity.class);
//        intent.putExtra("CLAIM_ID", claimID);
//        context.startActivity(intent);
    }

}
