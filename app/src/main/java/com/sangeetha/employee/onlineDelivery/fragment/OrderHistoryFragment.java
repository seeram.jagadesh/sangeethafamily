package com.sangeetha.employee.onlineDelivery.fragment;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.TextInputEditText;
import com.sangeetha.employee.CommonResponse;
import com.sangeetha.employee.R;
import com.sangeetha.employee.RetrofitServiceGenerator;
import com.sangeetha.employee.SangeethaCareApiClient;
import com.sangeetha.employee.onlineDelivery.OnlineDeliverPresenter;
import com.sangeetha.employee.onlineDelivery.OnlineDeliveryEndPoint;
import com.sangeetha.employee.onlineDelivery.OnlineDeliveryOTPActivity;
import com.sangeetha.employee.onlineDelivery.OnlineDeliveryView;
import com.sangeetha.employee.onlineDelivery.OnlineOrderPresenterImpl;
import com.sangeetha.employee.onlineDelivery.adapter.OrderListHistoryAdapter;
import com.sangeetha.employee.onlineDelivery.interfaces.ISearch;
import com.sangeetha.employee.onlineDelivery.model.DataItem;
import com.sangeetha.employee.onlineDelivery.model.DeliverRatingClick;
import com.sangeetha.employee.onlineDelivery.model.OrderListResponse;
import com.sangeetha.employee.onlineDelivery.model.ReviewValueResponse;
import com.sangeetha.employee.onlineDelivery.model.orderDetails.OrderDetailsResponse;
import com.sangeetha.employee.onlineDelivery.ratings.RatingClick;
import com.sangeetha.employee.onlineDelivery.ratings.RatingOptionsAdapter;
import com.sangeetha.employee.utils.AppConstants;
import com.sangeetha.employee.utils.AppPreference;
import com.sangeetha.employee.utils.ValidationUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class OrderHistoryFragment extends Fragment implements OnlineDeliveryView, ISearch, DeliverRatingClick,
        RatingClick, RatingOptionsAdapter.SingleClickListener {


    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    AppCompatTextView ratingText;
    private String ratingNo = "";
    DeliverRatingClick deliverRatingClick;
    ReviewValueResponse reviewValueResponse;
    private AppCompatTextView noOrder;
    private static final String ARG_SEARCHTERM = "search_term";
    private AppPreference appPreference;
    List<DataItem> ordersData;
    private String mSearchTerm = null;
    OnlineDeliveryEndPoint onlineDeliveryEndPoint;
    RatingClick clickRating;
    RatingOptionsAdapter ratingsAdapter;


    OrderListHistoryAdapter historyFragmentAdapter;
    private OnlineDeliverPresenter onlineDeliverPresenter;

    public OrderHistoryFragment() {
    }

    public static OrderHistoryFragment newInstance(String searchTerm) {
        OrderHistoryFragment fragment = new OrderHistoryFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_SEARCHTERM, searchTerm);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view;
        view = inflater.inflate(R.layout.fragment_order_history, container, false);

        recyclerView = view.findViewById(R.id.recyclerView);
        progressBar = view.findViewById(R.id.progress_bar);
        noOrder = view.findViewById(R.id.tv);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        appPreference = new AppPreference(getActivity());
        onlineDeliveryEndPoint = SangeethaCareApiClient.getClient1().create(OnlineDeliveryEndPoint.class);
        clickRating = this;
        deliverRatingClick = this;
        onlineDeliverPresenter = new OnlineOrderPresenterImpl(this);
        if (getArguments() != null) {
            mSearchTerm = (String) getArguments().get(ARG_SEARCHTERM);
        }
        OnlineDeliveryEndPoint getval = SangeethaCareApiClient.getEcomClient().create(OnlineDeliveryEndPoint.class);
        getval.getValueReviews().enqueue(new Callback<ReviewValueResponse>() {
            @Override
            public void onResponse(@NotNull Call<ReviewValueResponse> call, @NotNull Response<ReviewValueResponse> response) {
                if (response.isSuccessful()) {
                    reviewValueResponse = response.body();
                } else {
                    showMsg("Error while receiving review messages");
                }
            }

            @Override
            public void onFailure(@NotNull Call<ReviewValueResponse> call, @NotNull Throwable t) {
                showMsg("Failed to get review messages");
            }
        });
        return view;
    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();

        getData();
    }

    @Override
    public void showMessage(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void getOrders(OrderListResponse orderListResponse) {
        if (orderListResponse.getData() != null && orderListResponse.getData().size() > 0) {
            ordersData = orderListResponse.getData();
            historyFragmentAdapter = new OrderListHistoryAdapter(orderListResponse.getData(), "pending", mSearchTerm, this);
            recyclerView.setAdapter(historyFragmentAdapter);
            noOrder.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.GONE);
            noOrder.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void getDeliveredOrders(OrderListResponse orderListResponse) {

    }

    @Override
    public void orderDetails(OrderDetailsResponse orderListResponse) {

    }

    @Override
    public void onTextQuery(String text) {
        Log.e("text", text);
        Log.e("text", text);
        historyFragmentAdapter.getFilter().filter(text);
        historyFragmentAdapter.notifyDataSetChanged();
    }


    @Override
    public void getSelected(String number) {
        ratingNo = number;
        ratingText.setVisibility(View.VISIBLE);
        switch ((int) Integer.parseInt(number)) {
            case 1:
                ratingText.setText(reviewValueResponse.getData().getOne());
                break;
            case 2:
                ratingText.setText(reviewValueResponse.getData().getTwo());
                break;
            case 3:
                ratingText.setText(reviewValueResponse.getData().getThree());
                break;
            case 4:
                ratingText.setText(reviewValueResponse.getData().getFour());
                break;
            case 5:
                ratingText.setText(reviewValueResponse.getData().getFive());
                break;
            default:
                ratingText.setText("");
        }
    }

    @Override
    public void onItemClickListener(int position, View view) {
        ratingsAdapter.selectedItem();
    }



    public void showMsg(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    private void getData() {
        if (ValidationUtils.isThereInternet(requireActivity())) {
            onlineDeliverPresenter.getOrders(appPreference.getEmpData().getUserId());
        } else {
            Toast.makeText(getActivity(), "There is no internet connection.Please connect to internet.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void getRating(String rateId,String amount) {
        startActivity(new Intent(getActivity(), OnlineDeliveryOTPActivity.class)
        .putExtra("order_id",rateId).putExtra("info","rating")
                .putExtra("amount",amount));
    }

    @Override
    public void getEmpInfo(String orderId,String amount) {
//        getEmployeeInfo(orderId);
        startActivity(new Intent(getActivity(), OnlineDeliveryOTPActivity.class)
                .putExtra("order_id",orderId).putExtra("info","employee")
        .putExtra("amount",amount));
    }


}
