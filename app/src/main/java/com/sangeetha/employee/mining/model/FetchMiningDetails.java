package com.sangeetha.employee.mining.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FetchMiningDetails {


    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("customer_name")
    @Expose
    private String customer_name;
    @SerializedName("customer_mobile")
    @Expose
    private String customer_mobile;
    @SerializedName("category_type")
    @Expose
    private String category_type;
    @SerializedName("invoice_no")
    @Expose
    private String invoice_no;
    @SerializedName("invoice_date")
    @Expose
    private String invoice_date;
    @SerializedName("model")
    @Expose
    private String model;

    @Override
    public String toString() {
        return "FetchMiningDetails{" +
                "id='" + id + '\'' +
                ", customer_name='" + customer_name + '\'' +
                ", customer_mobile='" + customer_mobile + '\'' +
                ", category_type='" + category_type + '\'' +
                ", invoice_no='" + invoice_no + '\'' +
                ", invoice_date='" + invoice_date + '\'' +
                ", model='" + model + '\'' +
                '}';
    }

    public String getInvoice_no() {
        return invoice_no;
    }

    public void setInvoice_no(String invoice_no) {
        this.invoice_no = invoice_no;
    }

    public String getInvoice_date() {
        return invoice_date;
    }

    public void setInvoice_date(String invoice_date) {
        this.invoice_date = invoice_date;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getCustomer_mobile() {
        return customer_mobile;
    }

    public void setCustomer_mobile(String customer_mobile) {
        this.customer_mobile = customer_mobile;
    }

    public String getCategory_type() {
        return category_type;
    }

    public void setCategory_type(String category_type) {
        this.category_type = category_type;
    }
}
