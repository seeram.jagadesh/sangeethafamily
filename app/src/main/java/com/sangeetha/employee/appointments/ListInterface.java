package com.sangeetha.employee.appointments;

import com.sangeetha.employee.CommonResponse;
import com.sangeetha.employee.appointments.appointmentDetails.AppointmentDateResponse;
import com.sangeetha.employee.appointments.appointmentDetails.AppointmentDetailsResponse;
import com.sangeetha.employee.appointments.appointmentDetails.AppointmentSlotsResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface ListInterface {
    @POST("appointment-list-app")
    @FormUrlEncoded
    Call<AppointmentsListResponse> getDataList(@Header("sangeetha_token") String token,
                                               @Field("appointment_list_type") String appointmentListType,
                                               @Field("appointment_id") String appointmentId
    );

    @POST("appointment-details-app")
    @FormUrlEncoded
    Call<AppointmentDetailsResponse> getAppointmentDetails(@Header("sangeetha_token") String token,
                                                           @Field("appointment_id") String appointmentId,
                                                           @Field("user_id") String userId);


    @POST("appointment-otp-send-app")
    @FormUrlEncoded
    Call<CommonResponse> generateOTP(@Header("sangeetha_token") String token,
                                     @Field("appointment_unique_id") String appointmentId);


    @POST("appointment-otp-verify-app")
    @FormUrlEncoded
    Call<CommonResponse> verifyOTP(@Header("sangeetha_token") String token,
                                   @Field("appointment_unique_id") String appointmentId,
                                   @Field("otp") String otp);


    @POST("get-date-list-app")
    @FormUrlEncoded
    Call<AppointmentDateResponse> getDateList(@Header("sangeetha_token") String token,
                                              @Field("appointment_type") String appointmentId);


    @POST("get-time-slot-app")
    @FormUrlEncoded
    Call<AppointmentSlotsResponse> getTimeSlots(@Header("sangeetha_token") String token,
                                                @Field("unique_id") String appointmentId,
                                                @Field("is_from") String isFrom,
                                                @Field("date") String date);


    @POST("appointment-status-update-app")
    @FormUrlEncoded
    Call<CommonResponse> submitData(@Header("sangeetha_token") String token,
                                    @Field("appointment_unique_id") String appointmentId,
                                    @Field("appointment_status") String appointmentStatus,
                                    @Field("appointment_status_1") String appointmentStatus1,
                                    @Field("date") String date,
                                    @Field("time") String time,
                                    @Field("invoice_no") String invoiceNo,
                                    @Field("invoice_date") String invoiceDate,
                                    @Field("notes") String notes);


}
