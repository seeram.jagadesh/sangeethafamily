package com.sangeetha.employee.servify.model.remote

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.sangeetha.employee.SangeethaCareApiClient
import com.sangeetha.employee.managerDiscount.manager_pojo.MakeResponse
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelResponse
import com.sangeetha.employee.servify.model.data.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.Header
import timber.log.Timber

class ServifyRepository {

    private var apiServify: APIServify =
            SangeethaCareApiClient.getClient().create(APIServify::class.java)

    fun checkReferenceCode(token: String,
                           referenceCode: String
    ): MutableLiveData<CheckReferenceCodeData> {
        val phoneData: MutableLiveData<CheckReferenceCodeData> = MutableLiveData()
        val call = apiServify.getCheckExchange(token, referenceCode)
        call.enqueue(object : Callback<CheckReferenceCodeData> {
            @SuppressLint("TimberExceptionLogging")
            override fun onFailure(call: Call<CheckReferenceCodeData>, t: Throwable) {
                Timber.e(t)
            }

            override fun onResponse(
                    call: Call<CheckReferenceCodeData>,
                    response: Response<CheckReferenceCodeData>
            ) {
                if (response.isSuccessful) {
                    phoneData.value = response.body()
                } else {
                    Timber.e(response.message())
                }
            }

        })
        return phoneData
    }

    fun getServifyMake(): MutableLiveData<ServifyMake> {
//        apiServify = SangeethaCareApiClient.getDevelopClient().create(APIServify::class.java)
        val makeData: MutableLiveData<ServifyMake> = MutableLiveData()
        val call = apiServify.getServifyMake()
        call.enqueue(object : Callback<ServifyMake> {
            override fun onFailure(call: Call<ServifyMake>, t: Throwable) {

            }

            override fun onResponse(call: Call<ServifyMake>, response: Response<ServifyMake>) {
                if (response.isSuccessful) {
                    makeData.value = response.body()
                }
            }

        })
        return makeData
    }

    fun getServifyMakeNew(token: String,productType:String): MutableLiveData<MakeResponse> {
        val makeResponse: MutableLiveData<MakeResponse> = MutableLiveData()
        apiServify.getMakeDetail(token, productType).enqueue(object : Callback<MakeResponse> {
            override fun onResponse(call: Call<MakeResponse>, response: Response<MakeResponse>) {
                if (response.isSuccessful) {
                    makeResponse.value = response.body()
                }
            }

            override fun onFailure(call: Call<MakeResponse>, t: Throwable) {

            }
        })
        return makeResponse
    }

    fun getServifyModelNew(token: String, makeId: String,productType: String): MutableLiveData<ModelResponse> {
        val modelResponse: MutableLiveData<ModelResponse> = MutableLiveData()
        apiServify.getModel(token, productType, makeId).enqueue(object : Callback<ModelResponse> {
            override fun onResponse(call: Call<ModelResponse>, response: Response<ModelResponse>) {
                if (response.isSuccessful) {
                    modelResponse.value = response.body()
                }
            }

            override fun onFailure(call: Call<ModelResponse>, t: Throwable) {

            }
        })
        return modelResponse
    }

    fun getServifyModel(makeId: String): MutableLiveData<ServifyModel> {
//        apiServify = SangeethaCareApiClient.getDevelopClient().create(APIServify::class.java)
        val modelData: MutableLiveData<ServifyModel> = MutableLiveData()
        apiServify.getServifyModel(makeId)
                .enqueue(object : Callback<ServifyModel> {
                    override fun onFailure(call: Call<ServifyModel>, t: Throwable) {

                    }

                    override fun onResponse(
                            call: Call<ServifyModel>,
                            response: Response<ServifyModel>
                    ) {
                        if (response.isSuccessful) {
                            modelData.value = response.body()
                        }
                    }

                })
        return modelData
    }

    fun findEmployee(token: String, empId: String): MutableLiveData<FindEmpData> {
        //apiServify = SangeethaCareApiClient.getNetClient().create(APIServify::class.java)
        val empData: MutableLiveData<FindEmpData> = MutableLiveData()

        apiServify.findEmp(token, empId).enqueue(object : Callback<FindEmpData> {
            override fun onFailure(call: Call<FindEmpData>, t: Throwable) {

            }

            override fun onResponse(call: Call<FindEmpData>, response: Response<FindEmpData>) {
                empData.value = response.body()
            }

        })
        return empData
    }

    fun getCoupon(
            token: String,
            referenceCode: String,
            amount: String,
            tradeMode: String,
            imei: String,
            tradeStatus: String,
            customerName: String,
            customerMobile: String,
            customerEmail: String,
            make: String,
            model: String,
            empId: String,
            empName: String, agree: String,productType: String,retailerAmount:String,
            offerAmount : String,
            offerValue : String,
            sangOfferAmount: String,
            sangOfferValue : String
    ): MutableLiveData<ServifyCouponCode> {
        val couponData: MutableLiveData<ServifyCouponCode> = MutableLiveData()

        apiServify.exchangeSubmit(
                token,
                referenceCode,
                amount,
                tradeMode,
                imei,
                tradeStatus,
                customerName,
                customerMobile,
                customerEmail,
                make,
                model,
                empId,
                empName,
                agree,productType,retailerAmount,offerAmount,offerValue,sangOfferAmount,sangOfferValue
        )
                .enqueue(object : Callback<ServifyCouponCode> {
                    override fun onFailure(call: Call<ServifyCouponCode>, t: Throwable) {

                    }

                    override fun onResponse(
                            call: Call<ServifyCouponCode>,
                            response: Response<ServifyCouponCode>
                    ) {
                        couponData.value = response.body()
                    }

                })
        return couponData

    }

    fun getServifyHistory(token: String): MutableLiveData<ServifyHistory> {
        val historyData: MutableLiveData<ServifyHistory> = MutableLiveData()
        apiServify.viewHistory(token).enqueue(object : Callback<ServifyHistory> {
            override fun onFailure(call: Call<ServifyHistory>, t: Throwable) {

            }

            override fun onResponse(
                    call: Call<ServifyHistory>,
                    response: Response<ServifyHistory>
            ) {
                historyData.value = response.body()
            }

        })
        return historyData
    }

    fun getProducTypeList(token: String): MutableLiveData<ProductTypeResponse> {
        val historyData: MutableLiveData<ProductTypeResponse> = MutableLiveData()
        apiServify.getProductTypes(token).enqueue(object : Callback<ProductTypeResponse> {
            override fun onFailure(call: Call<ProductTypeResponse>, t: Throwable) {

            }

            override fun onResponse(
                    call: Call<ProductTypeResponse>,
                    response: Response<ProductTypeResponse>
            ) {
                historyData.value = response.body()
            }

        })
        return historyData
    }

    fun getServifyDetails(storeId: String, reference_id: String): MutableLiveData<ServifyHistoryData> {
        val historyData: MutableLiveData<ServifyHistoryData> = MutableLiveData()
        apiServify.viewDetails(storeId, reference_id).enqueue(object : Callback<ServifyHistoryData> {
            override fun onFailure(call: Call<ServifyHistoryData>, t: Throwable) {

            }

            override fun onResponse(
                    call: Call<ServifyHistoryData>,
                    response: Response<ServifyHistoryData>
            ) {
                if (response.isSuccessful) {
                    historyData.value = response.body()
                }
            }

        })
        return historyData
    }

    fun getServifyItemDetails(storeId: String, reference_id: String): MutableLiveData<ServifyHistoryResponseData> {
        val historyData: MutableLiveData<ServifyHistoryResponseData> = MutableLiveData()
        apiServify.viewItemDetails(storeId, reference_id).enqueue(object : Callback<ServifyHistoryResponseData> {
            override fun onFailure(call: Call<ServifyHistoryResponseData>, t: Throwable) {

            }

            override fun onResponse(
                    call: Call<ServifyHistoryResponseData>,
                    response: Response<ServifyHistoryResponseData>
            ) {
                if (response.isSuccessful) {
                    if (response.body()!!.status) {
                        historyData.value = response.body()
                    }
                }
            }

        })
        return historyData
    }

    fun changeMakeModel(
            storeId: String,
            reference_id: String,
            make: String,
            model: String,productType: String
    ): MutableLiveData<ResponseMessage> {
        val msgData: MutableLiveData<ResponseMessage> = MutableLiveData()
        apiServify.changeModel(storeId, reference_id, make, model,productType).enqueue(object :
                Callback<ResponseMessage> {
            override fun onFailure(call: Call<ResponseMessage>, t: Throwable) {

            }

            override fun onResponse(
                    call: Call<ResponseMessage>,
                    response: Response<ResponseMessage>
            ) {
                msgData.value = response.body()
            }

        })
        return msgData
    }

    fun cancelCoupon(storeId: String, reference_id: String): MutableLiveData<ResponseMessage> {
        val msgData: MutableLiveData<ResponseMessage> = MutableLiveData()
        apiServify.cancelCoupon(storeId, reference_id).enqueue(object : Callback<ResponseMessage> {
            override fun onFailure(call: Call<ResponseMessage>, t: Throwable) {

            }

            override fun onResponse(
                    call: Call<ResponseMessage>,
                    response: Response<ResponseMessage>
            ) {
                msgData.value = response.body()
            }

        })
        return msgData
    }
}