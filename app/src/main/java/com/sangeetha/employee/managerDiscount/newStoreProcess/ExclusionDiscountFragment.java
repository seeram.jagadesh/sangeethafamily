package com.sangeetha.employee.managerDiscount.newStoreProcess;


import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;

import com.github.florent37.expansionpanel.ExpansionLayout;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.chip.Chip;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.sangeetha.employee.CommonResponse;
import com.sangeetha.employee.CouponListDialogFragment;
import com.sangeetha.employee.R;
import com.sangeetha.employee.checkStock.StockCheckResponse;
import com.sangeetha.employee.customwidget.SearchableSpinner;
import com.sangeetha.employee.managerDiscount.couponCode.CheckCouponCode;
import com.sangeetha.employee.managerDiscount.manager_pojo.AlternativeMethodResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ApproverDetails;
import com.sangeetha.employee.managerDiscount.manager_pojo.DiscountDetailsResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.FindEmployeeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.MakeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelDiscountResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelItem;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelPriceResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.PaymentDataItem;
import com.sangeetha.employee.managerDiscount.manager_pojo.PaymentModeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ProductTypeDatItem;
import com.sangeetha.employee.managerDiscount.manager_pojo.ProductTypeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.SubmitDiscountResponse;
import com.sangeetha.employee.managerDiscount.storeASM.ApproveDiscountAdapter;
import com.sangeetha.employee.managerDiscount.storeASM.StoreASMPresenter;
import com.sangeetha.employee.managerDiscount.storeASM.StoreASMPresenterImpl;
import com.sangeetha.employee.managerDiscount.storeASM.StoreASMView;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.DataItem;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.DiscountDropdownItem;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.StoreListItem;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.StoreListResponse;
import com.sangeetha.employee.managerDiscount.storeManager.DiscountDetailActivity;
import com.sangeetha.employee.managerDiscount.storeManager.ManagerDiscountPresenter;
import com.sangeetha.employee.managerDiscount.storeManager.ManagerDiscountPresenterImp;
import com.sangeetha.employee.managerDiscount.storeManager.StoreAccountFragment;
import com.sangeetha.employee.managerDiscount.storeManager.StoreHistoryFragment;
import com.sangeetha.employee.managerDiscount.storeManager.StoreView;
import com.sangeetha.employee.mining.response.StoreModel;
import com.sangeetha.employee.peopleTracking.model.StoreResponse;
import com.sangeetha.employee.utils.AppPreference;
import com.sangeetha.employee.utils.ValidationUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExclusionDiscountFragment extends androidx.fragment.app.Fragment implements AdapterView.OnItemSelectedListener,
        StoreView, View.OnClickListener, BottomNavigationView.OnNavigationItemSelectedListener, DiscountDetailView, StoreASMView,
        StoreHistoryFragment.OnFragmentInteractionListener, StoreAccountFragment.OnFragmentInteractionListener, CouponListDialogFragment.Listener {

    private AppCompatSpinner filterSpinner;
    private StoreASMPresenter storeASMPresenter;
    private ProgressBar progressBar;
    private ArrayList<DataItem> dataItems = new ArrayList<>();
    private ApproveDiscountAdapter approveDiscountAdapter;
    private AppPreference appPreference;
    private String empId;

    List<DiscountDropdownItem> dropdownItemList;

    private ExpansionLayout productLayout, discountLayout, customerLayout, approvalLayout;
    private AppCompatTextView nameTextView;
    private AppCompatTextView priceTextView;
    private AppCompatTextView productPrice;
    private AppCompatTextView discountAmount;
    private AppCompatTextView discountedPrice;
    private AppCompatTextView discountErrorMsg;
    private AppCompatTextView otpWillExpireTV;
    private AppCompatTextView empName;
    private AppCompatTextView refId;
    private AppCompatTextView statusTxt;
    public TextInputEditText customerMobile, otpText, employeeId, apxCode, customerName, customerEmail;
    private AppCompatEditText discountPercentage;
    public ManagerDiscountPresenter managerDiscountPresenter;
    private List<com.sangeetha.employee.managerDiscount.manager_pojo.DataItem> makeList;
    private List<ProductTypeDatItem> productTypeDatItems;
    private List<ModelItem> modelList;
    private List<StoreListItem> storeListItems;
    private List<StoreModel> storeList;
    private List<PaymentDataItem> paymentModeList;

    private String selectedMake, selectedModel, paymentID, selectedStore;
    String productId;
    private ModelDiscountResponse modelDiscountResponse;
    private boolean otpVerification = false;
    private String otpTypeValue;
    private String OTPDefValue = "1111";
    private LinearLayoutCompat otpValidation;
    private LinearLayout priceLayout;
    private TextInputLayout mobileInputLayout, employeeIdInputLayout;
    private String mangeId;
    private String referenceId;
    private Chip generateOtp;
    private LinearLayoutCompat refreshLayout;


    private CountDownTimer otpTimer;
    String userToken;
    private boolean isTimerRunning;
    private long timeRemaining;
    private boolean discountSubmit = false;
    private ArrayAdapter<String> htmlItemArrayAdapter;
    private String selectedApprovel;
    private ArrayList<ApproverDetails> approverHtmlItemList = new ArrayList<>();
    ApproverDetails approverDetails;
    private ArrayList<String> approverList;
    private AppCompatButton phoneDetailsContinue, submitDiscount;
    private long durationOfOtp = 120000;
    private BottomNavigationView bottomNavigationView;

    private AppCompatTextView discountTv;
    private ArrayAdapter<String> makeAdapter, modelAdapter, paymentAdapter, productTypeAdapter, storeAdapter;
    private ArrayList<String> stores = new ArrayList<>();
    private ArrayList<String> makes = new ArrayList<>();
    private ArrayList<String> models = new ArrayList<>();
    private ArrayList<String> payments = new ArrayList<>();
    private ArrayList<String> productTypes = new ArrayList<>();

    public ExclusionDiscountFragment() {
        // Required empty public constructor
    }


    private static boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            //     Log.e("Network Testing", "***Available***");
            return true;
        } else {
            //    Log.e("Network Testing", "***Not Available***");
            Toast.makeText(mContext, "No network available, please check your WiFi or EmpData connection", Toast.LENGTH_LONG).show();
            return false;
        }

    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);

    }

    @Override
    public void showCouponMessage(String msg) {

    }

    @Override
    public void showMessage(String msg) {
        if (getActivity() != null)
            Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();

    }

    @Override
    public void setStoreList(StoreListResponse storeList) {
        if (storeList.isStatus()) {
            storeListItems = storeList.getStoreList();
            for (StoreListItem item : storeListItems) {
                stores.add(item.getStoreName());
            }
            storeAdapter.notifyDataSetChanged();
        } else {
            showMessage(storeList.getMessage());
        }
    }

    @Override
    public void setStores(StoreResponse storeList) {

    }

    @Override
    public void setStocks(StockCheckResponse response) {

    }

    @Override
    public void setCheckEligibleException(CommonResponse commonResponse) {

    }

    @Override
    public void setSalesCheckCalculate(SubmitDiscountResponse percentageBasedDiscount) {
        if (percentageBasedDiscount.isStatus()) {
            discountPercentage.setText(String.valueOf(percentageBasedDiscount.getDiscountPercentage()));
            discountedPrice.setText(String.valueOf(percentageBasedDiscount.getFinalAmount()));
            discountAmount.setText(String.valueOf(percentageBasedDiscount.getDiscountValue()));
            discountSubmit = true;
//            approverList.clear();
//            approverList.add("Approval Authority");
//            approverDetails=percentageBasedDiscount.getApproverDetails();
//            approverHtmlItemList.clear();
//            approverHtmlItemList.add(percentageBasedDiscount.getApproverDetails());
//            for (ApproverDetails htmlItem : approverHtmlItemList) {
//                approverList.add(htmlItem.getEmployeeName());
//            }
//            htmlItemArrayAdapter.notifyDataSetChanged();
        } else {
            showMessage(percentageBasedDiscount.getMessage());
        }
    }

    @Override
    public void setSendExceptionRequest(CommonResponse commonResponse) {
        if(commonResponse.getStatus()){
            showMessage(commonResponse.getMessage());
        }else{
            showMessage(commonResponse.getMessage());
        }
    }

    @Override
    public void setMake(MakeResponse managerResponse) {
        makeList = managerResponse.getData();
        for (com.sangeetha.employee.managerDiscount.manager_pojo.DataItem dataItem : makeList) {
            makes.add(dataItem.getMakeName());
        }
        makeAdapter.notifyDataSetChanged();
    }

    @Override
    public void setProductTypes(ProductTypeResponse productTypesResponse) {
        productTypeDatItems = productTypesResponse.getData();
        for (ProductTypeDatItem dataItem : productTypeDatItems) {
            productTypes.add(dataItem.getProductName());
        }
        productTypeAdapter.notifyDataSetChanged();
    }

    @Override
    public void setPaymentModes(PaymentModeResponse managerResponse) {
        paymentModeList = managerResponse.getData();
        for (PaymentDataItem paymentDataItem : paymentModeList) {
            payments.add(paymentDataItem.getPaymentName());
        }
        paymentAdapter.notifyDataSetChanged();
    }

    @Override
    public void setModel(ModelResponse managerResponse) {
        modelList = managerResponse.getData();
        for (ModelItem modelItem : modelList) {
            models.add(modelItem.getModelName());
        }
        modelAdapter.notifyDataSetChanged();
    }

    @Override
    public void setProductPrice(ModelPriceResponse managerResponse) {
        nameTextView.setText(managerResponse.getModelName());
        priceTextView.setText(managerResponse.getItemPrice());
        productPrice.setText(managerResponse.getItemPrice());
    }

    @Override
    public void setDiscountMessage(ModelDiscountResponse discountMessage) {
        if (discountMessage.isStatus() && selectedModel != null) {
            mangeId = discountMessage.getManageId();
            modelDiscountResponse = discountMessage;
            if (ValidationUtils.isThereInternet(getActivity()))
                managerDiscountPresenter.getProductPrice(userToken, productId, selectedMake, selectedModel, paymentID);
            priceLayout.setVisibility(View.VISIBLE);
            discountErrorMsg.setVisibility(View.GONE);
            phoneDetailsContinue.setVisibility(View.VISIBLE);

            if (discountMessage.getTypeDiscount().equalsIgnoreCase("flat"))
                discountTv.setText(String.format("Discount %s", discountMessage.getTypeDiscount()));
        } else {
            priceLayout.setVisibility(View.GONE);
            discountErrorMsg.setVisibility(View.VISIBLE);
            discountErrorMsg.setText(discountMessage.getMessage());


        }
    }

    @Override
    public void setOtpVerification(boolean verification) {

    }

    @Override
    public void setCustomerVerification(boolean statue, String msg, int type, String otpType) {

    }

    @Override
    public void setEmployeeName(FindEmployeeResponse employeeName) {

    }

    @Override
    public void setAlternativeMethod(AlternativeMethodResponse alternativeMethod) {

    }

    @Override
    public void setApprovalRequest(boolean status, String refId) {

    }

    @Override
    public void setPercentageBasedDiscount(SubmitDiscountResponse percentageBasedDiscount) {
        if (percentageBasedDiscount.isStatus()) {
            discountPercentage.setText(String.valueOf(percentageBasedDiscount.getDiscountPercentage()));
            discountedPrice.setText(String.valueOf(percentageBasedDiscount.getFinalAmount()));
            discountAmount.setText(String.valueOf(percentageBasedDiscount.getDiscountValue()));
            discountSubmit = true;
//            approverList.clear();
//            approverList.add("Approval Authority");
//            approverDetails=percentageBasedDiscount.getApproverDetails();
//            approverHtmlItemList.clear();
//            approverHtmlItemList.add(percentageBasedDiscount.getApproverDetails());
//            for (ApproverDetails htmlItem : approverHtmlItemList) {
//                approverList.add(htmlItem.getEmployeeName());
//            }
//            htmlItemArrayAdapter.notifyDataSetChanged();
        } else {
            showMessage(percentageBasedDiscount.getMessage());
        }
    }

    @Override
    public void setAmountBasedDiscount(SubmitDiscountResponse amountBasedDiscount) {

    }

    @Override
    public void setDiscountDetails(DiscountDetailsResponse discountDetails) {

    }

    @Override
    public void setDiscountInfo(DiscountDetailsResponse discountDetails) {

    }

    @Override
    public void couponCancel(boolean status, String msg) {

    }

    @Override
    public void setCheckCouponCode(CheckCouponCode checkCouponCode) {

    }

    @Override
    public void setCouponCodeCancel(CommonResponse commonResponse) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_exclusion_store, container, false);
        appPreference = new AppPreference(getContext());
        phoneDetailsContinue = view.findViewById(R.id.phone_detail_continue);
        productLayout = view.findViewById(R.id.productLayout);
        discountLayout = view.findViewById(R.id.discountLayout);
        customerLayout = view.findViewById(R.id.customerLayout);
        SearchableSpinner storeSpinner = view.findViewById(R.id.storeSpinner);
        SearchableSpinner makeSpinner = view.findViewById(R.id.makeSpinner);
        SearchableSpinner modelSpinner = view.findViewById(R.id.modelSpinner);
        SearchableSpinner paymentSpinner = view.findViewById(R.id.PaymentSpinner);
        SearchableSpinner productSpinner = view.findViewById(R.id.ProductTypeSpinner);
        phoneDetailsContinue.setOnClickListener(this);

        nameTextView = view.findViewById(R.id.nameTextView);
        priceTextView = view.findViewById(R.id.priceTextView);


        userToken = appPreference.getUserToken();
        AppCompatButton chooseDiscountContinue = view.findViewById(R.id.choose_discount_continue);
        AppCompatButton chooseDiscountBack = view.findViewById(R.id.choose_discount_back);
        productPrice = view.findViewById(R.id.currentPrice);
        discountAmount = view.findViewById(R.id.choose_discount_amount);
        AppCompatTextView discountPercentageTxt = view.findViewById(R.id.discount_percentage_txt);
        discountPercentage = view.findViewById(R.id.choose_discount_percentage);
        discountedPrice = view.findViewById(R.id.choose_total_price);
        discountErrorMsg = view.findViewById(R.id.discount_error);
        AppCompatTextView discountLimitError = view.findViewById(R.id.discount_limit_error);
        discountTv = view.findViewById(R.id.discount_tv);

        LinearLayoutCompat otpLayout = view.findViewById(R.id.otp_layout);
        otpValidation = view.findViewById(R.id.otp_validation);
        priceLayout = view.findViewById(R.id.price_layout);


        mobileInputLayout = view.findViewById(R.id.mobileInputLayout);
        otpWillExpireTV = view.findViewById(R.id.otp_will_expire_tv);
        AppCompatButton customerValidationContinue = view.findViewById(R.id.customer_validation_continue);
        AppCompatButton customerValidationBack = view.findViewById(R.id.customer_validation_back);
        customerName = view.findViewById(R.id.txtName);
        customerMobile = view.findViewById(R.id.txtMobile);
        customerEmail = view.findViewById(R.id.txtEmailStore);

        approvalLayout = view.findViewById(R.id.approvalLayout);

        employeeIdInputLayout = view.findViewById(R.id.employeeIdInputLayout);
        refId = view.findViewById(R.id.ref_id);
        empName = view.findViewById(R.id.emp_name);
        otpText = view.findViewById(R.id.txtOtp);
        progressBar = view.findViewById(R.id.request_progress);
        AppCompatButton approvalRequestBack = view.findViewById(R.id.approval_request_back);
        employeeId = view.findViewById(R.id.txtEmployee);
        apxCode = view.findViewById(R.id.txtCode);
        AppCompatSpinner authoritySpinner = view.findViewById(R.id.approvalAuthoritySpinner);
        refreshLayout = view.findViewById(R.id.refresh_layout);
        statusTxt = view.findViewById(R.id.status_txt);
        submitDiscount = view.findViewById(R.id.submitDiscount);
        FloatingActionButton refreshButton = view.findViewById(R.id.refresh_button);
        refreshButton.setOnClickListener(this);


        chooseDiscountContinue.setOnClickListener(this);
        chooseDiscountBack.setOnClickListener(this);
        customerValidationContinue.setOnClickListener(this);
        customerValidationBack.setOnClickListener(this);
        submitDiscount.setOnClickListener(this);

        storeSpinner.setOnItemSelectedListener(this);
        storeSpinner.setTitle("Select Store");
        storeSpinner.setPositiveButton("Done");

        makeSpinner.setOnItemSelectedListener(this);
        makeSpinner.setTitle("Select Make");
        makeSpinner.setPositiveButton("Done");

        productSpinner.setOnItemSelectedListener(this);
        productSpinner.setTitle("Select Product Type");
        productSpinner.setPositiveButton("Done");

        modelSpinner.setOnItemSelectedListener(this);
        modelSpinner.setTitle("Select Model");
        modelSpinner.setPositiveButton("Done");

        paymentSpinner.setOnItemSelectedListener(this);
        paymentSpinner.setTitle("Select Payment Type");
        paymentSpinner.setPositiveButton("Done");

        models.add(0, "---- Select Model ----");
        stores.add(0, "---- Select Store ----");
        makes.add(0, "---- Select Make ----");
        productTypes.add(0, "---- Select Product Type ----");
        payments.add(0, "---- Select Payment Type ----");
        makeAdapter = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_layout, makes);
        modelAdapter = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_layout, models);
        paymentAdapter = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_layout, payments);
        productTypeAdapter = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_layout, productTypes);
        storeAdapter = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_layout, stores);

        makeSpinner.setAdapter(makeAdapter);
        modelSpinner.setAdapter(modelAdapter);
        storeSpinner.setAdapter(storeAdapter);
        paymentSpinner.setAdapter(paymentAdapter);
        productSpinner.setAdapter(productTypeAdapter);
        managerDiscountPresenter = new ManagerDiscountPresenterImp(this);
        storeASMPresenter = new StoreASMPresenterImpl(this);

        if (ValidationUtils.isThereInternet(getActivity())) {
            storeASMPresenter.getStoreList(userToken);
        }


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isNetworkAvailable(getContext())) {
            if (appPreference.getASMData() != null) {
                empId = appPreference.getASMData().getEmployeeId();
//                storeASMPresenter.getWaitingApproval(empId);
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//        switch (position) {
//            case 0:
//                if (isNetworkAvailable(Objects.requireNonNull(getContext())))
//                    storeASMPresenter.getWaitingApproval(empId);
//                break;
//            case 1:
//                if (isNetworkAvailable(Objects.requireNonNull(getContext())))
//                    storeASMPresenter.getNotUsedApproval(empId);
//                break;
//            case 2:
//                if (isNetworkAvailable(Objects.requireNonNull(getContext())))
//                    storeASMPresenter.getUsedApproval(empId);
//                break;
//        }

//        String dId = dropdownItemList.get(position).getId();
////        showMessage(dId);
//        if (isNetworkAvailable(requireContext()))
//            storeASMPresenter.getDiscountStatus(appPreference.getUserToken(), dId);
        if (position > 0) {
            switch (parent.getId()) {
                case R.id.makeSpinner:
                    String makeId = makeList.get(position - 1).getId();
                    selectedMake = makeId;
                    if (ValidationUtils.isThereInternet(getActivity()))
                        models.clear();
                    models.add(0, "---- Select Model ----");
                    modelAdapter.notifyDataSetChanged();
//                    paymentAdapter.notifyDataSetChanged();
                    managerDiscountPresenter.getModel(userToken, productId, makeId);
                    priceLayout.setVisibility(View.GONE);
                    discountErrorMsg.setVisibility(View.GONE);
                    break;
                case R.id.storeSpinner:
                    selectedStore = storeListItems.get(position - 1).getStoreId();
                    if (ValidationUtils.isThereInternet(getActivity()))
                        payments.clear();
                    payments.add(0, "---- Select Payment Type ----");
                    paymentAdapter.notifyDataSetChanged();
                    managerDiscountPresenter.getPaymentList(userToken);
                    break;
                case R.id.modelSpinner:
                    String modelId = modelList.get(position - 1).getId();
                    selectedModel = modelId;
                    if (ValidationUtils.isThereInternet(getActivity()))
                        managerDiscountPresenter.checkModelPaymentDiscount(userToken, productId, selectedMake, selectedModel, paymentID);

//                    paymentModeList
                    break;
                case R.id.PaymentSpinner:
                    paymentID = paymentModeList.get(position - 1).getId();
                    if (ValidationUtils.isThereInternet(getActivity())) {
                        productTypes.clear();
                        productTypes.add(0, "---- Select Product Type ----");
                        makes.clear();
                        makes.add(0, "---- Select Make ----");
                        models.clear();
                        models.add(0, "---- Select Model ----");
                        modelAdapter.notifyDataSetChanged();
                        productTypeAdapter.notifyDataSetChanged();
                        makeAdapter.notifyDataSetChanged();
                        managerDiscountPresenter.getProductType(userToken, paymentID);
                    }
                    break;
                case R.id.ProductTypeSpinner:
                    productId = productTypeDatItems.get(position - 1).getId();
                    if (ValidationUtils.isThereInternet(getActivity())) {

                        makes.clear();
                        makes.add(0, "---- Select Make ----");
                        models.clear();
                        models.add(0, "---- Select Model ----");
                        modelAdapter.notifyDataSetChanged();
                        makeAdapter.notifyDataSetChanged();
                        managerDiscountPresenter.getMake(userToken, productId);
                    }
                    break;
                case R.id.approvalAuthoritySpinner:
                    selectedApprovel = approverHtmlItemList.get(position - 1).getEmployeeId();
                    break;
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.phone_detail_continue:
                if (modelDiscountResponse != null) {
                    discountLayout.toggle(true);
                } else {
                    showMessage("Select Model");
                }
                break;
            case R.id.choose_discount_continue:
                if (discountSubmit) {
                    customerLayout.toggle(true);
                } else {
                    showMessage("Submit discount first!");
                }
                break;
            case R.id.choose_discount_back:
                productLayout.toggle(true);
                break;
            case R.id.customer_validation_continue:

                Log.e("response",paymentID+"  "+productId+"  "+selectedMake+"  "+selectedModel+"  "+
                        Objects.requireNonNull(discountPercentage.getText()).toString()+"  "+selectedStore+"  "+discountedPrice.getText().toString()+"  "+
                        Objects.requireNonNull(customerName.getText()).toString()+"  "+ Objects.requireNonNull(customerMobile.getText()).toString()+"  "+
                        Objects.requireNonNull(customerEmail.getText()).toString()+"  "+"1"+"  "+appPreference.getEmpData().getEmployeeId());


                storeASMPresenter.sendExceptionRequest(appPreference.getUserToken(),paymentID,productId,selectedMake,selectedModel,
                        Objects.requireNonNull(discountPercentage.getText()).toString(),selectedStore,productPrice.getText().toString(),
                        Objects.requireNonNull(customerName.getText()).toString(), Objects.requireNonNull(customerMobile.getText()).toString(),
                        Objects.requireNonNull(customerEmail.getText()).toString(),"1",appPreference.getEmpData().getEmployeeId());
                break;
            case R.id.customer_validation_back:
                discountLayout.toggle(true);
                break;
            case R.id.approval_request_continue:
                if (referenceId != null) {
                    Intent intent = new Intent(getActivity(), DiscountDetailActivity.class);
                    intent.putExtra("reference_id", referenceId);
                    startActivity(intent);
                    getActivity().finish();
                } else {
                    showMessage("Send Request for Approval");
                }
                break;
            case R.id.approval_request_back:
                customerLayout.toggle(true);
                break;

            case R.id.submitDiscount:
                if (!Objects.requireNonNull(discountPercentage.getText()).toString().isEmpty()) {

                    Log.e("data", userToken + "  " + productId + "  " + selectedMake + "  " + selectedModel + "  " + paymentID + "  " + discountPercentage.getText().toString() +"  "+selectedStore);
                    Log.e("data", userToken + "  " + productId + "  " + selectedMake + "  " + selectedModel + "  " + paymentID + "  " + discountPercentage.getText().toString() +"  "+selectedStore);
                    storeASMPresenter.salesCheckCalculate(userToken, paymentID, productId, selectedMake, selectedModel, discountPercentage.getText().toString(),selectedStore);
//                    managerDiscountPresenter.percentageBaseDiscount(userToken, productId, selectedMake, selectedModel, paymentID, mangeId, discountPercentage.getText().toString());
                }
                break;

            case R.id.generate_otp:
                if (customerName.getText().toString().isEmpty()) {
                    customerName.setError("Please enter customer name.");
                } else if (customerMobile.getText().toString().isEmpty()) {
                    customerMobile.setError("Please enter customer mobile No.");
                } else {
                    if (ValidationUtils.isThereInternet(getActivity())) {
//                        managerDiscountPresenter.checkCouponCode(customerMobile.getText().toString());
                        if (generateOtp.getText().toString().equalsIgnoreCase("resend otp")) {
                            managerDiscountPresenter.resendOtp(userToken, customerMobile.getText().toString());
                        } else {
                            managerDiscountPresenter.generateOTP(userToken, customerMobile.getText().toString());
                        }
                    }
                }

                break;

            case R.id.verify_otp:
                if (ValidationUtils.isThereInternet(getActivity()))
                    managerDiscountPresenter.validateOTP(userToken, customerMobile.getText().toString(), otpText.getText().toString());
                break;


            case R.id.submit_code:
                if (ValidationUtils.isThereInternet(getActivity()))
                    managerDiscountPresenter.alternativeMethod(userToken, apxCode.getText().toString(), "app");
                break;
            case R.id.refresh_button:
                if (ValidationUtils.isThereInternet(getActivity()))
                    managerDiscountPresenter.discountDetails(userToken, referenceId);
        }

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public void onItemClicked(int position) {

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
