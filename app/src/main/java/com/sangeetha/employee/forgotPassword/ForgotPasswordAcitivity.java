package com.sangeetha.employee.forgotPassword;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.cardview.widget.CardView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.sangeetha.employee.CommonResponse;
import com.sangeetha.employee.R;
import com.sangeetha.employee.SangeethaCareApiClient;
import com.sangeetha.employee.empLogin.EmployeeLogin;
import com.sangeetha.employee.utils.LanguageUtil;
import com.sangeetha.employee.utils.MyContextWrapper;
import com.sangeetha.employee.utils.ValidationUtils;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ForgotPasswordAcitivity extends AppCompatActivity {
    private final int RESOLVE_HINT = 23;
    String mobileNo = "";
    AppCompatButton submitMobileNumerBtn;
    CountDownTimer countDownTimer;
    private TextInputEditText confirmPassword = null, userId = null;
    private TextInputEditText password = null;
    private TextInputLayout confirmPasswordLayout = null;
    private TextInputLayout passwordInputLayout = null, userIdInputLayout;
    private ProgressBar progressBar = null;
    private CardView cardView, cardView1;

    // Construct a request for phone numbers and show the picker
    /*private void requestHint() {
       // GoogleApiClient apiClient = GoogleApiClient.
        SmsRetrieverClient smsRetrieverClient = SmsRetriever.getClient(ForgotPasswordAcitivity.this);

        HintRequest hintRequest = new HintRequest.Builder()
                .setPhoneNumberIdentifierSupported(true)
                .build();

        PendingIntent intent = Auth.CredentialsApi.getHintPickerIntent(
                apiClient, hintRequest);
        startIntentSenderForResult(intent.getIntentSender(),
                RESOLVE_HINT, null, 0, 0, 0);
    }*/
    private String mobNumber;
    private boolean isOtpAutoReadDone;
    private boolean isRequestForOtpDone;
    private long lastSavedTime;
    private ProgressDialog otpGenerateProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        submitMobileNumerBtn = findViewById(R.id.submit);
        Button signInButton1 = findViewById(R.id.submit1);
        confirmPassword = findViewById(R.id.txtConfirmPassword);
        userId = findViewById(R.id.userId);
        //password = findViewById(R.id.txtPassword);
        confirmPasswordLayout = findViewById(R.id.confirmPasswordInputLayout);
        passwordInputLayout = findViewById(R.id.passwordInputLayout);
        cardView = findViewById(R.id.cardview);
        cardView1 = findViewById(R.id.cardview1);

        submitMobileNumerBtn.setClickable(true);

        submitMobileNumerBtn.setClickable(true);

        userIdInputLayout = findViewById(R.id.userIdInputLayout);
        progressBar = findViewById(R.id.progress);
        progressBar.setVisibility(View.GONE);
        submitMobileNumerBtn.setOnClickListener(view -> {
            submitMobileNumerBtn.setClickable(false);
            //  doLogin(confirmPassword.getText().toString(), password.getText().toString());*/
            if (TextUtils.isEmpty(userId.getText().toString()) || userId.getText().toString().length() < 2) {
                userIdInputLayout.setError("Please provide valid empId.");

            } else {
                userIdInputLayout.setErrorEnabled(false);
                mobileNo = userId.getText().toString();

                if (ValidationUtils.isThereInternet(ForgotPasswordAcitivity.this)) {
                    showProgress("Please wait...");
                    forgotPwd(mobileNo);
                } else
                    Toast.makeText(ForgotPasswordAcitivity.this, "No internet connection!", Toast.LENGTH_SHORT).show();
            }

        });
        signInButton1.setOnClickListener(view -> {
            if (TextUtils.isEmpty(password.getText().toString()) || password.getText().toString().length() < 5 || password.getText().toString().length() > 12) {
                passwordInputLayout.setError("Please provide a valid password.");
                return;
            }
            passwordInputLayout.setErrorEnabled(false);

            if (TextUtils.isEmpty(confirmPassword.getText().toString()) || !confirmPassword.getText().toString().equals(password.getText().toString())) {
                confirmPasswordLayout.setError("Password and confirm password should be same.");
                return;
            }
            confirmPasswordLayout.setErrorEnabled(false);
            changePassword(mobileNo, password.getText().toString());

        });

      /*  Button signup = (Button) findViewById(R.id.submit);
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ForgotPasswordAcitivity.this, ForgotPasswordAcitivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        });*/
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        Locale languageType = LanguageUtil.getLanguageType(newBase);
        super.attachBaseContext(MyContextWrapper.wrap(newBase, languageType));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void forgotPwd(final String mob) {
        ForgotPasswordEndPoint apiCallable = SangeethaCareApiClient.getClient().create(ForgotPasswordEndPoint.class);
        apiCallable.forgotPassword(mob).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getStatus()) {
                            Toast.makeText(ForgotPasswordAcitivity.this, "OTP sent!", Toast.LENGTH_SHORT).show();
                            isRequestForOtpDone = true;
                            mobNumber = mob;
                            hideProgress();
                            moveToOtpScreen(mob);

                            //showBottomSheetDialogForOTP(mob, false);
                        } else {
                            hideProgress();
                            Toast.makeText(ForgotPasswordAcitivity.this, "This mobile number is not yet registered!", Toast.LENGTH_SHORT).show();
                            submitMobileNumerBtn.setClickable(true);
                        }
                    } else
                        submitMobileNumerBtn.setClickable(true);
                } else
                    submitMobileNumerBtn.setClickable(true);
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                hideProgress();
                submitMobileNumerBtn.setClickable(true);
            }
        });
    }

    private void moveToOtpScreen(String mob) {
        Intent intent = new Intent(ForgotPasswordAcitivity.this, OtpActivity.class);
        intent.putExtra("IS_FOR_REGISTRATION", false);
        intent.putExtra("MOB_NUM_FOR_OTP", mob);

        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        submitMobileNumerBtn.setClickable(true);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private void showProgress(String msg) {
        if (otpGenerateProgress != null)
            otpGenerateProgress = null;

        otpGenerateProgress = new ProgressDialog(ForgotPasswordAcitivity.this);
        otpGenerateProgress.setCancelable(false);
        otpGenerateProgress.setCanceledOnTouchOutside(false);
        otpGenerateProgress.setMessage(msg);
        otpGenerateProgress.show();
    }

    private void hideProgress() {
        if (otpGenerateProgress != null)
            otpGenerateProgress.dismiss();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (countDownTimer != null)
            countDownTimer.cancel();
      /*  if (otpTimerHandler != null)
            otpTimerHandler.removeCallbacks(otpTimerThread);*/
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (countDownTimer != null)
            countDownTimer.cancel();
        /*if (otpTimerHandler != null)
            otpTimerHandler.removeCallbacks(otpTimerThread);*/
    }

    private void changePassword(final String mobileNo, final String newPassword) {
        showProgress("Changing password...");

        ForgotPasswordEndPoint apiCallable = SangeethaCareApiClient.getClient().create(ForgotPasswordEndPoint.class);
        apiCallable.changePwd(mobileNo, newPassword).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getStatus()) {
                            hideProgress();
                            Toast.makeText(ForgotPasswordAcitivity.this, "Password changed successfully! Please login to continue.", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(ForgotPasswordAcitivity.this, EmployeeLogin.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        } else {
                            hideProgress();
                            Toast.makeText(ForgotPasswordAcitivity.this, "Unable to change password, Please try later", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        hideProgress();
                        Toast.makeText(ForgotPasswordAcitivity.this, "Unable to change password, Please try later", Toast.LENGTH_SHORT).show();

                    }
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                hideProgress();
                Toast.makeText(ForgotPasswordAcitivity.this, "Unable to change password, Please try later", Toast.LENGTH_SHORT).show();
            }
        });
    }

}