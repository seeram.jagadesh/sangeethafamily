package com.sangeetha.employee.servify

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation

import com.sangeetha.employee.R
import com.sangeetha.employee.databinding.FragmentServifySummaryBinding
import com.sangeetha.employee.employee.servify.viewmodel.ServifyViewModel
import com.sangeetha.employee.servify.model.data.ServifyHistoryData
import com.sangeetha.employee.utils.AppPreference

class ServifySummaryFragment : Fragment(), View.OnClickListener {

    companion object {
        fun newInstance() = ServifySummaryFragment()
    }

    private lateinit var viewModel: ServifyViewModel
    private var storeId = "560"
    private lateinit var orderData: ServifyHistoryData
    var appPreference: AppPreference? = null
    var userToken: String = ""
    private lateinit var binding: FragmentServifySummaryBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentServifySummaryBinding.inflate(inflater, container, false)
        orderData = requireArguments().getParcelable<ServifyHistoryData>("orderData")!!
        appPreference = AppPreference(activity)

        userToken = appPreference?.userToken.toString()

        binding.progressBar.visibility = View.VISIBLE
        viewModel = ViewModelProvider(this).get(ServifyViewModel::class.java)
        viewModel.getDetails(userToken, orderData.id).observe(viewLifecycleOwner, Observer {
            if (it.status) {
                binding.progressBar.visibility = View.GONE
                binding.nestedScroll.visibility = View.VISIBLE
                orderData = it.data
                setData()
            } else {
                showMessage(it.message)
            }
        })
        return binding.root
    }

    private fun setData() {
        binding.svRefid.text = orderData.tradeReferenceId
        binding.svDate.text = orderData.created
        binding.svStatus.text = orderData.couponStatus
        if(orderData.couponStatus.equals("NOT USED",true)){
            binding.changeModelLayout.visibility=View.VISIBLE
            binding.cancelLayout.visibility=View.VISIBLE
        }else{
            binding.changeModelLayout.visibility=View.GONE
            binding.cancelLayout.visibility=View.GONE
        }
        binding.svTradeModel.text = orderData.tradeModel
        binding.svQuoteAmount.text = orderData.servifyAmount
        binding.svCusMake.text = orderData.make
        binding.svCusModel.text = orderData.model
        binding.svTradeRefId.text = orderData.tradeReferenceId
        binding.svTradeImei.text = orderData.imei
        binding.svCusName.text = orderData.customerName
        binding.svCusEmail.text = orderData.customerEmail
        binding.svCusNo.text = orderData.customerMobile
        binding.svStoreCode.text = orderData.storeCode
        binding.svStoreName.text = orderData.storeName
        binding.svStoreEmail.text = orderData.storeId
        binding.svEmpId.text = orderData.employeeId
        binding.svEmpName.text = orderData.employeeName
        binding.svAPXcode.text = orderData.couponCode
        binding.svChangeModel.setOnClickListener(this)
        binding.svCancelCoupon.setOnClickListener(this)
        binding.svCopyCoupon.setOnClickListener(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ServifyViewModel::class.java)

    }

    fun copyCode(coupon: String) {
        val clipBoard = context?.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clipData = ClipData.newPlainText("Coupon", coupon)
        clipBoard.setPrimaryClip(clipData)
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.svChangeModel -> {
                val bundle = Bundle()
                bundle.putParcelable("orderData", orderData)
                v?.let { Navigation.findNavController(it).navigate(R.id.action_servifyReportFragment_to_changeModelFragment, bundle) }
            }
            binding.svCancelCoupon -> {
                viewModel.cancelCoupon(userToken, orderData.id).observe(viewLifecycleOwner, Observer {
                    if (it.status) {
//                        viewModel.getCouponDetails(userToken, orderData.id).observe(viewLifecycleOwner, Observer {
//                            orderData = it
//                            setData()
//                        })

                        viewModel.getDetails(userToken, orderData.id).observe(viewLifecycleOwner, Observer {
                            if (it.status) {
                                orderData = it.data
                                setData()
                            }
                        })
                    } else {
                        showMessage(it.message)
                    }
                })
            }
            binding.svCopyCoupon -> {
                copyCode(binding.svAPXcode.text.toString())

            }
        }

    }

    private fun showMessage(msg: String) {
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show()
    }
}
