package com.sangeetha.employee.managerDiscount.manager_pojo;

import com.google.gson.annotations.SerializedName;

public class AlternativeMethodResponse {

    @SerializedName("apx_coupon_code")
    private String apxCouponCode;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private boolean status;

    public String getApxCouponCode() {
        return apxCouponCode;
    }

    public void setApxCouponCode(String apxCouponCode) {
        this.apxCouponCode = apxCouponCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "AlternativeMethodResponse{" +
                        "apx_coupon_code = '" + apxCouponCode + '\'' +
                        ",message = '" + message + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}