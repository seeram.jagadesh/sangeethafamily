package com.sangeetha.employee.mining.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FetchMiningData {


    @SerializedName("mining_details")
    @Expose
    private FetchMiningDetails miningDetails;
    @SerializedName("mining_time")
    @Expose
    private String miningTime;
    @SerializedName("mining_category_details")
    @Expose
    private CategoryModel categoryModel;




    public String getMiningTime() {
        return miningTime;
    }

    public void setMiningTime(String miningTime) {
        this.miningTime = miningTime;
    }

    public FetchMiningDetails getMiningDetails() {
        return miningDetails;
    }

    public void setMiningDetails(FetchMiningDetails miningDetails) {
        this.miningDetails = miningDetails;
    }

    public CategoryModel getCategoryModel() {
        return categoryModel;
    }

    public void setCategoryModel(CategoryModel categoryModel) {
        this.categoryModel = categoryModel;
    }
}
