package com.sangeetha.employee.empLogin;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.material.textfield.TextInputEditText;
import com.sangeetha.employee.BuildConfig;
import com.sangeetha.employee.CommonResponse;
import com.sangeetha.employee.SangeethaCareApiClient;
import com.sangeetha.employee.callRecorder.domain.SharedPreferencesSmsStorage;
import com.sangeetha.employee.callRecorder.extensions.ContextExtensionsKt;
import com.sangeetha.employee.employeeHome.EmployeeLanding;
import com.sangeetha.employee.R;
import com.sangeetha.employee.empLogin.loginModel.ASMData;
import com.sangeetha.employee.empLogin.loginModel.EmpData;
import com.sangeetha.employee.empLogin.loginModel.OnlineDeliveryData;
import com.sangeetha.employee.empLogin.loginModel.StoreData;
import com.sangeetha.employee.forgotPassword.ForgotPasswordAcitivity;
import com.sangeetha.employee.mining.ApiCallable;
import com.sangeetha.employee.onlineDelivery.DeliveryOrdersActivity;
import com.sangeetha.employee.utils.AppPreference;
import com.sangeetha.employee.utils.LanguageUtil;
import com.sangeetha.employee.utils.MyContextWrapper;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.READ_PHONE_STATE;

public class EmployeeLogin extends AppCompatActivity implements EmpLoginView {

    private static final int READ_PHONE_STATE_REQ_CODE = 65;
    private static final int REQUEST_CODE_PERMISSION = 100;
    private static final int REQUEST_PERMISSION_SETTING = 101;
    //    private TelephonyManager telephonyManager;
    private TextInputEditText empId, empPassword;
    private Button empLogin;
    private RelativeLayout progressBar;
    private EmpPresenter empPresenter;
    private String IMEI;
    Spinner modeSpinner;
    private RadioButton hindi, english;
    AppPreference appPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_login);
        appPreference = new AppPreference(EmployeeLogin.this);
        checkPermissions();
        empId = findViewById(R.id.emp_id);
        modeSpinner = findViewById(R.id.spinSelectMode);
        empPassword = findViewById(R.id.emp_password);
        empLogin = findViewById(R.id.signInButton);
        progressBar = findViewById(R.id.progressLinear);
        hindi = findViewById(R.id.hindiButton);
        english = findViewById(R.id.englishButton);
        TextView version = findViewById(R.id.version);
        version.setText(BuildConfig.VERSION_NAME + " (" + BuildConfig.VERSION_CODE + ")");
        empPresenter = new EmpPresenterImp(this);
        empLogin.setOnClickListener(v -> {
            if (modeSpinner.getSelectedItem().toString().equalsIgnoreCase("select mode")) {
                showMessage("Please select a mode of login");
            } else if (modeSpinner.getSelectedItem().toString().equalsIgnoreCase("portal")) {
                empPresenter.login(empId.getText().toString(), empPassword.getText().toString(), "");
            } else if (modeSpinner.getSelectedItem().toString().equalsIgnoreCase("delivery boy")) {
                empPresenter.delLogin(empId.getText().toString(), empPassword.getText().toString());
            }
        });

        modeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selected = parent.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        findViewById(R.id.forgotPassword).setOnClickListener(view -> {
            Intent intent = new Intent(EmployeeLogin.this, ForgotPasswordAcitivity.class);
            startActivity(intent);
        });

        hindi.setOnClickListener(v -> {
            appPreference.setLanguage("hi", getApplicationContext());
            startActivity(new Intent(this, EmployeeLogin.class));
            finish();
        });
        english.setOnClickListener(v -> {
            appPreference.setLanguage("eg", getApplicationContext());
            startActivity(new Intent(this, EmployeeLogin.class));
            finish();
        });
    }

    public void checkResponse() {
        ApiCallable apiCallable = SangeethaCareApiClient.getClient().create(ApiCallable.class);
        apiCallable.checkData().enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(@NotNull Call<CommonResponse> call, @NotNull Response<CommonResponse> response) {
                if (response.isSuccessful()) {
                    CommonResponse response1 = response.body();
                    if (response1.getStatus()) {
                        showMessage(response1.getMessage());
                    } else {
                        showMessage(response1.getMessage());
                    }
                } else {
                    showMessage("Error while sending your request");
                }
            }

            @Override
            public void onFailure(@NotNull Call<CommonResponse> call, @NotNull Throwable t) {
                showMessage("Failed to process your request.");
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        Locale languageType = LanguageUtil.getLanguageType(newBase);
        super.attachBaseContext(MyContextWrapper.wrap(newBase, languageType));
    }

    @Override
    public void onIdError(String error) {
        empId.setError(error);

    }

    @Override
    public void onPassError(String error) {
        empPassword.setError(error);

    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(EmployeeLogin.this, message, Toast.LENGTH_LONG).show();
    }

    @SuppressLint("HardwareIds")
    @Override
    public void loginSuccesses(EmpData data) {
        SharedPreferencesSmsStorage smsStorage =
                new SharedPreferencesSmsStorage(Objects.requireNonNull(ContextExtensionsKt.getPreferences(this)));
        JSONObject jsonObject = new JSONObject();

        JSONObject infoJson = new JSONObject();
        try {
            infoJson.put("id", data.getEmployeeId());
            infoJson.put("username", data.getEmpName());
            infoJson.put("email", data.getEmail());
            infoJson.put("designation", data.getRole());
            JSONObject deviceInfoJson = new JSONObject();
            deviceInfoJson.put("brand", Build.BRAND);
            deviceInfoJson.put("model", Build.MODEL);
            deviceInfoJson.put("deviceId", Settings.Secure.getString(
                    getContentResolver(),
                    Settings.Secure.ANDROID_ID));
            infoJson.put("deviceInfo", deviceInfoJson);
            jsonObject.put("info", infoJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        smsStorage.setUserInfo(jsonObject.toString());
        appPreference.saveEmpData(data);
        appPreference.setUserLoggedIn(true);
        moveTo();
    }

    @Override
    public void setStoreData(StoreData storeData) {
        appPreference.saveStoreData(storeData);
        appPreference.setUserLoggedIn(true);
        moveTo();
    }

    @Override
    public void setASMData(ASMData asmData) {
        appPreference.saveASMData(asmData);
        appPreference.setUserLoggedIn(true);
        moveTo();
    }

    @Override
    public void setOnlineDeliveryData(OnlineDeliveryData onlineDeliveryData) {
        appPreference.saveOnlineDeliveryData(onlineDeliveryData);
        appPreference.setUserLoggedIn(true);
        moveTo();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);

    }

    @SuppressLint("HardwareIds")
    @Override
    public void setToken(String token) {
        SharedPreferencesSmsStorage smsStorage =
                new SharedPreferencesSmsStorage(Objects.requireNonNull(ContextExtensionsKt.getPreferences(this)));
        smsStorage.setToken(token);

        appPreference.setUserToken(token);
    }

    @Override
    public void setCallRecordingEnable(Boolean isEnable) {
        SharedPreferencesSmsStorage smsStorage =
                new SharedPreferencesSmsStorage(Objects.requireNonNull(ContextExtensionsKt.getPreferences(this)));
        smsStorage.setCallRecordingFeatureEnable(isEnable);
    }

    @SuppressLint("HardwareIds")
    @Override
    public void deliveryBoyLogin(EmpData data) {
        SharedPreferencesSmsStorage smsStorage =
                new SharedPreferencesSmsStorage(Objects.requireNonNull(ContextExtensionsKt.getPreferences(this)));
        JSONObject jsonObject = new JSONObject();

        JSONObject infoJson = new JSONObject();
        try {
            infoJson.put("id", data.getEmployeeId());
            infoJson.put("username", data.getEmpName());
            infoJson.put("email", data.getEmail());
            infoJson.put("designation", data.getRole());
            JSONObject deviceInfoJson = new JSONObject();
            deviceInfoJson.put("brand", Build.BRAND);
            deviceInfoJson.put("model", Build.MODEL);
            deviceInfoJson.put("deviceId", Settings.Secure.getString(
                    getContentResolver(),
                    Settings.Secure.ANDROID_ID));
            infoJson.put("deviceInfo", deviceInfoJson);
            jsonObject.put("info", infoJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        smsStorage.setUserInfo(jsonObject.toString());
        appPreference.saveEmpData(data);
        appPreference.setUserLoggedIn(true);
        moveTo();
    }


    private void moveTo() {
        if (appPreference.getEmpData().getGroup().equalsIgnoreCase("delivery")) {
            Intent intent = new Intent(EmployeeLogin.this, DeliveryOrdersActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(EmployeeLogin.this, EmployeeLanding.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (appPreference.getLanguage(getApplicationContext()).equalsIgnoreCase("hi")) {
            hindi.setChecked(true);
        } else {
            english.setChecked(true);
        }
    }

    private void checkPermissions() {
        if (!(ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED)
                && !(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                && !(ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED)
                && !(ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                && !(ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                && !(ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
        ) {

            ActivityCompat.requestPermissions(this,
                    new String[]{READ_PHONE_STATE,
                            Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.RECORD_AUDIO
                            , Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
                            , Manifest.permission.CAMERA}, REQUEST_CODE_PERMISSION);
        } else {
            //getUserCurrentLocation();
        }
    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_PERMISSION) {
            for (int i = 0, len = permissions.length; i < len; i++) {
                String permission = permissions[i];
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    boolean showRationale = shouldShowRequestPermissionRationale(permission);
                    Log.e("permission", "" + permission + "  " + showRationale);
                    if (!showRationale) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                    } else if (Manifest.permission.ACCESS_COARSE_LOCATION.equals(permission)) {
                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
                        alertBuilder.setCancelable(true);
                        alertBuilder.setTitle("Permission Necessary");
                        alertBuilder.setMessage("Allow Permission To access the Functionality Of our App.");
                        AlertDialog alert = alertBuilder.create();
                        alert.show();
                        alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(EmployeeLogin.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_CODE_PERMISSION);

                                alert.hide();
                            }
                        });

                    }
                } else {
                    checkPermissions();
                }
            }
        }

    }

}
