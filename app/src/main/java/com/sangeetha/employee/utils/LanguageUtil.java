package com.sangeetha.employee.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.util.DisplayMetrics;

import androidx.annotation.RequiresApi;


import com.sangeetha.employee.SangeethaFamilyApp;

import java.util.Locale;

/**
 * Created by turbo on 2017/2/16.
 */

public class LanguageUtil {
    @RequiresApi(api = Build.VERSION_CODES.N)
    public static void changeLanguageType(Context context, Locale localelanguage) {
       // Log.i("=======", "context = " + context);
//        Resources resources = context.getResources();
        Resources resources = SangeethaFamilyApp.getContext().getResources();
        DisplayMetrics dm = resources.getDisplayMetrics();
        Configuration config = resources.getConfiguration();
        if (VersionUtils.isAfter24()) {
            config.setLocale(localelanguage);
        } else {
            config.locale = localelanguage;
            resources.updateConfiguration(config, dm);
        }
    }

    public static Locale getLanguageType(Context context) {
       // Log.i("=======", "context = " + context);
//        Resources resources = context.getResources();
        Resources resources = SangeethaFamilyApp.getContext().getResources();
        Configuration config = resources.getConfiguration();
        // 应用用户选择语言
        if (VersionUtils.isAfter24()) {
            return config.getLocales().get(0);
        } else {
            return config.locale;
        }
    }
}
