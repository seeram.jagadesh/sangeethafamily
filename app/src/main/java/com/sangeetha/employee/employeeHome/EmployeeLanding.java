package com.sangeetha.employee.employeeHome;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.fondesa.kpermissions.PermissionStatus;
import com.fondesa.kpermissions.extension.PermissionsBuilderKt;
import com.fondesa.kpermissions.extension.SendPermissionRequestWithListenerKt;
import com.fondesa.kpermissions.request.PermissionRequest;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.CancellationToken;
import com.google.android.gms.tasks.CancellationTokenSource;
import com.google.android.gms.tasks.OnTokenCanceledListener;
import com.sangeetha.employee.AdapterCallback;
import com.sangeetha.employee.CommonEndPoint;
import com.sangeetha.employee.CommonResponse;
import com.sangeetha.employee.R;
import com.sangeetha.employee.SangeethaCareApiClient;
import com.sangeetha.employee.appointments.AppointmentsListActivity;
import com.sangeetha.employee.callRecorder.MainActivity;
import com.sangeetha.employee.callRecorder.domain.SharedPreferencesSmsStorage;
import com.sangeetha.employee.callRecorder.domain.model.Store;
import com.sangeetha.employee.callRecorder.domain.model.StoreResponse;
import com.sangeetha.employee.callRecorder.extensions.ContextExtensionsKt;
import com.sangeetha.employee.callRecorder.network.CUGApiClient;
import com.sangeetha.employee.callRecorder.services.CallRecordService;
import com.sangeetha.employee.callRecorder.services.CallRecorderAccessibilityService;
import com.sangeetha.employee.callRecorder.services.ServiceWorker;
import com.sangeetha.employee.callRecorder.utils.AppUtils;
import com.sangeetha.employee.callrecord.CallRecord;
import com.sangeetha.employee.callrecord.MyCallRecordReceiver;
import com.sangeetha.employee.cashify.CashifyDashboardActivity;
import com.sangeetha.employee.checkStock.StockCheckActivity;
import com.sangeetha.employee.empAudit.EmpAudit;
import com.sangeetha.employee.empLogin.EmployeeLogin;
import com.sangeetha.employee.employeeHome.adapter.IconSection;
import com.sangeetha.employee.employeeHome.adapter.PortalIconValues;
import com.sangeetha.employee.employeeHome.presenters.HomePresenter;
import com.sangeetha.employee.employeeHome.presenters.HomePresenterImpl;
import com.sangeetha.employee.managerDiscount.storeASM.StoreASMActivity;
import com.sangeetha.employee.managerDiscount.storeManager.StoreMainActivity;
import com.sangeetha.employee.mining.ApiCallable;
import com.sangeetha.employee.mining.MiningActivity;
import com.sangeetha.employee.onlineDelivery.DeliveryOrdersActivity;
import com.sangeetha.employee.peopleTracking.PeopleTrackingActivity;
import com.sangeetha.employee.servify.ServifyMainActivity;
import com.sangeetha.employee.storeOrders.tableActivity;
import com.sangeetha.employee.utils.AppConstants;
import com.sangeetha.employee.utils.AppPreference;
import com.sangeetha.employee.utils.LanguageUtil;
import com.sangeetha.employee.utils.MyContextWrapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class EmployeeLanding extends AppCompatActivity implements AdapterCallback, HomeView, IconSection.ClickListener {

    private ImageView audit, imgLogout;
    private AppPreference appPreference;
    private int[] icons = {R.drawable.ic_manpower,
            R.drawable.ic_vas_01,
            R.drawable.ic_manager_dicount_admin_01,
            R.drawable.ic_tracking,
            R.drawable.ic_online_delivery,
            R.drawable.ic_data_mining};
    private String[] titles = {"Manpower", "VAS", "Manager Discount", "People Tracking", "Online Delivery", "Data Mining"};
    private CallRecord callRecord;

    String userToken;
    HomePresenter homePresenter;
    RecyclerView homeRecycler;
    SectionedRecyclerViewAdapter sectionedAdapter;
    LinearLayout invoiceLayout;
    LinearLayout stockLayout;
    TextView btnManageCallRecording;
    private View checkInLayout;
    private View checkOutLayout;

    private FusedLocationProviderClient fusedLocationClient;

    private final String[] permissionsArr = {
            Manifest.permission.RECEIVE_SMS,
            Manifest.permission.READ_SMS,
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.READ_CALL_LOG,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_employee_landing);
        appPreference = new AppPreference(this);
        //getActionBar().setIcon(R.drawable.ic_logout);

        sectionedAdapter = new SectionedRecyclerViewAdapter();

        imgLogout = findViewById(R.id.imgLogout);
        invoiceLayout = findViewById(R.id.invoiceLayout);
        stockLayout = findViewById(R.id.stockLayout);
        homeRecycler = findViewById(R.id.dashboard_rv);
        btnManageCallRecording = findViewById(R.id.btnManageCallRecording);
        homeRecycler.setLayoutManager(new GridLayoutManager(this, 2));
//        homeRecycler.setHasFixedSize(true);
//        homeRecycler.setAdapter(new EmpDashboardAdapter(icons, titles, this));

        homePresenter = new HomePresenterImpl(this);
        //findViewById(R.id.auditButton).setOnClickListener(v -> startActivity(new Intent(EmployeeLanding.this, EmpAudit.class)));
//        addToken();

//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
//            if (appPreference.getStoreData() != null) {
//                boolean isRunning = AppConstants.checkServiceRunning(EmployeeLanding.this, RecordService.class);
//                if (!isRunning)
//                    startService(new Intent(EmployeeLanding.this, RecordService.class));
//            }
//        }

        userToken = appPreference.getUserToken();
        imgLogout.setOnClickListener(v -> {
            logoutPopUp();
        });

        homePresenter.portalIcons(userToken);
//        Toast.makeText(this, appPreference.getUserToken(), Toast.LENGTH_SHORT).show();
        callRecord = new CallRecord.Builder(this)
                .setRecordFileName("SangeethaRecorderFile")
                .setRecordDirName("SangeethaRecorder")
                .setAudioSource(MediaRecorder.AudioSource.VOICE_COMMUNICATION)
                .setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
                .setAudioEncoder(MediaRecorder.AudioEncoder.AAC)
                .setShowSeed(true)
                .build();
        callRecord.changeReceiver(new MyCallRecordReceiver(callRecord));
        callRecord.enableSaveFile();
        callRecord.startCallReceiver();

        stockLayout.setVisibility(View.GONE);
        stockLayout.setOnClickListener(v -> {
            startActivity(new Intent(this, StockCheckActivity.class));
        });
        invoiceLayout.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(AppConstants.GetInvoice));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setPackage("com.android.chrome");
            try {
                startActivity(intent);
            } catch (ActivityNotFoundException ex) {
                // Chrome browser presumably not installed so allow user to choose instead
                intent.setPackage(null);
                startActivity(intent);
            }
        });
        btnManageCallRecording.setOnClickListener(view -> startActivity(new Intent(getApplicationContext(), MainActivity.class)));

        SharedPreferencesSmsStorage smsStorage =
                new SharedPreferencesSmsStorage(Objects.requireNonNull(ContextExtensionsKt.getPreferences(this)));
        if (smsStorage.isCallRecordingFeatureEnable() && checkAllPermissionsAccepted()) {
            startCallRecordingServices();
        }

        initLocationServices();
        checkInLayout = findViewById(R.id.checkLayout);
        checkOutLayout = findViewById(R.id.checkOutLayout);
        findViewById(R.id.checkLayout).setOnClickListener(view -> postCheckInAndCheckOut());
        findViewById(R.id.checkOutLayout).setOnClickListener(view -> postCheckOut());
    }

    private void initLocationServices() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
    }

    private void postCheckOut() {
        if (checkLocationPermission()) {
            showProgress("Getting current location!!");
            fusedLocationClient.getCurrentLocation(LocationRequest.PRIORITY_HIGH_ACCURACY, new CancellationToken() {
                @NonNull
                @Override
                public CancellationToken onCanceledRequested(@NonNull OnTokenCanceledListener onTokenCanceledListener) {
                    hideProgress();
                    return new CancellationTokenSource().getToken();
                }

                @Override
                public boolean isCancellationRequested() {
                    return false;
                }
            }).addOnSuccessListener(location -> {
                hideProgress();
                if (location == null) {
                    return;
                }
                postCheck(null, "check_out", location);
            });
        } else {
            PermissionRequest permissionRequest = PermissionsBuilderKt.permissionsBuilder(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
            ).build();
            SendPermissionRequestWithListenerKt.send(permissionRequest, permissionStatuses -> {
                Timber.d("requestPermissions : result = $result");
                return null;
            });
        }
    }

    private void postCheckInAndCheckOut() {
        if (checkLocationPermission()) {
            showProgress("Getting current location!!");
            fusedLocationClient.getCurrentLocation(LocationRequest.PRIORITY_HIGH_ACCURACY, new CancellationToken() {
                @NonNull
                @Override
                public CancellationToken onCanceledRequested(@NonNull OnTokenCanceledListener onTokenCanceledListener) {
                    hideProgress();
                    return new CancellationTokenSource().getToken();
                }

                @Override
                public boolean isCancellationRequested() {
                    return false;
                }
            }).addOnSuccessListener(location -> {
                hideProgress();
                if (location == null) {
                    return;
                }
                getStores("check_in", location);
            });
        } else {
            PermissionRequest permissionRequest = PermissionsBuilderKt.permissionsBuilder(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
            ).build();
            SendPermissionRequestWithListenerKt.send(permissionRequest, permissionStatuses -> {
                Timber.d("requestPermissions : result = $result");
                return null;
            });
        }
    }

    private void postCheck(Store store, String value, Location location) {
        if (location == null) {
            hideProgress();
            return;
        }
        try {
            JSONArray dataArray = new JSONArray();
            JSONObject callJsonObj = new JSONObject();
            callJsonObj.put("lat", location.getLatitude());
            callJsonObj.put("long", location.getLongitude());

            if (store != null) {
                JSONObject storeObject = new JSONObject();
                storeObject.put("store_id", store.getId());
                storeObject.put("store_name", store.getStore_name());
                storeObject.put("store_code", store.getStore_code());
                callJsonObj.put("store", storeObject);
            }

            dataArray.put(callJsonObj);
            SharedPreferencesSmsStorage smsStorage =
                    new SharedPreferencesSmsStorage(
                            Objects.requireNonNull(ContextExtensionsKt.getPreferences(EmployeeLanding.this)));
            showProgress("Checking..");
            JSONObject infoObject = new JSONObject(Objects.requireNonNull(smsStorage.getUserInfo()));
            String token = smsStorage.getToken();
            CUGApiClient.INSTANCE.getService().postCheckIn(
                    "Bearer " + token,
                    value,
                    dataArray,
                    infoObject
            ).enqueue(new Callback<>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    String checkInMessage;
                    if (value.equalsIgnoreCase("check_in")) {
                        checkInLayout.setVisibility(View.GONE);
                        checkOutLayout.setVisibility(View.VISIBLE);
                        smsStorage.setCheckIn(SharedPreferencesSmsStorage.CHECK_OUT_VALUE);
                        checkInMessage = "Check In successfully";
                    } else {
                        checkInLayout.setVisibility(View.VISIBLE);
                        checkOutLayout.setVisibility(View.GONE);
                        smsStorage.setCheckIn(SharedPreferencesSmsStorage.CHECK_IN_VALUE);
                        checkInMessage = "Check Out successfully";
                    }
                    Toast.makeText(EmployeeLanding.this, checkInMessage, Toast.LENGTH_LONG).show();
                    hideProgress();
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    hideProgress();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getStores(String value, Location location) {
        SharedPreferencesSmsStorage smsStorage =
                new SharedPreferencesSmsStorage(
                        Objects.requireNonNull(ContextExtensionsKt.getPreferences(EmployeeLanding.this)));
        showProgress();
        String token = smsStorage.getToken();
        CUGApiClient.INSTANCE.getCheckInService().getStores(token).enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<StoreResponse> call, Response<StoreResponse> response) {
                hideProgress();
                showStoresDialog(value, location, response.body().getData());
            }

            @Override
            public void onFailure(Call<StoreResponse> call, Throwable t) {
                hideProgress();
            }
        });
    }

    private ArrayAdapter<String> itemsAdapter;

    private void showStoresDialog(String value, Location location, List<Store> data) {
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose an store");

        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_stores, null);
        builder.setView(dialogView);

        ArrayList<String> storeNames = new ArrayList<>();
        for (Store store : data) {
            storeNames.add(store.getStore_name());
        }
        AlertDialog dialog = builder.create();

        itemsAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, storeNames);

        ListView listStores = dialogView.findViewById(R.id.list_stores);
        listStores.setAdapter(itemsAdapter);

        listStores.setOnItemClickListener((adapterView, view, i, l) -> {
            for (Store store : data) {
                if (store.getStore_name().equals(storeNames.get(i))) {
                    dialog.dismiss();
                    postCheck(store, value, location);
                    break;
                }
            }
        });

        EditText editSearch = dialogView.findViewById(R.id.edit_search);

        editSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                storeNames.clear();
                for (Store store : data) {
                    if (store.getStore_name().toLowerCase().startsWith(editable.toString().toLowerCase())) {
                        storeNames.add(store.getStore_name());
                    }
                }
                itemsAdapter =
                        new ArrayAdapter<>(EmployeeLanding.this, android.R.layout.simple_list_item_1, storeNames);
                listStores.setAdapter(itemsAdapter);
            }
        });

        dialog.show();
    }

    private boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.emp_menu_bar, menu);
        return true;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        Locale languageType = LanguageUtil.getLanguageType(newBase);
        super.attachBaseContext(MyContextWrapper.wrap(newBase, languageType));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        logoutPopUp();
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
//        Toast.makeText(this, "Home screen", Toast.LENGTH_SHORT).show();
//        checkAllPermissions(null);
        SharedPreferencesSmsStorage smsStorage =
                new SharedPreferencesSmsStorage(Objects.requireNonNull(ContextExtensionsKt.getPreferences(this)));
        if (smsStorage.isCallRecordingFeatureEnable()) {
            if (checkAllPermissionsAccepted()) {
                btnManageCallRecording.setTextColor(Color.WHITE);
            } else {
                btnManageCallRecording.setTextColor(Color.RED);
                requestCallRecordingPermissions();
            }
        }
    }

    private void checkAllPermissions(Function1<Boolean, Unit> block) {
        if (!checkAllPermissionsAccepted()) {
            btnManageCallRecording.setTextColor(Color.RED);
            if (block != null) {
                block.invoke(false);
            }
        } else {
            btnManageCallRecording.setTextColor(Color.WHITE);
            if (block != null) {
                block.invoke(true);
            }
            startCallRecordingServices();
        }
    }

    private void startCallRecordingServices() {
        startServiceViaWorker();
        startForegroundService(new Intent(this, CallRecordService.class));
    }

    private void startServiceViaWorker() {
        Timber.d("startServiceViaWorker called");
        String UNIQUE_WORK_NAME = "CUG StartMyServiceViaWorker";
        WorkManager workManager = WorkManager.getInstance(this);

        // As per Documentation: The minimum repeat interval that can be defined is 15 minutes
        // (same as the JobScheduler API), but in practice 15 doesn't work. Using 16 here
        PeriodicWorkRequest request = new PeriodicWorkRequest.Builder(
                ServiceWorker.class,
                16,
                TimeUnit.MINUTES
        ).build();

        // to schedule a unique work, no matter how many times app is opened i.e. startServiceViaWorker gets called
        // do check for AutoStart permission
        workManager.enqueueUniquePeriodicWork(
                UNIQUE_WORK_NAME,
                ExistingPeriodicWorkPolicy.KEEP,
                request
        );
    }

    private void requestCallRecordingPermissions() {
        PermissionRequest permissionRequest = PermissionsBuilderKt.permissionsBuilder(this, Manifest.permission.RECEIVE_SMS,
                Manifest.permission.READ_SMS,
                Manifest.permission.READ_CONTACTS,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.READ_CALL_LOG,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION).build();

        SendPermissionRequestWithListenerKt.send(permissionRequest, new Function1<List<? extends PermissionStatus>, Unit>() {
            @Override
            public Unit invoke(List<? extends PermissionStatus> result) {
                Timber.d("requestPermissions : result = $result");
//                if (PermissionStatusKt.allGranted(result)) {
//                    Timber.d("requestPermissions : all permissions are granted.");
//                    btnManageCallRecording.setTextColor(Color.WHITE);
//                } else {
//                    btnManageCallRecording.setTextColor(Color.RED);
//                }
                checkAllPermissions(null);
                requestAdditionalPermissions();
                return null;
            }
        });
    }

    private boolean checkAllPermissionsAccepted() {
        for (String permission : permissionsArr) {
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED
            ) {
                return false;
            }
        }
        if (!AppUtils.canDrawOverlays(this)) {
            return false;
        } else if (!AppUtils.isAccessibilityServiceEnabled(
                this,
                CallRecorderAccessibilityService.class)
        ) {
            return false;
        }
        return true;
    }


    private void requestAdditionalPermissions() {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle("Alert");
        alertBuilder.setMessage("Please grant all the permissions to enable the Call Recording.");
        alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (!AppUtils.canDrawOverlays(getApplicationContext())) {
                    requestDrawOverlayPermission();
                } else if (!AppUtils.isAccessibilityServiceEnabled(
                        getApplicationContext(),
                        CallRecorderAccessibilityService.class
                )
                ) {
                    Intent openSettings = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
                    openSettings.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(openSettings);
                }
            }
        });
        alertBuilder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alert = alertBuilder.create();
        alert.show();
        Timber.d("requestAdditionalPermissions");
    }

    private void requestDrawOverlayPermission() {
        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
        intent.setData(Uri.parse("package:$packageName"));
        drawOverOtherAppsLauncher.launch(intent);
    }

    private ActivityResultLauncher drawOverOtherAppsLauncher =
            registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {

                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        requestAdditionalPermissions();
                    }
                }
            });

//    {
//        result -> {
//            if (result.resultCode == Activity.RESULT_OK) {
//                requestAdditionalPermissions();
//            }
//        }
//    }

    private void moveToLoginScreen() {
        Intent loginIntent = new Intent(EmployeeLanding.this, EmployeeLogin.class);
        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
    }

    private void logoutPopUp() {
        final Dialog dialog = new Dialog(EmployeeLanding.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.logout_pop_up);
        dialog.setCancelable(true);
        dialog.findViewById(R.id.yes_btn_logout_popup).setOnClickListener(v -> {
            dialog.dismiss();
            appPreference.clearAppPreference();
            moveToLoginScreen();
        });

        dialog.findViewById(R.id.no_btn_logout_popup).setOnClickListener(v -> dialog.dismiss());

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public void onClickCallback(int position) {
        switch (position) {
            case 0:
                if (appPreference.getEmpData() != null) {
                    startActivity(new Intent(EmployeeLanding.this, EmpAudit.class));
                } else {
                    Toast.makeText(EmployeeLanding.this, "You are not authorised.", Toast.LENGTH_LONG).show();
                }

                break;
            case 1:
               /* if (appPreference.getASMData() != null) {
                    startActivity(new Intent(EmployeeLanding.this, ClaimPending.class));
                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                } else {
                    Toast.makeText(EmployeeLanding.this, "You are not authorised.", Toast.LENGTH_LONG).show();
                }*/
                Toast.makeText(EmployeeLanding.this, "You are not authorised.", Toast.LENGTH_LONG).show();
                break;

            case 2:
                if (appPreference.getASMData() != null) {
                    startActivity(new Intent(EmployeeLanding.this, StoreASMActivity.class));
                } else if (appPreference.getStoreData() != null) {
                    startActivity(new Intent(EmployeeLanding.this, StoreMainActivity.class));

                } else {
                    Toast.makeText(EmployeeLanding.this, "You are not authorised.", Toast.LENGTH_LONG).show();
                }
                break;
            case 3:
                if (appPreference.getEmpData() != null || appPreference.getASMData() != null) {
                    startActivity(new Intent(EmployeeLanding.this, PeopleTrackingActivity.class));
                } else {
                    Toast.makeText(EmployeeLanding.this, "You are not authorised.", Toast.LENGTH_LONG).show();
                }
                break;
            case 4:
                if (appPreference.getOnlineDeliveryData() != null) {
                    startActivity(new Intent(EmployeeLanding.this, DeliveryOrdersActivity.class));
                } else {
                    Toast.makeText(EmployeeLanding.this, "You are not authorised.", Toast.LENGTH_LONG).show();
                }
                break;
            case 5:
                if (appPreference.getStoreData() != null) {
                    startActivity(new Intent(EmployeeLanding.this, MiningActivity.class));
                } else {
                    Toast.makeText(EmployeeLanding.this, "You are not authorised.", Toast.LENGTH_LONG).show();
                }
                break;

            case 6:
                if (appPreference.getStoreData() != null) {
                    startActivity(new Intent(EmployeeLanding.this, ServifyMainActivity.class));
                } else {
                    Toast.makeText(EmployeeLanding.this, "You are not authorised.", Toast.LENGTH_LONG).show();
                }
                break;
            case 7:
                if (appPreference.getStoreData() != null) {
                    startActivity(new Intent(EmployeeLanding.this, CashifyDashboardActivity.class));
                } else {
                    Toast.makeText(EmployeeLanding.this, "You are not authorised.", Toast.LENGTH_LONG).show();
                }
                break;

        }

    }

    private void addToken() {
        CommonEndPoint apiCallable = SangeethaCareApiClient.getClient().create(CommonEndPoint.class);
        if (appPreference.getToken() != null) {
            Timber.e(appPreference.getToken());
            String empId = null;
            if (appPreference.getASMData() != null) {
                empId = appPreference.getASMData().getEmployeeId();
            } else if (appPreference.getEmpData() != null) {
                empId = appPreference.getEmpData().getEmployeeId();
            }
            if (empId != null) {
                apiCallable.addNotificationToken(empId,
                        appPreference.getToken()).enqueue(new Callback<CommonResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<CommonResponse> call, @NonNull Response<CommonResponse> response) {

                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                if (response.body().getStatus()) {
                                    Timber.tag("EMP TOKEN ").e("Added");
                                    appPreference.clearToken();
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {

                    }
                });
            }
        }

    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    ProgressDialog pDialog;

    @Override
    public void showProgress() {
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Getting your data");
        pDialog.show();
    }

    @Override
    public void showProgress(String message) {
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage(message);
        pDialog.show();
    }

    @Override
    public void hideProgress() {
        if (pDialog != null) {
            pDialog.dismiss();
        }
    }

    @Override
    public void getPortalIcons(JSONObject data) {
        hideProgress();
        Iterator<String> iter = data.keys();

        while (iter.hasNext()) {
            String key = iter.next();
            try {
                Object value = data.get(key);
                JSONArray jsonArray = new JSONArray(value.toString());
                sectionedAdapter.addSection(new IconSection(key, getIconList(jsonArray), this));
                final GridLayoutManager glm = new GridLayoutManager(this, 2);
                glm.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                    @Override
                    public int getSpanSize(final int position) {
                        if (sectionedAdapter.getSectionItemViewType(position) == SectionedRecyclerViewAdapter.VIEW_TYPE_HEADER) {
                            return 2;
                        }
                        return 1;
                    }
                });
                homeRecycler.setLayoutManager(glm);
                homeRecycler.setAdapter(sectionedAdapter);

            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                showMessage("PI " + e.getMessage());
            }
        }
    }

    @Override
    public void onHeaderMoreButtonClicked(@NonNull IconSection section, int itemAdapterPosition) {

    }

    @Override
    public void onItemRootViewClicked(@NonNull IconSection section, int itemAdapterPosition,
                                      PortalIconValues iconValues, String title) {
        goToScreen(title, iconValues.getName());
    }

    public List<PortalIconValues> getIconList(JSONArray jsonArray) throws JSONException {
        final List<PortalIconValues> movieList = new ArrayList<>();

        for (int f = 0; f < jsonArray.length(); f++) {
            String name = jsonArray.getJSONObject(f).getString("name");
            String image = jsonArray.getJSONObject(f).getString("image");
            String path = jsonArray.getJSONObject(f).getString("path");
            if (name.equalsIgnoreCase("Check In")) {
                checkInLayout.setVisibility(View.VISIBLE);
            } else {
                // checkInLayout.setVisibility(View.GONE);
                movieList.add(new PortalIconValues(name, path, image));
            }
        }

        return movieList;
    }

    void goToScreen(String title, String name) {
        appPreference.setTitle(title);
        if (name.equalsIgnoreCase("manager discount")) {
            if (title.equalsIgnoreCase("store")) {
                startActivity(new Intent(EmployeeLanding.this, StoreMainActivity.class));
            } else if (title.equalsIgnoreCase("asm")) {
                startActivity(new Intent(EmployeeLanding.this, StoreASMActivity.class));
            }
        } else if (name.equalsIgnoreCase("data mining")) {
            startActivity(new Intent(EmployeeLanding.this, MiningActivity.class));
        } else if (name.equalsIgnoreCase("audit")) {
            startActivity(new Intent(EmployeeLanding.this, EmpAudit.class));
        } else if (name.equalsIgnoreCase("servify")) {
            startActivity(new Intent(EmployeeLanding.this, ServifyMainActivity.class));
        } else if (name.equalsIgnoreCase("E-Commerce")) {
            // startActivity(new Intent(EmployeeLanding.this, tableActivity.class));

            getBranchWiseDetails();
        } else if (name.equalsIgnoreCase("stock")) {
            startActivity(new Intent(this, AppointmentsListActivity.class));
//            startActivity(new Intent(this, StockCheckActivity.class));
            //getBranchWiseDetails();
        }
    }


    void getBranchWiseDetails() {
        showProgress();
        ApiCallable apiCallable = SangeethaCareApiClient.getClient().create(ApiCallable.class);
        apiCallable.getBranchWiseDetails(appPreference.getUserToken()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful()) {
                    hideProgress();
                    assert response.body() != null;
                    JSONObject data = null;
                    try {
                        String responseBody = response.body().string();
                        data = new JSONObject(responseBody);
                        boolean status = data.getBoolean("status");
                        if (status) {
                            JSONObject dataTable = data.getJSONObject("data");
                            JSONObject BranchData = dataTable.getJSONObject("branch_wise");
                            startActivity(new Intent(EmployeeLanding.this, tableActivity.class)
                                    .putExtra("JSON", BranchData.toString()));
                        } else {
                            String message = data.getString("message");
                            showMessage(message);
                        }
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    hideProgress();
                    showMessage("Error while processing your request.");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideProgress();
                showMessage("Failed to process data.");
            }
        });
    }
}
