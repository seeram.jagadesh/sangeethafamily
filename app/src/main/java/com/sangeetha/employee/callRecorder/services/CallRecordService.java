package com.sangeetha.employee.callRecorder.services;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.room.Room;

import com.sangeetha.employee.R;
import com.sangeetha.employee.callRecorder.MainActivity;
import com.sangeetha.employee.callRecorder.domain.CugSMS;
import com.sangeetha.employee.callRecorder.domain.SMSObserver;
import com.sangeetha.employee.callRecorder.domain.SharedPreferencesSmsStorage;
import com.sangeetha.employee.callRecorder.domain.model.AudioFileUploadResponse;
import com.sangeetha.employee.callRecorder.domain.room.CUGDao;
import com.sangeetha.employee.callRecorder.domain.room.CUGDatabase;
import com.sangeetha.employee.callRecorder.domain.room.model.CUGMessage;
import com.sangeetha.employee.callRecorder.network.CUGApiClient;
import com.sangeetha.employee.callRecorder.receiver.CallRecordReceiver;
import com.sangeetha.employee.callRecorder.receiver.ServiceRestartReceiver;
import com.sangeetha.employee.callRecorder.utils.AppUtils;
import com.sangeetha.employee.callRecorder.utils.ConnectionType;
import com.sangeetha.employee.callRecorder.utils.NetworkMonitorUtil;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.Priority;
import com.sangeetha.employee.callRecorder.extensions.ContextExtensionsKt;
import com.sangeetha.employee.utils.FileUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

import cafe.adriel.androidaudioconverter.AndroidAudioConverter;
import cafe.adriel.androidaudioconverter.callback.IConvertCallback;
import cafe.adriel.androidaudioconverter.model.AudioFormat;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class CallRecordService extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    public static final String TAG = CallRecordService.class.getSimpleName();
    private static final String ACTION_IN = "android.intent.action.PHONE_STATE";
    private static final String ACTION_OUT = "android.intent.action.NEW_OUTGOING_CALL";
    public static final String ACTION_UPDATE_CALL = "actionUpdateCall";

    private CallRecordReceiver callReceiver;

    //    public static NotificationManager notificationManager;
    public static String CHANNEL_ID = "channel1";
    public static String CHANNEL_NAME = "Records";
    public static final String CHANNEL1_ID = "FGServiceChannel";
    public static final int FG_NOTIFICATION_ID = 111;
    public static int notificationId = 1;

    private WindowManager windowManager;
    private RelativeLayout removeView;
    private ImageView removeImg;
    private RelativeLayout chatheadView;
    private ImageView chatheadImg;
    private Point szWindow = new Point();
    private int x_init_cord, y_init_cord, x_init_margin, y_init_margin;
    private LinearLayout txtView;
    private TextView txt1;
    private LinearLayout txt_linearlayout;
    private boolean isLeft = true;

    private boolean isRunning = false;
    private FusedLocationProviderApi fusedLocationApi = LocationServices.FusedLocationApi;
    private ContinuousThread thread;

    private LocationRequest locationRequest;
    private LocationManager locationManager;
    private GoogleApiClient apiClient;
    private UpdatesReceiver updatesReceiver = new UpdatesReceiver();

    private Location currentLocation;

    Handler myHandler = new Handler();
    Runnable myRunnable = new Runnable() {

        @Override
        public void run() {
            if (txtView != null) {
                txtView.setVisibility(View.GONE);
            }
        }
    };
    private int delayInTime;

    @Override
    public void onCreate() {
        super.onCreate();
//        startForegroundService();

        initSmsObserver();
        thread = new ContinuousThread();
        initLocationRequest();

        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
        localBroadcastManager.registerReceiver(updatesReceiver, new IntentFilter(ACTION_UPDATE_CALL));


        NetworkMonitorUtil networkMonitorUtil = new NetworkMonitorUtil(this);
        networkMonitorUtil.register();

        networkMonitorUtil.result = (Function2<Boolean, ConnectionType, Unit>) (isAvailable, connectionType) -> {
            if (isAvailable) {
                sendConnectivityCheck("internet_on", connectionType.name());
            } else {
                sendConnectivityCheck("internet_off", null);
            }
            return null;
        };
    }

  /*  private void startForegroundService() {
        createNotificationChannel();
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, PendingIntent.FLAG_IMMUTABLE | PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("CUG Recording Service")
                .setContentText("Start")
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentIntent(pendingIntent)
                .setAutoCancel(false)
                .setOngoing(true)
                .setCategory(Notification.CATEGORY_CALL)
                .build();
        startForeground(notificationId, notification);
    }*/

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Recording Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }

    private void initSmsObserver() {
        CUGDao cugDao = Room.databaseBuilder(
                this,
                CUGDatabase.class,
                CUGDatabase.DATABASE_NAME
        ).build().cugDao();

        SharedPreferences preferences = ContextExtensionsKt.getPreferences(this);
        SharedPreferencesSmsStorage sharedPreferencesSmsStorage = new SharedPreferencesSmsStorage(Objects.requireNonNull(preferences));
        // start the sms service
        SMSObserver smsObserver = new SMSObserver(getContentResolver(),
                sharedPreferencesSmsStorage,
                new Handler(Looper.getMainLooper()),
                cugSMS -> {
                    if (!sharedPreferencesSmsStorage.isSmsTrackingEnable()) {
                        return null;
                    }
                    if (ContextExtensionsKt.isNetworkAvailable(this)) {
                        sendSmsToBackend(cugSMS, sharedPreferencesSmsStorage);
                    } else {
                        // upload to a service api
                        cugDao.insert(new CUGMessage(
                                cugSMS.getId(),
                                cugSMS.getMessage(),
                                cugSMS.getAddress(),
                                cugSMS.getType(),
                                cugSMS.getDate()
                        ), AppUtils.INSTANCE.getContinuation(
                                (tokenResult, throwable) -> {
                                    System.out.println("Coroutines finished");
                                    System.out.println("Result: " + tokenResult);
                                    System.out.println("Exception: " + throwable);
                                }
                        ));
                    }
                    return null;
                }
        );
        getContentResolver().registerContentObserver(Uri.parse("content://sms/"),
                true, smsObserver);
    }

    private void sendSmsToBackend(CugSMS cugSMS, SharedPreferencesSmsStorage sharedPreferencesSmsStorage) {
        if (!sharedPreferencesSmsStorage.isCallRecordingFeatureEnable()) {
            return;
        }
        JSONArray dataArray = new JSONArray();
        String type;
        if (cugSMS.getType().equals("1")) {
            type = "sms_received";
        } else {
            type = "sms_sent";
        }
        try {
            JSONObject callJsonObj = new JSONObject();
            callJsonObj.put("sender_no", cugSMS.getAddress());
            callJsonObj.put("content", cugSMS.getMessage());
            dataArray.put(callJsonObj);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject infoObject = null;
        try {
            infoObject = new JSONObject(Objects.requireNonNull(sharedPreferencesSmsStorage.getUserInfo()));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        CUGApiClient.INSTANCE.getService().smsPost("Bearer " + sharedPreferencesSmsStorage.getToken()
                , type, dataArray, Objects.requireNonNull(infoObject)).enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 201 || response.code() == 200) {
                    Timber.d("Successfully Inserted");
                } else {
                    Timber.d("Error Occurred");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Timber.d("onFailure");
            }
        });
    }

    private void initLocationRequest() {
        if (!isRunning) {
            isRunning = true;
            thread.start();
        }
        SharedPreferences preferences = ContextExtensionsKt.getPreferences(this);
        SharedPreferencesSmsStorage sharedPreferencesSmsStorage = new SharedPreferencesSmsStorage(Objects.requireNonNull(preferences));
        delayInTime = sharedPreferencesSmsStorage.getIntervalTime();
        locationRequest = new LocationRequest.Builder(Priority.PRIORITY_HIGH_ACCURACY,
                sharedPreferencesSmsStorage.getIntervalTime())
                .setWaitForAccurateLocation(false)
                .build();
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        apiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        try {
            apiClient.connect();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    class ContinuousThread extends Thread {
        @Override
        public void run() {
            super.run();
            while (isRunning) {
                try {
                    sleep(delayInTime);
                    sendLocationToBackend();
                } catch (Exception exception) {
                    isRunning = false;
                }
            }
        }
    }

//    @Override
//    public void onAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
//        Log.d(TAG, "onAccessibilityEvent : type = " + accessibilityEvent.getEventType() + "  action = " + accessibilityEvent.getAction());
//    }
//
//    @Override
//    public void onInterrupt() {
//        Log.d(TAG, "onInterrupt");
//    }

//    @Override
//    public IBinder onBind(Intent arg0) {
//        return null;
//    }

    @Override
    public void onDestroy() {
//        this.unregisterReceiver(callReceiver);
        stopForeground(true);
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(updatesReceiver);
        // call MyReceiver which will restart this service via a worker
        Intent broadcastIntent = new Intent(this, ServiceRestartReceiver.class);
        sendBroadcast(broadcastIntent);

        super.onDestroy();
        Timber.d("onDestroy");
        if (chatheadView != null) {
            windowManager.removeView(chatheadView);
        }

        if (txtView != null) {
            windowManager.removeView(txtView);
        }

        if (removeView != null) {
            windowManager.removeView(removeView);
        }

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Timber.d("onStartCommand");
        setupNotificationChannel();
        createFGNotification();
        final IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_OUT);
        filter.addAction(ACTION_IN);
        this.callReceiver = new CallRecordReceiver();
//        this.registerReceiver(this.callReceiver, filter);

//        if (canDrawOverlays(getApplicationContext())) {
//            initAndAddViewToWindowManager();
//        }

        return START_NOT_STICKY;
    }


    private void createFGNotification() {
        // create Notification Channel
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL1_ID,
                    "FG Service Channel",
                    NotificationManager.IMPORTANCE_HIGH
            );
            NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            manager.createNotificationChannel(serviceChannel);
        }

        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_IMMUTABLE);

        // create Notification
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Call Recording Running...")
                .setContentText("Tap to Stop this FG Service!")
//                .setSmallIcon(R.drawable.ic_fg_notification)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                //.setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setAutoCancel(false)
                .setOngoing(true)
//                .setCategory(Notification.CATEGORY_CALL)
                .setContentIntent(pendingIntent)
                .build();

        startForeground(FG_NOTIFICATION_ID, notification);
    }


    private void setupNotificationChannel() {
        // 1. Get reference to Notification Manager
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        // 2. Create Notification Channel ONLY ONEs.
        //    Need for Android 8.0 (API level 26) and higher.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //Create channel only if it is not already created
            if (notificationManager.getNotificationChannel(CHANNEL_ID) == null) {
                NotificationChannel notificationChannel = new NotificationChannel(
                        CHANNEL_ID,
                        CHANNEL_NAME,
                        NotificationManager.IMPORTANCE_DEFAULT); // NotificationManager.IMPORTANCE_HIGH
                notificationManager.createNotificationChannel(notificationChannel);
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initAndAddViewToWindowManager() {
        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

        removeView = (RelativeLayout) inflater.inflate(R.layout.layout_chathead_remove, null);
        WindowManager.LayoutParams paramRemove = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                PixelFormat.TRANSLUCENT);
        paramRemove.gravity = Gravity.TOP | Gravity.LEFT;

        removeView.setVisibility(View.GONE);
        removeImg = (ImageView) removeView.findViewById(R.id.remove_img);
        windowManager.addView(removeView, paramRemove);


        chatheadView = (RelativeLayout) inflater.inflate(R.layout.layout_chathead, null);
        chatheadImg = (ImageView) chatheadView.findViewById(R.id.chathead_img);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            windowManager.getDefaultDisplay().getSize(szWindow);
        } else {
            int w = windowManager.getDefaultDisplay().getWidth();
            int h = windowManager.getDefaultDisplay().getHeight();
            szWindow.set(w, h);
        }

        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                PixelFormat.TRANSLUCENT);
        params.gravity = Gravity.TOP | Gravity.LEFT;
        params.x = 0;
        params.y = 100;
        windowManager.addView(chatheadView, params);

        chatheadView.setOnTouchListener(new View.OnTouchListener() {
            long time_start = 0, time_end = 0;
            boolean isLongclick = false, inBounded = false;
            int remove_img_width = 0, remove_img_height = 0;

            Handler handler_longClick = new Handler();
            Runnable runnable_longClick = new Runnable() {

                @Override
                public void run() {
                    // TODO Auto-generated method stub
                    Timber.d("Into runnable_longClick");

                    isLongclick = true;
                    removeView.setVisibility(View.VISIBLE);
                    chathead_longclick();
                }
            };

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                WindowManager.LayoutParams layoutParams = (WindowManager.LayoutParams) chatheadView.getLayoutParams();

                int x_cord = (int) event.getRawX();
                int y_cord = (int) event.getRawY();
                int x_cord_Destination, y_cord_Destination;

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        time_start = System.currentTimeMillis();
                        handler_longClick.postDelayed(runnable_longClick, 600);

                        remove_img_width = removeImg.getLayoutParams().width;
                        remove_img_height = removeImg.getLayoutParams().height;

                        x_init_cord = x_cord;
                        y_init_cord = y_cord;

                        x_init_margin = layoutParams.x;
                        y_init_margin = layoutParams.y;

                        if (txtView != null) {
                            txtView.setVisibility(View.GONE);
                            myHandler.removeCallbacks(myRunnable);
                        }
                        break;
                    case MotionEvent.ACTION_MOVE:
                        int x_diff_move = x_cord - x_init_cord;
                        int y_diff_move = y_cord - y_init_cord;

                        x_cord_Destination = x_init_margin + x_diff_move;
                        y_cord_Destination = y_init_margin + y_diff_move;

                        if (isLongclick) {
                            int x_bound_left = szWindow.x / 2 - (int) (remove_img_width * 1.5);
                            int x_bound_right = szWindow.x / 2 + (int) (remove_img_width * 1.5);
                            int y_bound_top = szWindow.y - (int) (remove_img_height * 1.5);

                            if ((x_cord >= x_bound_left && x_cord <= x_bound_right) && y_cord >= y_bound_top) {
                                inBounded = true;

                                int x_cord_remove = (int) ((szWindow.x - (remove_img_height * 1.5)) / 2);
                                int y_cord_remove = (int) (szWindow.y - ((remove_img_width * 1.5) + getStatusBarHeight()));

                                if (removeImg.getLayoutParams().height == remove_img_height) {
                                    removeImg.getLayoutParams().height = (int) (remove_img_height * 1.5);
                                    removeImg.getLayoutParams().width = (int) (remove_img_width * 1.5);

                                    WindowManager.LayoutParams param_remove = (WindowManager.LayoutParams) removeView.getLayoutParams();
                                    param_remove.x = x_cord_remove;
                                    param_remove.y = y_cord_remove;

                                    windowManager.updateViewLayout(removeView, param_remove);
                                }

                                layoutParams.x = x_cord_remove + (Math.abs(removeView.getWidth() - chatheadView.getWidth())) / 2;
                                layoutParams.y = y_cord_remove + (Math.abs(removeView.getHeight() - chatheadView.getHeight())) / 2;

                                windowManager.updateViewLayout(chatheadView, layoutParams);
                                break;
                            } else {
                                inBounded = false;
                                removeImg.getLayoutParams().height = remove_img_height;
                                removeImg.getLayoutParams().width = remove_img_width;

                                WindowManager.LayoutParams param_remove = (WindowManager.LayoutParams) removeView.getLayoutParams();
                                int x_cord_remove = (szWindow.x - removeView.getWidth()) / 2;
                                int y_cord_remove = szWindow.y - (removeView.getHeight() + getStatusBarHeight());

                                param_remove.x = x_cord_remove;
                                param_remove.y = y_cord_remove;

                                windowManager.updateViewLayout(removeView, param_remove);
                            }

                        }


                        layoutParams.x = x_cord_Destination;
                        layoutParams.y = y_cord_Destination;

                        windowManager.updateViewLayout(chatheadView, layoutParams);
                        break;
                    case MotionEvent.ACTION_UP:
                        isLongclick = false;
                        removeView.setVisibility(View.GONE);
                        removeImg.getLayoutParams().height = remove_img_height;
                        removeImg.getLayoutParams().width = remove_img_width;
                        handler_longClick.removeCallbacks(runnable_longClick);

                        if (inBounded) {
                            Timber.d("Finish activity");
//                            if (MyDialog.active) {
//                                MyDialog.myDialog.finish();
//                            }

                            stopService(new Intent(getApplicationContext(), CallRecordService.class));
                            inBounded = false;
                            break;
                        }


                        int x_diff = x_cord - x_init_cord;
                        int y_diff = y_cord - y_init_cord;

                        if (Math.abs(x_diff) < 5 && Math.abs(y_diff) < 5) {
                            time_end = System.currentTimeMillis();
                            if ((time_end - time_start) < 300) {
                                chathead_click();
                            }
                        }

                        y_cord_Destination = y_init_margin + y_diff;

                        int BarHeight = getStatusBarHeight();
                        if (y_cord_Destination < 0) {
                            y_cord_Destination = 0;
                        } else if (y_cord_Destination + (chatheadView.getHeight() + BarHeight) > szWindow.y) {
                            y_cord_Destination = szWindow.y - (chatheadView.getHeight() + BarHeight);
                        }
                        layoutParams.y = y_cord_Destination;

                        inBounded = false;
                        resetPosition(x_cord);

                        break;
                    default:
                        Timber.d("chatheadView.setOnTouchListener  -> event.getAction() : default");
                        break;
                }
                return true;
            }
        });


        txtView = (LinearLayout) inflater.inflate(R.layout.layout_txt, null);
        txt1 = (TextView) txtView.findViewById(R.id.txt1);
        txt_linearlayout = (LinearLayout) txtView.findViewById(R.id.txt_linearlayout);


        WindowManager.LayoutParams paramsTxt = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                PixelFormat.TRANSLUCENT);
        paramsTxt.gravity = Gravity.TOP | Gravity.LEFT;

        txtView.setVisibility(View.GONE);
        windowManager.addView(txtView, paramsTxt);
    }

    private void chathead_click() {
//        if(MyDialog.active){
//            MyDialog.myDialog.finish();
//        }else{
        Intent it = new Intent(this, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(it);
//        }
    }

    private void chathead_longclick() {
        Timber.d(" chathead_longclick: Into ChatHeadService.chathead_longclick() ");

        WindowManager.LayoutParams param_remove = (WindowManager.LayoutParams) removeView.getLayoutParams();
        int x_cord_remove = (szWindow.x - removeView.getWidth()) / 2;
        int y_cord_remove = szWindow.y - (removeView.getHeight() + getStatusBarHeight());

        param_remove.x = x_cord_remove;
        param_remove.y = y_cord_remove;

        windowManager.updateViewLayout(removeView, param_remove);
    }

    private void resetPosition(int x_cord_now) {
        if (x_cord_now <= szWindow.x / 2) {
            isLeft = true;
            moveToLeft(x_cord_now);
        } else {
            isLeft = false;
            moveToRight(x_cord_now);
        }
    }

    private void moveToLeft(final int x_cord_now) {
        final int x = szWindow.x - x_cord_now;

        new CountDownTimer(500, 5) {
            WindowManager.LayoutParams mParams = (WindowManager.LayoutParams) chatheadView.getLayoutParams();

            public void onTick(long t) {
                long step = (500 - t) / 5;
                mParams.x = 0 - (int) (double) bounceValue(step, x);
                windowManager.updateViewLayout(chatheadView, mParams);
            }

            public void onFinish() {
                mParams.x = 0;
                windowManager.updateViewLayout(chatheadView, mParams);
            }
        }.start();
    }

    private void moveToRight(final int x_cord_now) {
        new CountDownTimer(500, 5) {
            WindowManager.LayoutParams mParams = (WindowManager.LayoutParams) chatheadView.getLayoutParams();

            public void onTick(long t) {
                long step = (500 - t) / 5;
                mParams.x = szWindow.x + (int) (double) bounceValue(step, x_cord_now) - chatheadView.getWidth();
                windowManager.updateViewLayout(chatheadView, mParams);
            }

            public void onFinish() {
                mParams.x = szWindow.x - chatheadView.getWidth();
                windowManager.updateViewLayout(chatheadView, mParams);
            }
        }.start();
    }

    private double bounceValue(long step, long scale) {
        double value = scale * Math.exp(-0.055 * step) * Math.cos(0.08 * step);
        return value;
    }

    private int getStatusBarHeight() {
        int statusBarHeight = (int) Math.ceil(25 * getApplicationContext().getResources().getDisplayMetrics().density);
        return statusBarHeight;
    }

    public static boolean canDrawOverlays(Context context) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        } else {
            return Settings.canDrawOverlays(context);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        fusedLocationApi.requestLocationUpdates(apiClient, locationRequest, new LocationListener() {
            @Override
            public void onLocationChanged(@NonNull Location location) {
                currentLocation = location;
                Timber.d("Got the location" + location);
            }
        });
    }

    private void sendConnectivityCheck(String type, String connectionType) {
        SharedPreferences preferences = ContextExtensionsKt.getPreferences(this);
        SharedPreferencesSmsStorage sharedPreferencesSmsStorage = new SharedPreferencesSmsStorage(Objects.requireNonNull(preferences));

        if (sharedPreferencesSmsStorage.getUserInfo() == null) {
            return;
        }
        String format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date());
        JSONArray dataArray = new JSONArray();
        try {
            JSONObject callJsonObj = new JSONObject();
            if (type.equals("internet_on")) {
                callJsonObj.put("connection_type", connectionType);
                callJsonObj.put("date_connected", format);
            } else {
                callJsonObj.put("date_disconnected", format);
            }
            dataArray.put(callJsonObj);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject infoObject = null;
        try {
            infoObject = new JSONObject(sharedPreferencesSmsStorage.getUserInfo());
        } catch (JSONException e) {
            e.printStackTrace();
        }


        CUGApiClient.INSTANCE.getService().postConnectivity("Bearer " + sharedPreferencesSmsStorage.getToken()
                , type, dataArray, infoObject
        ).enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 201 || response.code() == 200) {
                    Timber.d("Successfully Inserted");
                } else {
                    Timber.d("Error Occurred");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Timber.d("onFailure");
            }
        });
    }

    private void sendLocationToBackend() {
        SharedPreferences preferences = ContextExtensionsKt.getPreferences(this);
        SharedPreferencesSmsStorage sharedPreferencesSmsStorage = new SharedPreferencesSmsStorage(Objects.requireNonNull(preferences));

        JSONArray dataArray = new JSONArray();
        try {
            JSONObject callJsonObj = new JSONObject();
            callJsonObj.put("lat", currentLocation.getLatitude());
            callJsonObj.put("long", currentLocation.getLongitude());
            dataArray.put(callJsonObj);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject infoObject = null;
        try {
            infoObject = new JSONObject(sharedPreferencesSmsStorage.getUserInfo());
        } catch (JSONException e) {
            e.printStackTrace();
        }


        CUGApiClient.INSTANCE.getService().postLocation("Bearer " + sharedPreferencesSmsStorage.getToken()
                , "location", dataArray, infoObject
        ).enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 201 || response.code() == 200) {
                    Timber.d("Successfully Inserted");
                } else {
                    Timber.d("Error Occurred");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Timber.d("onFailure");
            }
        });
    }

    @Override
    public void onConnectionSuspended(int i) {
        Timber.d("onConnectionSuspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Timber.d("onConnectionFailed");
    }

    private void updateCallRecordToServer(String callType, String phoneNumber, String date, long duration, long audioFileId) {
//        Timber.d(new IllegalStateException());
        SharedPreferences preferences = ContextExtensionsKt.getPreferences(this);
        SharedPreferencesSmsStorage sharedPreferencesSmsStorage = new SharedPreferencesSmsStorage(Objects.requireNonNull(preferences));
        if(!sharedPreferencesSmsStorage.isCallRecordingFeatureEnable()) {
            return;
        }
        try {
            JSONArray dataArray = new JSONArray();
            JSONObject callJsonObj = new JSONObject();
            callJsonObj.put("number", phoneNumber);
            callJsonObj.put("date", date);
            callJsonObj.put("duration", duration + "s");
            callJsonObj.put("overall", "");
            if (audioFileId != -1) {
                callJsonObj.put("image_id", audioFileId);
            }
            dataArray.put(callJsonObj);

            CUGApiClient.INSTANCE.getService().postCallData(
                    "Bearer " + sharedPreferencesSmsStorage.getToken()
                    , callType,
                    dataArray,
                    new JSONObject(sharedPreferencesSmsStorage.getUserInfo())
            ).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.code() == 201 || response.code() == 200) {
                        File sampleDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "/CUG Caller");
                        if (sampleDir.isDirectory()) {
                            for (File child : Objects.requireNonNull(sampleDir.listFiles())) {
                                child.delete();
                            }
                        }
                        Timber.d("updateCallRecordToServer Successfully Inserted");
                    } else {
                        Timber.d("updateCallRecordToServer Error Occurred");
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Timber.e("updateCallRecordToServer onFailure : call " + call);
                    Timber.e(t);
                }
            });
        } catch (Exception e) {
            Timber.e(e);
        }
    }


    private void convertAndUploadAudioFile(String callType, String phoneNumber, String date, long duration, String audioFilePath) {
        Timber.d("convertAndUploadAudioFile = " + audioFilePath);
        File wavFile = new File(audioFilePath);
        Timber.d("onActivityResult Result_OK audioFilePath = " + audioFilePath + "   File exists : " + wavFile.exists());
        IConvertCallback callback = new IConvertCallback() {
            @Override
            public void onSuccess(File convertedFile) {
                Timber.d("onSuccess : filePath = " + convertedFile.getAbsolutePath());
                updateAudioFile(callType, phoneNumber, date, duration, convertedFile.getAbsolutePath());
            }

            @Override
            public void onFailure(Exception error) {
                Timber.e("onFailure");
                Timber.e(error);
                updateAudioFile(callType, phoneNumber, date, duration, audioFilePath);
            }
        };
        AndroidAudioConverter.with(this)
                .setFile(wavFile)
                .setFormat(AudioFormat.MP3)
                .setCallback(callback)
                .convert();
    }

    private void updateAudioFile(String callType, String phoneNumber, String date, long duration, String audioFilePath) {
//        Timber.d(new IllegalStateException());
        Timber.d("updateAudioFile = path = " + audioFilePath);
        SharedPreferences preferences = ContextExtensionsKt.getPreferences(this);
        SharedPreferencesSmsStorage sharedPreferencesSmsStorage = new SharedPreferencesSmsStorage(Objects.requireNonNull(preferences));

        MultipartBody.Part audioFilePart = null;
        File audioFile = new File(audioFilePath);
        if (audioFile.exists()) {
            RequestBody audioBody = RequestBody.create(MediaType.parse("audio/*"), audioFile);
            audioFilePart = MultipartBody.Part.createFormData("attachment", audioFile.getName(), audioBody);
        }

        CUGApiClient.INSTANCE.getService().uploadAudioFile(
                "Bearer " + sharedPreferencesSmsStorage.getToken(),
                callType, audioFilePart
        ).enqueue(new Callback<AudioFileUploadResponse>() {
            @Override
            public void onResponse(Call<AudioFileUploadResponse> call, Response<AudioFileUploadResponse> response) {
                if (response.code() == 201 || response.code() == 200) {
                    Timber.d("updateAudioFile Successfully Inserted response = " + response.body());
                    try {
                        if (response.body() != null && response.body().getData() != null) {
                            long fileId = response.body().getData().getImageId();
                            updateCallRecordToServer(callType, phoneNumber, date, duration, fileId);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Timber.d("updateAudioFile Error Occurred");
                }
            }

            @Override
            public void onFailure(Call<AudioFileUploadResponse> call, Throwable t) {
                Timber.e("updateAudioFile onFailure : call " + call);
                Timber.e(t);
            }
        });
    }

    public class UpdatesReceiver extends BroadcastReceiver {
        public static final String BUNDLE_CALL_TYPE = "callType";
        public static final String BUNDLE_PHONE_NUMBER = "phoneNumber";
        public static final String BUNDLE_DATE = "date";
        public static final String BUNDLE_DURATION = "duration";
        public static final String BUNDLE_AUDIO_FILE_PATH = "audioFilePath";


        public static final String BUNDLE_CALL_TYPE_INCOMING = "incoming";
        public static final String BUNDLE_CALL_TYPE_OUTGOING = "outgoing";
        public static final String BUNDLE_CALL_TYPE_MISSED = "missed";

        @Override
        public void onReceive(Context context, Intent intent) {
            Timber.d("onReceive : " + intent.getAction());

            if (ACTION_UPDATE_CALL.equals(intent.getAction())) {
                Bundle bundle = intent.getExtras();
                String callType = bundle.getString(BUNDLE_CALL_TYPE);
                String phoneNumber = bundle.getString(BUNDLE_PHONE_NUMBER);
                String date = bundle.getString(BUNDLE_DATE);
                String audioFilePath = bundle.getString(BUNDLE_AUDIO_FILE_PATH, null);
                long duration = bundle.getLong(BUNDLE_DURATION);
                if (audioFilePath != null && audioFilePath.trim().length() > 0) {
//                    updateAudioFile(callType, phoneNumber, date, duration, audioFilePath);
                    convertAndUploadAudioFile(callType, phoneNumber, date, duration, audioFilePath);
                } else {
                    updateCallRecordToServer(callType, phoneNumber, date, duration, -1);
                }
            }
        }
    }

}
