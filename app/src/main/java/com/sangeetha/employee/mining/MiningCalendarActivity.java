package com.sangeetha.employee.mining;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.RectF;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;

import com.sangeetha.employee.R;
import com.sangeetha.employee.calendarView.DateTimeInterpreter;
import com.sangeetha.employee.calendarView.MonthLoader;
import com.sangeetha.employee.calendarView.WeekView;
import com.sangeetha.employee.calendarView.WeekViewEvent;
import com.sangeetha.employee.mining.response.CalendarResponse;
import com.sangeetha.employee.utils.AppPreference;
import com.sangeetha.employee.utils.CalendarUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import timber.log.Timber;

public class MiningCalendarActivity extends AppCompatActivity implements WeekView.EventClickListener, MonthLoader.MonthChangeListener, WeekView.EventLongPressListener, WeekView.EmptyViewLongPressListener ,WeekView.AddEventClickListener{
    private static final int TYPE_DAY_VIEW = 1;
    private static final int TYPE_THREE_DAY_VIEW = 2;
    private static final int TYPE_WEEK_VIEW = 3;
    private int mWeekViewType = TYPE_THREE_DAY_VIEW;
    private WeekView mWeekView;
    AppPreference appPreference;
    List<WeekViewEvent> events;
    private List<WeekViewEvent> matchedEvents;
    CalendarViewModel calendarViewModel;
    CalendarResponse response = null;
    String storeCode = "";

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(MiningCalendarActivity.this, MiningActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mining_calendar);

        Toolbar toolbar = findViewById(R.id.miningCalendarToolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.calendar));
        // Get a reference for the week view in the layout.
        mWeekView = findViewById(R.id.weekView);

        appPreference = new AppPreference(this);
        calendarViewModel = new ViewModelProvider(this).get(CalendarViewModel.class);
        // Show a toast message about the touched event.
        mWeekView.setAddEventClickListener(this);
        // The week view has infinite scrolling horizontally. We have to provide the events of a
        // month every time the month changes on the week view.
        mWeekView.setMonthChangeListener(this);
        // Set long press listener for events.
        mWeekView.setEventLongPressListener(this);
        // Set long press listener for empty view
        mWeekView.setEmptyViewLongPressListener(this);

        mWeekViewType = TYPE_WEEK_VIEW;
        mWeekView.setNumberOfVisibleDays(7);

        // Lets change some dimensions to best fit the view.
//        mWeekView.setColumnGap((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, getResources().getDisplayMetrics()));
        mWeekView.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 10, getResources().getDisplayMetrics()));
        mWeekView.setEventTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 10, getResources().getDisplayMetrics()));
        // Set up a date time interpreter to interpret how the date and time will be formatted in
        // the week view. This is optional.
        setupDateTimeInterpreter(true);
        storeCode = appPreference.getStoreData().getStoreCode();
        getCalendarTasks();
    }

    private void getCalendarTasks() {
        calendarViewModel.calendarTasks(storeCode).observe(MiningCalendarActivity.this, calendarResponse -> {
            if (calendarResponse.getStatus()) {
                Timber.e(calendarResponse.getData().toString());
                response = calendarResponse;
                onResume();
            } else {
                Toast.makeText(MiningCalendarActivity.this, calendarResponse.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.calendar_options, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        setupDateTimeInterpreter(id == R.id.action_week_view);
        switch (id) {
            case R.id.action_today:
                mWeekView.goToToday();
                return true;
            case R.id.action_day_view:
                if (mWeekViewType != TYPE_DAY_VIEW) {
                    item.setChecked(!item.isChecked());
                    mWeekViewType = TYPE_DAY_VIEW;
                    mWeekView.setNumberOfVisibleDays(1);

                    // Lets change some dimensions to best fit the view.
                    mWeekView.setColumnGap((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()));
                    mWeekView.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
                    mWeekView.setEventTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
                }
                return true;
            case R.id.action_three_day_view:
                if (mWeekViewType != TYPE_THREE_DAY_VIEW) {
                    item.setChecked(!item.isChecked());
                    mWeekViewType = TYPE_THREE_DAY_VIEW;
                    mWeekView.setNumberOfVisibleDays(3);

                    // Lets change some dimensions to best fit the view.
                    mWeekView.setColumnGap((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()));
                    mWeekView.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
                    mWeekView.setEventTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
                }
                return true;
            case R.id.action_week_view:
                if (mWeekViewType != TYPE_WEEK_VIEW) {
                    item.setChecked(!item.isChecked());
                    mWeekViewType = TYPE_WEEK_VIEW;
                    mWeekView.setNumberOfVisibleDays(7);

                    // Lets change some dimensions to best fit the view.
                    mWeekView.setColumnGap((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, getResources().getDisplayMetrics()));
                    mWeekView.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 10, getResources().getDisplayMetrics()));
                    mWeekView.setEventTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 10, getResources().getDisplayMetrics()));
                }
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }


        return super.onOptionsItemSelected(item);
    }

    /**
     * Set up a date time interpreter which will show short date values when in week view and long
     * date values otherwise.
     *
     * @param shortDate True if the date values should be short.
     */
    private void setupDateTimeInterpreter(final boolean shortDate) {
        mWeekView.setDateTimeInterpreter(new DateTimeInterpreter() {
            @NotNull
            @Override
            public String getFormattedTimeOfDay(int hour, int minutes) {
                return null;
            }

            @NotNull
            @Override
            public String getFormattedWeekDayTitle(@NotNull Calendar date) {
                return null;
            }

//            @Override
//            public String interpretDate(Calendar date) {
//                SimpleDateFormat weekdayNameFormat = new SimpleDateFormat("EEE", Locale.getDefault());
//                String weekday = weekdayNameFormat.format(date.getTime());
//                SimpleDateFormat format = new SimpleDateFormat(" MM/d", Locale.getDefault());
//
//                // All android api level do not have a standard way of getting the first letter of
//                // the week day name. Hence we get the first char programmatically.
//                // Details: http://stackoverflow.com/questions/16959502/get-one-letter-abbreviation-of-week-day-of-a-date-in-java#answer-16959657
//                if (shortDate)
//                    weekday = String.valueOf(weekday.charAt(0));
//                return weekday.toUpperCase() + format.format(date.getTime());
//            }
//
//            @Override
//            public String interpretTime(int hour) {
//                return hour > 11 ? (hour - 12) + " PM" : (hour == 0 ? "12 AM" : hour + " AM");
//            }
        });
    }

    protected String getEventTitle(Calendar time, String title) {
        return String.format(title + " at %02d:%02d %02d/%02d/%d", time.get(Calendar.HOUR_OF_DAY), time.get(Calendar.MINUTE), time.get(Calendar.MONTH) + 1, time.get(Calendar.DAY_OF_MONTH), time.get(Calendar.YEAR));
    }


    @Override
    public void onEventClick(WeekViewEvent event, RectF eventRect) {
//        Toast.makeText(this, "Clicked " + event.getName(), Toast.LENGTH_SHORT).show();
        showEventInfo(event);
    }

    @Override
    public void onEventLongPress(WeekViewEvent event, RectF eventRect) {
//        Toast.makeText(this, "Long pressed event: " + event.getName(), Toast.LENGTH_SHORT).show();
        showEventInfo(event);
    }

    @Override
    public void onEmptyViewLongPress(Calendar time) {
    }

    public WeekView getWeekView() {
        return mWeekView;
    }



    @Override
    public List<? extends WeekViewEvent> onMonthChange(int newYear, int newMonth) {
        // Populate the week view with some events.
        events = new ArrayList<WeekViewEvent>();
        matchedEvents = new ArrayList<>();

//        Calendar startTime = Calendar.getInstance();
//        startTime.set(Calendar.HOUR_OF_DAY, 3);
//        startTime.set(Calendar.MINUTE, 0);
//        startTime.set(Calendar.MONTH, newMonth - 1);
//        startTime.set(Calendar.YEAR, newYear);
//        Calendar endTime = (Calendar) startTime.clone();
//        endTime.add(Calendar.HOUR, 1);
//        endTime.set(Calendar.MONTH, newMonth - 1);
//        WeekViewEvent event = new WeekViewEvent(1, getEventTitle(startTime), startTime, endTime);
//        event.setColor(getResources().getColor(R.color.orange_light));
//        events.add(event);
//
//        startTime = Calendar.getInstance();
//        startTime.set(Calendar.HOUR_OF_DAY, 3);
//        startTime.set(Calendar.MINUTE, 30);
//        startTime.set(Calendar.MONTH, newMonth-1);
//        startTime.set(Calendar.YEAR, newYear);
//        endTime = (Calendar) startTime.clone();
//        endTime.set(Calendar.HOUR_OF_DAY, 4);
//        endTime.set(Calendar.MINUTE, 30);
//        endTime.set(Calendar.MONTH, newMonth-1);
//        event = new WeekViewEvent(10, getEventTitle(startTime), startTime, endTime);
//        event.setColor(getResources().getColor(R.color.colorPrimary));
//        events.add(event);
//
//        startTime = Calendar.getInstance();
//        startTime.set(Calendar.HOUR_OF_DAY, 4);
//        startTime.set(Calendar.MINUTE, 20);
//        startTime.set(Calendar.MONTH, newMonth-1);
//        startTime.set(Calendar.YEAR, newYear);
//        endTime = (Calendar) startTime.clone();
//        endTime.set(Calendar.HOUR_OF_DAY, 5);
//        endTime.set(Calendar.MINUTE, 0);
//        event = new WeekViewEvent(10, getEventTitle(startTime), startTime, endTime);
//        event.setColor(getResources().getColor(R.color.colorPrimaryDark));
//        events.add(event);
//
//        startTime = Calendar.getInstance();
//        startTime.set(Calendar.HOUR_OF_DAY, 5);
//        startTime.set(Calendar.MINUTE, 30);
//        startTime.set(Calendar.MONTH, newMonth-1);
//        startTime.set(Calendar.YEAR, newYear);
//        endTime = (Calendar) startTime.clone();
//        endTime.add(Calendar.HOUR_OF_DAY, 2);
//        endTime.set(Calendar.MONTH, newMonth-1);
//        event = new WeekViewEvent(2, getEventTitle(startTime), startTime, endTime);
//        event.setColor(getResources().getColor(R.color.colorPrimary));
//        events.add(event);
//
//        startTime = Calendar.getInstance();
//        startTime.set(Calendar.HOUR_OF_DAY, 5);
//        startTime.set(Calendar.MINUTE, 0);
//        startTime.set(Calendar.MONTH, newMonth - 1);
//        startTime.set(Calendar.YEAR, newYear);
//        startTime.add(Calendar.DATE, 1);
//        endTime = (Calendar) startTime.clone();
//        endTime.add(Calendar.HOUR_OF_DAY, 3);
//        endTime.set(Calendar.MONTH, newMonth - 1);
//        event = new WeekViewEvent(3, getEventTitle(startTime), startTime, endTime);
//        event.setColor(getResources().getColor(R.color.colorPrimaryApp));
//        events.add(event);
//
//        startTime = Calendar.getInstance();
//        startTime.set(Calendar.DAY_OF_MONTH, 15);
//        startTime.set(Calendar.HOUR_OF_DAY, 3);
//        startTime.set(Calendar.MINUTE, 0);
//        startTime.set(Calendar.MONTH, newMonth-1);
//        startTime.set(Calendar.YEAR, newYear);
//        endTime = (Calendar) startTime.clone();
//        endTime.add(Calendar.HOUR_OF_DAY, 3);
//        event = new WeekViewEvent(4, getEventTitle(startTime), startTime, endTime);
//        event.setColor(getResources().getColor(R.color.colorPrimaryDarkApp));
//        events.add(event);
//
//        startTime = Calendar.getInstance();
//        startTime.set(Calendar.DAY_OF_MONTH, 1);
//        startTime.set(Calendar.HOUR_OF_DAY, 3);
//        startTime.set(Calendar.MINUTE, 0);
//        startTime.set(Calendar.MONTH, newMonth-1);
//        startTime.set(Calendar.YEAR, newYear);
//        endTime = (Calendar) startTime.clone();
//        endTime.add(Calendar.HOUR_OF_DAY, 3);
//        event = new WeekViewEvent(5, getEventTitle(startTime), startTime, endTime);
//        event.setColor(getResources().getColor(R.color.orange_light));
//        events.add(event);
//
//        startTime = Calendar.getInstance();
//        startTime.set(Calendar.DAY_OF_MONTH, startTime.getActualMaximum(Calendar.DAY_OF_MONTH));
//        startTime.set(Calendar.HOUR_OF_DAY, 15);
//        startTime.set(Calendar.MINUTE, 0);
//        startTime.set(Calendar.MONTH, newMonth-1);
//        startTime.set(Calendar.YEAR, newYear);
//        endTime = (Calendar) startTime.clone();
//        endTime.add(Calendar.HOUR_OF_DAY, 3);
//        event = new WeekViewEvent(5, getEventTitle(startTime), startTime, endTime);
//        event.setColor(getResources().getColor(R.color.colorPrimary));
//        events.add(event);
//
//
//
         if (response!=null&&response.getData().size() > 0) {
            showEvents(newMonth, newYear);
        }



        return matchedEvents;
    }

    private boolean eventMatches(WeekViewEvent event, int year, int month) {
        return (event.getStartTime().get(Calendar.YEAR) == year && event.getStartTime().get(Calendar.MONTH) == (month)) || (event.getEndTime().get(Calendar.YEAR) == year && event.getEndTime().get(Calendar.MONTH) == month);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @SuppressLint("Assert")
    private void showEvents(int month, int year) {
        int idset = 0;
        int c = 0;


        Calendar startCal = null;
        Calendar endCal = null;

//        Calendar startTime = Calendar.getInstance();
//        startTime.set(Calendar.HOUR_OF_DAY, 3);
//        startTime.set(Calendar.MINUTE, 0);
//        startTime.set(Calendar.MONTH, month - 1);
//        startTime.set(Calendar.YEAR, year);
//        Calendar endTime = (Calendar) startTime.clone();
//        endTime.add(Calendar.HOUR, 1);
//        endTime.set(Calendar.MONTH, month - 1);
//        WeekViewEvent event = new WeekViewEvent(111, getEventTitle(startTime), startTime, endTime);
//        event.setColor(getResources().getColor(R.color.orange_light));
//        matchedEvents.add(event);

        for (int i = 0; i < response.getData().size(); i++) {

            startCal = Calendar.getInstance();
            endCal = (Calendar) startCal.clone();

            String date = response.getData().get(i).getStartDate();
            String title = response.getData().get(i).getTitle();
            String formattedDate = CalendarUtils.getDateFormat(date);
            Date myDate = CalendarUtils.getDateFromString("dd/MM/yyyy hh:mm", formattedDate);
            Calendar start = Calendar.getInstance();
            start.set(Calendar.DATE, myDate.getDate());
            start.set(Calendar.HOUR_OF_DAY, myDate.getHours());
            start.set(Calendar.MINUTE, myDate.getMinutes());
            start.set(Calendar.MONTH, myDate.getMonth());
            start.set(Calendar.YEAR, year);
            Calendar end = (Calendar) start.clone();
            end.add(Calendar.HOUR, 1);

            WeekViewEvent eventAPI = new WeekViewEvent(String.valueOf(i), "Call Back", start, end);
            eventAPI.setColor(getResources().getColor(R.color.radio5));
            eventAPI.setName(getEventTitle(start, title));


//            events.add(eventAPI);

            if (!events.contains(eventAPI)) {
                events.add(eventAPI);
            }

            for (WeekViewEvent we : events) {
                if (eventMatches(we, year, month)) {
                    if (!matchedEvents.contains(we)) {
                        matchedEvents.add(we);
                    }
                }
            }

            startCal = null;
            endCal = null;
            eventAPI = null;

        }



    }
    @SuppressLint("RestrictedApi")
    private void showEventInfo(WeekViewEvent event) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MiningCalendarActivity.this, R.style.Theme_Dialog_event);
        builder.setTitle("Event Info");

        float dpi = this.getResources().getDisplayMetrics().density;

        final TextView input = new TextView(this);
        input.setTextColor(getResources().getColor(R.color.black));
        input.setTextSize(16);

        builder.setView(input, (int) (25 * dpi), (int) (8 * dpi), (int) (14 * dpi), (int) (8 * dpi));

        String sdate = String.valueOf(event.getStartTime().get(Calendar.DAY_OF_MONTH));
        String smonth = String.valueOf(event.getStartTime().get(Calendar.MONTH));
        String syear = String.valueOf(event.getStartTime().get(Calendar.YEAR));
        String shour = String.valueOf(event.getStartTime().get(Calendar.HOUR_OF_DAY));
        String sminute = String.valueOf(event.getStartTime().get(Calendar.MINUTE));

        String edate = String.valueOf(event.getEndTime().get(Calendar.DAY_OF_MONTH));
        String emonth = String.valueOf(event.getEndTime().get(Calendar.MONTH));
        String eyear = String.valueOf(event.getEndTime().get(Calendar.YEAR));
        String ehour = String.valueOf(event.getEndTime().get(Calendar.HOUR_OF_DAY));
        String eminute = String.valueOf(event.getEndTime().get(Calendar.MINUTE));

        if (sdate.length() < 2) {
            sdate = "0" + sdate;
        }

        if (edate.length() < 2) {
            edate = "0" + edate;
        }

        if (smonth.length() < 2) {
            smonth = "0" + smonth;
        }

        if (emonth.length() < 2) {
            emonth = "0" + emonth;
        }

        if (shour.length() < 2) {
            shour = "0" + shour;
        }

        if (ehour.length() < 2) {
            ehour = "0" + ehour;
        }
        if (sminute.length() < 2) {
            sminute = "0" + sminute;
        }

        if (eminute.length() < 2) {
            eminute = "0" + eminute;
        }

        String start = sdate + "-" + smonth + "-" + syear + " " + shour + ":" + sminute;
        String end = edate + "-" + emonth + "-" + eyear + " " + ehour + ":" + eminute;

        input.setText( "Title: " + event.getName() + "\n"  + "Start Time: " + start + "\n" + "End Time: " + end);

        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //TODO
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onAddEventClicked(@NotNull Calendar startTime, @NotNull Calendar endTime) {

    }
}

