package com.sangeetha.employee.servify.model.data


import com.google.gson.annotations.SerializedName

data class OffersData(
    @SerializedName("offer_name")
    val offerName: String,
    @SerializedName("offer_value")
    val offerValue: String
)