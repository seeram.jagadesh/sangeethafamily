package com.sangeetha.employee.appointments.appointmentDetails;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AppointmentDetails {

    @SerializedName("unique_id")
    public String uniqueId;

    @SerializedName("appointment_type")
    public String appointmentType;

    @SerializedName("appointment_type_name")
    public String appointmentTypeName;

    @SerializedName("customer_details")
    public CustomerDetails customerDetails;

    @SerializedName("store_name")
    public String storeName;

    @SerializedName("store_address")
    public String storeAddress;

    @SerializedName("user_address")
    public String userAddress;

    @SerializedName("virtual_demo_type")
    public String virtualDemoType;

    @SerializedName("status")
    public String status;

    @SerializedName("date")
    public String date;

    @SerializedName("time")
    public String time;

    @SerializedName("model_details")
    public List<ModelDetails> modelDetailsList;

    @SerializedName("appointment_description")
    public Object appointmentDescription;


    @SerializedName("qr_code_image")
    public String qrCodeImage;

    @SerializedName("pdf_file")
    public String pdfFile;

    @SerializedName("appointment_notes")
    public String appointmentNotes;



    @Override
    public String toString() {
        return "AppointmentDetails{" +
                "uniqueId='" + uniqueId + '\'' +
                ", appointmentType='" + appointmentType + '\'' +
                ", appointmentTypeName='" + appointmentTypeName + '\'' +
                ", customerDetails=" + customerDetails +
                ", storeName='" + storeName + '\'' +
                ", storeAddress='" + storeAddress + '\'' +
                ", userAddress='" + userAddress + '\'' +
                ", virtualDemoType='" + virtualDemoType + '\'' +
                ", status='" + status + '\'' +
                ", date='" + date + '\'' +
                ", time='" + time + '\'' +
                ", modelDetailsList=" + modelDetailsList +
//                ", appointmentDescription=" + appointmentDescription +
                ", qrCodeImage='" + qrCodeImage + '\'' +
                ", pdfFile='" + pdfFile + '\'' +
                ", appointmentNotes='" + appointmentNotes + '\'' +
                '}';
    }


    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getAppointmentType() {
        return appointmentType;
    }

    public void setAppointmentType(String appointmentType) {
        this.appointmentType = appointmentType;
    }

    public Object getAppointmentDescription() {
        return appointmentDescription;
    }

    public void setAppointmentDescription(Object appointmentDescription) {
        this.appointmentDescription = appointmentDescription;
    }

    public String getAppointmentTypeName() {
        return appointmentTypeName;
    }

    public void setAppointmentTypeName(String appointmentTypeName) {
        this.appointmentTypeName = appointmentTypeName;
    }

    public CustomerDetails getCustomerDetails() {
        return customerDetails;
    }

    public void setCustomerDetails(CustomerDetails customerDetails) {
        this.customerDetails = customerDetails;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreAddress() {
        return storeAddress;
    }

    public void setStoreAddress(String storeAddress) {
        this.storeAddress = storeAddress;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getVirtualDemoType() {
        return virtualDemoType;
    }

    public void setVirtualDemoType(String virtualDemoType) {
        this.virtualDemoType = virtualDemoType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public List<ModelDetails> getModelDetailsList() {
        return modelDetailsList;
    }

    public void setModelDetailsList(List<ModelDetails> modelDetailsList) {
        this.modelDetailsList = modelDetailsList;
    }

//    public String getAppointmentDescription() {
//        return appointmentDescription;
//    }
//
//    public void setAppointmentDescription(String appointmentDescription) {
//        this.appointmentDescription = appointmentDescription;
//    }

    public String getQrCodeImage() {
        return qrCodeImage;
    }

    public void setQrCodeImage(String qrCodeImage) {
        this.qrCodeImage = qrCodeImage;
    }

    public String getPdfFile() {
        return pdfFile;
    }

    public void setPdfFile(String pdfFile) {
        this.pdfFile = pdfFile;
    }

    public String getAppointmentNotes() {
        return appointmentNotes;
    }

    public void setAppointmentNotes(String appointmentNotes) {
        this.appointmentNotes = appointmentNotes;
    }
}
