package com.sangeetha.employee.managerDiscount.storeASM;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.sangeetha.employee.R;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.DataItem;
import com.sangeetha.employee.managerDiscount.storeManager.DiscountDetailActivity;

import java.util.ArrayList;

public class ApproveDiscountAdapter extends RecyclerView.Adapter<ApproveDiscountAdapter.ApproveDiscountHolder> {

    private ArrayList<DataItem> dataItems;
    private Context context;

    public ApproveDiscountAdapter(ArrayList<DataItem> dataItems, Context context) {
        this.dataItems = dataItems;
        this.context = context;
    }

    @NonNull
    @Override
    public ApproveDiscountHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.approve_discount_list_item, parent, false);

        return new ApproveDiscountHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ApproveDiscountHolder holder, int position) {
        holder.dateTxt.setText(dataItems.get(position).getStoreSubmit());
        holder.storeTxt.setText(dataItems.get(position).getStoreId());
        holder.mobileNoTxt.setText(dataItems.get(position).getCustomerMobile());
        holder.idTxt.setText(dataItems.get(position).getEmployeeId());
        holder.modelTxt.setText(dataItems.get(position).getModelId());
        if(dataItems.get(position).getAmount().equalsIgnoreCase("")){
            holder.amountLayout.setVisibility(View.GONE);
        }else{
            holder.amountLayout.setVisibility(View.VISIBLE);
            holder.amountTxt.setText(dataItems.get(position).getAmount());
        }
        if (dataItems.get(position).getCouponStatus()==null ||dataItems.get(position).getCouponStatus().equalsIgnoreCase("")) {
            holder.statusTxt.setText(dataItems.get(position).getDiscountStatus());
        } else {
            holder.statusTxt.setText(dataItems.get(position).getCouponStatus());
        }
        holder.referenceId.setText(dataItems.get(position).getReferenceId());
        if (!dataItems.get(position).getCouponCode().isEmpty()) {
            holder.apxLayout.setVisibility(View.VISIBLE);
            holder.apxCodeTxt.setText(dataItems.get(position).getCouponCode());
        } else {
            holder.apxLayout.setVisibility(View.GONE);
        }
        holder.itemView.setOnClickListener(v -> {
            if (dataItems.get(position).getCouponCode().isEmpty()) {
                Intent intent = new Intent(context, ApprovalDetailActivity.class);
                intent.putExtra("reference_id", dataItems.get(position).getReferenceId());
                context.startActivity(intent);

            } else {
                Intent intent = new Intent(context, DiscountDetailActivity.class);
                intent.putExtra("reference_id", dataItems.get(position).getReferenceId());
              //  intent.putExtra("position", position);
               // intent.putParcelableArrayListExtra("data", dataItems);
                context.startActivity(intent);
            }

        });

    }

    @Override
    public int getItemCount() {
        return dataItems.size();
    }

    static class ApproveDiscountHolder extends RecyclerView.ViewHolder {

        AppCompatTextView dateTxt, storeTxt, mobileNoTxt, idTxt, modelTxt, apxCodeTxt, amountTxt, statusTxt, referenceId;
        LinearLayout apxLayout,amountLayout;

        ApproveDiscountHolder(@NonNull View itemView) {
            super(itemView);
            dateTxt = itemView.findViewById(R.id.dateTxt);
            storeTxt = itemView.findViewById(R.id.storeTxt);
            mobileNoTxt = itemView.findViewById(R.id.mobileNoTxt);
            idTxt = itemView.findViewById(R.id.idTxt);
            modelTxt = itemView.findViewById(R.id.modelTxt);
            amountTxt = itemView.findViewById(R.id.amountTxt);
            statusTxt = itemView.findViewById(R.id.statusTxt);
            referenceId = itemView.findViewById(R.id.reference_id);
            apxCodeTxt = itemView.findViewById(R.id.apx_code);
            apxLayout = itemView.findViewById(R.id.apx_layout);
            amountLayout = itemView.findViewById(R.id.amountLayout);


        }
    }
}
