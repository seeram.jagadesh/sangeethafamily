package com.sangeetha.employee.managerDiscount.storeASM;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.sangeetha.employee.CommonResponse;
import com.sangeetha.employee.R;
import com.sangeetha.employee.checkStock.StockCheckResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.MakeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ProductTypeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.SubmitDiscountResponse;
import com.sangeetha.employee.managerDiscount.newStoreProcess.ExclusionDiscountFragment;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.StoreListItem;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.StoreListResponse;
import com.sangeetha.employee.peopleTracking.model.StoreResponse;
import com.sangeetha.employee.utils.AppPreference;
import com.sangeetha.employee.utils.LanguageUtil;
import com.sangeetha.employee.utils.MyContextWrapper;
import com.sangeetha.employee.utils.RecyclerViewOnClick;

import java.util.ArrayList;
import java.util.Locale;

import static android.Manifest.permission.CALL_PHONE;

public class StoreASMActivity extends AppCompatActivity implements StoreASMView, RecyclerViewOnClick {

    private static final int RequestCallPermissionCode = 3;
    private ProgressBar progressBar;
    private ArrayList<StoreListItem> storeListItems = new ArrayList<>();
    private StoreHistoryAdapter storeHistoryAdapter;
    private String mobileNo;
    BottomNavigationView navigation, navigationExclusion;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
        switch (item.getItemId()) {
            case R.id.navigation_store_list:
                getSupportActionBar().setTitle("Stores Authorised");
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }
                restartActivity();
                return true;

            case R.id.navigation_approve_discount:
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }
                setFragment(new ApproveDiscountFragment());
                getSupportActionBar().setTitle("Approve Discount");
                return true;

            case R.id.navigation_exclusion_discount:
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }
                setFragment(new ExclusionDiscountFragment());
                getSupportActionBar().setTitle("Exception Discount");
                return true;
            case R.id.navigation_asm_account:
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }
                setFragment(new ASMAccountFragment());
                getSupportActionBar().setTitle("Account");
                return true;
        }
        return false;
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        navigation.getMenu().getItem(0).setChecked(true);
    }

    public static boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            //   Log.e("Network Testing", "***Available***");
            return true;
        } else {
            //  Log.e("Network Testing", "***Not Available***");
            Toast.makeText(mContext, "No network available, please check your WiFi or EmpData connection", Toast.LENGTH_LONG).show();
            return false;
        }

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        Locale languageType = LanguageUtil.getLanguageType(newBase);
        super.attachBaseContext(MyContextWrapper.wrap(newBase, languageType));
    }

    protected void setFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =
                fragmentManager.beginTransaction();
        fragmentTransaction.replace(android.R.id.content, fragment);
        fragmentTransaction.addToBackStack(fragment.getTag());
        fragmentTransaction.commit();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);

    }

    @Override
    public void setProductTypes(ProductTypeResponse productTypes) {

    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);

    }

    @Override
    public void showMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();

    }

    @Override
    public void setMake(MakeResponse managerResponse) {

    }

    @Override
    public void setModel(ModelResponse managerResponse) {

    }

    @Override
    public void setStoreList(StoreListResponse storeList) {
        if (storeList.isStatus()) {
            storeListItems.clear();
            storeListItems.addAll(storeList.getStoreList());
            storeHistoryAdapter.notifyDataSetChanged();
        } else {
            showMessage(storeList.getMessage());
        }

    }

    @Override
    public void setStores(StoreResponse storeList) {

    }

    @Override
    public void setStocks(StockCheckResponse response) {

    }

//    @Override
//    public void setCheckEligibleException(CommonResponse commonResponse) {
//
//    }

    @Override
    public void setCheckEligibleException(CommonResponse commonResponse) {
        if (commonResponse.getStatus()) {
            navigation.setVisibility(View.GONE);
            navigationExclusion.setVisibility(View.VISIBLE);
        } else {
            navigation.setVisibility(View.VISIBLE);
            navigationExclusion.setVisibility(View.GONE);
        }
    }

    @Override
    public void setSalesCheckCalculate(SubmitDiscountResponse commonResponse) {

    }

    @Override
    public void setSendExceptionRequest(CommonResponse commonResponse) {

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_store_asm);
        AppPreference appPreference = new AppPreference(this);
        progressBar = findViewById(R.id.progressBar4);
        navigation = findViewById(R.id.navigation);
        navigationExclusion = findViewById(R.id.navigationExclusion);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigationExclusion.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        RecyclerView recyclerView = findViewById(R.id.store_list_rv);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(false);
        storeHistoryAdapter = new StoreHistoryAdapter(storeListItems, this);
        recyclerView.setAdapter(storeHistoryAdapter);
        StoreASMPresenter storeASMPresenter = new StoreASMPresenterImpl(this);
        if (isNetworkAvailable(StoreASMActivity.this)) {
            storeASMPresenter.getStoreList(appPreference.getUserToken());

            storeASMPresenter.checkEligibleException(appPreference.getUserToken());
        }
    }


    @Override
    public void onClick(int position) {

    }

    @Override
    public void onClick(String mobileNo) {
        this.mobileNo = mobileNo;
        if (!checkCallPermission()) {
            requestCallPermission();
        } else {
            callDialogue();

        }

    }


    private void requestCallPermission() {
        ActivityCompat.requestPermissions(StoreASMActivity.this, new String[]
                {
                        CALL_PHONE,
                }, RequestCallPermissionCode);
    }

    //set  permission
    public boolean checkCallPermission() {


        int firstPermissionResult = ContextCompat.checkSelfPermission(StoreASMActivity.this, CALL_PHONE);


        return firstPermissionResult == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == RequestCallPermissionCode) {
            if (grantResults.length > 0) {

                boolean callPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (callPermission) {
                    Toast.makeText(StoreASMActivity.this, "Permission Granted", Toast.LENGTH_LONG).show();
                    callDialogue();
                } else {
                    Toast.makeText(StoreASMActivity.this, "Permission Denied", Toast.LENGTH_LONG).show();

                }
            }
        }
    }


    public void restartActivity() {
        Intent intent = getIntent();
        overridePendingTransition(0, 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);

        // StoreMainActivity.this.recreate();
    }

    public void callDialogue() {

        final Dialog dialog = new Dialog(StoreASMActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.call_dialogue);
        dialog.setCancelable(true);
        dialog.findViewById(R.id.yes_btn_logout_popup).setOnClickListener(v -> {
            dialog.dismiss();
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + mobileNo));
            if (ActivityCompat.checkSelfPermission(StoreASMActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            startActivity(intent);

        });

        dialog.findViewById(R.id.no_btn_logout_popup).setOnClickListener(v -> dialog.dismiss());

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }
}


