package com.sangeetha.employee.onlineDelivery;


import com.sangeetha.employee.SangeethaCareApiClient;
import com.sangeetha.employee.onlineDelivery.model.OrderListResponse;
import com.sangeetha.employee.onlineDelivery.model.orderDetails.OrderDetailsResponse;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OnlineDeliveryInteractorImpl implements OnlineDeliveryInteractor {

    private OnlineDeliveryEndPoint onlineDeliveryEndPoint;
    private OnlineDeliveryListener listener;

    OnlineDeliveryInteractorImpl(OnlineDeliveryListener listener) {
        this.listener = listener;
        onlineDeliveryEndPoint = SangeethaCareApiClient.getClient1().create(OnlineDeliveryEndPoint.class);
    }


    @Override
    public void getOrders(String userId) {
        onlineDeliveryEndPoint.getOrders(userId).enqueue(new Callback<OrderListResponse>() {
            @Override
            public void onResponse(@NotNull Call<OrderListResponse> call, @NotNull Response<OrderListResponse> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    if (response.body().isStatus()) {
                        OrderListResponse orderListResponse = response.body();
                        listener.getOrders(orderListResponse);
                    } else {
                        listener.showMessage(response.body().getMessage());
                    }
                } else {
                    listener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(@NotNull Call<OrderListResponse> call, @NotNull Throwable t) {
                listener.showMessage(t.getMessage());
            }
        });
    }

    @Override
    public void getDeliveredOrders(String userId) {
        onlineDeliveryEndPoint.getDeliveredOrders(userId).enqueue(new Callback<OrderListResponse>() {
            @Override
            public void onResponse(@NotNull Call<OrderListResponse> call, @NotNull Response<OrderListResponse> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    if (response.body().isStatus()) {
                        OrderListResponse orderListResponse = response.body();
                        listener.getDeliveredOrders(orderListResponse);
                    } else {
                        listener.showMessage(response.body().getMessage());
                    }
                } else {
                    listener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(@NotNull Call<OrderListResponse> call, @NotNull Throwable t) {
                listener.showMessage(t.getMessage());
            }
        });
    }

    @Override
    public void orderDetails(String id,String orderId) {
        onlineDeliveryEndPoint.orderDetails(id,orderId).enqueue(new Callback<OrderDetailsResponse>() {
            @Override
            public void onResponse(@NotNull Call<OrderDetailsResponse> call, @NotNull Response<OrderDetailsResponse> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    if (response.body().isStatus()) {
                        OrderDetailsResponse orderListResponse = response.body();
                        listener.orderDetails(orderListResponse);
                    } else {
                        listener.showMessage(response.body().getMessage());
                    }
                } else {
                    listener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(@NotNull Call<OrderDetailsResponse> call, @NotNull Throwable t) {
                listener.showMessage(t.getMessage());
            }
        });
    }
}
