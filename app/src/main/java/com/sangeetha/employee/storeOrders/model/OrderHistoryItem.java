package com.sangeetha.employee.storeOrders.model;

import com.google.gson.annotations.SerializedName;

public class OrderHistoryItem {
//     "id": "127570",
//             "order_process_type": "Order Placed",
//             "change_by": "User",
//             "processed_user": "",
//             "description": "<b>Order Placed </b>",
//             "next_time_stamp": "2021-05-14 15:16:11",
//             "created_at": "May 14, 2021 1:57 PM",
//             "updated_at": "May 14, 2021 1:57 PM"

    @SerializedName("id")
    private String id;

    @SerializedName("order_process_type")
    private String order_process_type;

    @SerializedName("change_by")
    private String change_by;

    @SerializedName("processed_user")
    private String processed_user;

    @SerializedName("description")
    private String description;

    @SerializedName("next_time_stamp")
    private String next_time_stamp;

    @SerializedName("created_at")
    private String created_at;

    @SerializedName("updated_at")
    private String updated_at;


    @Override
    public String toString() {
        return "OrderHistoryItem{" +
                "id='" + id + '\'' +
                ", order_process_type='" + order_process_type + '\'' +
                ", processed_user='" + processed_user + '\'' +
                ", description='" + description + '\'' +
                ", next_time_stamp='" + next_time_stamp + '\'' +
                ", created_at='" + created_at + '\'' +
                ", updated_at='" + updated_at + '\'' +
                '}';
    }

    public String getChange_by() {
        return change_by;
    }

    public void setChange_by(String change_by) {
        this.change_by = change_by;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrder_process_type() {
        return order_process_type;
    }

    public void setOrder_process_type(String order_process_type) {
        this.order_process_type = order_process_type;
    }

    public String getProcessed_user() {
        return processed_user;
    }

    public void setProcessed_user(String processed_user) {
        this.processed_user = processed_user;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNext_time_stamp() {
        return next_time_stamp;
    }

    public void setNext_time_stamp(String next_time_stamp) {
        this.next_time_stamp = next_time_stamp;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
