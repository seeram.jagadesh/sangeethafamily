package com.sangeetha.employee.peopleTracking.model;

import com.google.gson.annotations.SerializedName;

public class TrackingResponse {

    @SerializedName("track_id")
    private int trackId;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private boolean status;

    public int getTrackId() {
        return trackId;
    }

    public void setTrackId(int trackId) {
        this.trackId = trackId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "TrackingResponse{" +
                        "track_id = '" + trackId + '\'' +
                        ",message = '" + message + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}