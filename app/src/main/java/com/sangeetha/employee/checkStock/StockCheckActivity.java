package com.sangeetha.employee.checkStock;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;

import com.sangeetha.employee.CommonResponse;
import com.sangeetha.employee.R;
import com.sangeetha.employee.customwidget.SearchableSpinner;
import com.sangeetha.employee.employeeHome.EmployeeLanding;
import com.sangeetha.employee.managerDiscount.couponCode.CheckCouponCode;
import com.sangeetha.employee.managerDiscount.manager_pojo.AlternativeMethodResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.DataItem;
import com.sangeetha.employee.managerDiscount.manager_pojo.DiscountDetailsResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.FindEmployeeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.MakeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelDiscountResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelItem;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelPriceResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.PaymentModeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ProductTypeDatItem;
import com.sangeetha.employee.managerDiscount.manager_pojo.ProductTypeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.SubmitDiscountResponse;
import com.sangeetha.employee.managerDiscount.storeASM.StoreASMPresenter;
import com.sangeetha.employee.managerDiscount.storeASM.StoreASMPresenterImpl;
import com.sangeetha.employee.managerDiscount.storeASM.StoreASMView;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.StoreListResponse;
import com.sangeetha.employee.managerDiscount.storeManager.ManagerDiscountPresenter;
import com.sangeetha.employee.managerDiscount.storeManager.ManagerDiscountPresenterImp;
import com.sangeetha.employee.managerDiscount.storeManager.StoreView;
import com.sangeetha.employee.mining.response.StoreModel;
import com.sangeetha.employee.peopleTracking.model.StoreResponse;
import com.sangeetha.employee.utils.AppPreference;
import com.sangeetha.employee.utils.LanguageUtil;
import com.sangeetha.employee.utils.MyContextWrapper;
import com.sangeetha.employee.utils.ValidationUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class StockCheckActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener,
         View.OnClickListener, StoreASMView {
    SearchableSpinner storeSpinner;
    SearchableSpinner makeSpinner;
    SearchableSpinner modelSpinner;
    SearchableSpinner paymentSpinner;
//    public ManagerDiscountPresenter managerDiscountPresenter;
    private StoreASMPresenter storeASMPresenter;
    SearchableSpinner productSpinner;

    ProgressDialog progressDialog;

    AppCompatButton btnCheck;
    LinearLayout price_layout;
    AppPreference appPreference;

    AppCompatTextView storeCode, modelName, modelCode, modelPrice, stockCount;
    String userToken;
    private List<DataItem> makeList;
    private List<ProductTypeDatItem> productTypeDatItems;
    private List<ModelItem> modelList;
    private List<com.sangeetha.employee.peopleTracking.model.DataItem> storeListItems;
    private List<StoreModel> storeList;

    private String selectedMake = "", selectedModel = "", selectedStore = "";
    String productId = "";

    private ArrayAdapter<String> makeAdapter, modelAdapter, productTypeAdapter, storeAdapter;
    private ArrayList<String> stores = new ArrayList<>();
    private ArrayList<String> makes = new ArrayList<>();
    private ArrayList<String> models = new ArrayList<>();
    private ArrayList<String> productTypes = new ArrayList<>();

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, EmployeeLanding.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_check);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        appPreference = new AppPreference(this);
        storeSpinner = findViewById(R.id.storeSpinner);
        makeSpinner = findViewById(R.id.makeSpinner);
        modelSpinner = findViewById(R.id.modelSpinner);
        paymentSpinner = findViewById(R.id.PaymentSpinner);
        productSpinner = findViewById(R.id.ProductTypeSpinner);

        btnCheck = findViewById(R.id.btnCheck);

        price_layout = findViewById(R.id.price_layout);
        storeSpinner.setOnItemSelectedListener(this);
        storeSpinner.setTitle("Select Store");
        storeSpinner.setPositiveButton("Done");

        makeSpinner.setOnItemSelectedListener(this);
        makeSpinner.setTitle("Select Make");
        makeSpinner.setPositiveButton("Done");

        productSpinner.setOnItemSelectedListener(this);
        productSpinner.setTitle("Select Product Type");
        productSpinner.setPositiveButton("Done");

        modelSpinner.setOnItemSelectedListener(this);
        modelSpinner.setTitle("Select Model");
        modelSpinner.setPositiveButton("Done");

        models.add(0, "---- Select Model ----");
        stores.add(0, "---- Select Store ----");
        makes.add(0, "---- Select Make ----");
        productTypes.add(0, "---- Select Product Type ----");

        makeAdapter = new ArrayAdapter<String>(this, R.layout.custom_spinner_layout, makes);
        modelAdapter = new ArrayAdapter<String>(this, R.layout.custom_spinner_layout, models);
        productTypeAdapter = new ArrayAdapter<String>(this, R.layout.custom_spinner_layout, productTypes);
        storeAdapter = new ArrayAdapter<String>(this, R.layout.custom_spinner_layout, stores);

        makeSpinner.setAdapter(makeAdapter);
        modelSpinner.setAdapter(modelAdapter);
        storeSpinner.setAdapter(storeAdapter);
        productSpinner.setAdapter(productTypeAdapter);

        userToken = appPreference.getUserToken();

        storeASMPresenter = new StoreASMPresenterImpl(this);


        storeCode = findViewById(R.id.storeCode);
        modelName = findViewById(R.id.modelName);
        modelCode = findViewById(R.id.modelCode);
        modelPrice = findViewById(R.id.modelPrice);
        stockCount = findViewById(R.id.stockCount);

        btnCheck.setOnClickListener(v -> {

            if (selectedStore.equalsIgnoreCase("")) {
                showMessage("Please select store to proceed.");
            } else if (productId.equalsIgnoreCase("")) {
                showMessage("Please select product type to proceed.");
            } else if (selectedMake.equalsIgnoreCase("")) {
                showMessage("Please select make to proceed.");
            } else if (selectedModel.equalsIgnoreCase("")) {
                showMessage("Please select model to proceed.");
            } else {
                if (ValidationUtils.isThereInternet(this)) {
                    // showProgress();
                    storeASMPresenter.stockCheck(userToken, selectedStore, productId, selectedMake, selectedModel);
                }
            }
        });


        if (ValidationUtils.isThereInternet(this)) {
            storeASMPresenter.getStores(userToken);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
            return true;
        }
        return (super.onOptionsItemSelected(item));
    }


    private static boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            //     Log.e("Network Testing", "***Available***");
            return true;
        } else {
            //    Log.e("Network Testing", "***Not Available***");
            Toast.makeText(mContext, "No network available, please check your WiFi or EmpData connection", Toast.LENGTH_LONG).show();
            return false;
        }

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        Locale languageType = LanguageUtil.getLanguageType(newBase);
        super.attachBaseContext(MyContextWrapper.wrap(newBase, languageType));
    }


    @Override
    public void onClick(View v) {

    }

    @Override
    public void showProgress() {

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please wait..");
        progressDialog.show();
    }

    @Override
    public void hideProgress() {

        if (progressDialog != null)
            progressDialog.dismiss();
    }

    @Override
    public void showMessage(String msg) {

        Toast.makeText(StockCheckActivity.this, msg, Toast.LENGTH_LONG).show();

    }

    @Override
    public void setStoreList(StoreListResponse storeList) {

    }

    @Override
    public void setStores(StoreResponse storeList) {
        if (storeList.isStatus()) {
            storeListItems = storeList.getData();
            for (com.sangeetha.employee.peopleTracking.model.DataItem item : storeListItems) {
                stores.add(item.getStoreName());
            }
            storeAdapter.notifyDataSetChanged();
        } else {
            showMessage(storeList.getMessage());
        }
    }

    @Override
    public void setStocks(StockCheckResponse response) {


        if (response.isStatus()) {
            price_layout.setVisibility(View.VISIBLE);
            storeCode.setText(response.stockCheckData.storeCode);
            modelName.setText(response.stockCheckData.modelName);
            modelCode.setText(response.stockCheckData.modelCode);
            modelPrice.setText(String.format("%s %s", getString(R.string.rs), response.stockCheckData.price));
            stockCount.setText(response.stockCheckData.stockCount);
        } else {

            price_layout.setVisibility(View.GONE);
            showMessage(response.getMessage());
        }

    }

    @Override
    public void setCheckEligibleException(CommonResponse commonResponse) {

    }

    @Override
    public void setSalesCheckCalculate(SubmitDiscountResponse commonResponse) {

    }

    @Override
    public void setSendExceptionRequest(CommonResponse commonResponse) {

    }

    @Override
    public void setMake(MakeResponse managerResponse) {
        makeList = managerResponse.getData();
        for (com.sangeetha.employee.managerDiscount.manager_pojo.DataItem dataItem : makeList) {
            makes.add(dataItem.getMakeName());
        }
        makeAdapter.notifyDataSetChanged();
    }

    @Override
    public void setProductTypes(ProductTypeResponse productTypesResponse) {
        productTypeDatItems = productTypesResponse.getData();
        for (ProductTypeDatItem dataItem : productTypeDatItems) {
            productTypes.add(dataItem.getProductName());
        }
        productTypeAdapter.notifyDataSetChanged();
    }


    @Override
    public void setModel(ModelResponse managerResponse) {
        modelList = managerResponse.getData();
        for (ModelItem modelItem : modelList) {
            models.add(modelItem.getModelName());
        }
        modelAdapter.notifyDataSetChanged();
    }



    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if (position > 0) {
            if(price_layout.getVisibility()==View.VISIBLE){
                price_layout.setVisibility(View.GONE);
            }

            switch (parent.getId()) {
                case R.id.makeSpinner:
                    String makeId = makeList.get(position - 1).getId();
                    selectedMake = makeId;
                    if (ValidationUtils.isThereInternet(this))
                        models.clear();
                    models.add(0, "---- Select Model ----");
                    modelAdapter.notifyDataSetChanged();
//                    paymentAdapter.notifyDataSetChanged();
                    storeASMPresenter.getModel(userToken, productId, makeId);
                    break;
                case R.id.storeSpinner:
                    selectedStore = storeListItems.get(position - 1).getStoreCode();
                    if (ValidationUtils.isThereInternet(this))
                        productTypes.clear();
                    productTypes.add(0, "---- Select Product Type ----");
                    productTypeAdapter.notifyDataSetChanged();

                    makes.clear();
                    makes.add(0, "---- Select Make ----");

                    models.clear();
                    models.add(0, "---- Select Model ----");


                    makeAdapter.notifyDataSetChanged();
                    modelAdapter.notifyDataSetChanged();
                    storeASMPresenter.getProductType(userToken, "");
                    break;
                case R.id.modelSpinner:
                    String modelId = modelList.get(position - 1).getId();
                    selectedModel = modelId;

                    break;

                case R.id.ProductTypeSpinner:
                    productId = productTypeDatItems.get(position - 1).getId();
                    if (ValidationUtils.isThereInternet(this)) {

                        makes.clear();
                        makes.add(0, "---- Select Make ----");

                        models.clear();
                        models.add(0, "---- Select Model ----");

                        makeAdapter.notifyDataSetChanged();
                        modelAdapter.notifyDataSetChanged();
                        storeASMPresenter.getMake(userToken, productId);
                    }
                    break;

            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    void showAlert(String message) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle("Stock Check");
        alertBuilder.setMessage(message);
        AlertDialog alert = alertBuilder.create();
        alert.show();
        alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            public void onClick(DialogInterface dialog, int which) {
                alert.dismiss();
            }
        });
    }
}