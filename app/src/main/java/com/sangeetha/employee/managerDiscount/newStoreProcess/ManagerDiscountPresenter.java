package com.sangeetha.employee.managerDiscount.newStoreProcess;

public interface ManagerDiscountPresenter {
    void getMake(String userToken, String productId);

    void getProductType(String token, String paymentId);

    void getModel(String userToken, String productId, String makeId);

    void getPaymentList(String token);

    void getProductPrice(String token, String productType, String brandId
            , String modelId, String paymentMode);

    void checkModelDiscount(String modelId, String storeId);

    void checkModelPaymentDiscount(String token, String productType, String brandId
            , String modelId, String paymentMode);

    void generateOTP(String token,String mobileNumber);

    void validateOTP(String token,String mobileNumber, String otp);

    void resendOtp(String token,String mobileNumber);

    void sendRequest(String managerId,
                     String storeId,
                     String employeeId,
                     String modelId,
                     String discountPer,
                     String discountAmount,
                     String totalAmount,
                     String customerMobile,
                     boolean mobileVerified,
                     String emil,
                     String customerName,
                     String customerOtp,
                     String approver);

    void sendPaymentRequest(String token,String managerId,
                            String employeeId,
                            String modelId,
                            String discountPer,
                            String totalAmount,
                            String customerMobile,
                            boolean mobileVerified,
                            String emil,
                            String customerName,
                            String customerOtp,
                            String approver, String paymentID,
                            String productType,
                            String makeId);

    void submitDiscount();

    void getApproval();

    void getCouponCode();

    void findEmployeeName(String token,String empId);

    void alternativeMethod(String token,String approvalCode, String sourceFrom);

    void couponHistory(String storeId);

    void discountDetails(String token,String refId);

    void discountInfo(String token,String refId);

    void cancelCoupon(String refId);

    void percentageBaseDiscount(String token,
                                String productType,  String brandId,
                                String modelId, String paymentMode,
                                String manageId, String discountId);

    void amountBasedDiscount(String storeId, String modelId, String manageId, String discountValue, String totalValue);

    void checkCouponCode(String regId);

    void couponCodeCancel(String regId);

    void discountApprove(String token,String refId);

    void discountReject(String token,String refId, String reason, String note);

}
