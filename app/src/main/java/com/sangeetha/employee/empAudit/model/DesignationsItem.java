package com.sangeetha.employee.empAudit.model;

import com.google.gson.annotations.SerializedName;

public class DesignationsItem {

    @SerializedName("designation_name")
    private String designationName;

    @SerializedName("status")
    private String status;

    public String getDesignationName() {
        return designationName;
    }

    public void setDesignationName(String designationName) {
        this.designationName = designationName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "DesignationsItem{" +
                        "designation_name = '" + designationName + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}