package com.sangeetha.employee.callRecorder.domain.model

import com.google.gson.annotations.SerializedName

data class AudioFileUploadResponse(
    @SerializedName("status")
    val status: Boolean,
    @SerializedName("data")
    val data: AudioFileUploadData,
    @SerializedName("message")
    val message: String,
    @SerializedName("designation")
    val designation: String,
    @SerializedName("token")
    val token: String
)

data class AudioFileUploadData(
    @SerializedName("image_id")
    val imageId: Long
)
