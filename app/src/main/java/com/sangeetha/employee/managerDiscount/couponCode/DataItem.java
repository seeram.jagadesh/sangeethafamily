package com.sangeetha.employee.managerDiscount.couponCode;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class DataItem implements Parcelable {

    public static final Creator<DataItem> CREATOR = new Creator<DataItem>() {
        @Override
        public DataItem createFromParcel(Parcel in) {
            return new DataItem(in);
        }

        @Override
        public DataItem[] newArray(int size) {
            return new DataItem[size];
        }
    };
    @SerializedName("amount")
    private String amount;
    @SerializedName("coupon_code")
    private String couponCode;
    @SerializedName("offer_type")
    private String offerType;

    protected DataItem(Parcel in) {
        amount = in.readString();
        couponCode = in.readString();
        offerType = in.readString();
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getOfferType() {
        return offerType;
    }

    public void setOfferType(String offerType) {
        this.offerType = offerType;
    }

    @Override
    public String toString() {
        return
                "DataItem{" +
                        "amount = '" + amount + '\'' +
                        ",coupon_code = '" + couponCode + '\'' +
                        ",offer_type = '" + offerType + '\'' +
                        "}";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(amount);
        dest.writeString(couponCode);
        dest.writeString(offerType);
    }
}