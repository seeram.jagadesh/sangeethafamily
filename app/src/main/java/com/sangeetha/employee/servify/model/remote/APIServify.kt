package com.sangeetha.employee.servify.model.remote

import com.sangeetha.employee.managerDiscount.manager_pojo.MakeResponse
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelResponse
import com.sangeetha.employee.servify.model.data.*
import retrofit2.Call
import retrofit2.http.*

interface APIServify {

    @FormUrlEncoded
    @POST("api-servify-exchange-code")
    fun getCheckExchange(@Header("sangeetha_token") token: String, @Field("servify_cec") cec: String): Call<CheckReferenceCodeData>


    @POST("api-new-make")
    fun getServifyMake(): Call<ServifyMake>

    @FormUrlEncoded
    @POST("api-new-model")
    fun getServifyModel(@Field("make_id") model: String): Call<ServifyModel>

    @FormUrlEncoded
    @POST("api-check-employee")
    fun findEmp(@Header("sangeetha_token") token: String, @Field("employee_id") empName: String): Call<FindEmpData>

    @FormUrlEncoded
    @POST("api-servify-submit")
    fun exchangeSubmit(@Header("sangeetha_token") storeId: String,
                       @Field("trade_reference_id") referenceId: String,
                       @Field("servify_amount") amount: String,
                       @Field("trade_model") tradeModel: String,
                       @Field("imei") imei: String,
                       @Field("trade_status") tradeStatus: String,
                       @Field("customer_name") customerName: String,
                       @Field("customer_mobile") customerMobile: String,
                       @Field("customer_email") customerEmail: String,
                       @Field("make") make: String,
                       @Field("model") model: String,
                       @Field("employee_id") empId: String,
                       @Field("employee_name") empName: String,
                       @Field("agree") agree: String,
                       @Field("product_type") productId: String,
                       @Field("retailer_amount") retailerAmount: String,
                       @Field("offer_name") offerAmount: String,
                       @Field("offer_value") offerValue: String,
                       @Field("sang_offer_name") sangOfferAmount: String,
                       @Field("sang_offer_value") sangOfferValue: String): Call<ServifyCouponCode>

    @GET("api-servify-history")
    fun viewHistory(@Header("sangeetha_token") storeId: String): Call<ServifyHistory>

    @FormUrlEncoded
    @POST("api-serify-view-details")
    fun viewDetails(@Header("sangeetha_token") storeId: String,
                    @Field("reference_id") referenceId: String): Call<ServifyHistoryData>

    @FormUrlEncoded
    @POST("api-serify-view-details")
    fun viewItemDetails(@Header("sangeetha_token") storeId: String,
                        @Field("reference_id") referenceId: String): Call<ServifyHistoryResponseData>

    //yriv2kvihg
    @FormUrlEncoded
    @POST("api-serify-change-make-model")
    fun changeModel(@Header("sangeetha_token") storeId: String,
                    @Field("reference_id") referenceId: String,
                    @Field("make") make: String,
                    @Field("model") model: String,
                    @Field("product_type") productId: String): Call<ResponseMessage>

    @FormUrlEncoded
    @POST("api-serify-cancel-coupon-code")
    fun cancelCoupon(@Header("sangeetha_token") storeId: String,
                     @Field("reference_id") referenceId: String): Call<ResponseMessage>

    @FormUrlEncoded
    @POST("api-get-brand")
    fun getMakeDetail(@Header("sangeetha_token") token: String, @Field("product_value") productId: String): Call<MakeResponse>

    @FormUrlEncoded
    @POST("api-get-model")
    fun getModel(@Header("sangeetha_token") token: String, @Field("product_value") productId: String, @Field("brand_value") makeId: String): Call<ModelResponse>


    @POST("api-servify-fetch-product-type")
    fun getProductTypes(@Header("sangeetha_token") token: String): Call<ProductTypeResponse>


}