package com.sangeetha.employee.empLogin;


import com.sangeetha.employee.empLogin.loginModel.ASMData;
import com.sangeetha.employee.empLogin.loginModel.EmpData;
import com.sangeetha.employee.empLogin.loginModel.OnlineDeliveryData;
import com.sangeetha.employee.empLogin.loginModel.StoreData;

public interface EmpLoginView {
    void onIdError(String error);

    void onPassError(String error);

    void showMessage(String message);

    void loginSuccesses(EmpData data);

    void setStoreData(StoreData storeData);

    void setASMData(ASMData asmData);

    void setOnlineDeliveryData(OnlineDeliveryData onlineDeliveryData);

    void showProgress();

    void hideProgress();

    void setToken(String token);

    void setCallRecordingEnable(Boolean isEnable);

    void deliveryBoyLogin(EmpData data);
}
