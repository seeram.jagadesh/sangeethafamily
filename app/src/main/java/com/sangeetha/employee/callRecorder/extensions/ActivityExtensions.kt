package com.sangeetha.employee.callRecorder.extensions

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.widget.Toast


fun Activity.showToast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}