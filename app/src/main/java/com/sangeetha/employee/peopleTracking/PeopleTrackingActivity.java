package com.sangeetha.employee.peopleTracking;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.sangeetha.employee.BuildConfig;
import com.sangeetha.employee.R;
import com.sangeetha.employee.customwidget.SearchableSpinner;
import com.sangeetha.employee.peopleTracking.model.DataItem;
import com.sangeetha.employee.peopleTracking.model.StoreResponse;
import com.sangeetha.employee.peopleTracking.model.TrackingResponse;
import com.sangeetha.employee.utils.AppPreference;
import com.sangeetha.employee.utils.ValidationUtils;

import java.util.List;

import timber.log.Timber;

public class PeopleTrackingActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, TrackingView {

    private static final String TAG = PeopleTrackingActivity.class.getSimpleName();
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    protected Location mLastLocation;
    private RelativeLayout progressBar;
    private SearchableSpinner storeSpinner;
    private String storeCode, storeName;
    private List<DataItem> data;
    private TrackingPresenter trackingPresenter;
    private String trackingId;
    private AppPreference appPreference;
    private AppCompatButton materialButton;
    private String empId;
    private LinearLayoutCompat spinnerLayout;
    private FusedLocationProviderClient mFusedLocationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_people_tracking);
        appPreference = new AppPreference(this);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        spinnerLayout = findViewById(R.id.spinner_layout);
        progressBar = findViewById(R.id.progressLinear);
        storeSpinner = findViewById(R.id.store_spinner);
        storeSpinner.setOnItemSelectedListener(this);
        storeSpinner.setTitle("Select Store");
        storeSpinner.setPositiveButton("Done");
        trackingPresenter = new TrackingPresenterImpl(this);
        if (ValidationUtils.isThereInternet(this))
            trackingPresenter.getStoreList();
        materialButton = findViewById(R.id.tracking_submit);
        materialButton.setOnClickListener(v -> {
            if (TextUtils.isEmpty(trackingId) && ValidationUtils.isThereInternet(PeopleTrackingActivity.this) && mLastLocation != null) {
                if (appPreference.getEmpData() != null) {
                    empId = appPreference.getEmpData().getEmpId();
                } else if (appPreference.getASMData() != null) {
                    empId = appPreference.getASMData().getEmpId();
                }
                trackingPresenter.startTracking(empId, storeCode, "", "start", String.valueOf(mLastLocation.getLatitude()), String.valueOf(mLastLocation.getLongitude()));
            } else if (mLastLocation != null) {
                trackingPresenter.stopTracking(empId, storeCode, "", "stop", String.valueOf(mLastLocation.getLatitude()), String.valueOf(mLastLocation.getLongitude()), trackingId);
            } else {
                showMessage(getString(R.string.no_location_detected));
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        storeCode = data.get(position).getStoreCode();
        storeName = data.get(position).getStoreName();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void setStoreList(StoreResponse storeList) {
        this.data = storeList.getData();
        String[] stores = new String[data.size()];
        for (int i = 0; i < data.size(); i++) {
            stores[i] = data.get(i).getStoreName();
        }
        storeSpinner.setAdapter(new ArrayAdapter<>(PeopleTrackingActivity.this, R.layout.custom_spinner_layout, stores));
    }

    @Override
    public void showMessage(String msg) {
        Toast.makeText(PeopleTrackingActivity.this, msg, Toast.LENGTH_LONG).show();

    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setTrackingResponse(TrackingResponse trackingResponse) {
        if (trackingResponse.isStatus()) {
            if (trackingResponse.getTrackId() != 0) {
                spinnerLayout.setVisibility(View.INVISIBLE);
                trackingId = String.valueOf(trackingResponse.getTrackId());
                materialButton.setText("Stop");
            } else {
                spinnerLayout.setVisibility(View.VISIBLE);
                materialButton.setText("Start");
                showMessage(trackingResponse.getMessage());
                finish();
            }
        } else {
            showMessage(trackingResponse.getMessage());
        }

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (!checkPermissions()) {
            requestPermissions();
        } else {
            getLastLocation();
        }
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(PeopleTrackingActivity.this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                REQUEST_PERMISSIONS_REQUEST_CODE);
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION);
        if (shouldProvideRationale) {
            showSnackbar(R.string.permission_rationale, android.R.string.ok,
                    view -> {
                        startLocationPermissionRequest();
                    });

        } else {
            startLocationPermissionRequest();
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
 //       Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                Timber.i("User cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLastLocation();
            } else {
                showSnackbar(R.string.permission_denied_explanation, R.string.settings,
                        view -> {
                            Intent intent = new Intent();
                            intent.setAction(
                                    Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package",
                                    BuildConfig.APPLICATION_ID, null);
                            intent.setData(uri);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        });
            }
        }
    }

    private void getLastLocation() {
        mFusedLocationClient.getLastLocation()
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful() && task.getResult() != null) {
                        mLastLocation = task.getResult();

                        // Log.e(TAG, "lat "+mLastLocation.getLatitude()+" lng "+mLastLocation.getLongitude());
                    } else {
                        //Log.w(TAG, "getLastLocation:exception", task.getException());
                        showSnackbar(getString(R.string.no_location_detected));
                    }
                });
    }

    private void showSnackbar(final String text) {
        View container = findViewById(R.id.content);
        if (container != null) {
            Snackbar.make(container, text, Snackbar.LENGTH_LONG).show();
        }
    }

    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }

}