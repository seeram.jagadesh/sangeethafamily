package com.sangeetha.employee;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sangeetha.employee.utils.AppConstants;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public abstract class SangeethaCareApiClient {
    private static Retrofit retrofit, retrofit1;
    private static Retrofit rechargeRetrofit;
    private static Retrofit priceMatchClient;

    private static OkHttpClient getHttpClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(AppConstants.LoggingCode);

        return new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();
    }

    public static class CacheInterceptor implements Interceptor {
        @NotNull
        @Override
        public Response intercept(Chain chain) throws IOException {
            Response response = chain.proceed(chain.request());

            CacheControl cacheControl = new CacheControl.Builder()
                    .noCache()
                    .noStore().noTransform()
                    .maxAge(1, TimeUnit.MINUTES) // 15 minutes cache
                    .build();

            return response.newBuilder()
                    .removeHeader("Pragma")
                    .removeHeader("Cache-Control")
                    .header("Cache-Control", cacheControl.toString())
                    .build();
        }
    }
    //CHANGE BASE URL WHILE IN LIVE MODE
    public static Retrofit getRechargeClient() {
        Gson gson = new GsonBuilder()
                .create();

        if (rechargeRetrofit == null) {
            rechargeRetrofit = new Retrofit.Builder()
                    // .baseUrl(AppConstants.RECHARGE_BASE_URL)
                    .baseUrl(AppConstants.zapStoreBackEndBaseUrl)
                    .client(getUnsafeOkHttpClient())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        }
        return rechargeRetrofit;
    }

    public static Retrofit getClient() {
        Gson gson = new GsonBuilder()

                .create();
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(AppConstants.zapStoreBackEndBaseUrl)
                    .client(getUnsafeOkHttpClient())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        }
        return retrofit;
    }
//    public static Retrofit getDevelopClient() {
//        Gson gson = new GsonBuilder()
//
//                .create();
//        if (retrofit == null) {
//            retrofit = new Retrofit.Builder()
//                    .baseUrl(AppConstants.developAPI)
//                    .client(getUnsafeOkHttpClient())
//                    .addConverterFactory(GsonConverterFactory.create(gson))
//                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//                    .build();
//        }
//        return retrofit;
//    }

    public static Retrofit getEcomClient() {
        Gson gson = new GsonBuilder()

                .create();
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(AppConstants.sangeetain)
                    .client(getUnsafeOkHttpClient())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        }
        return retrofit;
    }

    private static OkHttpClient getUnsafeOkHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.readTimeout(240, TimeUnit.SECONDS);
        builder.writeTimeout(240,TimeUnit.SECONDS);
        builder.addInterceptor(new CacheInterceptor());
        builder.connectTimeout(240, TimeUnit.SECONDS);
        try {
            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init((KeyStore) null);
            TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();
            if (trustManagers.length != 1 || !(trustManagers[0] instanceof X509TrustManager)) {
                throw new IllegalStateException("Unexpected default trust managers:" + Arrays.toString(trustManagers));
            }
            X509TrustManager trustManager = (X509TrustManager) trustManagers[0];
            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, new TrustManager[]{trustManager}, null);
            SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
            builder.sslSocketFactory(sslSocketFactory, trustManager);
        } catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
            e.printStackTrace();
        }
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(AppConstants.LoggingCode);

        return builder
                .addInterceptor(interceptor)
                .build();
    }

    public static Retrofit getClient1() {

        Gson gson = new GsonBuilder()

                .create();
        if (retrofit1 == null) {
            retrofit1 = new Retrofit.Builder()
                    .baseUrl(AppConstants.onlineDelivery)
                    .client(getUnsafeOkHttpClient())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit1;
    }
}
