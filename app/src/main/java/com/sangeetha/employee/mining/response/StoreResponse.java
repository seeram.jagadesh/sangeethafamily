package com.sangeetha.employee.mining.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sangeetha.employee.mining.model.calendarModel;

import java.util.List;

public class StoreResponse {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<StoreModel> data;

    @Override
    public String toString() {
        return "StoreResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<StoreModel> getData() {
        return data;
    }

    public void setData(List<StoreModel> data) {
        this.data = data;
    }
}
