package com.sangeetha.employee.callRecorder.sms.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import com.sangeetha.employee.callRecorder.domain.repository.CUGRoomRepository
import com.sangeetha.employee.callRecorder.domain.room.model.CUGMessage
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

class SMSViewModel @Inject constructor(
    private val cugRoomRepository: CUGRoomRepository
) : ViewModel() {

    private val _messagesLiveData = MutableLiveData<PagingData<CUGMessage>>()
    val messagesLiveData: LiveData<PagingData<CUGMessage>> = _messagesLiveData

    fun loadMessages() {
        cugRoomRepository.getMessages().onEach {
            _messagesLiveData.value = it
        }.launchIn(viewModelScope)
    }
}