package com.sangeetha.employee.checkStock;

import com.google.gson.annotations.SerializedName;

public class StockCheckData {

    @SerializedName("id")
    String id;

    @SerializedName("model_code")
    String modelCode;

    @SerializedName("model_name")
    String modelName;

    @SerializedName("product_cat_id")
    String productCatId;

    @SerializedName("price")
    String price;

    @SerializedName("store_code")
    String storeCode;

    @SerializedName("stock_count")
    String stockCount;

    @Override
    public String toString() {
        return "StockCheckData{" +
                "id='" + id + '\'' +
                ", modelCode='" + modelCode + '\'' +
                ", modelName='" + modelName + '\'' +
                ", productCatId='" + productCatId + '\'' +
                ", price='" + price + '\'' +
                ", storeCode='" + storeCode + '\'' +
                ", stockCount='" + stockCount + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModelCode() {
        return modelCode;
    }

    public void setModelCode(String modelCode) {
        this.modelCode = modelCode;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getProductCatId() {
        return productCatId;
    }

    public void setProductCatId(String productCatId) {
        this.productCatId = productCatId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getStockCount() {
        return stockCount;
    }

    public void setStockCount(String stockCount) {
        this.stockCount = stockCount;
    }
}
