package com.sangeetha.employee.callRecorder.domain.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.sangeetha.employee.callRecorder.domain.room.CUGDao
import com.sangeetha.employee.callRecorder.domain.room.model.CUGMessage

@Database(entities = [CUGMessage::class], version = 1)
abstract class CUGDatabase : RoomDatabase() {

    companion object {
        const val DATABASE_NAME = "cug_database"
    }

    abstract fun cugDao(): CUGDao
}