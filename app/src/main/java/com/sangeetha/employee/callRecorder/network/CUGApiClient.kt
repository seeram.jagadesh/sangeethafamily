package com.sangeetha.employee.callRecorder.network

import com.sangeetha.employee.BuildConfig
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object CUGApiClient {

    private const val LOGIN_BASE_URL = "https://sangeetha-cug.bangalore2.com/"
    private const val CHECK_IN_BASE_URL = "https://connect.sangeethamobiles.net/"
    private const val BASE_URL = "https://apidevelop.sangeethamobiles.net/"

    val service: CUGAPIService = retrofit().create(CUGAPIService::class.java)
    val loginService: CUGAPIService = retrofitLogin().create(CUGAPIService::class.java)

    val checkInService: CUGAPIService = retrofitCheckIn().create(CUGAPIService::class.java)

    private fun retrofitLogin(): Retrofit {
        val okHttpClient = OkHttpClient.Builder().apply {
            addInterceptor(
                Interceptor { chain ->
                    val builder = chain.request().newBuilder()
                    builder.header("app_version", BuildConfig.VERSION_NAME)
                    return@Interceptor chain.proceed(builder.build())
                }
            )
            addInterceptor(
                HttpLoggingInterceptor().apply {
                    setLevel(HttpLoggingInterceptor.Level.BODY)
                }
            )
        }.build()
        return Retrofit.Builder()
            .baseUrl(LOGIN_BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun retrofitCheckIn(): Retrofit {
        val okHttpClient = OkHttpClient.Builder().apply {
            addInterceptor(
                Interceptor { chain ->
                    val builder = chain.request().newBuilder()
                    builder.header("app_version", BuildConfig.VERSION_NAME)
                    return@Interceptor chain.proceed(builder.build())
                }
            )
            addInterceptor(
                HttpLoggingInterceptor().apply {
                    setLevel(HttpLoggingInterceptor.Level.BODY)
                }
            )
        }.build()
        return Retrofit.Builder()
            .baseUrl(CHECK_IN_BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun retrofit(): Retrofit {
        val okHttpClient = OkHttpClient.Builder().apply {
            addInterceptor(
                Interceptor { chain ->
                    val builder = chain.request().newBuilder()
                    builder.header("app_version", BuildConfig.VERSION_NAME)
                    return@Interceptor chain.proceed(builder.build())
                }
            )
            addInterceptor(
                HttpLoggingInterceptor().apply {
                    setLevel(HttpLoggingInterceptor.Level.BODY)
                }
            )
        }.build()
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
}