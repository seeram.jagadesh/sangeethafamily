package com.sangeetha.employee.managerDiscount.storeManager;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.github.florent37.expansionpanel.ExpansionLayout;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.chip.Chip;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.sangeetha.employee.CommonResponse;
import com.sangeetha.employee.CouponListDialogFragment;
import com.sangeetha.employee.R;
import com.sangeetha.employee.customwidget.SearchableSpinner;
import com.sangeetha.employee.managerDiscount.couponCode.CheckCouponCode;
import com.sangeetha.employee.managerDiscount.manager_pojo.AlternativeMethodResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ApproverDetails;
import com.sangeetha.employee.managerDiscount.manager_pojo.ApproverHtmlItem;
import com.sangeetha.employee.managerDiscount.manager_pojo.Data;
import com.sangeetha.employee.managerDiscount.manager_pojo.DataItem;
import com.sangeetha.employee.managerDiscount.manager_pojo.DiscountDetailsResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.FindEmployeeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.MakeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelDiscountResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelItem;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelPriceDetails;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelPriceResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.PaymentDataItem;
import com.sangeetha.employee.managerDiscount.manager_pojo.PaymentModeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ProductTypeDatItem;
import com.sangeetha.employee.managerDiscount.manager_pojo.ProductTypeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.SubmitDiscountResponse;
import com.sangeetha.employee.utils.AppConstants;
import com.sangeetha.employee.utils.AppPreference;
import com.sangeetha.employee.utils.LanguageUtil;
import com.sangeetha.employee.utils.MyContextWrapper;
import com.sangeetha.employee.utils.ValidationUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class StoreMainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener,
        StoreView, View.OnClickListener, BottomNavigationView.OnNavigationItemSelectedListener,
        StoreHistoryFragment.OnFragmentInteractionListener, StoreAccountFragment.OnFragmentInteractionListener, CouponListDialogFragment.Listener {


    private ExpansionLayout productLayout, discountLayout, customerLayout, approvalLayout;
    private AppCompatTextView nameTextView;
    private AppCompatTextView priceTextView;
    private AppCompatTextView productPrice;
    private AppCompatTextView discountAmount;
    private AppCompatTextView discountedPrice;
    private AppCompatTextView discountErrorMsg;
    private AppCompatTextView otpWillExpireTV;
    private AppCompatTextView empName;
    private AppCompatTextView refId;
    private AppCompatTextView statusTxt;
    public TextInputEditText customerMobile, otpText, employeeId, apxCode, customerName, customerEmail;
    private AppCompatEditText discountPercentage;
    public ManagerDiscountPresenter managerDiscountPresenter;
    private List<DataItem> makeList;
    private List<ProductTypeDatItem> productTypeDatItems;
    private List<ModelItem> modelList;
    private List<PaymentDataItem> paymentModeList;
    private ProgressBar progressBar;

    private String selectedMake, selectedModel, paymentID;
    String productId;
    private ModelDiscountResponse modelDiscountResponse;
    private boolean otpVerification = false;
    private String otpTypeValue;
    private String OTPDefValue = "1111";
    private LinearLayoutCompat otpValidation;
    private LinearLayout priceLayout;
    private TextInputLayout mobileInputLayout, employeeIdInputLayout;
    public AppPreference appPreference;
    private String mangeId;
    private String referenceId;
    private Chip generateOtp;
    private LinearLayoutCompat refreshLayout;


    private CountDownTimer otpTimer;
    String userToken;
    private boolean isTimerRunning;
    private long timeRemaining;
    private boolean discountSubmit = false;
    private ArrayAdapter<String> htmlItemArrayAdapter;
    private String selectedApprovel;
    private ArrayList<ApproverDetails> approverHtmlItemList = new ArrayList<>();
    ApproverDetails approverDetails;
    private ArrayList<String> approverList;
    private AppCompatButton phoneDetailsContinue, submitDiscount;
    private long durationOfOtp = 120000;
    private BottomNavigationView bottomNavigationView;

    private AppCompatTextView discountTv;
    private ArrayAdapter<String> makeAdapter, modelAdapter, paymentAdapter, productTypeAdapter;
    private ArrayList<String> makes = new ArrayList<>();
    private ArrayList<String> models = new ArrayList<>();
    private ArrayList<String> payments = new ArrayList<>();
    private ArrayList<String> productTypes = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_store_main);

        appPreference = new AppPreference(this);
        bottomNavigationView = findViewById(R.id.store_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

        phoneDetailsContinue = findViewById(R.id.phone_detail_continue);
        productLayout = findViewById(R.id.productLayout);
        discountLayout = findViewById(R.id.discountLayout);
        customerLayout = findViewById(R.id.customerLayout);
        approvalLayout = findViewById(R.id.approvalLayout);

        SearchableSpinner makeSpinner = findViewById(R.id.makeSpinner);
        SearchableSpinner modelSpinner = findViewById(R.id.modelSpinner);
        SearchableSpinner paymentSpinner = findViewById(R.id.PaymentSpinner);
        SearchableSpinner productSpinner = findViewById(R.id.ProductTypeSpinner);
        phoneDetailsContinue.setOnClickListener(this);

        nameTextView = findViewById(R.id.nameTextView);
        priceTextView = findViewById(R.id.priceTextView);


        userToken = appPreference.getUserToken();
        AppCompatButton chooseDiscountContinue = findViewById(R.id.choose_discount_continue);
        AppCompatButton chooseDiscountBack = findViewById(R.id.choose_discount_back);
        productPrice = findViewById(R.id.currentPrice);
        discountAmount = findViewById(R.id.choose_discount_amount);
        AppCompatTextView discountPercentageTxt = findViewById(R.id.discount_percentage_txt);
        discountPercentage = findViewById(R.id.choose_discount_percentage);
        discountedPrice = findViewById(R.id.choose_total_price);
        discountErrorMsg = findViewById(R.id.discount_error);
        AppCompatTextView discountLimitError = findViewById(R.id.discount_limit_error);
        discountTv = findViewById(R.id.discount_tv);

        LinearLayoutCompat otpLayout = findViewById(R.id.otp_layout);
        otpValidation = findViewById(R.id.otp_validation);
        priceLayout = findViewById(R.id.price_layout);


        mobileInputLayout = findViewById(R.id.mobileInputLayout);
        otpWillExpireTV = findViewById(R.id.otp_will_expire_tv);
        AppCompatButton customerValidationContinue = findViewById(R.id.customer_validation_continue);
        AppCompatButton customerValidationBack = findViewById(R.id.customer_validation_back);
         customerName = findViewById(R.id.txtName);
        customerMobile = findViewById(R.id.txtMobile);
        customerEmail = findViewById(R.id.txtEmailStore);

        approvalLayout = findViewById(R.id.approvalLayout);

        employeeIdInputLayout = findViewById(R.id.employeeIdInputLayout);
        refId = findViewById(R.id.ref_id);
        empName = findViewById(R.id.emp_name);
        otpText = findViewById(R.id.txtOtp);
        progressBar = findViewById(R.id.request_progress);
        AppCompatButton approvalRequestBack = findViewById(R.id.approval_request_back);
        employeeId = findViewById(R.id.txtEmployee);
        apxCode = findViewById(R.id.txtCode);
        AppCompatSpinner authoritySpinner = findViewById(R.id.approvalAuthoritySpinner);
        refreshLayout = findViewById(R.id.refresh_layout);
        statusTxt = findViewById(R.id.status_txt);
        submitDiscount = findViewById(R.id.submitDiscount);
        FloatingActionButton refreshButton = findViewById(R.id.refresh_button);
        refreshButton.setOnClickListener(this);


        makeSpinner.setOnItemSelectedListener(this);
        makeSpinner.setTitle("Select Make");
        makeSpinner.setPositiveButton("Done");

        productSpinner.setOnItemSelectedListener(this);
        productSpinner.setTitle("Select Product Type");
        productSpinner.setPositiveButton("Done");

        modelSpinner.setOnItemSelectedListener(this);
        modelSpinner.setTitle("Select Model");
        modelSpinner.setPositiveButton("Done");

        paymentSpinner.setOnItemSelectedListener(this);
        paymentSpinner.setTitle("Select Payment Type");
        paymentSpinner.setPositiveButton("Done");

        models.add(0, "---- Select Model ----");
        makes.add(0, "---- Select Make ----");
        productTypes.add(0, "---- Select Product Type ----");
        payments.add(0, "---- Select Payment Type ----");
        makeAdapter = new ArrayAdapter<String>(StoreMainActivity.this, R.layout.custom_spinner_layout, makes);
        modelAdapter = new ArrayAdapter<String>(StoreMainActivity.this, R.layout.custom_spinner_layout, models);
        paymentAdapter = new ArrayAdapter<String>(StoreMainActivity.this, R.layout.custom_spinner_layout, payments);
        productTypeAdapter = new ArrayAdapter<String>(StoreMainActivity.this, R.layout.custom_spinner_layout, productTypes);

        makeSpinner.setAdapter(makeAdapter);
        modelSpinner.setAdapter(modelAdapter);
        paymentSpinner.setAdapter(paymentAdapter);
        productSpinner.setAdapter(productTypeAdapter);

        managerDiscountPresenter = new ManagerDiscountPresenterImp(this);

        if (ValidationUtils.isThereInternet(this)) {
            managerDiscountPresenter.getPaymentList(userToken);
        }


        chooseDiscountContinue.setOnClickListener(this);
        chooseDiscountBack.setOnClickListener(this);
        customerValidationContinue.setOnClickListener(this);
        customerValidationBack.setOnClickListener(this);
        approvalRequestBack.setOnClickListener(this);

        submitDiscount.setOnClickListener(this);
        generateOtp = findViewById(R.id.generate_otp);
        generateOtp.setOnClickListener(this);
        findViewById(R.id.verify_otp).setOnClickListener(this);
        findViewById(R.id.send_request).setOnClickListener(this);
        findViewById(R.id.submit_code).setOnClickListener(this);
//        submitDiscount.setBackground(AppConstants.setButtonbg(getResources().getDrawable(R.drawable.white_bg),
//                Color.parseColor("#03a9f3"),Color.parseColor("#03a9f3"),0));

        employeeId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() >= 3) {
                    if (ValidationUtils.isThereInternet(StoreMainActivity.this))
                        managerDiscountPresenter.findEmployeeName(userToken,s.toString());
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        approverList = new ArrayList<>();
        approverList.add("Approval Authority");
        htmlItemArrayAdapter = new ArrayAdapter<String>(StoreMainActivity.this, R.layout.custom_spinner_layout, approverList);
        authoritySpinner.setAdapter(htmlItemArrayAdapter);
        authoritySpinner.setOnItemSelectedListener(this);
        //productLayout.toggle(true);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        Locale languageType = LanguageUtil.getLanguageType(newBase);
        super.attachBaseContext(MyContextWrapper.wrap(newBase, languageType));
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position > 0) {
            switch (parent.getId()) {
                case R.id.makeSpinner:
                    String makeId = makeList.get(position - 1).getId();
                    selectedMake = makeId;
                    if (ValidationUtils.isThereInternet(StoreMainActivity.this))
                        models.clear();
                    models.add(0, "---- Select Model ----");
                     modelAdapter.notifyDataSetChanged();
//                    paymentAdapter.notifyDataSetChanged();
                    managerDiscountPresenter.getModel(userToken, productId, makeId);
                    priceLayout.setVisibility(View.GONE);
                    discountErrorMsg.setVisibility(View.GONE);
                    break;
                case R.id.modelSpinner:
                    String modelId = modelList.get(position - 1).getId();
                    selectedModel = modelId;
                    if (ValidationUtils.isThereInternet(StoreMainActivity.this))
                        managerDiscountPresenter.checkModelPaymentDiscount(userToken, productId, selectedMake, selectedModel, paymentID);

//                    paymentModeList
                    break;
                case R.id.PaymentSpinner:
                    paymentID = paymentModeList.get(position - 1).getId();
                    if (ValidationUtils.isThereInternet(StoreMainActivity.this)) {
                        productTypes.clear();
                        productTypes.add(0, "---- Select Product Type ----");
                        makes.clear();
                        makes.add(0, "---- Select Make ----");
                        models.clear();
                        models.add(0, "---- Select Model ----");
                        modelAdapter.notifyDataSetChanged();
                        productTypeAdapter.notifyDataSetChanged();
                        makeAdapter.notifyDataSetChanged();
                        managerDiscountPresenter.getProductType(userToken, paymentID);
                    }
                    break;
                case R.id.ProductTypeSpinner:
                    productId = productTypeDatItems.get(position - 1).getId();
                    if (ValidationUtils.isThereInternet(StoreMainActivity.this)) {

                        makes.clear();
                        makes.add(0, "---- Select Make ----");
                        models.clear();
                        models.add(0, "---- Select Model ----");
                        modelAdapter.notifyDataSetChanged();
                        makeAdapter.notifyDataSetChanged();
                        managerDiscountPresenter.getMake(userToken, productId);
                    }
                    break;
                case R.id.approvalAuthoritySpinner:
                    selectedApprovel = approverHtmlItemList.get(position - 1).getEmployeeId();
                    break;
            }
        }


    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showMessage(String msg) {
        Toast.makeText(StoreMainActivity.this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setMake(MakeResponse managerResponse) {
        makeList = managerResponse.getData();
        for (DataItem dataItem : makeList) {
            makes.add(dataItem.getMakeName());
        }
        makeAdapter.notifyDataSetChanged();
    }

    @Override
    public void setProductTypes(ProductTypeResponse productTypesResponse) {
        productTypeDatItems = productTypesResponse.getData();
        for (ProductTypeDatItem dataItem : productTypeDatItems) {
            productTypes.add(dataItem.getProductName());
        }
        productTypeAdapter.notifyDataSetChanged();
    }

    @Override
    public void setPaymentModes(PaymentModeResponse managerResponse) {
        paymentModeList = managerResponse.getData();
        for (PaymentDataItem paymentDataItem : paymentModeList) {
            payments.add(paymentDataItem.getPaymentName());
        }
        paymentAdapter.notifyDataSetChanged();
    }

    @Override
    public void setModel(ModelResponse managerResponse) {
        modelList = managerResponse.getData();
        for (ModelItem modelItem : modelList) {
            models.add(modelItem.getModelName());
        }
        modelAdapter.notifyDataSetChanged();

    }

    @Override
    public void setProductPrice(ModelPriceResponse managerResponse) {
//        ModelPriceDetails modelPriceDetails = managerResponse.getData();
        nameTextView.setText(managerResponse.getModelName());
        priceTextView.setText(managerResponse.getItemPrice());
        productPrice.setText(managerResponse.getItemPrice());
    }

    @Override
    public void setDiscountMessage(ModelDiscountResponse discountMessage) {
        //     Log.e("discount msg", discountMessage.toString());

        if (discountMessage.isStatus() && selectedModel != null) {
            mangeId = discountMessage.getManageId();
            modelDiscountResponse = discountMessage;
            if (ValidationUtils.isThereInternet(StoreMainActivity.this))
                managerDiscountPresenter.getProductPrice(userToken, productId, selectedMake, selectedModel, paymentID);
            priceLayout.setVisibility(View.VISIBLE);
            discountErrorMsg.setVisibility(View.GONE);
            phoneDetailsContinue.setVisibility(View.VISIBLE);

            if (discountMessage.getTypeDiscount().equalsIgnoreCase("flat"))
                discountTv.setText(String.format("Discount %s", discountMessage.getTypeDiscount()));
        } else {
            priceLayout.setVisibility(View.GONE);
            discountErrorMsg.setVisibility(View.VISIBLE);
            discountErrorMsg.setText(discountMessage.getMessage());


        }

    }

    @Override
    public void setOtpVerification(boolean verification) {
        otpVerification = verification;
        if (verification) {
            otpWillExpireTV.setText("OTP Verified Successfully!!!");
            otpWillExpireTV.setVisibility(View.VISIBLE);
            otpWillExpireTV.setTextColor(getResources().getColor(R.color.green));
            if (otpTimer != null)
                otpTimer.cancel();
        }


    }

    @Override
    public void setCustomerVerification(boolean statue, String msg, int type, String otpType) {
        if (statue) {
            otpTypeValue = otpType;
            if (!otpTypeValue.equalsIgnoreCase("skip_otp")) {
                otpValidation.setVisibility(View.VISIBLE);
                startOtpTimer(durationOfOtp);
                customerName.setEnabled(false);
                customerMobile.setEnabled(false);
                customerEmail.setEnabled(false);
            } else {
                otpValidation.setVisibility(View.GONE);
                otpVerification = true;
                otpWillExpireTV.setVisibility(View.GONE);
            }
            generateOtp.setVisibility(View.GONE);
        } else {
            if (otpTimer != null)
                otpTimer.cancel();
            otpWillExpireTV.setVisibility(View.GONE);
            generateOtp.setVisibility(View.VISIBLE);
            otpValidation.setVisibility(View.GONE);
            mobileInputLayout.setError(msg);
        }

    }

    @Override
    public void setEmployeeName(FindEmployeeResponse employeeName) {
        if (employeeName.isStatus()) {
            empName.setText(employeeName.getEmployeeName());
            empName.setVisibility(View.VISIBLE);
            employeeIdInputLayout.setErrorEnabled(false);
        } else {
            empName.setVisibility(View.GONE);
            employeeIdInputLayout.setErrorEnabled(true);
            employeeIdInputLayout.setError(employeeName.getMessage());
        }

    }

    @Override
    public void setAlternativeMethod(AlternativeMethodResponse alternativeMethod) {
        showMessage(alternativeMethod.getMessage());

    }

    @Override
    public void setApprovalRequest(boolean status, String string) {
        progressBar.setVisibility(View.GONE);
        if (status) {
            referenceId = string;
            refId.setText(String.format("Reference ID :  %s", string));
            // refId.setTextColor(getResources().getColor(R.color.orange_color));
            findViewById(R.id.approval_request_back)
                    .setVisibility(View.INVISIBLE);
            refreshLayout.setVisibility(View.VISIBLE);
            findViewById(R.id.alternative_layout)
                    .setVisibility(View.VISIBLE);

        } else {
            showMessage(string);
            findViewById(R.id.send_request)
                    .setVisibility(View.VISIBLE);
            findViewById(R.id.send_request)
                    .setClickable(true);
        }


    }

    @Override
    public void setPercentageBasedDiscount(SubmitDiscountResponse percentageBasedDiscount) {
        if (percentageBasedDiscount.isStatus()) {
            discountPercentage.setText(String.valueOf(percentageBasedDiscount.getDiscountPercentage()));
            discountedPrice.setText(String.valueOf(percentageBasedDiscount.getFinalAmount()));
            discountAmount.setText(String.valueOf(percentageBasedDiscount.getDiscountValue()));
            discountSubmit = true;
            approverList.clear();
            approverList.add("Approval Authority");
            approverDetails=percentageBasedDiscount.getApproverDetails();
            approverHtmlItemList.clear();
            approverHtmlItemList.add(percentageBasedDiscount.getApproverDetails());
            for (ApproverDetails htmlItem : approverHtmlItemList) {
                approverList.add(htmlItem.getEmployeeName());
            }
            htmlItemArrayAdapter.notifyDataSetChanged();
        } else {
            showMessage(percentageBasedDiscount.getMessage());
        }


    }

    @Override
    public void setAmountBasedDiscount(SubmitDiscountResponse amountBasedDiscount) {
        if (amountBasedDiscount.isStatus()) {
            discountPercentage.setText(String.valueOf(amountBasedDiscount.getDiscountPercentage()));
            discountedPrice.setText(String.valueOf(amountBasedDiscount.getFinalAmount()));
            discountAmount.setText(String.valueOf(amountBasedDiscount.getDiscountValue()));
            discountSubmit = true;
            approverList.clear();
            approverList.add("Approval Authority");
            approverDetails=amountBasedDiscount.getApproverDetails();
            approverHtmlItemList.clear();
            approverHtmlItemList.add(amountBasedDiscount.getApproverDetails());
            for (ApproverDetails htmlItem : approverHtmlItemList) {
                approverList.add(htmlItem.getEmployeeName());
            }
            htmlItemArrayAdapter.notifyDataSetChanged();
        } else {
            showMessage(amountBasedDiscount.getMessage());
        }

    }

    @Override
    public void setDiscountDetails(DiscountDetailsResponse discountDetails) {
          //Log.e("refresh Discount", discountDetails.toString());

        if (discountDetails.isStatus()) {
//            Data data = discountDetails.getData();
//            if (data.getCouponCode() != null && !data.getCouponCode().isEmpty()) {
//                statusTxt.setText(String.format("Coupon Code: %s ", data.getCouponCode()));
//            } else {
//                statusTxt.setText(discountDetails.getData().getStatus());
//            }
            showMessage(discountDetails.getMessage());
        } else {
            showMessage(discountDetails.getMessage());
        }
    }

    @Override
    public void setCheckCouponCode(CheckCouponCode checkCouponCode) {
        if (checkCouponCode.isStatus()) {
            CouponListDialogFragment cf = CouponListDialogFragment.newInstance(checkCouponCode.getData());
            cf.setCancelable(false);
            cf.show(getSupportFragmentManager(), "TAG");

        } else {
            if (generateOtp.getText().toString().equalsIgnoreCase("resend otp")) {
                managerDiscountPresenter.resendOtp(userToken,customerMobile.getText().toString());
            } else {
                managerDiscountPresenter.generateOTP(userToken,customerMobile.getText().toString());
            }
//            otpValidation.setVisibility(View.VISIBLE);
//            startOtpTimer(durationOfOtp);
//            customerName.setEnabled(false);
//            customerMobile.setEnabled(false);
//            customerEmail.setEnabled(false);
//            generateOtp.setVisibility(View.GONE);
        }


    }

    @Override
    public void setCouponCodeCancel(CommonResponse commonResponse) {
        showMessage(commonResponse.getMessage());
        if (commonResponse.getStatus()) {
            if (generateOtp.getText().toString().equalsIgnoreCase("resend otp")) {
                managerDiscountPresenter.resendOtp(userToken,customerMobile.getText().toString());
            } else {
                managerDiscountPresenter.generateOTP(userToken,customerMobile.getText().toString());
            }
            otpValidation.setVisibility(View.VISIBLE);
            startOtpTimer(durationOfOtp);
            customerName.setEnabled(false);
            customerMobile.setEnabled(false);
            customerEmail.setEnabled(false);
            generateOtp.setVisibility(View.GONE);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.phone_detail_continue:
                if (modelDiscountResponse != null) {
                    discountLayout.toggle(true);
                } else {
                    showMessage("Select Model");
                }
                break;
            case R.id.choose_discount_continue:
                if (discountSubmit) {
                    customerLayout.toggle(true);
                } else {
                    showMessage("Submit discount first!");
                }
                break;
            case R.id.choose_discount_back:
                productLayout.toggle(true);
                break;
            case R.id.customer_validation_continue:
                if (otpVerification) {
                    approvalLayout.toggle(true);
                } else {
                    showMessage(" Customer verification " + otpVerification);
                }
                break;
            case R.id.customer_validation_back:
                discountLayout.toggle(true);
                break;
            case R.id.approval_request_continue:
                if (referenceId != null) {
                    Intent intent = new Intent(StoreMainActivity.this, DiscountDetailActivity.class);
                    intent.putExtra("reference_id", referenceId);
                    startActivity(intent);
                    finish();
                } else {
                    showMessage("Send Request for Approval");
                }
                break;
            case R.id.approval_request_back:
                customerLayout.toggle(true);
                break;

            case R.id.submitDiscount:
                if (!Objects.requireNonNull(discountPercentage.getText()).toString().isEmpty()) {
                    managerDiscountPresenter.percentageBaseDiscount(userToken, productId, selectedMake, selectedModel, paymentID, mangeId, discountPercentage.getText().toString());
//                    managerDiscountPresenter.percentageBaseDiscount(appPreference.getStoreData().getStoreId(), selectedModel, mangeId, discountPercentage.getText().toString());
                }
                break;

            case R.id.generate_otp:
                if (customerName.getText().toString().isEmpty()) {
                    customerName.setError("Please enter customer name.");
                } else if (customerMobile.getText().toString().isEmpty()) {
                    customerMobile.setError("Please enter customer mobile No.");
                } else {
                    if (ValidationUtils.isThereInternet(StoreMainActivity.this)) {
//                        managerDiscountPresenter.checkCouponCode(customerMobile.getText().toString());
                        if (generateOtp.getText().toString().equalsIgnoreCase("resend otp")) {
                            managerDiscountPresenter.resendOtp(userToken,customerMobile.getText().toString());
                        } else {
                            managerDiscountPresenter.generateOTP(userToken,customerMobile.getText().toString());
                        }
                    }
                }

                break;

            case R.id.verify_otp:
                if (ValidationUtils.isThereInternet(StoreMainActivity.this))
                    managerDiscountPresenter.validateOTP(userToken,customerMobile.getText().toString(), otpText.getText().toString());
                break;
            case R.id.send_request:
                if (ValidationUtils.isThereInternet(StoreMainActivity.this)) {


                    if (!otpTypeValue.equalsIgnoreCase("skip_otp")) {
                        OTPDefValue = otpText.getText().toString();
                    }

                    progressBar.setVisibility(View.VISIBLE);
                    findViewById(R.id.send_request).setVisibility(View.GONE);
                    findViewById(R.id.send_request)
                            .setClickable(false);
//                    managerDiscountPresenter.sendRequest(mangeId, appPreference.getStoreData().getStoreId(), employeeId.getText().toString(), selectedModel, discountPercentage.getText().toString(),
//                            discountAmount.getText().toString(), discountedPrice.getText().toString(),
//                            customerMobile.getText().toString(), true, customerEmail.getText().toString(),
//                            customerName.getText().toString(), otpText.getText().toString(), selectedApprovel);

                    managerDiscountPresenter.sendPaymentRequest(userToken,mangeId,  employeeId.getText().toString(), selectedModel, discountPercentage.getText().toString(),
                            discountAmount.getText().toString(),
                            customerMobile.getText().toString(), true, customerEmail.getText().toString(),
                            customerName.getText().toString(), OTPDefValue, selectedApprovel, paymentID,productId,selectedMake);
                    //discountedPrice.getText().toString(),

                }

                break;
            case R.id.submit_code:
                if (ValidationUtils.isThereInternet(StoreMainActivity.this))
                    managerDiscountPresenter.alternativeMethod(userToken,apxCode.getText().toString(), "app");
                break;
            case R.id.refresh_button:
                if (ValidationUtils.isThereInternet(StoreMainActivity.this))
                    managerDiscountPresenter.discountDetails(userToken,referenceId);
        }

    }

    private void startOtpTimer(long duration) {
        otpTimer = new CountDownTimer(duration, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                isTimerRunning = true;
                timeRemaining = millisUntilFinished;
                updateTimerUI();
            }

            @Override
            public void onFinish() {
                isTimerRunning = false;
                if (otpTimer != null)
                    otpTimer.cancel();
                generateOtp.setText("Resend OTP");
                generateOtp.setVisibility(View.VISIBLE);
                otpWillExpireTV.setVisibility(View.GONE);

            }
        };
        otpTimer.start();

    }

    private void updateTimerUI() {

        int secCompleted = (int) (timeRemaining / 1000);

        int min;
        String timeLeft;

        if (secCompleted > 60) {
            min = secCompleted / 60;
            if (secCompleted % 60 == 0) {
                timeLeft = min + "min";
            } else {
                secCompleted = secCompleted - 60;
                timeLeft = min + "min " + secCompleted + " sec";
            }
        } else {
            timeLeft = secCompleted + " sec";
        }
        String otpExpire = String.format("Your OTP will expire in %s", timeLeft);

        otpWillExpireTV.setText(otpExpire);
        otpWillExpireTV.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.store_manager_discount:
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }
                restartActivity();
                break;
            case R.id.store_manager_history:
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }
                setFragment(StoreHistoryFragment.newInstance("", ""));
                break;
            case R.id.store_manager_account:
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }
                setFragment(StoreAccountFragment.newInstance("", ""));
                break;
        }
        return true;
    }

    protected void setFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =
                fragmentManager.beginTransaction();
        fragmentTransaction.replace(android.R.id.content, fragment);
        fragmentTransaction.addToBackStack(fragment.getTag());
        fragmentTransaction.commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        //   Log.e("frag uri", "" + uri);

    }

    public void restartActivity() {
        Intent intent = getIntent();
        overridePendingTransition(0, 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);

        // StoreMainActivity.this.recreate();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        bottomNavigationView.getMenu().getItem(0).setChecked(true);
    }

    @Override
    public void onItemClicked(int position) {

    }
}
