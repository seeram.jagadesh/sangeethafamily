package com.sangeetha.employee.storeOrders.model;

import com.google.gson.annotations.SerializedName;

public class orderDetailsItem {

    @SerializedName("id")
    private String id;

    @SerializedName("order_id")
    private String order_id;

    @SerializedName("product_id")
    private String product_id;

    @SerializedName("product_code")
    private String product_code;

    @SerializedName("product_price")
    private String product_price;

    @SerializedName("product_special_price")
    private String product_special_price;

    @SerializedName("product_pre_book_price")
    private String product_pre_book_price;

    @SerializedName("vas_id")
    private String vas_id;

    @SerializedName("vas_amount")
    private String vas_amount;

    @SerializedName("quantity")
    private String quantity;

    @SerializedName("tax")
    private String tax;

    @SerializedName("order_status")
    private String order_status;

    @SerializedName("notes")
    private String notes;

    @SerializedName("app_special_price")
    private String app_special_price;

    @SerializedName("product_apx_name")
    private String product_apx_name;

    @SerializedName("created_at")
    private String created_at;

    @SerializedName("updated_at")
    private String updated_at;

    @SerializedName("product_name")
    private String product_name;

    @SerializedName("sku")
    private String sku;

    @SerializedName("price")
    private String price;

    @SerializedName("gst")
    private String gst;


    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getGst() {
        return gst;
    }

    public void setGst(String gst) {
        this.gst = gst;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }

    public String getProduct_price() {
        return product_price;
    }

    public void setProduct_price(String product_price) {
        this.product_price = product_price;
    }

    public String getProduct_special_price() {
        return product_special_price;
    }

    public void setProduct_special_price(String product_special_price) {
        this.product_special_price = product_special_price;
    }

    public String getProduct_pre_book_price() {
        return product_pre_book_price;
    }

    public void setProduct_pre_book_price(String product_pre_book_price) {
        this.product_pre_book_price = product_pre_book_price;
    }

    public String getVas_id() {
        return vas_id;
    }

    public void setVas_id(String vas_id) {
        this.vas_id = vas_id;
    }

    public String getVas_amount() {
        return vas_amount;
    }

    public void setVas_amount(String vas_amount) {
        this.vas_amount = vas_amount;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getApp_special_price() {
        return app_special_price;
    }

    public void setApp_special_price(String app_special_price) {
        this.app_special_price = app_special_price;
    }

    public String getProduct_apx_name() {
        return product_apx_name;
    }

    public void setProduct_apx_name(String product_apx_name) {
        this.product_apx_name = product_apx_name;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
