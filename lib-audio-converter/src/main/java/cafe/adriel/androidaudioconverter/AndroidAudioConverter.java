package cafe.adriel.androidaudioconverter;

import android.content.Context;
import android.util.Log;

import com.arthenica.mobileffmpeg.Config;
import com.arthenica.mobileffmpeg.ExecuteCallback;
import com.arthenica.mobileffmpeg.FFmpeg;

import java.io.File;
import java.io.IOException;

import cafe.adriel.androidaudioconverter.callback.IConvertCallback;
import cafe.adriel.androidaudioconverter.callback.ILoadCallback;
import cafe.adriel.androidaudioconverter.model.AudioFormat;

import static com.arthenica.mobileffmpeg.Config.RETURN_CODE_CANCEL;
import static com.arthenica.mobileffmpeg.Config.RETURN_CODE_SUCCESS;

public class AndroidAudioConverter {

    private static boolean loaded;

    private Context context;
    private File audioFile;
    private AudioFormat format;
    private IConvertCallback callback;

    private AndroidAudioConverter(Context context) {
        this.context = context;
    }

    public static boolean isLoaded() {
        return loaded;
    }

    public static void load(Context context, final ILoadCallback callback) {
        try {
//            FFmpeg.getInstance(context).loadBinary(new FFmpegLoadBinaryResponseHandler() {
//                @Override
//                public void onStart() {
//
//                }
//
//                @Override
//                public void onSuccess() {
//                    loaded = true;
//                    callback.onSuccess();
//                }
//
//                @Override
//                public void onFailure() {
//                    loaded = false;
//                    callback.onFailure(new Exception("Failed to loaded FFmpeg lib"));
//                }
//
//                @Override
//                public void onFinish() {
//
//                }
//            });
        } catch (Exception e) {
            loaded = false;
            callback.onFailure(e);
        }
    }

    public static AndroidAudioConverter with(Context context) {
        return new AndroidAudioConverter(context);
    }

    public AndroidAudioConverter setFile(File originalFile) {
        this.audioFile = originalFile;
        return this;
    }

    public AndroidAudioConverter setFormat(AudioFormat format) {
        this.format = format;
        return this;
    }

    public AndroidAudioConverter setCallback(IConvertCallback callback) {
        this.callback = callback;
        return this;
    }

    public void convert() {
//        if (!isLoaded()) {
//            callback.onFailure(new Exception("FFmpeg not loaded"));
//            return;
//        }
        if (audioFile == null || !audioFile.exists()) {
            callback.onFailure(new IOException("File not exists"));
            return;
        }
        if (!audioFile.canRead()) {
            callback.onFailure(new IOException("Can't read the file. Missing permission?"));
            return;
        }
        final File convertedFile = getConvertedFile(audioFile, format);
        Log.d("AndroidAudioConverter:", "convert : convertedFile : " + convertedFile.getAbsolutePath());
        final String[] cmd = new String[]{"-y", "-i", audioFile.getPath(), "-acodec", "libmp3lame", convertedFile.getPath()};
        try {
//            FFmpeg.getInstance(context).execute(cmd, new FFmpegExecuteResponseHandler() {
//                        @Override
//                        public void onStart() {
//
//                        }
//
//                        @Override
//                        public void onProgress(String message) {
//
//                        }
//
//                        @Override
//                        public void onSuccess(String message) {
//                            callback.onSuccess(convertedFile);
//                        }
//
//                        @Override
//                        public void onFailure(String message) {
//                            callback.onFailure(new IOException(message));
//                        }
//
//                        @Override
//                        public void onFinish() {
//
//                        }
//                    });
            FFmpeg.executeAsync(cmd, new ExecuteCallback() {

                @Override
                public void apply(final long executionId, final int returnCode) {
                    if (returnCode == RETURN_CODE_SUCCESS) {
                        callback.onSuccess(convertedFile);
                        Log.i(Config.TAG, "Async command execution completed successfully.");
                    } else if (returnCode == RETURN_CODE_CANCEL) {
                        callback.onFailure(new IOException("Cancelled by user"));
                        Log.i(Config.TAG, "Async command execution cancelled by user.");
                    } else {
                        callback.onFailure(new IOException(String.format("Async command execution failed with returnCode=%d.", returnCode)));
                        Log.i(Config.TAG, String.format("Async command execution failed with returnCode=%d.", returnCode));
                    }
                }
            });
        } catch (Exception e) {
            callback.onFailure(e);
        }
    }

    private static File getConvertedFile(File originalFile, AudioFormat format) {
        String[] f = originalFile.getPath().split("\\.");
        String filePath = originalFile.getPath().replace(f[f.length - 1], format.getFormat());

        File file = new File(filePath);
//        File parentFile = new File(file.getParentFile(), "Convertions");
//
//        File file1 = new File(parentFile, "abc." + format.getFormat());
//        file1.mkdirs();
        return file;
    }
}