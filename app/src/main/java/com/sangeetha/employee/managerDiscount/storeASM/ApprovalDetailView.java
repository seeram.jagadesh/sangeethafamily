package com.sangeetha.employee.managerDiscount.storeASM;

import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.PhoneDetailsResponse;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.RejectReasonResponse;

public interface ApprovalDetailView {

    void showProgress();

    void hideProgress();

    void showMessage(String msg);

    void setDiscountApproval(String couponCode);

    void setDiscountReject(String msg);

    void setDiscountBargain(String msg);

    void setSubmitBargain(String msg);

    void setRejectionList(RejectReasonResponse rejectionList);

    void setDiscountCheck(String jsonData);

    void setPhoneDetail(PhoneDetailsResponse phoneDetail);

}
