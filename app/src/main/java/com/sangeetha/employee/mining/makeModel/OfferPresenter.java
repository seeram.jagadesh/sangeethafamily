package com.sangeetha.employee.mining.makeModel;

public interface OfferPresenter {

    void getMake(String token);

    void getModel(String token,String makeId);

    void getStores(String token);

}
