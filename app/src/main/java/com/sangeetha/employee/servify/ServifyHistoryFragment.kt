package com.sangeetha.employee.servify

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.sangeetha.employee.R
import com.sangeetha.employee.databinding.FragmentServifyHistoryListBinding
import com.sangeetha.employee.employee.servify.viewmodel.ServifyViewModel
import com.sangeetha.employee.servify.model.data.ServifyHistoryData
import com.sangeetha.employee.utils.AppPreference

/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [ServifyHistoryFragment.OnListFragmentInteractionListener] interface.
 */
class ServifyHistoryFragment : Fragment() {

    // TODO: Customize parameters
    private var columnCount = 1

    private var listener: OnListFragmentInteractionListener? = null
    private lateinit var servifyViewModel: ServifyViewModel
    private lateinit var servifyHistoryData: ArrayList<ServifyHistoryData>
    private var storeId = "560"
    var userToken: String = ""
    var appPreference: AppPreference? = null
    private lateinit var servifyHistoryAdapter: ServifyHistoryAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        servifyViewModel = ViewModelProvider(this).get(ServifyViewModel::class.java)


        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentServifyHistoryListBinding.inflate(inflater, container, false)
        servifyHistoryData = ArrayList()
        appPreference = AppPreference(activity)
        binding.servifyHistoryList.layoutManager = LinearLayoutManager(context)
        servifyHistoryAdapter = ServifyHistoryAdapter(servifyHistoryData, listener)
        binding.servifyHistoryList.adapter = servifyHistoryAdapter

        userToken = appPreference?.userToken.toString()
        servifyViewModel.getHistory(userToken).observe(viewLifecycleOwner, Observer {
            binding.shimmerViewContainer.stopShimmer()
            binding.shimmerViewContainer.visibility = View.GONE
            if (it.status) {
                servifyHistoryData.addAll(it.servifyHistoryData)
                servifyHistoryAdapter.notifyDataSetChanged()

            } else {
                showMessage(it.message)
            }
        })


        return binding.root
    }

    private fun showMessage(msg: String) {
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson
     * [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onListFragmentInteraction(item: ServifyHistoryData?)
    }

    companion object {

        // TODO: Customize parameter argument names
        const val ARG_COLUMN_COUNT = "column-count"

        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance(columnCount: Int) =
            ServifyHistoryFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                }
            }
    }
}
