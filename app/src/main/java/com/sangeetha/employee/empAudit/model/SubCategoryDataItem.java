package com.sangeetha.employee.empAudit.model;

import com.google.gson.annotations.SerializedName;

public class SubCategoryDataItem {

    @SerializedName("brand_name")
    private String brandName;

    @SerializedName("brand_id")
    private String brandId;

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    @Override
    public String toString() {
        return
                "SubCategoryDataItem{" +
                        "brand_name = '" + brandName + '\'' +
                        ",brand_id = '" + brandId + '\'' +
                        "}";
    }
}