package com.sangeetha.employee.managerDiscount.storeASM;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import androidx.fragment.app.Fragment;

import com.sangeetha.employee.R;
import com.sangeetha.employee.empLogin.EmployeeLogin;
import com.sangeetha.employee.utils.AppPreference;

/**
 * A simple {@link Fragment} subclass.
 */
public class ASMAccountFragment extends Fragment {

    private AppPreference appPreference;

    public ASMAccountFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_asmaccount, container, false);
        appPreference = new AppPreference(getActivity());
        view.findViewById(R.id.asm_logout)
                .setOnClickListener(v -> logoutPopUp());
        return view;
    }

    private void moveToLoginScreen() {
        Intent loginIntent = new Intent(getActivity(), EmployeeLogin.class);
        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
    }

    private void logoutPopUp() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.logout_pop_up);
        dialog.setCancelable(true);
        dialog.findViewById(R.id.yes_btn_logout_popup).setOnClickListener(v -> {
            dialog.dismiss();
            appPreference.clearAppPreference();
            moveToLoginScreen();
        });

        dialog.findViewById(R.id.no_btn_logout_popup).setOnClickListener(v -> dialog.dismiss());

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

}
