package com.sangeetha.employee.storeOrders.model;

import com.google.gson.annotations.SerializedName;

public class OrderStoreDetails {


    @SerializedName("store_code")
    private String store_code;

    @SerializedName("store_name")
    private String store_name;

    @SerializedName("sto_mobile")
    private String sto_mobile;

    @SerializedName("asm_emp_id")
    private String asm_emp_id;

    @SerializedName("employee_name")
    private String employee_name;

    @SerializedName("emp_mobile")
    private String emp_mobile;

    @SerializedName("emp_email")
    private String emp_email;

    @SerializedName("employee_id")
    private String employee_id;


    public String getStore_code() {
        return store_code;
    }

    public void setStore_code(String store_code) {
        this.store_code = store_code;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public String getSto_mobile() {
        return sto_mobile;
    }

    public void setSto_mobile(String sto_mobile) {
        this.sto_mobile = sto_mobile;
    }

    public String getAsm_emp_id() {
        return asm_emp_id;
    }

    public void setAsm_emp_id(String asm_emp_id) {
        this.asm_emp_id = asm_emp_id;
    }

    public String getEmployee_name() {
        return employee_name;
    }

    public void setEmployee_name(String employee_name) {
        this.employee_name = employee_name;
    }

    public String getEmp_mobile() {
        return emp_mobile;
    }

    public void setEmp_mobile(String emp_mobile) {
        this.emp_mobile = emp_mobile;
    }

    public String getEmp_email() {
        return emp_email;
    }

    public void setEmp_email(String emp_email) {
        this.emp_email = emp_email;
    }

    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }
}
