package com.sangeetha.employee.managerDiscount.storeASM;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.google.android.material.textfield.TextInputEditText;
import com.sangeetha.employee.R;
import com.sangeetha.employee.managerDiscount.manager_pojo.Data;
import com.sangeetha.employee.managerDiscount.manager_pojo.DiscountDetailsResponse;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.DataItem;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.PhoneDetailsResponse;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.ReasonListItem;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.RejectReasonResponse;
import com.sangeetha.employee.managerDiscount.storeManager.DiscountDetailActivity;
import com.sangeetha.employee.managerDiscount.storeManager.DiscountDetailView;
import com.sangeetha.employee.managerDiscount.storeManager.ManagerDiscountPresenter;
import com.sangeetha.employee.managerDiscount.storeManager.ManagerDiscountPresenterImp;
import com.sangeetha.employee.utils.AppConstants;
import com.sangeetha.employee.utils.AppPreference;
import com.sangeetha.employee.utils.LanguageUtil;
import com.sangeetha.employee.utils.MyContextWrapper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class ApprovalDetailActivity extends AppCompatActivity implements View.OnClickListener, ApprovalDetailView, DiscountDetailView {

    private AppCompatTextView make, model, price, sangeethaPrice,
            discountPrice, totalPrice, mobileNo, email,
            employeeId, name, approverName, dateTxt, referenceNoTxt,
            coupon, discountPercentage, messageView;
    private ProgressBar progressBar, checkProgress, submitProgress;
    private String couponCode, rejectionReason, rejectionNote;
    private StoreASMPresenter storeASMPresenter;
    private DataItem dataItem;
    private ArrayList<String> rejectReasons = new ArrayList<>();
    private ArrayList<String> reasonList = new ArrayList<>();
    private AppCompatEditText approveDiscount,
            approveDiscountAmount, approveTotal;
    private AlertDialog bargainAlertDialog;
    private Button submit;
    private ManagerDiscountPresenter managerDiscountPresenter;
    private AppCompatButton cal;

    private String[] bargainOptions;
    private String bargainOption;
    private AppPreference appPreference;

    AppCompatButton approve,reject,bargain;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_approval_detail);
        storeASMPresenter = new StoreASMPresenterImpl(this);
        managerDiscountPresenter = new ManagerDiscountPresenterImp(this);
        appPreference = new AppPreference(this);
        initView();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        Locale languageType = LanguageUtil.getLanguageType(newBase);
        super.attachBaseContext(MyContextWrapper.wrap(newBase, languageType));
    }
    private void initView() {
        progressBar = findViewById(R.id.progressBar);
        make = findViewById(R.id.make);
        model = findViewById(R.id.model);
        price = findViewById(R.id.price);
        sangeethaPrice = findViewById(R.id.sangeethaPrice);
        discountPrice = findViewById(R.id.discountPrice);
        totalPrice = findViewById(R.id.totalPrice);
        mobileNo = findViewById(R.id.mobileNo);
        email = findViewById(R.id.email);
        employeeId = findViewById(R.id.employeeId);
        name = findViewById(R.id.name);
        // approverName = findViewById(R.id.approverName);
        dateTxt = findViewById(R.id.dateTxt);
        referenceNoTxt = findViewById(R.id.referenceNoTxt);
        coupon = findViewById(R.id.coupon);
        approve = findViewById(R.id.approve);
        reject = findViewById(R.id.reject);
        bargain = findViewById(R.id.bargain);
        discountPercentage = findViewById(R.id.discount_percentage);
        approve.setOnClickListener(this);
        reject.setOnClickListener(this);
        bargain.setOnClickListener(this);
        String refId = getIntent().getStringExtra("reference_id");
        managerDiscountPresenter.discountInfo(appPreference.getUserToken(), refId);
//        storeASMPresenter.getPhoneDetailByRefId(appPreference.getUserToken(),refId);
        storeASMPresenter.getRejectionList(appPreference.getUserToken());
//        approve.setBackground(AppConstants.setButtonbg(getResources().getDrawable(R.drawable.white_bg),
//                Color.parseColor("#03a9f3"),Color.parseColor("#03a9f3"),0));
//        reject.setBackground(AppConstants.setButtonbg(getResources().getDrawable(R.drawable.white_bg),
//                Color.parseColor("#FE8E02"),Color.parseColor("#FE8E02"),0));
//        bargain.setBackground(AppConstants.setButtonbg(getResources().getDrawable(R.drawable.white_bg),
//                Color.parseColor("#054DC5"),getResources().getColor(R.color.grey),0));

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.approve:
                storeASMPresenter.discountApprove(appPreference.getUserToken(), referenceNoTxt.getText().toString());
                break;
            case R.id.reject:
                rejectDialog();
                break;
            case R.id.bargain:
                bargainDialog();
                break;
        }

    }

    private void approveDialog(String coupon) {
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.asm_approve_dialogue, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView);
        AppCompatTextView code = promptsView.findViewById(R.id.txt_code);
        code.setText(coupon);
        AlertDialog alertDialog = alertDialogBuilder.create();
        Button cancel = promptsView.findViewById(R.id.cancel_action);
        cancel.setOnClickListener(v -> alertDialog.cancel());
        Button copyCode = promptsView.findViewById(R.id.copy_code);
        copyCode.setOnClickListener(v -> {
            couponCode = code.getText().toString();
            alertDialog.cancel();
            finish();
        });
        alertDialog.show();
    }

    private void bargainDialog() {
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.asm_bargain_dialogue, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView);
        bargainAlertDialog = alertDialogBuilder.create();

        AppCompatTextView currentPrice = promptsView.findViewById(R.id.currentPrice);
        AppCompatTextView currentPercentage = promptsView.findViewById(R.id.current_percentage);
        AppCompatTextView currentDiscount = promptsView.findViewById(R.id.currentDiscount);
        AppCompatTextView currentTotalPrice = promptsView.findViewById(R.id.currentTotalPrice);

        messageView = promptsView.findViewById(R.id.message_view);
        checkProgress = promptsView.findViewById(R.id.check_progress);
        submitProgress = promptsView.findViewById(R.id.submit_progress);

        currentPrice.setText(dataItem.getModelPrice());
        currentPercentage.setText(dataItem.getDiscountPercentage());
        currentDiscount.setText(dataItem.getDiscountAmount());
        currentTotalPrice.setText(dataItem.getTotalAmount());

        AppCompatTextView currentPrice1 = promptsView.findViewById(R.id.currentPrice1);
        currentPrice1.setText(dataItem.getModelPrice());

        AppCompatTextView bargainText = promptsView.findViewById(R.id.bargain_text);
        TextInputEditText bargainValue = promptsView.findViewById(R.id.bargainValue);
        TextInputEditText bargainProduct = promptsView.findViewById(R.id.bargain_product);
        approveDiscount = promptsView.findViewById(R.id.discount_percentage);
        approveDiscount.setText(dataItem.getDiscountPercentage());
        approveDiscountAmount = promptsView.findViewById(R.id.discount_amount);
        approveDiscountAmount.setText(dataItem.getDiscountAmount());
        approveTotal = promptsView.findViewById(R.id.totalPrice1);
        approveTotal.setText(dataItem.getTotalAmount());

        LinearLayout bargainLayout = promptsView.findViewById(R.id.bargainInputLayout);
        AppCompatSpinner bargainSpinner = promptsView.findViewById(R.id.asm_bargain_spinner);
        CardView discountView = promptsView.findViewById(R.id.discountCardView1);
        bargainOptions = getResources().getStringArray(R.array.bargain_option);
        ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<String>(ApprovalDetailActivity.this, R.layout.custom_spinner_layout, bargainOptions);
        bargainSpinner.setAdapter(stringArrayAdapter);

        bargainSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                bargainOption = bargainOptions[position];
                switch (position) {
                    case 0:
                        discountView.setVisibility(View.VISIBLE);
                        bargainLayout.setVisibility(View.GONE);
                        break;
                    case 1:
                        discountView.setVisibility(View.GONE);
                        bargainLayout.setVisibility(View.VISIBLE);
                        bargainText.setText("Please notify amount to be discounted for Accessory");
                        break;
                    case 2:
                        discountView.setVisibility(View.GONE);
                        bargainLayout.setVisibility(View.VISIBLE);
                        bargainText.setText("Please notify amount to be discounted for VAS");
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Button cancel = promptsView.findViewById(R.id.cancel_action);
        cancel.setOnClickListener(v -> bargainAlertDialog.cancel());
        submit = promptsView.findViewById(R.id.ok_action);
        TextWatcher disPercentage = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().isEmpty()) {
                    float d = Float.parseFloat(s.toString());
                    float price = Float.parseFloat(dataItem.getModelPrice());
                    approveDiscountAmount.setText(String.valueOf(getDiscountAmount(d, price)));
                    float disPrice = Float.parseFloat(approveDiscountAmount.getText().toString());
                    float total = price - disPrice;
                    approveTotal.setText("");
                    approveTotal.setText(String.valueOf(total));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                //  approveTotal.setText(String.valueOf(currentPrice1));
            }
        };
        approveDiscount.addTextChangedListener(disPercentage);
        approveDiscountAmount.setOnFocusChangeListener((v, hasFocus) -> {
            float d = Float.parseFloat(approveDiscountAmount.getText().toString());
            float price = Float.parseFloat(dataItem.getModelPrice());
            approveTotal.setText(String.valueOf(price - d));
        });
        cal = promptsView.findViewById(R.id.calculate);
        cal.setOnClickListener(v -> {
            if (approveTotal.getText().toString().isEmpty()) {
                showMessage("Please enter total price");
            } else {
                cal.setVisibility(View.INVISIBLE);
                checkProgress.setVisibility(View.VISIBLE);

                float totalPrice = Float.parseFloat(approveTotal.getText().toString());
                float disAmount = Float.parseFloat(dataItem.getModelPrice()) - totalPrice;
                float disPer = getPercentage(disAmount, totalPrice);
                //approveDiscountAmount.setText(String.valueOf(disAmount));
                // approveDiscount.setText(String.valueOf(disPer));
                storeASMPresenter.checkBargain(dataItem.getReferenceId(), String.valueOf((int) totalPrice), employeeId.getText().toString());
            }

        });
        bargainAlertDialog.show();
        submit.setOnClickListener(v -> {
            submitProgress.setVisibility(View.VISIBLE);
            submit.setVisibility(View.INVISIBLE);
            storeASMPresenter.submitBargain(dataItem.getReferenceId(), approveTotal.getText().toString(), employeeId.getText().toString(),
                    bargainOption, bargainValue.getText().toString(), bargainValue.getText().toString(), bargainProduct.getText().toString(), bargainProduct.getText().toString());
        });
    }

    private void rejectDialog() {
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.asm_rejected_dialogue, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView);
        AlertDialog alertDialog = alertDialogBuilder.create();
        AppCompatSpinner reasonSpinner = promptsView.findViewById(R.id.reasonSpinner);
        reasonSpinner.setAdapter(new ArrayAdapter<String>(ApprovalDetailActivity.this, R.layout.custom_spinner_layout, rejectReasons));
        reasonSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    rejectionReason = reasonList.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        AppCompatEditText note = promptsView.findViewById(R.id.note);
        Button cancel = promptsView.findViewById(R.id.cancel_action);
        cancel.setOnClickListener(v -> alertDialog.cancel());
        Button submit = promptsView.findViewById(R.id.submit_action);
        submit.setOnClickListener(v -> {
            rejectionNote = note.getText().toString();
            storeASMPresenter.discountReject(appPreference.getUserToken(),referenceNoTxt.getText().toString(), rejectionReason, rejectionNote);
        });
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
            return true;
        }
        return (super.onOptionsItemSelected(item));
    }

    public float getPercentage(float amount, float productPrice) {
        if (amount > 0 && productPrice > 0) {
            float per = (amount * 100.0f) / productPrice;
         //   Log.e("amount ", "" + amount);
           // Log.e("productPrice ", "" + productPrice);
            //Log.e("per ", "" + per);
            return Math.round(per);
        }
        return 0;
    }

    public float getDiscountAmount(float percentage, float productPrice) {
        if (percentage > 0 && productPrice > 0) {
            //  Log.e("per amount ", "percentage "+percentage +" productPrice "+ productPrice +"  %%%% "+(productPrice*(percentage/100.0f)));
            return Math.round(productPrice * (percentage / 100.0f));
        }
        return 0;

    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);

    }

    @Override
    public void showCouponMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setDiscountDetails(DiscountDetailsResponse discountDetails) {

    }

    @Override
    public void setDiscountInfo(DiscountDetailsResponse discountDetails) {
        Data data = discountDetails.getData();
        make.setText(data.getMakeId());
        model.setText(data.getModelId());
        price.setText(data.getModelPrice());
        sangeethaPrice.setText(data.getModelPrice());
        discountPercentage.setText(String.format("Discount amount @ %s%%", data.getDiscountPercentage()));
        discountPrice.setText(data.getDiscountAmount());
        totalPrice.setText(String.valueOf(Float.parseFloat(data.getModelPrice()) - Float.parseFloat(data.getDiscountAmount())));
        mobileNo.setText(data.getCustomerMobile());
        email.setText(data.getCustomerEmail());
        employeeId.setText(data.getStoreId());
        name.setText(data.getEmployeeId());
//        approverName.setText(data.getApproverName());
        dateTxt.setText(data.getCreated());
        referenceNoTxt.setText(data.getReferenceId());
//        statusTxt.setText(data.getStatus());
//        coupon.setText(data.getCouponCode());
//        customerName.setText(data.getCustomerName());
        if(data.getDiscountStatusText()!=null) {
            if (data.getDiscountStatusText().equalsIgnoreCase("rejected")) {
                findViewById(R.id.cancelLayout).setVisibility(View.GONE);
            } else {
                findViewById(R.id.cancelLayout).setVisibility(View.VISIBLE);
            }
        }else{
            findViewById(R.id.cancelLayout).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void couponCancel(boolean status, String msg) {

    }

    @Override
    public void showMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();

    }

    @Override
    public void setDiscountApproval(String couponCode) {
        if (!isFinishing()) {
            approveDialog(couponCode);
        }

    }

    @Override
    public void setDiscountReject(String msg) {
        showMessage(msg);
        finish();

    }

    @Override
    public void setDiscountBargain(String msg) {

    }

    @Override
    public void setSubmitBargain(String msg) {
        try {
            JSONObject responseJson = new JSONObject(msg);
            if (responseJson.optBoolean("status")) {
                if (bargainAlertDialog.isShowing()) {
                    bargainAlertDialog.dismiss();
                }
                showMessage(responseJson.optString("message"));
                Intent intent = new Intent(ApprovalDetailActivity.this, DiscountDetailActivity.class);
                intent.putExtra("reference_id", dataItem.getReferenceId());
                startActivity(intent);
                finish();
            } else {
                submitProgress.setVisibility(View.GONE);
                submit.setVisibility(View.VISIBLE);
                messageView.setTextColor(ContextCompat.getColor(ApprovalDetailActivity.this, R.color.red));
                messageView.setText(responseJson.optString("message"));
                showMessage(responseJson.optString("message"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void setRejectionList(RejectReasonResponse rejectionList) {
        if (rejectionList.isStatus()) {
            reasonList.clear();
            reasonList.addAll(rejectionList.getReasonList());
            rejectReasons.clear();
            rejectReasons.add(0, "---- Select Reason ----");
            for (String reasonListItem : reasonList) {
                rejectReasons.add(reasonListItem);
            }

        } else {
            showMessage(rejectionList.getMessage());
        }

    }

    @Override
    public void setDiscountCheck(String jsonData) {
        try {
            checkProgress.setVisibility(View.GONE);
            cal.setVisibility(View.VISIBLE);
            JSONObject responseJson = new JSONObject(jsonData);
            if (responseJson.optBoolean("status")) {
                messageView.setTextColor(ContextCompat.getColor(ApprovalDetailActivity.this, R.color.black));
                approveDiscount.setText(responseJson.optString("discount_percentage"));
                approveDiscountAmount.setText(responseJson.optString("discount_value"));
                approveTotal.setText(responseJson.optString("final_amount"));
                messageView.setText(String.format("Final Amount %s, Discount Percentage %S, Discount Value %s", responseJson.optString("final_amount"),
                        responseJson.optString("discount_percentage"), responseJson.optString("discount_value")));

            } else {
                messageView.setTextColor(ContextCompat.getColor(ApprovalDetailActivity.this, R.color.red));
                messageView.setText(responseJson.optString("message"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setPhoneDetail(PhoneDetailsResponse approval) {
        if (approval.isStatus()) {
            dataItem = approval.getData();
            make.setText(dataItem.getMakeId());
            model.setText(dataItem.getModelId());
            price.setText(dataItem.getModelPrice());
            sangeethaPrice.setText(dataItem.getModelPrice());
            discountPrice.setText(dataItem.getDiscountAmount());
            totalPrice.setText(dataItem.getTotalAmount());
            mobileNo.setText(dataItem.getCustomerMobile());
            email.setText(dataItem.getCustomerEmail());
            employeeId.setText(dataItem.getEmployeeId());
            name.setText(dataItem.getCustomerName());
            // approverName.setText(dataItem.getApprover());
            dateTxt.setText(dataItem.getCreated());
            referenceNoTxt.setText(dataItem.getReferenceId());
            discountPercentage.setText(String.format("Discount amount %s%%", dataItem.getDiscountPercentage()));
        } else {
            showMessage(approval.getMessage());
        }
    }


}
