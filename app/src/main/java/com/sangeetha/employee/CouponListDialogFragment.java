package com.sangeetha.employee;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.button.MaterialButton;
import com.sangeetha.employee.managerDiscount.couponCode.DataItem;
import com.sangeetha.employee.managerDiscount.storeManager.StoreMainActivity;
import com.sangeetha.employee.utils.AppConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>A fragment that shows a list of items as a modal bottom sheet.</p>
 * <p>You can show this modal bottom sheet from your activity like this:</p>
 * <pre>
 *     CouponListDialogFragment.newInstance(30).show(getSupportFragmentManager(), "dialog");
 * </pre>
 * <p>You activity (or fragment) needs to implement {@link CouponListDialogFragment.Listener}.</p>
 */
public class CouponListDialogFragment extends BottomSheetDialogFragment {

    // TODO: Customize parameter argument names
    private static final String ARG_ITEM_COUNT = "item_count";
    private Listener mListener;

    // TODO: Customize parameters
    public static CouponListDialogFragment newInstance(List<DataItem> data) {
        final CouponListDialogFragment fragment = new CouponListDialogFragment();
        final Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_ITEM_COUNT, (ArrayList<? extends Parcelable>) data);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_item_list_dialog, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        final RecyclerView recyclerView = view.findViewById(R.id.list);
        final AppCompatTextView msgTxt = view.findViewById(R.id.appCompatTextView2);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new ItemAdapter(getArguments().getParcelableArrayList(ARG_ITEM_COUNT)));
        final MaterialButton cancel = view.findViewById(R.id.button_cancel);
        cancel.setOnClickListener(v -> dismiss());
        final MaterialButton ok = view.findViewById(R.id.button_ok);
        if (getActivity() instanceof StoreMainActivity) {
            msgTxt.setText(getString(R.string.coupon_cancel_store));
        }
        ok.setOnClickListener(v -> {
            StoreMainActivity storeMainActivity = (StoreMainActivity) getActivity();
            storeMainActivity.managerDiscountPresenter.couponCodeCancel(storeMainActivity.customerMobile.getText().toString());
            dismiss();
        });


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        final Fragment parent = getParentFragment();
        if (parent != null) {
            mListener = (Listener) parent;
        } else {
            mListener = (Listener) context;
        }
    }

    @Override
    public void onDetach() {
        mListener = null;
        super.onDetach();
    }

    public interface Listener {
        void onItemClicked(int position);
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        private AppCompatTextView offerName, couponCode, couponAmount;
        LinearLayout linear;
        ViewHolder(LayoutInflater inflater, ViewGroup parent) {
            // TODO: Customize the item layout
            super(inflater.inflate(R.layout.fragment_item_list_dialog_item, parent, false));
            offerName = itemView.findViewById(R.id.offer_name);
            couponCode = itemView.findViewById(R.id.coupon_code);
            couponAmount = itemView.findViewById(R.id.coupon_amount);
            linear = itemView.findViewById(R.id.linear);

        }

    }

    private class ItemAdapter extends RecyclerView.Adapter<ViewHolder> {

        private List<DataItem> data;

        ItemAdapter(List<DataItem> data) {
            this.data = data;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()), parent);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.offerName.setText(String.valueOf(data.get(position).getOfferType()));
            holder.couponCode.setText(String.valueOf(data.get(position).getCouponCode()));
            holder.couponAmount.setText(String.format(getString(R.string.rs_symbol), data.get(position).getAmount()));

            Drawable bg= AppConstants.setStroke(getResources().getDrawable(R.drawable.button_bg), Color.parseColor("#CF8F71"));
            holder.linear.setBackground(bg);
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

    }

}
