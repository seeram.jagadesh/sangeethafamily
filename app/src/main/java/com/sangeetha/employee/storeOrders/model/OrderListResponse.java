package com.sangeetha.employee.storeOrders.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderListResponse {

    @SerializedName("status")
    private boolean status;

    @SerializedName("data")
    List<OrderListItemData> data;

    @SerializedName("message")
    private String message;

    @Override
    public String toString() {
        return "OrderListResponse{" +
                "status=" + status +
                ", data=" + data +
                ", message='" + message + '\'' +
                '}';
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<OrderListItemData> getData() {
        return data;
    }

    public void setData(List<OrderListItemData> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
