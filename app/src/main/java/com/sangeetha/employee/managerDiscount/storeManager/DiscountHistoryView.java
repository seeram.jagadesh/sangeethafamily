package com.sangeetha.employee.managerDiscount.storeManager;

import com.sangeetha.employee.managerDiscount.manager_pojo.CouponHistoryResponse;

public interface DiscountHistoryView {
    void showMessage(String msg);

    void setCouponHistory(CouponHistoryResponse couponHistory);

}
