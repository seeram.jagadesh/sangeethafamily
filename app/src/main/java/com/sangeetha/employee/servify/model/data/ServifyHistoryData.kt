package com.sangeetha.employee.servify.model.data


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ServifyHistoryData(
    @SerializedName("coupon_code")
    val couponCode: String,
    @SerializedName("coupon_status")
    val couponStatus: String,
    @SerializedName("created")
    val created: String,
    @SerializedName("customer_email")
    val customerEmail: String,
    @SerializedName("customer_mobile")
    val customerMobile: String,
    @SerializedName("customer_name")
    val customerName: String,
    @SerializedName("employee_id")
    val employeeId: String,
    @SerializedName("employee_name")
    val employeeName: String,
    @SerializedName("fromdate")
    val fromdate: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("imei")
    val imei: String,
    @SerializedName("invoice_date")
    val invoiceDate: String,
    @SerializedName("invoice_no")
    val invoiceNo: String,
    @SerializedName("logistics_reference_id")
    val logisticsReferenceId: String,
    @SerializedName("make")
    val make: String,
    @SerializedName("model")
    val model: String,
    @SerializedName("modified")
    val modified: String,
    @SerializedName("servify_amount")
    val servifyAmount: String,
    @SerializedName("store_id")
    val storeId: String,
    @SerializedName("store_code")
    val storeCode: String,
    @SerializedName("store_name")
    val storeName: String,
    @SerializedName("todate")
    val todate: String,
    @SerializedName("trade_model")
    val tradeModel: String,
    @SerializedName("trade_reference_id")
    val tradeReferenceId: String,
    @SerializedName("trade_status")
    val tradeStatus: String
) : Parcelable

