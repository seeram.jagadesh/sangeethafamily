package com.sangeetha.employee.callRecorder.domain

data class CugSMS(
    val id: Int,
    val message: String,
    val address: String,
    val type: String,
    val date: String,
)