package com.sangeetha.employee.mining;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;

import com.google.android.material.card.MaterialCardView;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.sangeetha.employee.CommonResponse;
import com.sangeetha.employee.employeeHome.EmployeeLanding;
import com.sangeetha.employee.R;
import com.sangeetha.employee.SangeethaCareApiClient;
import com.sangeetha.employee.customwidget.SearchableSpinner;
import com.sangeetha.employee.empLogin.loginModel.StoreData;
import com.sangeetha.employee.managerDiscount.manager_pojo.DataItem;
import com.sangeetha.employee.managerDiscount.manager_pojo.MakeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelItem;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelResponse;
import com.sangeetha.employee.mining.makeModel.OfferPresenter;
import com.sangeetha.employee.mining.makeModel.OfferPresenterImpl;
import com.sangeetha.employee.mining.makeModel.OfferView;
import com.sangeetha.employee.mining.model.CategoryModel;
import com.sangeetha.employee.mining.model.FetchMiningData;
import com.sangeetha.employee.mining.model.FetchReasonModel;
import com.sangeetha.employee.mining.response.CategoryMiningResponse;
import com.sangeetha.employee.mining.response.FetchMiningResponse;
import com.sangeetha.employee.mining.response.FetchReasonResponse;
import com.sangeetha.employee.mining.response.StoreModel;
import com.sangeetha.employee.mining.response.StoreResponse;
import com.sangeetha.employee.utils.AppConstants;
import com.sangeetha.employee.utils.AppPreference;
import com.sangeetha.employee.utils.CalendarUtils;
import com.sangeetha.employee.utils.ValidationUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MiningActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, OfferView {

    int TIME_DIALOG_ID = 1111;
    int hr;
    int min;
    String actionType = "";
    Spinner spinnerCategory, spinActions;
    LinearLayout descriptionLayout;
    private OfferPresenter offerPresenter;
    ExtendedFloatingActionButton fabTimer;
    AppCompatTextView txtDescription;
    SearchableSpinner brandMiningSpinner, modelMiningSpinner, storeSpinner;
    AppCompatButton btnSubmit, PlanActionSubmit;
    MaterialCardView whatToSpeakCard, customerDetailsCard, actionPlanCard, purchaseDetailsCard;
    AppCompatTextView whatToSpeak, txtCustomerName, txtMobileNumber, txtSource, txtNoData, txtInvoiceNo, txtInvoiceDate, txtModel;
    List<CategoryModel> categoryModelList;
    List<FetchReasonModel> reasonModelList;
    CategoryModel categoryModel;
    String customerMobile, InvoiceId, storeId, reason;
    FetchMiningData fetchMiningData;
    String categoryId, storeCode, reasonId;
    AppPreference appPreference;
    String miningTime;
    CountDownTimer countDownTimer;
    TextInputEditText edtDate, edtNotes, edtTime, edtResponseNotes, edtNeedaPhoneNotes, edtPurchaseDate;
    LinearLayout actionPlanCallback, actionPlanNoResponse, actionPlanNeedaPhone;
    TextInputLayout txtPurchaseDate;
    private int mYear, mMonth, mDay, mHour, mMinute;

    private String selectedMake;
    private String selectedModel;
    private String selectedStore;

    private ArrayList<String> makes = new ArrayList<>();
    private ArrayList<String> models = new ArrayList<>();
    private ArrayList<String> stores = new ArrayList<>();

    private List<DataItem> makeList = new ArrayList<>();
    private List<StoreModel> storeList = new ArrayList<>();
    private List<ModelItem> modelList = new ArrayList<>();

    private ArrayAdapter<String> makeAdapter;
    private ArrayAdapter<String> storeAdapter;
    private ArrayAdapter<String> modelAdapter;
    String userToken;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(MiningActivity.this, EmployeeLanding.class));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (item.getItemId() == R.id.calendarMining) {
            startActivity(new Intent(MiningActivity.this, MiningCalendarBaseActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mining_menu, menu);
        return true;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mining);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        appPreference = new AppPreference(this);
        FindViews();

        userToken = appPreference.getUserToken();

        getCategoryList();

        edtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCalendarForDatePicking();
            }
        });
        edtPurchaseDate.setOnClickListener(v -> {
            showPurchaseDatePicking();
        });
        brandMiningSpinner.setTitle("Select Make");
        brandMiningSpinner.setPositiveButton("Done");
        modelMiningSpinner.setTitle("Select Model");
        modelMiningSpinner.setPositiveButton("Done");
        storeSpinner.setTitle("Select Store");
        storeSpinner.setPositiveButton("Done");
        makes.add(0, "--- Select Make ---");
        stores.add(0, "--- Select Store ---");
        models.add(0, "--- Select Model ---");
        makeAdapter = new ArrayAdapter<String>(this, R.layout.custom_spinner_layout, makes);
        storeAdapter = new ArrayAdapter<String>(this, R.layout.custom_spinner_layout, stores);

        modelAdapter = new ArrayAdapter<>(this, R.layout.custom_spinner_layout, models);

        brandMiningSpinner.setAdapter(makeAdapter);
        modelMiningSpinner.setAdapter(modelAdapter);
        storeSpinner.setAdapter(storeAdapter);

        brandMiningSpinner.setOnItemSelectedListener(this);
        storeSpinner.setOnItemSelectedListener(this);
        modelMiningSpinner.setOnItemSelectedListener(this);

        edtTime.setOnTouchListener((v, event) -> {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    final Calendar c = Calendar.getInstance();
                    mHour = c.get(Calendar.HOUR_OF_DAY);
                    mMinute = c.get(Calendar.MINUTE);

                    // Launch Time Picker Dialog
                    TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                            new TimePickerDialog.OnTimeSetListener() {

                                @Override
                                public void onTimeSet(TimePicker view, int hourOfDay,
                                                      int minute) {

                                    edtTime.setText(AppConstants.getTime(hourOfDay, minute));
                                }
                            }, mHour, mMinute, false);
                    timePickerDialog.show();
                    break;
                case MotionEvent.ACTION_UP:
                    break;//9010897666   123456
            }
            return true;
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StoreData storeData = appPreference.getStoreData();
//                storeCode = storeData.getStoreCode();
                // storeId = storeData.getStoreId();
                String spinselect = spinnerCategory.getSelectedItem().toString();
                if (spinselect.equalsIgnoreCase("Select Category Type")) {
                    AppConstants.showMessage(MiningActivity.this, "Please select a category to proceed.");
                } else {
                    if (countDownTimer != null) {
                        countDownTimer.cancel();
                    }
                    fetchMiningDetails(categoryId);
                }
            }
        });

        PlanActionSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//            if (actionType.equalsIgnoreCase("text")) {
//                actionPlanCallback.setVisibility(View.VISIBLE);
//                actionPlanNoResponse.setVisibility(View.GONE);
//            }else if (actionType.equalsIgnoreCase("")) {
//                actionPlanNoResponse.setVisibility(View.GONE);
//                actionPlanCallback.setVisibility(View.GONE);
//            }  else if (actionType.equalsIgnoreCase("textarea")) {
//                actionPlanCallback.setVisibility(View.GONE);
//                actionPlanNoResponse.setVisibility(View.VISIBLE);
//            }
                if (actionType.equalsIgnoreCase("Select Reason Type")) {
                    AppConstants.showMessage(MiningActivity.this, "Please select a reason to proceed.");
                } else if (actionType.equalsIgnoreCase("text")) {
                    MiningActivity.this.validateCallBack();
                } else if (actionType.equalsIgnoreCase("")) {
                    MiningActivity.this.validateDonotDisturb();
                } else if (actionType.equalsIgnoreCase("textarea")) {
                    String edtrespon = edtResponseNotes.getText().toString();
                    ApiCallable apiCallable = SangeethaCareApiClient.getClient().create(ApiCallable.class);
                    apiCallable.updateNoResponse(userToken, customerMobile, InvoiceId, storeCode, storeId, reason,
                            edtrespon);
                    MiningActivity.this.validateNoResponse(apiCallable);
                } else if (actionType.equalsIgnoreCase("tradein")) {//trade_make,trade_model
                    MiningActivity.this.validateNeedaPhone();
                }
            }
        });


    }

    private void FindViews() {

        offerPresenter = new OfferPresenterImpl(this);

        spinActions = findViewById(R.id.spinActions);
        spinnerCategory = findViewById(R.id.spinnerCategory);
        btnSubmit = findViewById(R.id.btnSubmit);
        whatToSpeakCard = findViewById(R.id.whatToSpeakCard);
        customerDetailsCard = findViewById(R.id.customerDetailsCard);
        actionPlanCard = findViewById(R.id.actionPlanCard);
        purchaseDetailsCard = findViewById(R.id.purchaseDetailsCard);
        whatToSpeak = findViewById(R.id.whatToSpeak);
        txtCustomerName = findViewById(R.id.txtCustomerName);
        txtDescription = findViewById(R.id.txtDescription);
        descriptionLayout = findViewById(R.id.descriptionLayout);
        txtMobileNumber = findViewById(R.id.txtMobileNumber);
        txtSource = findViewById(R.id.txtSource);
        txtInvoiceNo = findViewById(R.id.txtInvoiceNo);
        txtInvoiceDate = findViewById(R.id.txtInvoiceDate);
        txtModel = findViewById(R.id.txtModel);
        txtNoData = findViewById(R.id.txtNoData);
        edtDate = findViewById(R.id.edtDateSelect);
        edtNotes = findViewById(R.id.edtNotes);
        edtTime = findViewById(R.id.edtTimeSelect);
        actionPlanCallback = findViewById(R.id.actionPlanCallBack);
        edtResponseNotes = findViewById(R.id.edtResponseNotes);
        edtNeedaPhoneNotes = findViewById(R.id.edtNeedaPhoneNotes);
        edtPurchaseDate = findViewById(R.id.edtDatePurchase);
        actionPlanNoResponse = findViewById(R.id.actionPlanNoResponse);
        actionPlanNeedaPhone = findViewById(R.id.actionPlanNeedaPhone);
        brandMiningSpinner = findViewById(R.id.brandMiningSpinner);
        modelMiningSpinner = findViewById(R.id.modelMiningSpinner);
        storeSpinner = findViewById(R.id.storeSpinner);
        PlanActionSubmit = findViewById(R.id.PlanActionSubmit);
        fabTimer = findViewById(R.id.fabTimer);
        txtPurchaseDate = findViewById(R.id.txtDate);
    }

    void fetchMiningDetails(String id) {
        ApiCallable apiCallable = SangeethaCareApiClient.getClient().create(ApiCallable.class);
        AppConstants.showProgress(MiningActivity.this, "Getting details");
        apiCallable.fetchMiningDetails(userToken, id).enqueue(new Callback<FetchMiningResponse>() {
            @Override
            public void onResponse(Call<FetchMiningResponse> call, Response<FetchMiningResponse> response) {
                if (response.isSuccessful()) {
                    FetchMiningResponse fetchMiningResponse = response.body();
                    if (fetchMiningResponse.getStatus()) {
                        fabTimer.setVisibility(View.VISIBLE);
                        PlanActionSubmit.setVisibility(View.GONE);
                        txtNoData.setVisibility(View.GONE);
                        whatToSpeakCard.setVisibility(View.VISIBLE);
                        customerDetailsCard.setVisibility(View.VISIBLE);
                        actionPlanCard.setVisibility(View.VISIBLE);
                        AppConstants.hideProgress();
                        fetchMiningData = fetchMiningResponse.getData();

                        miningTime = fetchMiningResponse.getData().getMiningTime();
                        // time=Integer.parseInt(miningTime);
                        int noOfMinutes = Integer.parseInt(miningTime) * 60 * 1000;//Convert minutes into milliseconds
                        startTimer(noOfMinutes);
                        txtCustomerName.setText(fetchMiningData.getMiningDetails().getCustomer_name());
                        txtMobileNumber.setText(fetchMiningData.getMiningDetails().getCustomer_mobile());
                        String desc = fetchMiningData.getCategoryModel().getCategoryDescription();
                        if (desc.equalsIgnoreCase("") || desc == null) {
                            descriptionLayout.setVisibility(View.GONE);
                        } else {
                            descriptionLayout.setVisibility(View.VISIBLE);
                            txtDescription.setText(desc);
                        }
                        String invoiceNumber = fetchMiningData.getMiningDetails().getInvoice_no();
                        if (invoiceNumber.equalsIgnoreCase("")) {
                            purchaseDetailsCard.setVisibility(View.GONE);
                        } else {
                            purchaseDetailsCard.setVisibility(View.VISIBLE);
                            txtInvoiceNo.setText(invoiceNumber);
                            txtInvoiceDate.setText(fetchMiningData.getMiningDetails().getInvoice_date());
                            txtModel.setText(fetchMiningData.getMiningDetails().getModel());
                        }
                        customerMobile = fetchMiningData.getMiningDetails().getCustomer_mobile();
                        InvoiceId = fetchMiningData.getMiningDetails().getId();
                        if (categoryModel.getContent().equalsIgnoreCase("") || categoryModel.getContent() == null) {
                            whatToSpeakCard.setVisibility(View.GONE);
                        } else {
                            whatToSpeakCard.setVisibility(View.VISIBLE);
                            whatToSpeak.setText(categoryModel.getContent());
                        }
                        txtSource.setText(spinnerCategory.getSelectedItem().toString());
                        getReasonList();
                    } else {
                        AppConstants.hideProgress();
                        txtNoData.setVisibility(View.VISIBLE);
                        whatToSpeakCard.setVisibility(View.GONE);
                        PlanActionSubmit.setVisibility(View.GONE);
                        customerDetailsCard.setVisibility(View.GONE);
                        purchaseDetailsCard.setVisibility(View.GONE);
                        fabTimer.setVisibility(View.GONE);
                        actionPlanCard.setVisibility(View.GONE);
                        AppConstants.showMessage(MiningActivity.this, fetchMiningResponse.getMessage());
                    }
                } else {
                    AppConstants.hideProgress();
                    AppConstants.showMessage(MiningActivity.this, "Error while getting detail.");
                }
            }

            @Override
            public void onFailure(Call<FetchMiningResponse> call, Throwable t) {
                AppConstants.hideProgress();
                AppConstants.showMessage(MiningActivity.this, "Failed to get mining informations.");
            }
        });
    }

    void getReasonList() {
        ApiCallable apiCallable = SangeethaCareApiClient.getClient().create(ApiCallable.class);
        AppConstants.showProgress(MiningActivity.this, "Getting action plan reason list");
        apiCallable.getReasonList(userToken, categoryId).enqueue(new Callback<FetchReasonResponse>() {
            @Override
            public void onResponse(Call<FetchReasonResponse> call, Response<FetchReasonResponse> response) {
                if (response.isSuccessful()) {
                    FetchReasonResponse fetchReasonResponse = response.body();
                    if (fetchReasonResponse.getStatus()) {
                        AppConstants.hideProgress();
                        reasonModelList = fetchReasonResponse.getData();
                        ArrayList<String> arrayList = new ArrayList<>();
                        arrayList.add(0, "Select Reason Type");
                        for (int g = 0; g < reasonModelList.size(); g++) {
                            arrayList.add(reasonModelList.get(g).getReasonName());
                        }

                        ArrayAdapter<String> adapterSubDesignation = new ArrayAdapter<>(MiningActivity.this,
                                R.layout.custom_spinner_layout, arrayList);

                        spinActions.setAdapter(adapterSubDesignation);

                        spinActions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                String selected = parent.getSelectedItem().toString();
                                if (!selected.equalsIgnoreCase("Select Reason Type")) {
                                    int pos = parent.getSelectedItemPosition();
                                    reasonId = reasonModelList.get(pos - 1).getId();
                                    String reasonName = reasonModelList.get(pos - 1).getReasonName();
                                    actionType = reasonModelList.get(pos - 1).getType();
                                    Log.e("reason id", actionType);
                                    resetViews();
                                    if (actionType.equalsIgnoreCase("text")) {
                                        actionPlanCallback.setVisibility(View.VISIBLE);
                                        actionPlanNoResponse.setVisibility(View.GONE);
                                        actionPlanNeedaPhone.setVisibility(View.GONE);
                                    } else if (actionType.equalsIgnoreCase("")) {
                                        actionPlanNoResponse.setVisibility(View.GONE);
                                        actionPlanNeedaPhone.setVisibility(View.GONE);
                                        actionPlanCallback.setVisibility(View.GONE);
                                    } else if (actionType.equalsIgnoreCase("textarea")) {
                                        actionPlanCallback.setVisibility(View.GONE);
                                        actionPlanNeedaPhone.setVisibility(View.GONE);
                                        actionPlanNoResponse.setVisibility(View.VISIBLE);
                                    } else if (actionType.equalsIgnoreCase("tradein")) {
                                        actionPlanCallback.setVisibility(View.GONE);
                                        actionPlanNoResponse.setVisibility(View.GONE);
                                        actionPlanNeedaPhone.setVisibility(View.VISIBLE);
                                        if (reasonModelList.get(pos - 1).getId().contains("already")) {
                                            offerPresenter.getStores(userToken);
                                            txtPurchaseDate.setVisibility(View.VISIBLE);
                                            storeSpinner.setVisibility(View.VISIBLE);
                                        }else{
                                            txtPurchaseDate.setVisibility(View.GONE);
                                            storeSpinner.setVisibility(View.GONE);
                                        }
                                        if (ValidationUtils.isThereInternet(MiningActivity.this)) {
                                            offerPresenter.getMake(userToken);
                                        }
                                    }
//                                    else if (actionType.equalsIgnoreCase("tradein")) {//trade_make,trade_model
//                                        actionPlanCallback.setVisibility(View.GONE);
//                                        actionPlanNoResponse.setVisibility(View.GONE);
//                                        actionPlanNeedaPhone.setVisibility(View.VISIBLE);
//
//                                        if (ValidationUtils.isThereInternet(MiningActivity.this)) {
//                                            offerPresenter.getMake();
//                                        }
//                                    }
//                                    else if (selected.equalsIgnoreCase("Request Review / Rating")) {
//                                        actionPlanCallback.setVisibility(View.GONE);
//                                        actionPlanNoResponse.setVisibility(View.VISIBLE);
//                                    } else if (selected.equalsIgnoreCase("Send message regarding moratorium")) {
//                                        actionPlanCallback.setVisibility(View.GONE);
//                                        actionPlanNoResponse.setVisibility(View.VISIBLE);
//                                    }
                                } else {
                                    actionType = "select reason type";
                                    actionPlanCallback.setVisibility(View.GONE);
                                    actionPlanNoResponse.setVisibility(View.GONE);
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    } else {
                        AppConstants.hideProgress();
                        AppConstants.showMessage(MiningActivity.this, fetchReasonResponse.getMessage());
                    }
                } else {
                    AppConstants.hideProgress();
                    AppConstants.showMessage(MiningActivity.this, "Error while getting action plan reason lists.");
                }
            }

            @Override
            public void onFailure(Call<FetchReasonResponse> call, Throwable t) {
                AppConstants.hideProgress();
                AppConstants.showMessage(MiningActivity.this, "Failed to get action plan's reason list informations.");
            }
        });
    }

    private void resetViews() {
        edtResponseNotes.setText("");
        edtNotes.setText("");
        edtTime.setText("");
        edtDate.setText("");
        edtPurchaseDate.setText("");
        edtNeedaPhoneNotes.setText("");
        brandMiningSpinner.setSelection(0);
        modelMiningSpinner.setSelection(0);
        storeSpinner.setSelection(0);
    }

    void getCategoryList() {
        ApiCallable apiCallable = SangeethaCareApiClient.getClient().create(ApiCallable.class);
        AppConstants.showProgress(MiningActivity.this, "Getting Category Lists");
        apiCallable.getMiningCategory(userToken).enqueue(new Callback<CategoryMiningResponse>() {
            @Override
            public void onResponse(Call<CategoryMiningResponse> call, Response<CategoryMiningResponse> response) {
                if (response.isSuccessful()) {
                    CategoryMiningResponse categoryMiningResponse = response.body();
                    if (categoryMiningResponse.getStatus()) {
                        AppConstants.hideProgress();
                        categoryModelList = categoryMiningResponse.getData();
                        ArrayList<String> arrayList = new ArrayList<>();
                        arrayList.add(0, "Select Category Type");
                        for (int g = 0; g < categoryModelList.size(); g++) {
                            arrayList.add(categoryModelList.get(g).getCategoryName());
                        }

                        ArrayAdapter<String> adapterSubDesignation = new ArrayAdapter<>(MiningActivity.this,
                                R.layout.custom_spinner_layout, arrayList);

                        spinnerCategory.setAdapter(adapterSubDesignation);

                        spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                String selected = parent.getSelectedItem().toString();
                                if (!selected.equalsIgnoreCase("Select Category Type")) {
                                    int selectedPos = parent.getSelectedItemPosition();
                                    categoryId = categoryModelList.get(selectedPos - 1).getId();
                                    Log.e("Cate", categoryId);
                                    categoryModel = categoryModelList.get(selectedPos - 1);
                                } else if (selected.equalsIgnoreCase("Select Category Type")) {

                                    txtNoData.setVisibility(View.GONE);
                                    whatToSpeakCard.setVisibility(View.GONE);
                                    customerDetailsCard.setVisibility(View.GONE);
                                    actionPlanCard.setVisibility(View.GONE);
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    } else {
                        AppConstants.hideProgress();
                        AppConstants.showMessage(MiningActivity.this, categoryMiningResponse.getMessage());
                    }
                } else {
                    AppConstants.hideProgress();
                    AppConstants.showMessage(MiningActivity.this, "Error while getting category lists.");
                }
            }

            @Override
            public void onFailure(Call<CategoryMiningResponse> call, Throwable t) {
                AppConstants.hideProgress();
                AppConstants.showMessage(MiningActivity.this, "Failed to get category list informations.");
            }
        });
    }

    private void validateDonotDisturb() {
        reason = reasonId;
        ApiCallable apiCallable = SangeethaCareApiClient.getClient().create(ApiCallable.class);
        AppConstants.showProgress(MiningActivity.this, "Submitting data..");
        apiCallable.updateDoNotDisturb(userToken, customerMobile, InvoiceId, storeCode, storeId, reason).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (response.isSuccessful()) {
                    CommonResponse commonResponse = response.body();
                    if (commonResponse.getStatus()) {
                        AppConstants.hideProgress();
                        AppConstants.showMessage(MiningActivity.this, commonResponse.getMessage());
                        restartActivity();
                    } else {
                        AppConstants.hideProgress();
                        AppConstants.showMessage(MiningActivity.this, commonResponse.getMessage());
                    }
                } else {
                    AppConstants.hideProgress();
                    AppConstants.showMessage(MiningActivity.this, "Error while submitting callback data.");
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                AppConstants.hideProgress();
                AppConstants.showMessage(MiningActivity.this, "Failed to submit callback data.");
            }
        });
    }

    private void validateNoResponse(ApiCallable apiCallable) {
        String responseNotes = edtResponseNotes.getText().toString();
        if (responseNotes.equalsIgnoreCase("")) {
            AppConstants.showMessage(MiningActivity.this, "Please enter notes to proceed.");
        } else {
            reason = reasonId;
            submitRequestReview(apiCallable);
//            if (spinActions.getSelectedItem().toString().equalsIgnoreCase("No response")) {
//                submitNoResponse(apiCallable);
//            } else if (spinActions.getSelectedItem().toString().equalsIgnoreCase("Request Review / Rating")) {
//                submitRequestReview(apiCallable);
//            } else if (spinActions.getSelectedItem().toString().equalsIgnoreCase("Send message regarding moratorium")) {
//                submitZest(apiCallable);
//            }
        }
    }

    private void validateNeedaPhone() {
        String responseNotes = Objects.requireNonNull(edtNeedaPhoneNotes.getText()).toString();
        if(reasonId.contains("already")){
            if(edtPurchaseDate.getText().toString().equalsIgnoreCase("")){
                AppConstants.showMessage(MiningActivity.this, "Please select Purchase date to proceed.");
            }else if(selectedStore.equalsIgnoreCase("--- Select Store ---")){
                AppConstants.showMessage(MiningActivity.this, "Please select a store to proceed.");
            }else{
                reason = reasonId;
                String datepurchase=edtPurchaseDate.getText().toString();
                submitNeedaPhone(datepurchase,selectedStore);
            }
        }
        else {
            if (responseNotes.equalsIgnoreCase("")) {
                AppConstants.showMessage(MiningActivity.this, "Please enter notes to proceed.");
            } else if (selectedMake.equalsIgnoreCase("--- Select Make ---")) {
                AppConstants.showMessage(MiningActivity.this, "Please select a make to proceed.");
            } else if (selectedModel.equalsIgnoreCase("--- Select Model ---")) {
                AppConstants.showMessage(MiningActivity.this, "Please select a model to proceed.");
            } else {
                reason = reasonId;

                submitNeedaPhone("", "");
            }
        }
    }

    void submitNeedaPhone(String date,String code) {
        ApiCallable callable = SangeethaCareApiClient.getClient().create(ApiCallable.class);
        String response = Objects.requireNonNull(edtNeedaPhoneNotes.getText()).toString();
        AppConstants.showProgress(MiningActivity.this, "Submitting data..");
        callable.updateNeedaPhone(userToken, customerMobile, InvoiceId, storeCode, storeId, reason,
                response, selectedMake, selectedModel,date,code).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (response.isSuccessful()) {
                    AppConstants.hideProgress();
                    CommonResponse commonResponse = response.body();
                    if (commonResponse.getStatus()) {
                        AppConstants.showMessage(MiningActivity.this, commonResponse.getMessage());
                        restartActivity();
                    } else {
                        AppConstants.hideProgress();
                        AppConstants.showMessage(MiningActivity.this, commonResponse.getMessage());
                    }
                } else {
                    AppConstants.hideProgress();
                    AppConstants.showMessage(MiningActivity.this, "Error while submitting no response data.");
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                AppConstants.hideProgress();
                AppConstants.showMessage(MiningActivity.this, "Failed to submit No response data.");
            }
        });
    }

    void submitRequestReview(ApiCallable callable) {
        AppConstants.showProgress(MiningActivity.this, "Submitting data..");
        callable.updateRequestReview(userToken, customerMobile, InvoiceId, storeCode, storeId, reason,
                edtResponseNotes.getText().toString()).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (response.isSuccessful()) {
                    CommonResponse commonResponse = response.body();
                    if (commonResponse.getStatus()) {
                        AppConstants.hideProgress();
                        AppConstants.showMessage(MiningActivity.this, commonResponse.getMessage());
                        restartActivity();
                    } else {
                        AppConstants.hideProgress();
                        AppConstants.showMessage(MiningActivity.this, commonResponse.getMessage());
                    }
                } else {
                    AppConstants.hideProgress();
                    AppConstants.showMessage(MiningActivity.this, "Error while submitting no response data.");
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                AppConstants.hideProgress();
                AppConstants.showMessage(MiningActivity.this, "Failed to submit No response data.");
            }
        });
    }

    private void validateCallBack() {
        String date = edtDate.getText().toString();
        String time = edtTime.getText().toString();
        String notes = edtNotes.getText().toString();

        if (date.equalsIgnoreCase("")) {
            AppConstants.showMessage(MiningActivity.this, "Please select a date to proceed.");
        } else if (time.equalsIgnoreCase("")) {
            AppConstants.showMessage(MiningActivity.this, "Please select a time to proceed.");
        } else if (notes.equalsIgnoreCase("")) {
            AppConstants.showMessage(MiningActivity.this, "Please enter notes to proceed.");
        } else {
            reason = reasonId;
            ApiCallable apiCallable = SangeethaCareApiClient.getClient().create(ApiCallable.class);
            AppConstants.showProgress(MiningActivity.this, "Submitting data..");
            apiCallable.updateCallBack(userToken, customerMobile, InvoiceId, storeCode, storeId, reason, notes,
                    date, time).enqueue(new Callback<CommonResponse>() {
                @Override
                public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                    if (response.isSuccessful()) {
                        CommonResponse commonResponse = response.body();
                        if (commonResponse.getStatus()) {
                            AppConstants.hideProgress();
                            AppConstants.showMessage(MiningActivity.this, commonResponse.getMessage());
                            restartActivity();
                        } else {
                            AppConstants.hideProgress();
                            AppConstants.showMessage(MiningActivity.this, commonResponse.getMessage());
                        }
                    } else {
                        AppConstants.hideProgress();
                        AppConstants.showMessage(MiningActivity.this, "Error while submitting callback data.");
                    }
                }

                @Override
                public void onFailure(Call<CommonResponse> call, Throwable t) {
                    AppConstants.hideProgress();
                    AppConstants.showMessage(MiningActivity.this, "Failed to submit callback data.");
                }
            });
        }
    }

    @SuppressLint("ObsoleteSdkInt")
    void restartActivity() {
        if (Build.VERSION.SDK_INT >= 11) {
            recreate();
        } else {
            Intent intent = getIntent();
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            finish();
            overridePendingTransition(0, 0);

            startActivity(intent);
            overridePendingTransition(0, 0);
        }
    }


    private void showCalendarForDatePicking() {
        Calendar calendar = Calendar.getInstance();
        Date d = calendar.getTime();
        int yearD = calendar.get(Calendar.YEAR);

        String currentYear = String.valueOf(yearD);

        int yearDefault = d.getYear();
        final int monthDefault = d.getMonth();

        final int dateDefault = d.getDate();
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                (view, year, monthOfYear, dayOfMonth) -> {
                    Calendar datePickedFromCalendar = Calendar.getInstance();
                    datePickedFromCalendar.set(year, monthOfYear, dayOfMonth);
                    String dateForView = CalendarUtils.getDateInCustomFormat(datePickedFromCalendar.getTime(), "dd-MM-yyyy");
//                    Toast.makeText(this, monthDefault+"", Toast.LENGTH_SHORT).show();
                    String acade = AppConstants.getAcademicYear(dateForView);
//                    invoiceYearText.setText(acade);
                    edtDate.setText(dateForView);
                }, yearD, monthDefault, dateDefault);
//        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        //datePickerDialog.getDatePicker().setCalendarViewShown(false);
        datePickerDialog.show();
    }

    private void showPurchaseDatePicking() {
        Calendar calendar = Calendar.getInstance();
        Date d = calendar.getTime();
        int yearD = calendar.get(Calendar.YEAR);

        String currentYear = String.valueOf(yearD);

        int yearDefault = d.getYear();
        final int monthDefault = d.getMonth();

        final int dateDefault = d.getDate();
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                (view, year, monthOfYear, dayOfMonth) -> {
                    Calendar datePickedFromCalendar = Calendar.getInstance();
                    datePickedFromCalendar.set(year, monthOfYear, dayOfMonth);
                    String dateForView = CalendarUtils.getDateInCustomFormat(datePickedFromCalendar.getTime(), "dd-MM-yyyy");
//                    Toast.makeText(this, monthDefault+"", Toast.LENGTH_SHORT).show();
                    String acade = AppConstants.getAcademicYear(dateForView);
//                    invoiceYearText.setText(acade);
                    edtPurchaseDate.setText(dateForView);
                }, yearD, monthDefault, dateDefault);
//        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        //datePickerDialog.getDatePicker().setCalendarViewShown(false);
        datePickerDialog.show();
    }

    //Start Countodwn method
    private void startTimer(int noOfMinutes) {
        countDownTimer = new CountDownTimer(noOfMinutes, 1000) {
            public void onTick(long millisUntilFinished) {
                long millis = millisUntilFinished;
                //Convert milliseconds into hour,minute and seconds
                @SuppressLint("DefaultLocale")
                String hms = String.format("%02d:%02d",
                        TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                        TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
                fabTimer.setText(hms);//set text
            }

            public void onFinish() {

                fabTimer.setText("00:00"); //On finish change timer text
                countDownTimer = null;//set CountDownTimer to null
//                restartActivity();
                PlanActionSubmit.setVisibility(View.VISIBLE);
                fabTimer.setVisibility(View.GONE);
                //  startTimer.setText(getString(R.string.start_timer));//Change button text
            }
        }.start();

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position > 0) {
            switch (parent.getId()) {
                case R.id.brandMiningSpinner:
                    String makeId = makeList.get(position - 1).getId();
                    selectedMake = makeId;
                    if (ValidationUtils.isThereInternet(MiningActivity.this))
                        offerPresenter.getModel(userToken, makeId);
                    modelList.clear();
                    models.clear();
                    models.add(0, "--- Select Model ---");
                    modelAdapter.notifyDataSetChanged();
                    break;
                case R.id.modelMiningSpinner:
                    //  modelNameStr = modelList.get(position - 1).getModelName();
                    selectedModel = modelList.get(position - 1).getId();

                    break;
                case R.id.storeSpinner:
                    selectedStore = storeList.get(position - 1).getStore_code();
                    break;
            }

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void showMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void setMake(MakeResponse managerResponse) {
        if (managerResponse.isStatus()) {
            makeList.addAll(managerResponse.getData());
            for (DataItem item : makeList) {
                makes.add(item.getMakeName());
            }
            makeAdapter.notifyDataSetChanged();
        } else {
            showMessage(managerResponse.getMessage());
        }

    }

    @Override
    public void setModel(ModelResponse managerResponse) {
        if (managerResponse.isStatus()) {
            modelList.addAll(managerResponse.getData());
            for (ModelItem item : modelList) {
                models.add(item.getModelName());
            }

            modelAdapter.notifyDataSetChanged();
        } else {
            showMessage(managerResponse.getMessage());
        }


    }

    @Override
    public void setStores(StoreResponse managerResponse) {
        if (managerResponse.getStatus()) {
            storeList.addAll(managerResponse.getData());
            for (StoreModel item : storeList) {
                stores.add(item.getStore_name());
            }

            storeAdapter.notifyDataSetChanged();
        } else {
            showMessage(managerResponse.getMessage());
        }
    }

}
