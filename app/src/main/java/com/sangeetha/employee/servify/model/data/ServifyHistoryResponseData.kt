package com.sangeetha.employee.servify.model.data


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class ServifyHistoryResponseData(
        @SerializedName("status")
        val status: Boolean,
        @SerializedName("data")
        val data: ServifyHistoryData,
        @SerializedName("message")
        val message: String

)

