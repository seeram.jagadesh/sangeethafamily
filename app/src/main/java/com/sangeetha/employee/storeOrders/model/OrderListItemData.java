package com.sangeetha.employee.storeOrders.model;

import com.google.gson.annotations.SerializedName;

public class OrderListItemData {

    @SerializedName("order_unique_id")
    private String order_unique_id;

    @SerializedName("customer_name")
    private String customer_name;

    @SerializedName("customer_mobile")
    private String customer_mobile;

    @SerializedName("allocated_store")
    private String allocated_store;

    @SerializedName("payment_option")
    private String payment_option;

    @SerializedName("payment_gateway_name")
    private String payment_gateway_name;

    @SerializedName("user_id")
    private String user_id;

    @SerializedName("total_price")
    private String total_price;

    @SerializedName("order_status")
    private String order_status;

    @SerializedName("is_app")
    private String is_app;

    @SerializedName("transaction_id")
    private String transaction_id;

    @SerializedName("payment_transaction_id")
    private String payment_transaction_id;

    @SerializedName("payment_status")
    private String payment_status;

    @SerializedName("created_at")
    private String created_at;


    @Override
    public String toString() {
        return "OrderListItemData{" +
                "order_unique_id='" + order_unique_id + '\'' +
                ", customer_name='" + customer_name + '\'' +
                ", customer_mobile='" + customer_mobile + '\'' +
                ", allocated_store='" + allocated_store + '\'' +
                ", payment_option='" + payment_option + '\'' +
                ", payment_gateway_name='" + payment_gateway_name + '\'' +
                ", user_id='" + user_id + '\'' +
                ", total_price='" + total_price + '\'' +
                ", order_status='" + order_status + '\'' +
                ", is_app='" + is_app + '\'' +
                ", transaction_id='" + transaction_id + '\'' +
                ", payment_transaction_id='" + payment_transaction_id + '\'' +
                ", payment_status='" + payment_status + '\'' +
                ", created_at='" + created_at + '\'' +
                '}';
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getOrder_unique_id() {
        return order_unique_id;
    }

    public void setOrder_unique_id(String order_unique_id) {
        this.order_unique_id = order_unique_id;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getCustomer_mobile() {
        return customer_mobile;
    }

    public void setCustomer_mobile(String customer_mobile) {
        this.customer_mobile = customer_mobile;
    }

    public String getAllocated_store() {
        return allocated_store;
    }

    public void setAllocated_store(String allocated_store) {
        this.allocated_store = allocated_store;
    }

    public String getPayment_option() {
        return payment_option;
    }

    public void setPayment_option(String payment_option) {
        this.payment_option = payment_option;
    }

    public String getPayment_gateway_name() {
        return payment_gateway_name;
    }

    public void setPayment_gateway_name(String payment_gateway_name) {
        this.payment_gateway_name = payment_gateway_name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTotal_price() {
        return total_price;
    }

    public void setTotal_price(String total_price) {
        this.total_price = total_price;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getIs_app() {
        return is_app;
    }

    public void setIs_app(String is_app) {
        this.is_app = is_app;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getPayment_transaction_id() {
        return payment_transaction_id;
    }

    public void setPayment_transaction_id(String payment_transaction_id) {
        this.payment_transaction_id = payment_transaction_id;
    }

    public String getPayment_status() {
        return payment_status;
    }

    public void setPayment_status(String payment_status) {
        this.payment_status = payment_status;
    }
}
