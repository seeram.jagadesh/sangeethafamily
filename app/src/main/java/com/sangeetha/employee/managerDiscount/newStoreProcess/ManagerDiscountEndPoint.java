package com.sangeetha.employee.managerDiscount.newStoreProcess;

import com.sangeetha.employee.CommonResponse;
import com.sangeetha.employee.managerDiscount.couponCode.CheckCouponCode;
import com.sangeetha.employee.managerDiscount.manager_pojo.AlternativeMethodResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.CouponHistoryResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.DiscountDetailsResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.FindEmployeeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.MakeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelDiscountResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelPriceResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.PaymentModeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ProductTypeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.SubmitDiscountResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

interface ManagerDiscountEndPoint {

    @FormUrlEncoded
    @POST("api-get-brand")
    Call<MakeResponse> getMakeDetail(@Header("sangeetha_token") String token, @Field("product_value") String productId);

    @FormUrlEncoded
    @POST("api-get-model")
    Call<ModelResponse> getModel(@Header("sangeetha_token") String token, @Field("product_value") String productId, @Field("brand_value") String makeId);

    @FormUrlEncoded
    @POST("api-price-list")
    Call<ModelPriceResponse> getModelPriceDetail(@Header("sangeetha_token") String token, @Field("product_type") String productType, @Field("brand_id") String brandId
            , @Field("model_id") String modelId, @Field("payment_mode") String paymentMode);

    @FormUrlEncoded
    @POST("api-manager-check-discount")
    Call<ModelDiscountResponse> checkModelDiscount(@Field("model_id") String modelId,
                                                   @Field("store_id") String storeId);

    @FormUrlEncoded
    @POST("api-manager-generate-otp")
    Call<ResponseBody> generateOtp(@Header("sangeetha_token") String token, @Field("customer_mobile") String customerMobile);

    @FormUrlEncoded
    @POST("api-manager-verify-otp")
    Call<ResponseBody> verifyOTP(@Header("sangeetha_token") String token, @Field("customer_mobile") String customerMobile,
                                 @Field("customer_otp") String customerOtp);

    @FormUrlEncoded
    @POST("api-manager-generate-otp")
    Call<ResponseBody> resendOtp(@Header("sangeetha_token") String token, @Field("customer_mobile") String customerMobile);

    @FormUrlEncoded
    @POST("api-manager-send-request")
    Call<ResponseBody> sendRequest(@Field("manage_id") String managerId,
                                   @Field("store_id") String storeId,
                                   @Field("employee_id") String employeeId,
                                   @Field("model_id") String modelId,
                                   @Field("discount_percentage") String discountPer,
                                   @Field("discount_amount") String discountAmount,
                                   @Field("total_amount") String totalAmount,
                                   @Field("customer_mobile") String customerMobile,
                                   @Field("customer_mobile_verified") boolean mobileVerified,
                                   @Field("customer_email") String emil,
                                   @Field("customer_name") String customerName,
                                   @Field("customer_otp") String customerOtp,
                                   @Field("approver") String approver);

    @FormUrlEncoded
    @POST("api-manager-send-request")
    Call<ResponseBody> sendPaymentRequest(@Header("sangeetha_token") String token,
                                          @Field("manage_id") String managerId,
                                          @Field("employee_id") String employeeId,
                                          @Field("model_id") String modelId,
                                          @Field("discount_value") String discountAmount,
                                          @Field("total_amount") String totalAmount,
                                          @Field("customer_mobile") String customerMobile,
                                          @Field("customer_mobile_verified") boolean mobileVerified,
                                          @Field("customer_email") String emil,
                                          @Field("customer_name") String customerName,
                                          @Field("customer_otp") String customerOtp,
                                          @Field("approver") String approver,
                                          @Field("payment_mode") String paymentMode,
                                          @Field("product_type") String productType,
                                          @Field("make_id") String makeId);


    @FormUrlEncoded
    @POST("api-manager-alternative-method")
    Call<AlternativeMethodResponse> alternativeMethod(@Header("sangeetha_token") String token, @Field("alternative_code") String approvalCode, @Field("source_from") String sourceFrom);

    @FormUrlEncoded
    @POST("api-manager-submit")
    Call<ResponseBody> submitDiscount();

    @FormUrlEncoded
    @POST("api-check-employee")
    Call<FindEmployeeResponse> findEmployeeName(@Header("sangeetha_token") String token, @Field("employee_id") String employeeId);

    @FormUrlEncoded
    @POST("api-manager-cancel-coupon")
    Call<ResponseBody> cancelCoupon(@Field("reference_id") String referenceId);

    @GET("api-manager-store-history")
    Call<CouponHistoryResponse> getCouponHistory(@Header("sangeetha_token") String token);

    @FormUrlEncoded
    @POST("api-manager-reference-method")
    Call<DiscountDetailsResponse> getDiscountDetails(@Header("sangeetha_token") String token, @Field("reference_id") String refId);

    @FormUrlEncoded
    @POST("api-manager-discount-summary")
    Call<DiscountDetailsResponse> getDiscountInfo(@Header("sangeetha_token") String token, @Field("reference_id") String refId);

    @FormUrlEncoded
    @POST("api-check-manager-discount-calculate")
    Call<SubmitDiscountResponse> percentageBaseDiscount(@Header("sangeetha_token") String token,
                                                        @Field("product_type") String productType, @Field("make_id") String brandId,
                                                        @Field("model_id") String modelId, @Field("payment_mode") String paymentMode,
                                                        @Field("manage_id") String manageId, @Field("discount_value") String discountId);

    @FormUrlEncoded
    @POST("api-manager-check-discount-calculate")
    Call<SubmitDiscountResponse> amountBasedDiscount(@Field("store_id") String storeId,
                                                     @Field("model_id") String modelId,
                                                     @Field("manage_id") String manageId,
                                                     @Field("discount_value") String discountValue,
                                                     @Field("total_value") String totalValue);

    @FormUrlEncoded
    @POST("api-manager-check-coupon-code")
    Call<CheckCouponCode> checkCouponCode(@Field("customer_mobile") String customer);

    @FormUrlEncoded
    @POST("api-manager-check-coupon-code-cancel")
    Call<CommonResponse> couponCodeCancel(@Field("customer_mobile") String customer);


    @POST("api-fetch-payment")
    Call<PaymentModeResponse> getPaymentModeList(@Header("sangeetha_token") String token);

    @POST("api-fetch-product-type")
    Call<ProductTypeResponse> getProductTypesList(@Header("sangeetha_token") String token);


    @FormUrlEncoded
    @POST("api-check-model-discount")
    Call<ModelDiscountResponse> checkModelPaymentDiscount(@Header("sangeetha_token") String token, @Field("product_type") String productType, @Field("make_id") String brandId
            , @Field("model_id") String modelId, @Field("payment_mode") String paymentMode);

}
