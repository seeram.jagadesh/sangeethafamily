package com.sangeetha.employee.servify

import android.text.TextUtils
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import com.google.android.material.chip.Chip
import com.sangeetha.employee.databinding.FragmentServifyHistoryBinding
import com.sangeetha.employee.servify.model.data.ServifyHistoryData

/**
 * [RecyclerView.Adapter] that can display a [DummyItem] and makes a call to the
 * specified [OnListFragmentInteractionListener].
 * TODO: Replace the implementation with code for your data type.
 */
class ServifyHistoryAdapter(
        private val mValues: List<ServifyHistoryData>,
        private val mListener: ServifyHistoryFragment.OnListFragmentInteractionListener?)
    : RecyclerView.Adapter<ServifyHistoryAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as ServifyHistoryData
            // Notify the active callbacks interface (the activity, if the fragment is attached to
            // one) that an item has been selected.
            mListener?.onListFragmentInteraction(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = FragmentServifyHistoryBinding.inflate(LayoutInflater.from(parent.context),  parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        holder.mDate.text = item.created
        holder.mCompany.text = "Sangeetha"
        holder.storeCode.text = item.storeCode
        holder.storeName.text = item.storeName
        holder.orderId.text = item.tradeReferenceId
        holder.customerName.text = item.customerName
        if(TextUtils.isEmpty(item.customerEmail)){
            holder.customerEmail.text = "---"
        }else {
            holder.customerEmail.text = item.customerEmail
        }
        holder.customerNo.text = item.customerMobile
        holder.apxCode.text = item.couponCode
        holder.amount.text = item.servifyAmount
        holder.couponStatus.text = item.couponStatus

        with(holder.binding.root) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val binding: FragmentServifyHistoryBinding) : RecyclerView.ViewHolder(binding.root) {
        val mDate: AppCompatTextView = binding.svDate
        val mCompany: AppCompatTextView = binding.svCompany
        val storeCode: AppCompatTextView = binding.svStoreCode
        val storeName: AppCompatTextView = binding.svStoreName
        val orderId: AppCompatTextView = binding.svOrderId
        val customerName: AppCompatTextView = binding.svCusName
        val customerEmail: AppCompatTextView = binding.svCusEmail
        val customerNo: AppCompatTextView = binding.svCusNo
        val apxCode: AppCompatTextView = binding.couponCode
        val amount: AppCompatTextView = binding.svAmount
        val couponStatus: Chip = binding.svStatus



        override fun toString(): String {
            return super.toString() + " '" + mCompany.text + "'"
        }
    }
}
