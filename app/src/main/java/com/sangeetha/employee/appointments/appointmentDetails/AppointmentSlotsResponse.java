package com.sangeetha.employee.appointments.appointmentDetails;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AppointmentSlotsResponse {

    @SerializedName("status")
    public boolean status;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public List<Object> dataArray;


    @Override
    public String toString() {
        return "AppointmentSlotsResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", dataArray=" + dataArray +
                '}';
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Object> getDataArray() {
        return dataArray;
    }

    public void setDataArray(List<Object> dataArray) {
        this.dataArray = dataArray;
    }
}
