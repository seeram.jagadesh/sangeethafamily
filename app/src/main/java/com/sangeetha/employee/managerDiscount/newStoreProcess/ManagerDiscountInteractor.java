package com.sangeetha.employee.managerDiscount.newStoreProcess;


import com.sangeetha.employee.CommonResponse;
import com.sangeetha.employee.managerDiscount.couponCode.CheckCouponCode;
import com.sangeetha.employee.managerDiscount.manager_pojo.AlternativeMethodResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.CouponHistoryResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.DiscountDetailsResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.FindEmployeeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.MakeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelDiscountResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelPriceResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.PaymentModeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ProductTypeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.SubmitDiscountResponse;

public interface ManagerDiscountInteractor {

    void getMake(String userToken, String productId);

    void getModule(String userToken, String productId, String makeId);

    void getProductType(String token, String paymentId);

    void getProductPrice(String token, String productType, String brandId
            , String modelId, String paymentMode);

    void checkModelDiscount(String modelId, String storeId);

    void generateOTP(String token, String mobileNumber);

    void validateOTP(String token, String mobileNumber, String otp);

    void resendOtp(String token, String mobileNumber);

    void submitDiscount();

    void getApproval();

    void findEmployeeName(String token, String empId);

    void sendRequest(String managerId,
                     String storeId,
                     String employeeId,
                     String modelId,
                     String discountPer,
                     String discountAmount,
                     String totalAmount,
                     String customerMobile,
                     boolean mobileVerified,
                     String emil,
                     String customerName,
                     String customerOtp,
                     String approver);

    void sendPaymentRequest(String token, String managerId,
                            String employeeId,
                            String modelId,
                            String discountPer,
                            String totalAmount,
                            String customerMobile,
                            boolean mobileVerified,
                            String emil,
                            String customerName,
                            String customerOtp,
                            String approver, String paymentID,
                            String productType,
                            String makeId);

    void alternativeMethod(String token, String approvalCode, String sourceFrom);

    void couponHistory(String storeId);

    void discountDetails(String token, String refId);

    void discountInfo(String token, String refId);

    void cancelCoupon(String refId);

    void percentageBaseDiscount(String token,
                                String productType, String brandId,
                                String modelId, String paymentMode,
                                String manageId, String discountId);

    void amountBasedDiscount(String storeId, String modelId, String manageId, String discountValue, String totalValue);

    void checkCouponCode(String regId);

    void couponCodeCancel(String regId);

    void getPayments(String token);

    void checkModelPaymentDiscount(String token, String productType, String brandId
            , String modelId, String paymentMode);


    interface ManagerDiscountListener {

        void showMessage(String msg);

        void setMake(MakeResponse managerResponse);

        void setModel(ModelResponse managerResponse);

        void setProductPrice(ModelPriceResponse managerResponse);

        void setPaymentModes(PaymentModeResponse managerResponse);

        void setProductTypes(ProductTypeResponse productTypeResponse);

        void setDiscountMessage(ModelDiscountResponse discountMessage);

        void setOtpVerification(boolean verification);

        void setCustomerVerification(boolean statue, String msg, int type, String otpType);

        void setApprovalRequest(boolean status, String refId);

        void setEmployeeName(FindEmployeeResponse employeeName);

        void setAlternativeMethod(AlternativeMethodResponse alternativeMethod);

        void setCouponHistory(CouponHistoryResponse couponHistory);

        void setDiscountDetails(DiscountDetailsResponse discountDetails);

        void setDiscountInfo(DiscountDetailsResponse discountDetails);

        void couponCancel(boolean status, String msg);

        void setPercentageBasedDiscount(SubmitDiscountResponse percentageBasedDiscount);

        void setAmountBasedDiscount(SubmitDiscountResponse amountBasedDiscount);

        void setCheckCouponCode(CheckCouponCode checkCouponCode);

        void setCouponCodeCancel(CommonResponse commonResponse);
    }
}
