package com.sangeetha.employee.onlineDelivery;

public interface OnlineDeliverPresenter {

    void getOrders(String userId);

    void getDeliverOrders(String userId);

    void orderDetails(String userId, String orderId);


}
