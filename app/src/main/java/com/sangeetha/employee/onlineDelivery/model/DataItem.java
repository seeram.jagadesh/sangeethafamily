package com.sangeetha.employee.onlineDelivery.model;


import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;


public class DataItem{

	@SerializedName("customer_mobile_number")
	private String customerMobileNumber;

	@SerializedName("payment_type")
	private String paymentType;

	@SerializedName("amount_paid")
	private String amountPaid;

	@SerializedName("order_unique_id")
	private String orderUniqueId;

	@SerializedName("order_amount")
	private String orderAmount;

	@SerializedName("customer_name")
	private String customerName;

	@SerializedName("review_added_status")
	private String review_added_status;

	@SerializedName("delivery_details_added_status")
	private String delivery_details_added_status;

	@SerializedName("otp_verified")
	private String otp_verified;

	public String getDelivery_details_added_status() {
		return delivery_details_added_status;
	}

	public void setDelivery_details_added_status(String delivery_details_added_status) {
		this.delivery_details_added_status = delivery_details_added_status;
	}

	public String getOtp_verified() {
		return otp_verified;
	}

	public void setOtp_verified(String otp_verified) {
		this.otp_verified = otp_verified;
	}

	public String getReview_added_status() {
		return review_added_status;
	}

	public void setReview_added_status(String review_added_status) {
		this.review_added_status = review_added_status;
	}

	public void setCustomerMobileNumber(String customerMobileNumber){
		this.customerMobileNumber = customerMobileNumber;
	}

	public String getCustomerMobileNumber(){
		return customerMobileNumber;
	}

	public void setPaymentType(String paymentType){
		this.paymentType = paymentType;
	}

	public String getPaymentType(){
		return paymentType;
	}

	public void setAmountPaid(String amountPaid){
		this.amountPaid = amountPaid;
	}

	public String getAmountPaid(){
		return amountPaid;
	}

	public void setOrderUniqueId(String orderUniqueId){
		this.orderUniqueId = orderUniqueId;
	}

	public String getOrderUniqueId(){
		return orderUniqueId;
	}

	public void setOrderAmount(String orderAmount){
		this.orderAmount = orderAmount;
	}

	public String getOrderAmount(){
		return orderAmount;
	}

	public void setCustomerName(String customerName){
		this.customerName = customerName;
	}

	public String getCustomerName(){
		return customerName;
	}

	@NotNull
	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"customer_mobile_number = '" + customerMobileNumber + '\'' + 
			",payment_type = '" + paymentType + '\'' + 
			",amount_paid = '" + amountPaid + '\'' + 
			",order_unique_id = '" + orderUniqueId + '\'' + 
			",order_amount = '" + orderAmount + '\'' + 
			",customer_name = '" + customerName + '\'' + 
			"}";
		}
}