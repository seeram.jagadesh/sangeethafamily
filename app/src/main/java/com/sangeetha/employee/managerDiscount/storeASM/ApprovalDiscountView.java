package com.sangeetha.employee.managerDiscount.storeASM;

import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.ApproveDiscountResponse;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.DiscountTypeResponse;

public interface ApprovalDiscountView {
    void showProgress();

    void hideProgress();

    void showMessage(String msg);

    void setApproval(ApproveDiscountResponse approval);

    void setDiscountTypes(DiscountTypeResponse discountTypes);

    void setDiscountStatus(ApproveDiscountResponse approveDiscountResponse);
}
