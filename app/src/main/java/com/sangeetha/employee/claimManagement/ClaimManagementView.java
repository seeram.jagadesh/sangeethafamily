package com.sangeetha.employee.claimManagement;

public interface ClaimManagementView {

    void showMessage(String msg);

    void showProgress();

    void hideProgress();

    void setClaimDetails();
}
