package com.sangeetha.employee.empLogin.loginModel;

import com.google.gson.annotations.SerializedName;

public class ASMData {

    @SerializedName("asm_id")
    private String asmId;

    @SerializedName("employee_id")
    private String employeeId;

    @SerializedName("employee_name")
    private String employeeName;

    @SerializedName("email")
    private String email;

    @SerializedName("emp_id")
    private String empId;

    public String getAsmId() {
        return asmId;
    }

    public void setAsmId(String asmId) {
        this.asmId = asmId;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    @Override
    public String toString() {
        return
                "ASMData{" +
                        "asm_id = '" + asmId + '\'' +
                        ",employee_id = '" + employeeId + '\'' +
                        ",employee_name = '" + employeeName + '\'' +
                        ",email = '" + email + '\'' +
                        ",emp_id = '" + empId + '\'' +
                        "}";
    }
}