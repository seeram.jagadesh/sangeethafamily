package com.sangeetha.employee.servify.model.data

import com.google.gson.annotations.SerializedName

data class ProductTypeData(
        @SerializedName("id")
        val id: String,
        @SerializedName("product_name")
        val productName: String,
        @SerializedName("status")
        val status: String

)