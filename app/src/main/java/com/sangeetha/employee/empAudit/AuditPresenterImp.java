package com.sangeetha.employee.empAudit;

import android.text.TextUtils;

import com.sangeetha.employee.empAudit.model.DataItem;
import com.sangeetha.employee.empAudit.model.DesignationsItem;
import com.sangeetha.employee.empAudit.model.DropdownDetailsItem;
import com.sangeetha.employee.empAudit.model.EmpCategoryResponse;
import com.sangeetha.employee.empAudit.model.SearchEmpItem;
import com.sangeetha.employee.empAudit.model.StartAuditResponse;
import com.sangeetha.employee.empAudit.model.SubCategoryResponse;

import java.util.List;


public class AuditPresenterImp implements AuditPresenter, AuditInteractor.AuditListener {
    private AuditView auditView;
    private AuditInteractor auditInteractor;
    private EmpAuditFragment auditFragment;

    AuditPresenterImp(AuditView auditView) {
        this.auditView = auditView;
        auditInteractor = new AuditInteractorImp(this);
    }

    AuditPresenterImp(EmpAuditFragment auditFragment) {
        auditInteractor = new AuditInteractorImp(this);
        this.auditFragment = auditFragment;
    }

    @Override
    public void showMessage(String msg) {
        if (auditView != null) {
            auditView.hideProgress();
            auditView.showMessage(msg);
        } else if (auditFragment != null) {
            auditFragment.hideProgress();
            auditFragment.showMessage(msg);
        }

    }

    @Override
    public void setStoreList(List<DataItem> data) {
        if (auditView != null) {
            auditView.hideProgress();
            auditView.setStoreList(data);
        }

    }

    @Override
    public void setEmpStatus(List<DropdownDetailsItem> dropdownDetails) {

        if (auditFragment != null) {
            auditFragment.hideProgress();
            auditFragment.setEmpStatus(dropdownDetails);
        }

    }

    @Override
    public void submitStore(StartAuditResponse startAuditResponse) {
        if (auditView != null) {

            auditView.submitStore(startAuditResponse);
        }
    }

    @Override
    public void setBrandList(List<DataItem> data) {
        if (auditFragment != null) {
            // auditFragment.hideProgress();
            auditFragment.setBrandList(data);
        }

    }

    @Override
    public void auditReportStatus(boolean status) {
        if (auditFragment != null) {
            auditFragment.hideProgress();
            auditFragment.auditReportStatus(status);
        }
    }

    @Override
    public void setEmpName(SearchEmpItem name) {
        if (auditFragment != null) {
            auditFragment.setEmpName(name);
        }
    }

    @Override
    public void empIdError() {
        if (auditFragment != null) {
            auditFragment.empIdError();
        }
    }

    @Override
    public void setDesignation(List<DesignationsItem> designations) {
        if (auditFragment != null) {

            auditFragment.setDesignation(designations);
        }
    }

    @Override
    public void setEmpCategory(EmpCategoryResponse empCategory) {
        if (auditFragment != null) {
            auditFragment.setEmpCategory(empCategory);
        }

    }

    @Override
    public void setSubCategory(SubCategoryResponse subCategory) {
        if (auditFragment != null) {
            auditFragment.setSubCategory(subCategory);
        }

    }

    @Override
    public void getStoreList(String token) {
        if (auditView != null) {
            auditView.showProgress();
            auditInteractor.getStoreList(token);
        }

    }

    @Override
    public void getBrandList(String token) {
        if (auditFragment != null) {
            // auditFragment.showProgress();
            auditInteractor.getBrandList(token);
        }
    }

    @Override
    public void getEmpStatus(String token) {
        if (auditFragment != null) {
            auditFragment.showProgress();
            auditInteractor.getEmpStatus(token);
        }
    }

    @Override
    public void getEmpDetail(String token,String empId) {
        if (auditFragment != null) {
            if (!empId.isEmpty()) {
                auditInteractor.getEmployeeDetails(token,empId);
            }
        }
    }

    @Override
    public void startAudit(String token,String empId, String storeCode, String latLon) {
        if (auditView != null) {
            auditView.showProgress();
            auditInteractor.startAudit(token,empId, storeCode, latLon);
        }
    }

    @Override
    public void submitAuditReport(String token,String auditId, String empId, String storeCode, String empDetail, String proDetail) {
        if (auditFragment != null) {
            auditFragment.showProgress();
            if (TextUtils.isEmpty(auditId) && TextUtils.isEmpty(empId) && TextUtils.isEmpty(storeCode) && TextUtils.isEmpty(empDetail) && TextUtils.isEmpty(proDetail)) {
                auditFragment.showMessage("enter all data");
            } else {
                auditInteractor.submitAuditReport(token,auditId, empId, storeCode, empDetail, proDetail);
            }
        }
    }

    @Override
    public void getDesignation(String token) {
        if (auditFragment != null) {
            auditInteractor.getDesignation(token);
        }
    }

    @Override
    public void getEmpCategory(String token) {
        if (auditFragment != null) {
            auditInteractor.getEmpCategory(token);
        }

    }

    @Override
    public void getSubCategory(String token,String categoryId) {
        if (auditFragment != null) {
            if (categoryId.isEmpty()) {
                auditFragment.showMessage("Select employee type");
            } else {
                auditInteractor.getSubCategory(token,categoryId);
            }

        }

    }
}
