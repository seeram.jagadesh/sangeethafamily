package com.sangeetha.employee;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.sangeetha.employee.callRecorder.AudioConverterMainActivity;
import com.sangeetha.employee.empLogin.EmployeeLogin;
import com.sangeetha.employee.employeeHome.EmployeeLanding;
import com.sangeetha.employee.onlineDelivery.DeliveryOrdersActivity;
import com.sangeetha.employee.utils.AppPreference;
import com.sangeetha.employee.utils.LanguageUtil;
import com.sangeetha.employee.utils.MyContextWrapper;

import java.util.Locale;


public class SplashScreen extends AppCompatActivity {
    private AppPreference appPreference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        Locale languageType = LanguageUtil.getLanguageType(newBase);
        super.attachBaseContext(MyContextWrapper.wrap(newBase, languageType));
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        appPreference = new AppPreference(SplashScreen.this);
        new Handler().postDelayed(() -> {
            if (appPreference.isUserLoggedIn()) {
                if (appPreference.getEmpData().getGroup().equalsIgnoreCase("delivery")) {
                    Intent intent = new Intent(SplashScreen.this, DeliveryOrdersActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(SplashScreen.this, EmployeeLanding.class);
//                    Intent intent = new Intent(SplashScreen.this, AudioConverterMainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }
            } else {
                startActivity(new Intent(SplashScreen.this, EmployeeLogin.class));
                finish();
            }

        }, 3000L);
    }
}

