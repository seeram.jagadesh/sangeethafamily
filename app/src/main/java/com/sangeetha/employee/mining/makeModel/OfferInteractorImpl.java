package com.sangeetha.employee.mining.makeModel;

import androidx.annotation.NonNull;

import com.sangeetha.employee.SangeethaCareApiClient;
import com.sangeetha.employee.managerDiscount.manager_pojo.MakeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelResponse;
import com.sangeetha.employee.mining.response.StoreResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OfferInteractorImpl implements OfferInteractor {

    private NewYearOfferEndPoint newYearOfferEndPoint;
    private OfferInteractor.DiwaliOfferListener listener;

    public OfferInteractorImpl(DiwaliOfferListener listener) {
        this.listener = listener;
        newYearOfferEndPoint = SangeethaCareApiClient.getClient().create(NewYearOfferEndPoint.class);
    }

    @Override
    public void getMake(String token) {
        newYearOfferEndPoint.getMake(token,"3").enqueue(new Callback<MakeResponse>() {
            @Override
            public void onResponse(@NonNull Call<MakeResponse> call, @NonNull Response<MakeResponse> response) {

                if (response.isSuccessful()) {
                    listener.setMake(response.body());
                } else {
                    listener.showMessage(response.message());
                }

            }

            @Override
            public void onFailure(@NonNull Call<MakeResponse> call, @NonNull Throwable t) {
                listener.showMessage(t.getMessage());

            }
        });

    }

    @Override
    public void getModel(String token,String makeId) {
        newYearOfferEndPoint.getModel(token,makeId,"3").enqueue(new Callback<ModelResponse>() {
            @Override
            public void onResponse(@NonNull Call<ModelResponse> call, @NonNull Response<ModelResponse> response) {
                if (response.isSuccessful()) {
                    listener.setModel(response.body());
                } else {
                    listener.showMessage(response.message());
                }

            }

            @Override
            public void onFailure(@NonNull Call<ModelResponse> call, @NonNull Throwable t) {
                listener.showMessage(t.getMessage());

            }
        });

    }

    @Override
    public void getStoreList(String token) {
        newYearOfferEndPoint.getStoreList(token).enqueue(new Callback<StoreResponse>() {
            @Override
            public void onResponse(@NonNull Call<StoreResponse> call, @NonNull Response<StoreResponse> response) {
                if (response.isSuccessful()) {
                    listener.setStores(response.body());
                } else {
                    listener.showMessage(response.message());
                }

            }

            @Override
            public void onFailure(@NonNull Call<StoreResponse> call, @NonNull Throwable t) {
                listener.showMessage(t.getMessage());

            }
        });

    }


}
