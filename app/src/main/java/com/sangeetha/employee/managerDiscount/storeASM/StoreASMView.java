package com.sangeetha.employee.managerDiscount.storeASM;

import com.sangeetha.employee.CommonResponse;
import com.sangeetha.employee.checkStock.StockCheckResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.MakeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ProductTypeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.SubmitDiscountResponse;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.StoreListResponse;
import com.sangeetha.employee.peopleTracking.model.StoreResponse;

public interface StoreASMView {

    void showProgress();

    void setProductTypes(ProductTypeResponse productTypes);

    void hideProgress();

    void showMessage(String msg);

    void setMake(MakeResponse managerResponse);

    void setModel(ModelResponse managerResponse);

    void setStoreList(StoreListResponse storeList);

    void setStores(StoreResponse storeList);

    void setStocks(StockCheckResponse response);


    void setCheckEligibleException(CommonResponse commonResponse);

    void setSalesCheckCalculate(SubmitDiscountResponse commonResponse);

    void setSendExceptionRequest(CommonResponse commonResponse);
}
