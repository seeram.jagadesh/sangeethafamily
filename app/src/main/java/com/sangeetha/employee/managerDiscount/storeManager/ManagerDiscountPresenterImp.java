package com.sangeetha.employee.managerDiscount.storeManager;

import android.text.TextUtils;

import com.sangeetha.employee.CommonResponse;
import com.sangeetha.employee.managerDiscount.couponCode.CheckCouponCode;
import com.sangeetha.employee.managerDiscount.manager_pojo.AlternativeMethodResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.CouponHistoryResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.DiscountDetailsResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.FindEmployeeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.MakeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelDiscountResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelPriceResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.PaymentModeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ProductTypeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.SubmitDiscountResponse;


public class ManagerDiscountPresenterImp implements ManagerDiscountPresenter, ManagerDiscountInteractor.ManagerDiscountListener {

    private StoreView storeView;
    private ManagerDiscountInteractor managerDiscountInteractor;
    private DiscountHistoryView discountHistoryView;
    private DiscountDetailView discountDetailView;

    public ManagerDiscountPresenterImp(StoreView storeView) {
        this.storeView = storeView;
        managerDiscountInteractor = new MangerDiscountInteractorImp(this);
    }

   public ManagerDiscountPresenterImp(DiscountHistoryView discountHistoryView) {
        managerDiscountInteractor = new MangerDiscountInteractorImp(this);
        this.discountHistoryView = discountHistoryView;
    }

    public ManagerDiscountPresenterImp(DiscountDetailView discountDetailView) {
        this.discountDetailView = discountDetailView;
        managerDiscountInteractor = new MangerDiscountInteractorImp(this);
    }

    @Override
    public void getMake(String userToken,String productId) {
        if (storeView != null) {
            managerDiscountInteractor.getMake(userToken,productId);
        }

    }

    @Override
    public void getProductType(String token, String paymentId) {
        if (storeView != null) {
            managerDiscountInteractor.getProductType(token,paymentId);
        }
    }

    @Override
    public void getModel(String userToken,String productId,String makeId) {
        if (storeView != null) {
            managerDiscountInteractor.getModule(userToken,productId,makeId);
        }

    }

    @Override
    public void getPaymentList(String token) {
        if (storeView != null) {
            managerDiscountInteractor.getPayments(token);
        }

    }

    @Override
    public void getProductPrice(String token, String productType, String brandId
            , String modelId, String paymentMode) {
        if (storeView != null) {

            managerDiscountInteractor.getProductPrice(token, productType, brandId, modelId, paymentMode);
        }

    }

    @Override
    public void checkModelDiscount(String modelId, String storeId) {
        if (TextUtils.isEmpty(modelId)) {

            storeView.showMessage("Select model");
        } else {
            managerDiscountInteractor.checkModelDiscount(modelId, storeId);
        }

    }

    @Override
    public void checkModelPaymentDiscount(String token, String productType, String brandId
            , String modelId, String paymentMode) {
        if (TextUtils.isEmpty(modelId)) {

            storeView.showMessage("Select Model Type");
        } else {
            managerDiscountInteractor.checkModelPaymentDiscount(token,productType,brandId,modelId,paymentMode);
        }
    }


    @Override
    public void generateOTP(String token,String mobileNumber) {
        if (storeView != null) {
            if (TextUtils.isEmpty(mobileNumber)) {
                storeView.showMessage("Please enter customer mobile number");

            } else {
                managerDiscountInteractor.generateOTP(token,mobileNumber);
            }
        }

    }

    @Override
    public void validateOTP(String token,String mobileNumber, String otp) {
        if (storeView != null) {
            if (TextUtils.isEmpty(mobileNumber)) {
                storeView.showMessage("Enter customer mobile number");
            } else if (TextUtils.isEmpty(otp)) {
                storeView.showMessage("Enter OTP");
            } else {
                managerDiscountInteractor.validateOTP(token,mobileNumber, otp);
            }
        }
    }

    @Override
    public void resendOtp(String token,String mobileNumber) {
        if (storeView != null) {
            if (TextUtils.isEmpty(mobileNumber)) {
                storeView.showMessage("Please enter customer mobile number");

            } else {
                managerDiscountInteractor.resendOtp(token,mobileNumber);
            }

        }

    }

    @Override
    public void sendRequest(String managerId, String storeId, String employeeId, String modelId, String discountPer, String discountAmount, String totalAmount, String customerMobile, boolean mobileVerified, String emil, String customerName, String customerOtp, String approver) {
        if (storeView != null) {
            if (TextUtils.isEmpty(approver)) {
                showMessage("select approver");

            } else {
                managerDiscountInteractor.sendRequest(managerId, storeId, employeeId, modelId, discountPer, discountAmount, totalAmount, customerMobile, mobileVerified, emil, customerName, customerOtp, approver);

            }
        }
    }

    @Override
    public void sendPaymentRequest(String token,String managerId,  String employeeId,
                                   String modelId, String discountPer,
                                   String totalAmount, String customerMobile, boolean mobileVerified,
                                   String emil, String customerName, String customerOtp,
                                   String approver, String paymentID,
                                   String productType,
                                   String makeId) {
        if (storeView != null) {
            if (TextUtils.isEmpty(approver)) {
                showMessage("select approver");

            } else {
                managerDiscountInteractor.sendPaymentRequest(token,managerId, employeeId, modelId, discountPer,
                         totalAmount, customerMobile, mobileVerified, emil, customerName,
                        customerOtp, approver,paymentID,productType,makeId);

            }
        }
    }


    @Override
    public void submitDiscount() {

    }

    @Override
    public void getApproval() {

    }

    @Override
    public void getCouponCode() {

    }

    @Override
    public void findEmployeeName(String token,String empId) {
        if (storeView != null) {
            managerDiscountInteractor.findEmployeeName(token,empId);
        }
    }

    @Override
    public void alternativeMethod(String token,String approvalCode, String sourceFrom) {
        if (approvalCode.isEmpty()) {
            if (storeView != null) {
                storeView.showMessage("Enter approval code.");
            }
        } else {
            managerDiscountInteractor.alternativeMethod(token,approvalCode, sourceFrom);
        }

    }

    @Override
    public void couponHistory(String storeId) {
        if (TextUtils.isEmpty(storeId)) {
            discountHistoryView.showMessage("Store id is empty");
        } else {
            managerDiscountInteractor.couponHistory(storeId);
        }

    }

    @Override
    public void discountDetails(String token,String refId) {
        if (discountDetailView != null) {
            if (TextUtils.isEmpty(refId)) {
                discountDetailView.showCouponMessage("Reference id is empty");
            } else {
                discountDetailView.showProgress();
                managerDiscountInteractor.discountInfo(token,refId);
            }

        } else if (storeView != null) {
            if (TextUtils.isEmpty(refId)) {
                storeView.showMessage("Reference id is empty");
            } else {
                //  storeView.showProgress();
                managerDiscountInteractor.discountDetails(token,refId);
            }
        }

    }

    @Override
    public void discountInfo(String token, String refId) {
        if (TextUtils.isEmpty(refId)) {
            discountDetailView.showCouponMessage("Reference id is empty");
        } else {
            discountDetailView.showProgress();
            managerDiscountInteractor.discountInfo(token,refId);
        }
    }

    @Override
    public void cancelCoupon(String refId) {
        if (TextUtils.isEmpty(refId)) {
            discountDetailView.showCouponMessage("Reference id is empty");
        } else {
            discountDetailView.showProgress();
            managerDiscountInteractor.cancelCoupon(refId);
        }

    }

    @Override
    public void percentageBaseDiscount(String token,
                                       String productType,  String brandId,
                                       String modelId, String paymentMode,
                                       String manageId, String discountId) {
        if (storeView != null) {
            assert modelId != null;
            if (TextUtils.isEmpty(modelId)) {
                storeView.showMessage("Model Id is empty");
            } else if (manageId.isEmpty()) {
                storeView.showMessage("Manage Id is empty");
            } else if (discountId.isEmpty()) {
                storeView.showMessage("Enter discount value");
            } else {
                managerDiscountInteractor.percentageBaseDiscount(token, productType, brandId, modelId, paymentMode, manageId, discountId);
            }
        }
    }

    @Override
    public void amountBasedDiscount(String storeId, String modelId, String manageId, String discountValue, String totalValue) {
        if (storeView != null) {
            if (TextUtils.isEmpty(modelId)) {
                storeView.showMessage("Model Id is empty");
            } else if (TextUtils.isEmpty(manageId)) {
                storeView.showMessage("Manage Id is empty");
            } else if (TextUtils.isEmpty(totalValue)) {
                storeView.showMessage("Enter total amount");
            } else {
                managerDiscountInteractor.amountBasedDiscount(storeId, modelId, manageId, discountValue, totalValue);
            }
        }
    }

    @Override
    public void checkCouponCode(String regId) {
        if (storeView != null) {
            if (TextUtils.isEmpty(regId)) {
                storeView.showMessage("Please login");
            } else {
                storeView.showProgress();
                managerDiscountInteractor.checkCouponCode(regId);
            }
        }

    }

    @Override
    public void couponCodeCancel(String regId) {
        if (storeView != null) {
            if (TextUtils.isEmpty(regId)) {
                storeView.showMessage("Please Login");
            } else {
                storeView.showProgress();
                managerDiscountInteractor.couponCodeCancel(regId);
            }
        }

    }

    @Override
    public void discountApprove(String token, String refId) {

    }

    @Override
    public void discountReject(String token, String refId, String reason, String note) {

    }

    @Override
    public void showMessage(String msg) {
        if (storeView != null) {
            storeView.showMessage(msg);
        }

    }

    @Override
    public void setMake(MakeResponse managerResponse) {
        if (storeView != null) {
            storeView.setMake(managerResponse);
        }

    }

    @Override
    public void setModel(ModelResponse modelResponse) {
        if (storeView != null) {
            storeView.setModel(modelResponse);
        }

    }

    @Override
    public void setProductPrice(ModelPriceResponse productPrice) {
        if (storeView != null) {
            storeView.setProductPrice(productPrice);
        }

    }

    @Override
    public void setPaymentModes(PaymentModeResponse managerResponse) {
        if (storeView != null) {
            storeView.setPaymentModes(managerResponse);
        }
    }

    @Override
    public void setProductTypes(ProductTypeResponse productTypeResponse) {
        if (storeView != null) {
            storeView.setProductTypes(productTypeResponse);
        }
    }

    @Override
    public void setDiscountMessage(ModelDiscountResponse discountMessage) {
        if (storeView != null) {
            storeView.setDiscountMessage(discountMessage);
        }

    }

    @Override
    public void setOtpVerification(boolean verification) {
        if (storeView != null) {
            storeView.setOtpVerification(verification);
        }

    }

    @Override
    public void setCustomerVerification(boolean statue, String msg, int type,String otpType) {
        if (storeView != null) {
            storeView.setCustomerVerification(statue, msg, type,otpType);
        }

    }

    @Override
    public void setApprovalRequest(boolean status, String refId) {
        if (storeView != null) {
            storeView.setApprovalRequest(status, refId);
        }
    }


    @Override
    public void setEmployeeName(FindEmployeeResponse employeeName) {
        if (storeView != null) {
            storeView.setEmployeeName(employeeName);
        }

    }

    @Override
    public void setAlternativeMethod(AlternativeMethodResponse alternativeMethod) {
        if (storeView != null) {
            storeView.setAlternativeMethod(alternativeMethod);
        }
    }

    @Override
    public void setCouponHistory(CouponHistoryResponse couponHistory) {
        if (discountHistoryView != null) {
            discountHistoryView.setCouponHistory(couponHistory);

        }

    }

    @Override
    public void setDiscountDetails(DiscountDetailsResponse discountDetails) {
        if (discountDetailView != null) {
            discountDetailView.hideProgress();
            discountDetailView.setDiscountDetails(discountDetails);
        } else {
            storeView.setDiscountDetails(discountDetails);
        }

    }

    @Override
    public void setDiscountInfo(DiscountDetailsResponse discountDetails) {
        if (discountDetailView != null) {
            discountDetailView.hideProgress();
            discountDetailView.setDiscountInfo(discountDetails);
        } else {
            storeView.setDiscountDetails(discountDetails);
        }
    }

    @Override
    public void couponCancel(boolean status, String msg) {
        if (discountDetailView != null) {
            discountDetailView.hideProgress();
            discountDetailView.couponCancel(status, msg);
        }
    }

    @Override
    public void setPercentageBasedDiscount(SubmitDiscountResponse percentageBasedDiscount) {
        if (storeView != null) {
            storeView.setPercentageBasedDiscount(percentageBasedDiscount);
        }

    }

    @Override
    public void setAmountBasedDiscount(SubmitDiscountResponse amountBasedDiscount) {
        if (storeView != null) {
            storeView.setAmountBasedDiscount(amountBasedDiscount);
        }

    }

    @Override
    public void setCheckCouponCode(CheckCouponCode checkCouponCode) {
        if (storeView != null) {
            storeView.hideProgress();
            storeView.setCheckCouponCode(checkCouponCode);
        }

    }

    @Override
    public void setCouponCodeCancel(CommonResponse commonResponse) {
        if (storeView != null) {
            storeView.hideProgress();
            storeView.setCouponCodeCancel(commonResponse);
        }

    }


}
