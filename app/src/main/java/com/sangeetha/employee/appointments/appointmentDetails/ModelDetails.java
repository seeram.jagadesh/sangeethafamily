package com.sangeetha.employee.appointments.appointmentDetails;

import com.google.android.gms.common.internal.ApiExceptionUtil;
import com.google.gson.annotations.SerializedName;

public class ModelDetails {
    @SerializedName("brand_name")
    public String brandName;

    @SerializedName("model_name")
    public String modelName;

    @SerializedName("model_code")
    public String modelCode;

    @Override
    public String toString() {
        return "ModelDetails{" +
                "brandName='" + brandName + '\'' +
                ", modelName='" + modelName + '\'' +
                ", modelCode='" + modelCode + '\'' +
                '}';
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getModelCode() {
        return modelCode;
    }

    public void setModelCode(String modelCode) {
        this.modelCode = modelCode;
    }
}
