package com.sangeetha.employee.forgotPassword;

import android.Manifest;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.sangeetha.employee.CommonResponse;
import com.sangeetha.employee.R;
import com.sangeetha.employee.SangeethaCareApiClient;
import com.sangeetha.employee.registrationForm.model.RegistrationForm;
import com.sangeetha.employee.utils.AppPreference;
import com.sangeetha.employee.utils.LanguageUtil;
import com.sangeetha.employee.utils.MyContextWrapper;
import com.sangeetha.employee.utils.ValidationUtils;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.READ_PHONE_STATE;

public class OtpActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static final int SMS_READ_CODE = 1235;
    private static final int SMS_RETRIEVAL_REQ_CODE = 66;
    String otpEntered;
    private AppCompatEditText otpEd;
    private Button otpSubmitBtn;
    private TextView otpExpireTimeTV;
    private boolean isForRegistration, isForAlternateMobAdd,
            isForPriceMatchCoupon;
    private AppPreference appPreference;
    private String mobNumber;
    private RegistrationForm registrationForm;
    private String redeemID;
    private ProgressDialog otpProgressDialog;
    private CountDownTimer otpTimer;
    private boolean isTimerRunning;


    //SMS RETRIEVER ***************************************
    private long timeRemaining;
    private long durationOfOtp = 120000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        appPreference = new AppPreference(OtpActivity.this);

        mobNumber = getIntent().getStringExtra("MOB_NUM_FOR_OTP");
        isForRegistration = getIntent().getBooleanExtra("IS_FOR_REGISTRATION", false);
        isForPriceMatchCoupon = getIntent().getBooleanExtra(
                "IS_FOR_PRICE_MATCH",
                false
        );
        if (isForPriceMatchCoupon)
            redeemID = getIntent().getStringExtra("REDEEM_ID");

        isForAlternateMobAdd = getIntent().getBooleanExtra(
                "IS_FOR_ALT_MOB_ADD", false
        );

        if (isForRegistration)
            registrationForm = (RegistrationForm) getIntent().getSerializableExtra("REGISTRATION_FORM");

        if (!checkIsReadDeviceStatePermissionGiven())
            requestReadDeviceState();

        initOtpView();
        startOtpTimer(durationOfOtp);
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        Locale languageType = LanguageUtil.getLanguageType(newBase);
        super.attachBaseContext(MyContextWrapper.wrap(newBase, languageType));
    }
    //*********************************************************

    private boolean checkIsReadDeviceStatePermissionGiven() {
        boolean isGiven;
        if (Build.VERSION.SDK_INT >= 23)
            isGiven = true;
        else
            isGiven = ContextCompat.checkSelfPermission(getApplicationContext(), READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED;
        return isGiven;
    }

    private void requestReadDeviceState() {
        ActivityCompat.requestPermissions(OtpActivity.this, new String[]
                {Manifest.permission.READ_PHONE_STATE
                }, SMS_READ_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == SMS_READ_CODE) {
            if (grantResults.length > 0) {
                boolean phoneStatePermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                if (phoneStatePermission) {
                    requestReadDeviceState();
                }
            }
        }
    }

    private void initOtpView() {
        otpExpireTimeTV = findViewById(R.id.otp_expire_time_tv);
        otpEd = findViewById(R.id.otp_ed);
        otpSubmitBtn = findViewById(R.id.otp_submit_btn);

        otpSubmitBtn.setText("Submit OTP");

        otpSubmitBtn.setClickable(true);

        otpSubmitBtn.setOnClickListener(v -> submitButtonClickBehaviour());

        //initSmsRetrieving();
    }

    private void initSmsRetrieving() {
        GoogleApiClient apiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .enableAutoManage(this, this)
                .addApi(Auth.CREDENTIALS_API)
                .build();
        HintRequest hintRequest = new HintRequest.Builder()
                .setPhoneNumberIdentifierSupported(true)
                .build();
        PendingIntent intent = Auth.CredentialsApi.getHintPickerIntent(
                apiClient, hintRequest);
        try {
            startIntentSenderForResult(intent.getIntentSender(), SMS_RETRIEVAL_REQ_CODE, null, 0, 0, 0);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }

        // Get an instance of SmsRetrieverClient, used to start listening for a matching
// SMS message.
        SmsRetrieverClient client = SmsRetriever.getClient(this /* context */);

        // Starts SmsRetriever, which waits for ONE matching SMS message until timeout
// (5 minutes). The matching SMS message will be sent via a Broadcast Intent with
// action SmsRetriever#SMS_RETRIEVED_ACTION.
        Task<Void> task = client.startSmsRetriever();

// Listen for success/failure of the start Task. If in a background thread, this
// can be made blocking using Tasks.await(task, [timeout]);
        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                // Successfully started retriever, expect broadcast intent
                // ...

              //  Log.d("OTP", "Added SMS retriever");
            }
        });

        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                // Failed to start retriever, inspect Exception for more details
                // ...
                e.printStackTrace();
              //  Log.d("OTP", "Failed SMS retriever: " + e.toString());
            }
        });

    }

    // Obtain the phone number from the result
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SMS_RETRIEVAL_REQ_CODE) {
            if (resultCode == RESULT_OK) {
                Credential credential = data.getParcelableExtra(Credential.EXTRA_KEY);

//                if (credential != null)
//                    Log.e("OTP", "Otp Credentials: " + new Gson().toJson(
//                            credential
//                    ));

                // credential.getId();  <-- will need to process phone number string
            }
        }
    }

    private void submitButtonClickBehaviour() {
        String buttonFor = otpSubmitBtn.getText().toString();

        if (buttonFor.equalsIgnoreCase("Submit OTP")) {
            if (ValidationUtils.isThereInternet(OtpActivity.this)) {
                doOtpValidation();
            } else
                showToast("No internet connection!");
        } else {
            forgotPwd(mobNumber);
        }
    }

//    private void sendOtpForAddingAlternateNumber() {
//        showProgress("Generating OTP...");
//        OtpCallable otpCallable = SangeethaCareApiClient.getClient().create(
//                OtpCallable.class
//        );
//
//        otpCallable.generateOtp(
//                appPreference.getLoginData().getRegistrationId(),
//                mobNumber
//        ).enqueue(new Callback<GenerateOtpResponse>() {
//            @Override
//            public void onResponse(@NonNull Call<GenerateOtpResponse> call, @NonNull Response<GenerateOtpResponse> response) {
//                if (response.code() == 200) {
//                    assert response.body() != null;
//                    if (response.body().getStatus()) {
//                        hideProgress();
//                        Toast.makeText(OtpActivity.this, "Otp sent successfully!", Toast.LENGTH_SHORT).show();
//                        durationOfOtp = 120000;
//                        startOtpTimer(durationOfOtp);
//                    } else {
//                        hideProgress();
//                        Toast.makeText(OtpActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
//
//                    }
//                } else {
//                    hideProgress();
//                    Toast.makeText(OtpActivity.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            @Override
//            public void onFailure(@NonNull Call<GenerateOtpResponse> call, Throwable t) {
//                //  hideProgress();
//            }
//        });
//    }

    private void doOtpValidation() {
        otpEntered = otpEd.getText().toString().trim();

        if (TextUtils.isEmpty(otpEntered))
            showToast("OTP should not be empty");
        else if (otpEntered.length() < 4)
            showToast("OTP should be 4 digit length");
        else {

                validateOtp(otpEntered);
        }
    }

//    private void resendOTP() {
//
//            // used for re-generate otp while from forgot pwd
//            forgotPwd(mobNumber);
//    }

    private void forgotPwd(final String mob) {
        ForgotPasswordEndPoint apiCallable = SangeethaCareApiClient.getClient().create(ForgotPasswordEndPoint.class);
        apiCallable.forgotPassword(mob).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getStatus()) {
                            Toast.makeText(OtpActivity.this, "OTP sent!", Toast.LENGTH_SHORT).show();
                            durationOfOtp = 120000;
                            startOtpTimer(durationOfOtp);
                        } else {
                            Toast.makeText(OtpActivity.this, "This mobile number is not yet registered!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {

            }
        });
    }

    private void validateOtp(String otpEntered) {
        showProgress("Verifying OTP...");
        ForgotPasswordEndPoint apiCallable = SangeethaCareApiClient.getClient().create(ForgotPasswordEndPoint.class);
        apiCallable.verifyOtp(mobNumber, otpEntered).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getStatus()) {
                            hideProgress();
                            if (!isForRegistration)
                                moveToChangePwdScreen();
                        } else {
                            hideProgress();
                            Toast.makeText(OtpActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (!TextUtils.isEmpty(response.errorBody().toString())) {
                            Toast.makeText(OtpActivity.this, "Unable to verify OTP! Please try again...", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OtpActivity.this, "Unable to verify OTP! Please try again...", Toast.LENGTH_SHORT).show();
                        }
                        hideProgress();
                    }
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                hideProgress();
                Toast.makeText(OtpActivity.this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void moveToChangePwdScreen() {
        Intent intent = new Intent(OtpActivity.this, ChangePwdActivity.class);
        intent.putExtra("MOB_NUM_CHANGE_PWD", mobNumber);
        startActivity(intent);
        finish();
    }
//
//    private void registerUser(RegistrationForm registrationForm) {
//        Gson gson = new Gson();
//      //  Log.i("REGISTRATION", "Form data: " + gson.toJson(registrationForm));
//
//        showProgress("Creating account...");
//        ApiCallable apiCallable = SangeethaCareApiClient.getClient().create(ApiCallable.class);
//
//        String reference = "";
//
//        //DURING ANNIVERSARY SALE
//
///*        apiCallable.registerUser(true, registrationForm.getMob(), registrationForm.getPwd(),
//                registrationForm.getName(), registrationForm.getEmail(), registrationForm.getDob(),
//                registrationForm.getAnniversary(), registrationForm.getGender(),
//                registrationForm.getInvoiceNum(), registrationForm.getMake(),
//                registrationForm.getModel(), registrationForm.getInvoiceTotal(), registrationForm.getImeiNum(), reference)*/
//
////NEW REGISTRATION
//        apiCallable.registerUser(true, registrationForm.getMob(), registrationForm.getPwd(),
//                registrationForm.getName(), registrationForm.getEmail(), registrationForm.getDob(),
//                registrationForm.getAnniversary(), registrationForm.getGender(),
//                registrationForm.getAlternateNum())
//
//                .enqueue(new Callback<CommonResponse>() {
//                    @Override
//                    public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
//                   //     Log.i("REGISTRATION", "Response raw: " + response.raw().toString());
//                        if (response.isSuccessful()) {
//                            if (response.body() != null) {
//                                if (response.body().getStatus()) {
//                                    appPreference.setAfterRegistration(true);
//                                    hideProgress();
//                                    Toast.makeText(OtpActivity.this, "Registered Successfully! Please Login to continue...", Toast.LENGTH_SHORT).show();
//                                    Intent intent = new Intent(OtpActivity.this, LoginAcitivity.class);
//                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                    startActivity(intent);
//                                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
//                                } else {
//                                    hideProgress();
//                                    Toast.makeText(OtpActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
//                                }
//
//                            } else {
//                                hideProgress();
//                                Toast.makeText(OtpActivity.this, "We are unable to create account now! Please try again! .", Toast.LENGTH_SHORT).show();
//                            }
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<CommonResponse> call, Throwable t) {
//                        hideProgress();
//                        if (t instanceof SocketTimeoutException) {
//                            //message = "Socket Time out. Please try again.";
//                            Toast.makeText(OtpActivity.this, "There was a problem in network connection!", Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(OtpActivity.this, "There was a problem in network connection!", Toast.LENGTH_SHORT).show();
//                        }
//
//                    }
//                });
//    }

    private void showProgress(String msg) {
        if (otpProgressDialog != null)
            otpProgressDialog = null;

        otpProgressDialog = new ProgressDialog(OtpActivity.this);
        otpProgressDialog.setCancelable(false);
        otpProgressDialog.setCanceledOnTouchOutside(false);
        otpProgressDialog.setMessage(msg);
        otpProgressDialog.show();
    }

    private void hideProgress() {
        if (otpProgressDialog != null)
            otpProgressDialog.dismiss();
    }

    private void showToast(String msg) {
        Toast.makeText(this, "" + msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStart() {
        super.onStart();

        durationOfOtp = appPreference.getOtpDuration();
        timeRemaining = appPreference.getOtpTimeRemaining();
        isTimerRunning = appPreference.isOtpTimerRunning();

        if (timeRemaining > 0)
            startOtpTimer(durationOfOtp);

        otpSubmitBtn.setClickable(true);
    }

    @Override
    protected void onStop() {
        super.onStop();

        try {

            if (otpTimer != null)
                otpTimer.cancel();

            appPreference.saveOtpDuration(durationOfOtp);
            appPreference.setOtpTimerRunning(isTimerRunning);
            appPreference.saveOtpTimeRemaining(timeRemaining);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startOtpTimer(long duration) {
        otpTimer = new CountDownTimer(duration, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                isTimerRunning = true;
                timeRemaining = millisUntilFinished;
              //  Log.e("TIMER", " time: " + timeRemaining);
                updateTimerUI();
                otpSubmitBtn.setClickable(true);
            }

            @Override
            public void onFinish() {
                isTimerRunning = false;
                if (otpTimer != null)
                    otpTimer.cancel();

                updateTimerUI();
                otpSubmitBtn.setClickable(true);
            }
        };
        otpTimer.start();

    }

    private void updateTimerUI() {
        TextView otpWillExpireTV = findViewById(R.id.otp_will_expire_tv);
        if (!isTimerRunning) {
            otpWillExpireTV.setText("Your otp expired please use Resend OTP to continue!");
            otpExpireTimeTV.setVisibility(View.GONE);
            otpSubmitBtn.setText("Resend OTP");
        } else {
            otpSubmitBtn.setText("Submit OTP");
            otpWillExpireTV.setText("Your OTP will expire in ");
            otpExpireTimeTV.setVisibility(View.VISIBLE);

            int secCompleted = (int) (timeRemaining / 1000);

            int min;
            String timeLeft;

            if (secCompleted > 60) {
                min = secCompleted / 60;
                if (secCompleted % 60 == 0) {
                    timeLeft = min + "min";
                } else {
                    secCompleted = secCompleted - 60;
                    timeLeft = min + "min " + secCompleted + " sec";
                }
            } else {
                timeLeft = secCompleted + " sec";
            }

            otpExpireTimeTV.setText(timeLeft);
        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
