package com.sangeetha.employee.managerDiscount.manager_pojo;

import com.google.gson.annotations.SerializedName;

public class ModelPriceResponse {

//    @SerializedName("data")
//    private ModelPriceDetails data;


    @SerializedName("model_name")
    private String modelName;

    @SerializedName("model_code")
    private String internalCode;

    @SerializedName("model_price")
    private String itemPrice;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private boolean status;

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getInternalCode() {
        return internalCode;
    }

    public void setInternalCode(String internalCode) {
        this.internalCode = internalCode;
    }

    public String getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(String itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }


    @Override
    public String toString() {
        return "ModelPriceResponse{" +
                "modelName='" + modelName + '\'' +
                ", internalCode='" + internalCode + '\'' +
                ", itemPrice='" + itemPrice + '\'' +
                ", message='" + message + '\'' +
                ", status=" + status +
                '}';
    }
}
