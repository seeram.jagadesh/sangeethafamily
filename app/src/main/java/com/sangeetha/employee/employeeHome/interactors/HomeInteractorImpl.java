package com.sangeetha.employee.employeeHome.interactors;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sangeetha.employee.CommonEndPoint;
import com.sangeetha.employee.RetrofitServiceGenerator;
import com.sangeetha.employee.empLogin.loginModel.EmpData;
import com.sangeetha.employee.utils.AppConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeInteractorImpl implements HomeInteractor {
    HomeInteractor.HomeListener homeListener;

    public HomeInteractorImpl(HomeListener homeListener) {
        this.homeListener = homeListener;
    }

    @Override
    public void getPortalIcons(String token) {
        CommonEndPoint commonEndPoint = RetrofitServiceGenerator.getClient(AppConstants.zapStoreBackEndBaseUrl).create(CommonEndPoint.class);
        commonEndPoint.portalIcons(token).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        assert response.body() != null;
                        String res = response.body().string();
                        JSONObject responseJson = new JSONObject(res);
                        if (responseJson.optBoolean("status")) {
                            String jsonData = responseJson.getString("data");
                            JSONObject jsn = new JSONObject(jsonData);
                            homeListener.setPortalIcons(jsn);

                        } else {
                            homeListener.showMessage(responseJson.optString("message"));
                        }

                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    homeListener.showMessage(response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                homeListener.showMessage(t.getMessage());
            }
        });
    }
}
