package com.sangeetha.employee.peopleTracking;

import androidx.annotation.NonNull;

import com.sangeetha.employee.SangeethaCareApiClient;
import com.sangeetha.employee.peopleTracking.model.StoreResponse;
import com.sangeetha.employee.peopleTracking.model.TrackingResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TrackingInteractorImpl implements TrackingInteractor {

    private TrackingListener trackingListener;
    private TrackingEndPoint trackingEndPoint;

    TrackingInteractorImpl(TrackingListener trackingListener) {
        this.trackingListener = trackingListener;
        trackingEndPoint = SangeethaCareApiClient.getClient().create(TrackingEndPoint.class);
    }

    @Override
    public void getStoreList() {
        trackingEndPoint.getStores().enqueue(new Callback<StoreResponse>() {
            @Override
            public void onResponse(@NonNull Call<StoreResponse> call, @NonNull Response<StoreResponse> response) {
                if (response.isSuccessful()) {
                    trackingListener.setStoreList(response.body());
                } else {
                    trackingListener.setMessage(response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<StoreResponse> call, @NonNull Throwable t) {
                trackingListener.setMessage(t.getMessage());

            }
        });

    }

    @Override
    public void startTracking(String regId, String storeId, String imei, String type, String latValue, String lngValue) {
        trackingEndPoint.startTracking(regId, storeId, imei, type, latValue, lngValue).enqueue(new Callback<TrackingResponse>() {
            @Override
            public void onResponse(@NonNull Call<TrackingResponse> call, @NonNull Response<TrackingResponse> response) {
                if (response.isSuccessful()) {
                    trackingListener.setTracking(response.body());
                } else {
                    trackingListener.setMessage(response.message());
                }

            }

            @Override
            public void onFailure(@NonNull Call<TrackingResponse> call, @NonNull Throwable t) {
                trackingListener.setMessage(t.getMessage());

            }
        });

    }

    @Override
    public void stopTracking(String regId, String storeId, String imei, String type, String latValue, String lngValue, String trackingId) {
        trackingEndPoint.stopTracking(regId, storeId, imei, type, latValue, lngValue, trackingId).enqueue(new Callback<TrackingResponse>() {
            @Override
            public void onResponse(@NonNull Call<TrackingResponse> call, @NonNull Response<TrackingResponse> response) {
                if (response.isSuccessful()) {
                    trackingListener.setTracking(response.body());
                } else {
                    trackingListener.setMessage(response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<TrackingResponse> call, @NonNull Throwable t) {
                trackingListener.setMessage(t.getMessage());

            }
        });

    }
}
