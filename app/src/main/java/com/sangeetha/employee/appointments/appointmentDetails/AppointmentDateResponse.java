package com.sangeetha.employee.appointments.appointmentDetails;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AppointmentDateResponse {

    @SerializedName("status")
    public boolean status;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public List<AppointmentDateModel> appointmentDateModelList;

    @Override
    public String toString() {
        return "AppointmentDateResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", appointmentDateModelList=" + appointmentDateModelList +
                '}';
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<AppointmentDateModel> getAppointmentDateModelList() {
        return appointmentDateModelList;
    }

    public void setAppointmentDateModelList(List<AppointmentDateModel> appointmentDateModelList) {
        this.appointmentDateModelList = appointmentDateModelList;
    }
}
