package com.sangeetha.employee.servify.model.data

import com.google.gson.annotations.SerializedName

data class CheckReferenceCodeData(
        @SerializedName("manual_question")
        val referenceCodeData: ReferenceCodeData,
        @SerializedName("status")
        val status: Boolean,
        @SerializedName("message")
        val message: String

)