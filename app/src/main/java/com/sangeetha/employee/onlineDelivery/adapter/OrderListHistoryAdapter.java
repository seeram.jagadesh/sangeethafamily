package com.sangeetha.employee.onlineDelivery.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.sangeetha.employee.R;
import com.sangeetha.employee.onlineDelivery.DeliveryOrdersDetailsActivity;
import com.sangeetha.employee.onlineDelivery.OnlineDeliveryOTPActivity;
import com.sangeetha.employee.onlineDelivery.model.DataItem;
import com.sangeetha.employee.onlineDelivery.model.DeliverRatingClick;

import java.util.ArrayList;
import java.util.List;


public class OrderListHistoryAdapter extends RecyclerView.Adapter<OrderListHistoryAdapter.MyViewHolder> implements Filterable {

    private List<DataItem> data;
    private List<DataItem> dataList;
    private Context context;
    private String value, SearchId = "";
    DeliverRatingClick deliveryRating;

    @Override
    public Filter getFilter() {
        return exampleFilter;
    }

    private Filter exampleFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<DataItem> filteredList = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(dataList);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (DataItem item : dataList) {
                    if (item.getOrderUniqueId().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            data.clear();
            data.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    static class MyViewHolder extends RecyclerView.ViewHolder {
        private AppCompatTextView name, orderNo, mobileNo, paymentMode, amount;
        private LinearLayout itemLayout;
        AppCompatButton btnReviewAdd;

        private MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            itemLayout = view.findViewById(R.id.itemLayout);
            orderNo = view.findViewById(R.id.order_no);
            mobileNo = view.findViewById(R.id.mobile_no);
            amount = view.findViewById(R.id.amount);
            btnReviewAdd = view.findViewById(R.id.btnReviewAdd);
            paymentMode = view.findViewById(R.id.payment_mode);

        }
    }


    public OrderListHistoryAdapter(List<DataItem> data, String value, String searchData, DeliverRatingClick ratingClick) {
        this.data = data;
        this.value = value;
        this.SearchId = searchData;
        dataList = new ArrayList<>(data);
        this.deliveryRating = ratingClick;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.online_order_history_item, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        holder.name.setText(data.get(position).getCustomerName());
        holder.orderNo.setText(data.get(position).getOrderUniqueId());
        holder.mobileNo.setText(data.get(position).getCustomerMobileNumber());
        holder.paymentMode.setText(data.get(position).getPaymentType());
        holder.amount.setText(String.format("₹ %s", data.get(position).getOrderAmount()));

        String reviewStatus = data.get(position).getReview_added_status();
        String deliveryStatus = data.get(position).getDelivery_details_added_status();
        String otpVerified = data.get(position).getOtp_verified();

//        if (reviewStatus.equalsIgnoreCase("0") && deliveryStatus.equalsIgnoreCase("1")) {
//            holder.btnReviewAdd.setVisibility(View.VISIBLE);
//
//        } else if (reviewStatus.equalsIgnoreCase("0")
//                && deliveryStatus.equalsIgnoreCase("0")
//                && otpVerified.equalsIgnoreCase("1")) {
//            holder.btnReviewAdd.setText("Add Details");
//            holder.btnReviewAdd.setVisibility(View.VISIBLE);
//        }

        holder.btnReviewAdd.setOnClickListener(v -> {
            if (!holder.btnReviewAdd.getText().toString().equalsIgnoreCase("Add details")) {
                deliveryRating.getRating(data.get(position).getOrderUniqueId(), data.get(position).getOrderAmount());
            } else {
                deliveryRating.getEmpInfo(data.get(position).getOrderUniqueId(), data.get(position).getOrderAmount());
            }
        });
//        if(status.equalsIgnoreCase("0")){
//            holder.btnReviewAdd.setVisibility(View.VISIBLE);
//        }else{
//            holder.btnReviewAdd.setVisibility(View.GONE);
//        }
        holder.itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (reviewStatus.equalsIgnoreCase("0")
                        && deliveryStatus.equalsIgnoreCase("1")) {
                    context.startActivity(new Intent(context, OnlineDeliveryOTPActivity.class)
                            .putExtra("order_id", data.get(position).getOrderUniqueId())
                            .putExtra("info", "rating")
                            .putExtra("amount_paid",data.get(position).getAmountPaid())
                            .putExtra("amount", data.get(position).getOrderAmount()));
                } else if (reviewStatus.equalsIgnoreCase("0")
                        && deliveryStatus.equalsIgnoreCase("0")
                        && otpVerified.equalsIgnoreCase("1")) {
                    context.startActivity(new Intent(context, OnlineDeliveryOTPActivity.class)
                            .putExtra("order_id", data.get(position).getOrderUniqueId()).putExtra("info", "employee")
                            .putExtra("amount_paid",data.get(position).getAmountPaid())
                            .putExtra("amount", data.get(position).getOrderAmount()));
                }else if(otpVerified.equalsIgnoreCase("0")){
                    context.startActivity(new Intent(context, DeliveryOrdersDetailsActivity.class)
                            .putExtra("order_id", data.get(position).getOrderUniqueId())
                            .putExtra("value", value)
                            .putExtra("amount_paid",data.get(position).getAmountPaid()));
                }

            }
        });


    }


    @Override
    public int getItemCount() {
        return data.size();
    }


}