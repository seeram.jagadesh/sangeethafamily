package com.sangeetha.employee.managerDiscount.manager_pojo;

import com.google.gson.annotations.SerializedName;

public class ApproverHtmlItem {

    @SerializedName("emp_name")
    private String empName;

    @SerializedName("emp_id")
    private String empId;

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    @Override
    public String toString() {
        return
                "ApproverHtmlItem{" +
                        "emp_name = '" + empName + '\'' +
                        ",emp_id = '" + empId + '\'' +
                        "}";
    }
}