package com.sangeetha.employee.onlineDelivery;


import com.sangeetha.employee.CommonResponse;
import com.sangeetha.employee.onlineDelivery.model.OrderListResponse;
import com.sangeetha.employee.onlineDelivery.model.ReviewValueResponse;
import com.sangeetha.employee.onlineDelivery.model.orderDetails.OrderDetailsResponse;
import com.sangeetha.employee.onlineDelivery.model.otpVerification.OtpVerification;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface OnlineDeliveryEndPoint {

    @FormUrlEncoded
    @POST("order-list")
    Call<OrderListResponse> getOrders(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("order-delivered-list")
    Call<OrderListResponse> getDeliveredOrders(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("order-details-app")
    Call<OrderDetailsResponse> orderDetails(@Field("user_id") String user_id, @Field("order_unique_id") String order_unique_id);


    @FormUrlEncoded
    @POST("resend-otp-app")
    Call<OtpVerification> generateOtp(@Field("user_id") String user_id, @Field("order_unique_id") String order_unique_id);

    @FormUrlEncoded
    @POST("otp-verification-app")
    Call<OtpVerification> verifyOtp(@Field("user_id") String user_id, @Field("order_unique_id") String order_unique_id, @Field("otp") String otp);


    @FormUrlEncoded
    @POST("verify-order-process-app")
    Call<OtpVerification> verifyOrder(@Field("user_id") String user_id, @Field("order_unique_id") String order_unique_id,
                                      @Field("payment_type") String payment_type, @Field("reference_number") String reference_number,
                                      @Field("attachment_id") String attachment_id, @Field("paid_by") String paidBy);

    @FormUrlEncoded
    @POST("order-review-app")
    Call<CommonResponse> submitOrderReview(@Field("order_unique_id") String order_id,
                                           @Field("rating") String rating, @Field("comments") String comments);

    @GET("order-review-value-app")
    Call<ReviewValueResponse> getValueReviews();

    @POST("delivery-details-app")
    @FormUrlEncoded
    Call<CommonResponse> submitEmpInfo(@Field("order_unique_id") String orderId, @Field("emp_id") String empId,
                               @Field("vehicle_no") String vehicleNo,@Field("distance") String distance);

}
