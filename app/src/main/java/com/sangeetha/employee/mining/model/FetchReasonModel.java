package com.sangeetha.employee.mining.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FetchReasonModel {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("reason_name")
    @Expose
    private String reasonName;
    @SerializedName("type")
    @Expose
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReasonName() {
        return reasonName;
    }

    public void setReasonName(String reasonName) {
        this.reasonName = reasonName;
    }

    @Override
    public String toString() {
        return "FetchReasonModel{" +
                "id='" + id + '\'' +
                ", reasonName='" + reasonName + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
