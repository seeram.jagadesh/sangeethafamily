package com.sangeetha.employee.empLogin.loginModel;

import com.google.gson.annotations.SerializedName;

public class EmpData {

    @SerializedName("user_id")
    private String userId;

    @SerializedName("username")
    private String username;

    @SerializedName("role")
    private String role;

    @SerializedName("controller")
    private String controller;

    @SerializedName("group")
    private String group;

    @SerializedName("type")
    private String type;

    @SerializedName("change_flag")
    private String changeFlag;

    @SerializedName("employee_id")
    private String employeeId;

    @SerializedName("email")
    private String email;

    @SerializedName("emp_id")
    private String empId;

    @SerializedName("employee_name")
    private String empName;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getController() {
        return controller;
    }

    public void setController(String controller) {
        this.controller = controller;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getChangeFlag() {
        return changeFlag;
    }

    public void setChangeFlag(String changeFlag) {
        this.changeFlag = changeFlag;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    @Override
    public String toString() {
        return
                "EmpData{" +
                        "employee_id = '" + employeeId + '\'' +
                        ",email = '" + email + '\'' +
                        ",emp_id = '" + empId + '\'' +
                        "}";
    }
}