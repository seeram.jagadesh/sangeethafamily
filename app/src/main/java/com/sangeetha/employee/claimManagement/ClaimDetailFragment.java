package com.sangeetha.employee.claimManagement;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.fragment.app.Fragment;

import com.sangeetha.employee.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ClaimDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ClaimDetailFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ClaimDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ClaimDetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ClaimDetailFragment newInstance(String param1, String param2) {
        ClaimDetailFragment fragment = new ClaimDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_claim_detail, container, false);
        //customer details///////
        AppCompatTextView claimId = view.findViewById(R.id.claim_id_tv);
        AppCompatTextView claimType = view.findViewById(R.id.claim_type_tv);
        AppCompatTextView customerName = view.findViewById(R.id.name_tv);
        AppCompatTextView customerMobile = view.findViewById(R.id.mob_num_tv);
        AppCompatTextView customerEmail = view.findViewById(R.id.email_tv);
        AppCompatTextView customerDob = view.findViewById(R.id.dob_tv);
        AppCompatTextView anniversaryDate = view.findViewById(R.id.anniversary_date_tv);

        //invoice details/////
        AppCompatTextView invoiceNo = view.findViewById(R.id.invoice_no);
        AppCompatTextView invoiceDate = view.findViewById(R.id.invoice_date);
        AppCompatTextView modelName = view.findViewById(R.id.model_name);
        AppCompatTextView imeiNumber = view.findViewById(R.id.imei_number);
        AppCompatTextView handSet = view.findViewById(R.id.handset_value);
        AppCompatTextView discountValue = view.findViewById(R.id.discount_value);
        AppCompatTextView chashbackValue = view.findViewById(R.id.cashback_value);

        // vas details////
        AppCompatTextView vasEligibility = view.findViewById(R.id.vas_eligibility_tv);
        AppCompatTextView vasEligibilityValue = view.findViewById(R.id.vas_eligibility_value);
        AppCompatTextView vasPack = view.findViewById(R.id.vas_pack_tv);
        AppCompatTextView vasPackValue = view.findViewById(R.id.vas_pack_value);
        AppCompatTextView vasExpire = view.findViewById(R.id.vas_expire_tv);
        AppCompatTextView vasExpireValue = view.findViewById(R.id.vas_expire_value);

        // attachment details////

        LinearLayoutCompat attachmentLayout = view.findViewById(R.id.attachment_layout);
        addAttachment(attachmentLayout);
        view.findViewById(R.id.proceed_claim)
                .setOnClickListener(v -> {
                    ClaimPending claimPending = (ClaimPending) getActivity();
                    claimPending.setMainFragment(ProceedClaimFragment.newInstance("", ""));
                });

        return view;
    }

    public void addAttachment(LinearLayoutCompat layoutCompat) {
        for (int i = 0; i < 5; i++) {
            View v = LayoutInflater.from(getActivity()).inflate(R.layout.attachment_view, layoutCompat, false);

            AppCompatImageView attachmentImage = v.findViewById(R.id.attachment_image);
            AppCompatTextView attachmentText = v.findViewById(R.id.attachment_text);
            AppCompatImageButton attachmentDownload = v.findViewById(R.id.attachment_download);
            attachmentDownload.setTag(String.valueOf(i));
            attachmentDownload.setOnClickListener(v1 -> {
              //  Log.e("download attachment ", "" + v1.getTag());
            });
            layoutCompat.addView(v, i);
        }
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
