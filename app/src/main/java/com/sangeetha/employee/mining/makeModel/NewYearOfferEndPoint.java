package com.sangeetha.employee.mining.makeModel;


import com.sangeetha.employee.managerDiscount.manager_pojo.MakeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelResponse;
import com.sangeetha.employee.mining.response.StoreResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface NewYearOfferEndPoint {

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("api-get-brand")
    Call<MakeResponse> getMake(@Header("sangeetha_token") String token, @Field("product_value") String productId);

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("api-get-model")
    Call<ModelResponse> getModel(@Header("sangeetha_token") String token, @Field("brand_value") String makeId, @Field("product_value") String productId);

    @Headers("Accept: application/json")
    @GET("api-store-list")
    Call<StoreResponse> getStoreList(@Header("sangeetha_token") String token);


}
