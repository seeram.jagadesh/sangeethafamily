package com.sangeetha.employee.cashify

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.sangeetha.employee.R
import com.sangeetha.employee.cashify.dummy.DummyContent
import com.sangeetha.employee.databinding.FragmentVoucherHistoryBinding


/**
 * [RecyclerView.Adapter] that can display a [DummyItem] and makes a call to the
 * specified [OnListFragmentInteractionListener].
 * TODO: Replace the implementation with code for your data type.
 */
class VoucherAdapter(
        private val mValues: List<DummyContent.DummyItem>,
        private val mListener: VoucherHistoryFragment.OnListFragmentInteractionListener?)
    : RecyclerView.Adapter<VoucherAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as DummyContent.DummyItem
            // Notify the active callbacks interface (the activity, if the fragment is attached to
            // one) that an item has been selected.
            mListener?.onListFragmentInteraction(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = FragmentVoucherHistoryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
       // holder.mIdView.text = item.id
       // holder.mContentView.text = item.content

        with(holder.binding.root) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val binding: FragmentVoucherHistoryBinding)
        : RecyclerView.ViewHolder(binding.root) {
       // val mIdView: TextView = mView.item_number
        //val mContentView: TextView = mView.content

        override fun toString(): String {
            return ""
        }
    }
}
