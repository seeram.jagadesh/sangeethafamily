package com.sangeetha.employee.empAudit.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FetchEmployeeResponse {

    @SerializedName("employee_details")
    private List<EmployeeDetailsItem> employeeDetails;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private boolean status;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<EmployeeDetailsItem> getEmployeeDetails() {
        return employeeDetails;
    }

    public void setEmployeeDetails(List<EmployeeDetailsItem> employeeDetails) {
        this.employeeDetails = employeeDetails;
    }
}