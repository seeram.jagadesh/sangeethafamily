package com.sangeetha.employee.managerDiscount.manager_pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class CouponHistoryData implements Parcelable {
// "id": "7",
//         "reference_id": "RNMD000007",
//         "customer_mobile": "9659430301",
//         "model_id": "V11 6GB 64GB Nebula Purple - Vivo",
//         "discount_status": "Waiting for Apporval",
//         "coupon_code": "",
//         "amount": "",
//         "store_submit": "2020-07-12 22:10:55",
//         "coupon_status": ""
    @SerializedName("reference_id")
    private String referenceId;

    @SerializedName("discount_amount")
    private String discountAmount;

    @SerializedName("discount_percentage")
    private String discountPercentage;

    @SerializedName("fromdate")
    private String fromdate;

    @SerializedName("invoice_date")
    private String invoiceDate;

    @SerializedName("store_submit")
    private String storeSubmit;

    @SerializedName("approver_type")
    private String approverType;

    @SerializedName("model_price")
    private String modelPrice;

    @SerializedName("todate")
    private String todate;

    @SerializedName("id")
    private String id;

    @SerializedName("store_id")
    private String storeId;

    @SerializedName("approver")
    private String approver;

    @SerializedName("approval_code")
    private String approvalCode;

    @SerializedName("customer_mobile")
    private String customerMobile;

    @SerializedName("coupon_code")
    private String couponCode;

    @SerializedName("created")
    private String created;

    @SerializedName("invoice_no")
    private String invoiceNo;

    @SerializedName("employee_name")
    private String employeeName;

    @SerializedName("model_id")
    private String modelId;

    @SerializedName("reject_reason")
    private String rejectReason;

    @SerializedName("make_id")
    private String makeId;

    @SerializedName("reject_note")
    private String rejectNote;

    @SerializedName("manage_id")
    private String manageId;

    @SerializedName("internal_code")
    private String internalCode;

    @SerializedName("customer_email")
    private String customerEmail;

    @SerializedName("employee_id")
    private String employeeId;

    @SerializedName("customer_name")
    private String customerName;

    @SerializedName("coupon_status")
    private String status;

    @SerializedName("discount_status")
    private String discountStatus;

    public static final Creator<CouponHistoryData> CREATOR = new Creator<CouponHistoryData>() {
        @Override
        public CouponHistoryData createFromParcel(Parcel in) {
            return new CouponHistoryData(in);
        }

        @Override
        public CouponHistoryData[] newArray(int size) {
            return new CouponHistoryData[size];
        }
    };

    public String getDiscountStatus() {
        return discountStatus;
    }

    public void setDiscountStatus(String discountStatus) {
        this.discountStatus = discountStatus;
    }

    protected CouponHistoryData(Parcel in) {
        referenceId = in.readString();
        discountAmount = in.readString();
        discountPercentage = in.readString();
        fromdate = in.readString();
        invoiceDate = in.readString();
        approverType = in.readString();
        modelPrice = in.readString();
        todate = in.readString();
        id = in.readString();
        storeId = in.readString();
        approver = in.readString();
        approvalCode = in.readString();
        customerMobile = in.readString();
        couponCode = in.readString();
        created = in.readString();
        invoiceNo = in.readString();
        employeeName = in.readString();
        modelId = in.readString();
        rejectReason = in.readString();
        makeId = in.readString();
        rejectNote = in.readString();
        manageId = in.readString();
        internalCode = in.readString();
        customerEmail = in.readString();
        employeeId = in.readString();
        customerName = in.readString();
        status = in.readString();
        discountStatus=in.readString();
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(String discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public String getFromdate() {
        return fromdate;
    }

    public void setFromdate(String fromdate) {
        this.fromdate = fromdate;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getApproverType() {
        return approverType;
    }

    public void setApproverType(String approverType) {
        this.approverType = approverType;
    }

    public String getModelPrice() {
        return modelPrice;
    }

    public void setModelPrice(String modelPrice) {
        this.modelPrice = modelPrice;
    }

    public String getTodate() {
        return todate;
    }

    public void setTodate(String todate) {
        this.todate = todate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getApprover() {
        return approver;
    }

    public void setApprover(String approver) {
        this.approver = approver;
    }

    public String getApprovalCode() {
        return approvalCode;
    }

    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }

    public String getCustomerMobile() {
        return customerMobile;
    }

    public void setCustomerMobile(String customerMobile) {
        this.customerMobile = customerMobile;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getStoreSubmit() {
        return storeSubmit;
    }

    public void setStoreSubmit(String storeSubmit) {
        this.storeSubmit = storeSubmit;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getRejectReason() {
        return rejectReason;
    }

    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }

    public String getMakeId() {
        return makeId;
    }

    public void setMakeId(String makeId) {
        this.makeId = makeId;
    }

    public String getRejectNote() {
        return rejectNote;
    }

    public void setRejectNote(String rejectNote) {
        this.rejectNote = rejectNote;
    }

    public String getManageId() {
        return manageId;
    }

    public void setManageId(String manageId) {
        this.manageId = manageId;
    }

    public String getInternalCode() {
        return internalCode;
    }

    public void setInternalCode(String internalCode) {
        this.internalCode = internalCode;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "CouponHistoryData{" +
                        "reference_id = '" + referenceId + '\'' +
                        ",discount_amount = '" + discountAmount + '\'' +
                        ",discount_percentage = '" + discountPercentage + '\'' +
                        ",fromdate = '" + fromdate + '\'' +
                        ",invoice_date = '" + invoiceDate + '\'' +
                        ",approver_type = '" + approverType + '\'' +
                        ",model_price = '" + modelPrice + '\'' +
                        ",todate = '" + todate + '\'' +
                        ",id = '" + id + '\'' +
                        ",store_id = '" + storeId + '\'' +
                        ",approver = '" + approver + '\'' +
                        ",approval_code = '" + approvalCode + '\'' +
                        ",customer_mobile = '" + customerMobile + '\'' +
                        ",coupon_code = '" + couponCode + '\'' +
                        ",created = '" + created + '\'' +
                        ",invoice_no = '" + invoiceNo + '\'' +
                        ",employee_name = '" + employeeName + '\'' +
                        ",model_id = '" + modelId + '\'' +
                        ",reject_reason = '" + rejectReason + '\'' +
                        ",make_id = '" + makeId + '\'' +
                        ",reject_note = '" + rejectNote + '\'' +
                        ",manage_id = '" + manageId + '\'' +
                        ",internal_code = '" + internalCode + '\'' +
                        ",customer_email = '" + customerEmail + '\'' +
                        ",employee_id = '" + employeeId + '\'' +
                        ",customer_name = '" + customerName + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(referenceId);
        dest.writeString(discountAmount);
        dest.writeString(discountPercentage);
        dest.writeString(fromdate);
        dest.writeString(invoiceDate);
        dest.writeString(approverType);
        dest.writeString(modelPrice);
        dest.writeString(todate);
        dest.writeString(id);
        dest.writeString(storeId);
        dest.writeString(approver);
        dest.writeString(approvalCode);
        dest.writeString(customerMobile);
        dest.writeString(couponCode);
        dest.writeString(created);
        dest.writeString(invoiceNo);
        dest.writeString(employeeName);
        dest.writeString(modelId);
        dest.writeString(rejectReason);
        dest.writeString(makeId);
        dest.writeString(rejectNote);
        dest.writeString(manageId);
        dest.writeString(internalCode);
        dest.writeString(customerEmail);
        dest.writeString(employeeId);
        dest.writeString(customerName);
        dest.writeString(status);
        dest.writeString(discountStatus);
    }
}