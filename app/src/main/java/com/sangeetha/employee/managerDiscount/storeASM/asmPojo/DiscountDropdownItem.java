package com.sangeetha.employee.managerDiscount.storeASM.asmPojo;

public class DiscountDropdownItem {

    String id,name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
