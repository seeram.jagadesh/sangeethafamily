package com.sangeetha.employee.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;

import com.sangeetha.employee.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class CalendarUtils {
    public static String getDateInCustomFormat(Date date, String requiredDateFormat) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(requiredDateFormat);
        return simpleDateFormat.format(date);
    }

    public static boolean isFirstDateLesserThanSecondDate(String first, String second) {
        boolean status = false;
        try {

            SimpleDateFormat formatter = new SimpleDateFormat("dd - MM - yyyy");

            Date date1 = formatter.parse(first);
            Date date2 = formatter.parse(second);

            status = date2.compareTo(date1) <= 0;

        } catch (ParseException e1) {
            e1.printStackTrace();
        }
        return status;
    }

    public static Date getDateFromString(String formatOfDate, String dateString) {
        Date date = null;
        SimpleDateFormat format = new SimpleDateFormat(formatOfDate);
        try {
            date = format.parse(dateString);
            System.out.println(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String getDateInCustomFormat(String outputFormat, String currentDate) {
        DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat(outputFormat, Locale.ENGLISH);
        Date date = null;
        try {
            date = originalFormat.parse(currentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return targetFormat.format(date);
    }
    public static String getDateFormat(String currentDate) {
        //Wed Apr 29 2020 06:18:00
        //EEE MMM d yyyy hh:mm:ss
        DateFormat originalFormat = new SimpleDateFormat("EEE MMM d yyyy hh:mm:ss", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm", Locale.ENGLISH);
        Date date = null;
        try {
            date = originalFormat.parse(currentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return targetFormat.format(date);
    }

    public static String getYearInCustomFormat(String outputFormat, String currentDate) {
        DateFormat originalFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat(outputFormat, Locale.ENGLISH);
        Date date = null;
        try {
            date = originalFormat.parse(currentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return targetFormat.format(date);
    }

    public static String getDateInCustomFormat(String outputFormat, String curDateFormat,
                                               String currentDate) {
        DateFormat originalFormat = new SimpleDateFormat(curDateFormat, Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat(outputFormat, Locale.ENGLISH);
        Date date = null;
        try {
            date = originalFormat.parse(currentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return targetFormat.format(date);
    }

    public static boolean isFirstDateLesserThanSecondDate(String first, String second, String format) {
        boolean status = false;
        try {

            SimpleDateFormat formatter = new SimpleDateFormat(format);

            Date date1 = formatter.parse(first);
            Date date2 = formatter.parse(second);

            status = date2.compareTo(date1) <= 0;

        } catch (ParseException e1) {
            e1.printStackTrace();
         //   Log.e("CALENDAR", "exc: " + e1.toString());
        }
        return status;
    }

    public static String getTodayDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date date = Calendar.getInstance().getTime();
        return formatter.format(date);
    }

    public static String getTodayDate(String format) {
        SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.ENGLISH);
        Date date = Calendar.getInstance().getTime();
        return formatter.format(date);
    }

    public static boolean isDateExistsBetweenTheRange(String enteredDate,
                                                      String startDate, String endDate) {
        boolean isDateExists;
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        try {
            Date cDate = format.parse(enteredDate);
            Date sDate = format.parse(startDate);
            Date eDate = format.parse(endDate);

            int diffInDays = sDate.compareTo(cDate) * cDate.compareTo(eDate);
         //   Log.e("DATE", "Difference: " + diffInDays);

            isDateExists = diffInDays >= 0;
        } catch (Exception e) {
            e.printStackTrace();
            isDateExists = false;
           // Log.e("DATE", "Exc: " + e.toString());
        }
        return isDateExists;
    }

    public static boolean spDate(String enteredDate) {
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        try {
            Date cDate = format.parse(enteredDate);
            Date sDate = format.parse(" 22-12-2018");
            return sDate.after(cDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean cpDate(String enteredDate) {
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        try {
            Date cDate = format.parse(enteredDate);
            Date sDate = format.parse("06-04-2019");
            return sDate.after(cDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean isDateExistsBetweenTheRange(String enteredDate,
                                                      String startDate, String endDate, String formatDate) {
        boolean isDateExists;
        DateFormat format = new SimpleDateFormat(formatDate, Locale.ENGLISH);
        try {
            Date cDate = format.parse(enteredDate);
            Date sDate = format.parse(startDate);
            Date eDate = format.parse(endDate);

            int diffInDays = sDate.compareTo(cDate) * cDate.compareTo(eDate);
         //   Log.e("DATE", "Difference: " + enteredDate);

            int compareDates = eDate.compareTo(cDate);
        //    Log.e("DATE", "COMPARED: " + compareDates);

            isDateExists = compareDates >= 0;
           /* if (cDate.before(eDate) || cDate.equals(eDate)){
                isDateExists = true;
            }else{
                isDateExists = false;
            }*/

        } catch (Exception e) {
            e.printStackTrace();
            isDateExists = false;
          //  Log.e("DATE", "Exc: " + e.toString());
        }
        return isDateExists;
    }

    public static boolean isDatesWithin30DaysRange(String startDate, String endDate) {
        boolean yesThere = false;

        DateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);


        Date firstDate, secDate;

        try {
            firstDate = format.parse(startDate);
            secDate = format.parse(endDate);

            //Comparing dates
            long difference = Math.abs(firstDate.getTime() - secDate.getTime());
            long differenceDates = difference / (24 * 60 * 60 * 1000);

            //Convert long to String
            int dayDifference = (int) differenceDates;

            if (dayDifference <= 30)

                yesThere = true;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return yesThere;
    }

    public static CharSequence getStoreToReachTime(Context context, Date futureDate, Date claimApplied) {
        StringBuilder countdownText = new StringBuilder();

        // Calculate the time between now and the future date.
        long timeRemaining = futureDate.getTime() - claimApplied.getTime();


        // If there is no time between (ie. the date is now or in the past), do nothing
        if (timeRemaining > 0) {
            Resources resources = context.getResources();

            // Calculate the days/hours/minutes/seconds within the time difference.
            //
            // It's important to subtract the value from the total time remaining after each is calculated.
            // For example, if we didn't do this and the time was 25 hours from now,
            // we would get `1 day, 25 hours`.
            int days = (int) TimeUnit.MILLISECONDS.toDays(timeRemaining);
            timeRemaining -= TimeUnit.DAYS.toMillis(days);
            int hours = (int) TimeUnit.MILLISECONDS.toHours(timeRemaining);
            timeRemaining -= TimeUnit.HOURS.toMillis(hours);
            int minutes = (int) TimeUnit.MILLISECONDS.toMinutes(timeRemaining);
            timeRemaining -= TimeUnit.MINUTES.toMillis(minutes);
            int seconds = (int) TimeUnit.MILLISECONDS.toSeconds(timeRemaining);

            // For each time unit, add the quantity string to the output, with a space.
            if (days > 0) {
                countdownText.append(resources.getQuantityString(R.plurals.days, days, days));
                countdownText.append(" ");
            }
            if (days > 0 || hours > 0) {
                countdownText.append(resources.getQuantityString(R.plurals.hours, hours, hours));
                countdownText.append(" ");
            }
            if (days > 0 || hours > 0 || minutes > 0) {
                countdownText.append(resources.getQuantityString(R.plurals.minutes, minutes, minutes));
                countdownText.append(" ");
            }
            if (days > 0 || hours > 0 || minutes > 0 || seconds > 0) {
                countdownText.append(resources.getQuantityString(R.plurals.seconds, seconds, seconds));
                countdownText.append(" ");
            }
        }

        return countdownText.toString();
    }

    public static CharSequence getCountdownText(Context context, Date futureDate) {
        StringBuilder countdownText = new StringBuilder();
        try {


            Date dateNow = new Date();

            // Calculate the time between now and the future date.
            long timeRemaining = futureDate.getTime() - dateNow.getTime();

            // If there is no time between (ie. the date is now or in the past), do nothing
            if (timeRemaining > 0) {
                Resources resources = context.getResources();

                // Calculate the days/hours/minutes/seconds within the time difference.
                //
                // It's important to subtract the value from the total time remaining after each is calculated.
                // For example, if we didn't do this and the time was 25 hours from now,
                // we would get `1 day, 25 hours`.
                int days = (int) TimeUnit.MILLISECONDS.toDays(timeRemaining);
                timeRemaining -= TimeUnit.DAYS.toMillis(days);
                int hours = (int) TimeUnit.MILLISECONDS.toHours(timeRemaining);
                timeRemaining -= TimeUnit.HOURS.toMillis(hours);
                int minutes = (int) TimeUnit.MILLISECONDS.toMinutes(timeRemaining);
                timeRemaining -= TimeUnit.MINUTES.toMillis(minutes);
                int seconds = 0;

                // For each time unit, add the quantity string to the output, with a space.
                if (days > 0) {
                    countdownText.append(resources.getQuantityString(R.plurals.days, days, days));
                    countdownText.append(" ");
                }
                if (days > 0 || hours > 0) {
                    countdownText.append(resources.getQuantityString(R.plurals.hours, hours, hours));
                    countdownText.append(" ");
                }
                if (days > 0 || hours > 0 || minutes > 0) {
                    countdownText.append(resources.getQuantityString(R.plurals.minutes, minutes, minutes));
                    countdownText.append(" ");
                }
//                if (days > 0 || hours > 0 || minutes > 0 || seconds > 0) {
//                    countdownText.append(resources.getQuantityString(R.plurals.seconds, seconds, seconds));
//                    countdownText.append(" ");
//                }
            }

            //   Log.e("apply claim", ""+countdownText.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }


        return countdownText.toString();
    }

    public static String getExpiryDate(int hrs) {
        String expiryDate;
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        String todayDate = format.format(date);
        calendar.set(Calendar.HOUR_OF_DAY, hrs);

        Date expDate = calendar.getTime();
        expiryDate = format.format(expDate);
        return expiryDate;
    }

    public static boolean isDateExpiry(String expiryDate) {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        try {
            Date eDate = format.parse(expiryDate);
            String todayDate = format.format(date);
            Date tDate = format.parse(todayDate);
            return tDate.after(eDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return false;
    }

    public static String getExpiryDate(String startDate, String endDate) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        try {
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            String todayDate = format.format(date);
            Date eDate = format.parse(endDate);
            Date sDate = format.parse(startDate);
            int diffInDays = (int) ((eDate.getTime() - sDate.getTime())
                    / (1000 * 60 * 60 * 24));
            return "" + diffInDays;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean isAnniversaryDate(String selectedDate) {
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        try {
            Date cDate = format.parse(selectedDate);
            Date sDate = format.parse(" 30-05-2019");
            Date bDate = format.parse(" 08-07-2019");
            return cDate.after(sDate) && cDate.before(bDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }


    @SuppressLint("SimpleDateFormat")
    public static String changeDateFormat(String dateStr) {
        String newDate = "";

        Date date = null;
        try {
            date = new SimpleDateFormat("yyyyMMdd").parse(dateStr);
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            newDate = formatter.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return newDate;
    }

}
