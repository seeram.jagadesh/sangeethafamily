package com.sangeetha.employee.empAudit.model;

import com.google.gson.annotations.SerializedName;

public class SearchEmpItem {

    @SerializedName("employee_email")
    private String employeeEmail;

    @SerializedName("employee_mobile")
    private String employeeMobile;

    @SerializedName("employee_id")
    private String employeeId;

    @SerializedName("employee_name")
    private String employeeName;

    @SerializedName("designation")
    private String designation;

    @SerializedName("department")
    private String department;

    public String getEmployeeEmail() {
        return employeeEmail;
    }

    public void setEmployeeEmail(String employeeEmail) {
        this.employeeEmail = employeeEmail;
    }

    public String getEmployeeMobile() {
        return employeeMobile;
    }

    public void setEmployeeMobile(String employeeMobile) {
        this.employeeMobile = employeeMobile;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    @Override
    public String toString() {
        return
                "SearchEmpItem{" +
                        "employee_email = '" + employeeEmail + '\'' +
                        ",employee_mobile = '" + employeeMobile + '\'' +
                        ",employee_id = '" + employeeId + '\'' +
                        ",employee_name = '" + employeeName + '\'' +
                        ",designation = '" + designation + '\'' +
                        ",department = '" + department + '\'' +
                        "}";
    }
}