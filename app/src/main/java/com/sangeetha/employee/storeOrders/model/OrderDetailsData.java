package com.sangeetha.employee.storeOrders.model;

import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import java.util.List;

public class OrderDetailsData {

    @SerializedName("order_history")
    List<OrderHistoryItem> orderHistoryItemList;

    @SerializedName("order_details")
    List<orderDetailsItem> orderDetailsItemList;

    @SerializedName("order_no")
    private String orderNo;

    @SerializedName("order_store_details")
    private OrderStoreDetails orderStoreDetails;

//    @SerializedName("order_shipment")
//    private String orderShip;

//    @SerializedName("order_shipment")
//    private String orderShipment;

    @SerializedName("order_status")
    private String order_status;

    @SerializedName("order_delivery_status")
    private String order_delivery_status;

    @SerializedName("order_summary")
    private OrderSummary orderSummary;


    @Override
    public String toString() {
        return "OrderDetailsData{" +
                "orderHistoryItemList=" + orderHistoryItemList +
                ", orderDetailsItemList=" + orderDetailsItemList +
                ", orderNo='" + orderNo + '\'' +
                ", orderStoreDetails=" + orderStoreDetails +
               // ", orderShipment=" + orderShipment +
                ", order_status='" + order_status + '\'' +
                ", order_delivery_status='" + order_delivery_status + '\'' +
                ", orderSummary=" + orderSummary +
                '}';
    }

//    public String getOrderShipment() {
//        return orderShipment;
//    }
//
//    public void setOrderShipment(String orderShipment) {
//        this.orderShipment = orderShipment;
//    }

    public OrderStoreDetails getOrderStoreDetails() {
        return orderStoreDetails;
    }

    public void setOrderStoreDetails(OrderStoreDetails orderStoreDetails) {
        this.orderStoreDetails = orderStoreDetails;
    }

    public List<OrderHistoryItem> getOrderHistoryItemList() {
        return orderHistoryItemList;
    }

    public void setOrderHistoryItemList(List<OrderHistoryItem> orderHistoryItemList) {
        this.orderHistoryItemList = orderHistoryItemList;
    }

    public List<orderDetailsItem> getOrderDetailsItemList() {
        return orderDetailsItemList;
    }

    public void setOrderDetailsItemList(List<orderDetailsItem> orderDetailsItemList) {
        this.orderDetailsItemList = orderDetailsItemList;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getOrder_delivery_status() {
        return order_delivery_status;
    }

    public void setOrder_delivery_status(String order_delivery_status) {
        this.order_delivery_status = order_delivery_status;
    }

    public OrderSummary getOrderSummary() {
        return orderSummary;
    }

    public void setOrderSummary(OrderSummary orderSummary) {
        this.orderSummary = orderSummary;
    }
}
