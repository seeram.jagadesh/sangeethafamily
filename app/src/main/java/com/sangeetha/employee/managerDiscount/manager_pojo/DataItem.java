package com.sangeetha.employee.managerDiscount.manager_pojo;

import com.google.gson.annotations.SerializedName;

public class DataItem {

    @SerializedName("brand_id")
    private String id;

    @SerializedName("brand_name")
    private String makeName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMakeName() {
        return makeName;
    }

    public void setMakeName(String makeName) {
        this.makeName = makeName;
    }

    @Override
    public String toString() {
        return
                "DataItem{" +
                        "id = '" + id + '\'' +
                        ",make_name = '" + makeName + '\'' +
                        "}";
    }
}