package com.sangeetha.employee;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommonResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("tips_id")
    @Expose
    private String tipsID;

    @SerializedName("data")
    @Expose
    private String messageData;

    @SerializedName("coupon_code")
    @Expose
    private String couponCode;

    @SerializedName("image_id")
    @Expose
    private String imageId;

    @SerializedName("reference_id")
    @Expose
    private String referenceId;

    @SerializedName("apx_coupon_code")
    @Expose
    private String apxCouponCode;

    @SerializedName("employee_name")
    @Expose
    private String employee_name;


    public String getEmployee_name() {
        return employee_name;
    }

    public void setEmployee_name(String employee_name) {
        this.employee_name = employee_name;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getApxCouponCode() {
        return apxCouponCode;
    }

    public void setApxCouponCode(String apxCouponCode) {
        this.apxCouponCode = apxCouponCode;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTipsID() {
        return tipsID;
    }

    public void setTipsID(String tipsID) {
        this.tipsID = tipsID;
    }

    public String getMessageData() {
        return messageData;
    }

    public void setMessageData(String messageData) {
        this.messageData = messageData;
    }
}