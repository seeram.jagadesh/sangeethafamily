package com.sangeetha.employee.servify.model.data


import com.google.gson.annotations.SerializedName

data class SangOffersData(
    @SerializedName("sang_offer_name")
    val offerName: String,
    @SerializedName("sang_offer_value")
    val offerValue: String
)