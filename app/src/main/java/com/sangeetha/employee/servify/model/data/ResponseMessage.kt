package com.sangeetha.employee.servify.model.data


import com.google.gson.annotations.SerializedName

data class ResponseMessage(
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Boolean
)