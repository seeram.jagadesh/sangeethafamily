package com.sangeetha.employee.managerDiscount.manager_pojo;

import com.google.gson.annotations.SerializedName;

public class ModelItem {

    @SerializedName("model_name")
    private String modelName;

    @SerializedName("id")
    private String id;

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return
                "ModelItem{" +
                        "model_name = '" + modelName + '\'' +
                        ",id = '" + id + '\'' +
                        "}";
    }
}