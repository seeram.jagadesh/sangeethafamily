package com.sangeetha.employee.empAudit;


import com.sangeetha.employee.empAudit.model.DataItem;
import com.sangeetha.employee.empAudit.model.DesignationsItem;
import com.sangeetha.employee.empAudit.model.DropdownDetailsItem;
import com.sangeetha.employee.empAudit.model.EmpCategoryResponse;
import com.sangeetha.employee.empAudit.model.SearchEmpItem;
import com.sangeetha.employee.empAudit.model.StartAuditResponse;
import com.sangeetha.employee.empAudit.model.SubCategoryResponse;

import java.util.List;

public interface AuditInteractor {

    void getStoreList(String token);

    void getBrandList(String token);

    void getEmpStatus(String token);

    void getEmployeeDetails(String token,String empId);

    void startAudit(String token,String empId, String storeCode, String latLon);

    void submitAuditReport(String token,String auditId, String empId, String storeCode, String empDetail, String proDetail);

    void getDesignation(String token);

    void getEmpCategory(String token);

    void getSubCategory(String token,String categoryId);

    interface AuditListener {
        void showMessage(String msg);

        void setStoreList(List<DataItem> data);

        void setEmpStatus(List<DropdownDetailsItem> dropdownDetails);

        void submitStore(StartAuditResponse startAuditResponse);

        void setBrandList(List<DataItem> data);

        void auditReportStatus(boolean status);

        void setEmpName(SearchEmpItem empList);

        void empIdError();

        void setDesignation(List<DesignationsItem> designations);

        void setEmpCategory(EmpCategoryResponse empCategory);

        void setSubCategory(SubCategoryResponse subCategory);
    }
}
