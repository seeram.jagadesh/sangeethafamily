package com.sangeetha.employee.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;

public class ValidationUtils {

    private static final String EMAIL_PATTERN_1 = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private static final String EMAIL_PATTERN_2 = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+\\.+[a-z]+";
    private static final boolean YES = true;
    private static final boolean NO = false;

    public static boolean validateEmail(String email) {
        if (!TextUtils.isEmpty(email)) {
            email = removeBlankSpace(email);
            return email.matches(EMAIL_PATTERN_1)
                    || email.matches(EMAIL_PATTERN_2);
        } else {
            //Log.d("SERI_PAR->Error", "edit text object is null");
            return NO;
        }
    }

    private static String removeBlankSpace(String value) {
        value = value.replace(" ", "");
        return value;
    }

    public static boolean isThereInternet(Context context) {

        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        assert cm != null;
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }
}
