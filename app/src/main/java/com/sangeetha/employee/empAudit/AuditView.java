package com.sangeetha.employee.empAudit;


import com.sangeetha.employee.empAudit.model.DataItem;
import com.sangeetha.employee.empAudit.model.StartAuditResponse;

import java.util.List;

public interface AuditView {

    void setStoreList(List<DataItem> data);

    void showMessage(String msg);

    void showProgress();

    void hideProgress();

    void submitStore(StartAuditResponse startAuditResponse);
}
