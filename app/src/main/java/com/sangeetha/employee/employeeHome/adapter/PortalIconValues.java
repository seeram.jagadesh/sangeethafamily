package com.sangeetha.employee.employeeHome.adapter;

public class PortalIconValues {
    String name;
    String path;
    String image;

    public PortalIconValues(String name, String path, String image) {
        this.name = name;
        this.path = path;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
