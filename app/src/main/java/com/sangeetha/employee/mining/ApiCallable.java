package com.sangeetha.employee.mining;

import com.sangeetha.employee.CommonResponse;
import com.sangeetha.employee.forgotPassword.apiResponse.MakeResponse;
import com.sangeetha.employee.forgotPassword.apiResponse.ModelResponse;
import com.sangeetha.employee.mining.response.CategoryMiningResponse;
import com.sangeetha.employee.mining.response.FetchMiningResponse;
import com.sangeetha.employee.mining.response.FetchReasonResponse;
import com.sangeetha.employee.storeOrders.model.OrderListResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiCallable {


    //api-mining-category

    @POST("api-mining-category")
    Call<CategoryMiningResponse> getMiningCategory(@Header("sangeetha_token") String token);

    @POST("api-update-order-status-step-1")
    @FormUrlEncoded
    Call<CommonResponse> updateOrderStatusStep1(@Header("sangeetha_token") String token, @Field("order_id") String orderId, @Field("order_status") String status);

    @POST("api-fetch-order-imei-step-2")
    @FormUrlEncoded
    Call<CommonResponse> updateOrderStatusStep2(@Header("sangeetha_token") String token, @Field("order_id") String orderId, @Field("order_status") String status
            , @Field("invoice_no") String invoice_no, @Field("invoice_date") String invoiceDate, @Field("imei_no") String imei_no);

    @GET("api-order-status-type")
    Call<ResponseBody> getOrderStatusType(@Header("sangeetha_token") String token);

    @GET("api-order-shipment-courier")
    Call<ResponseBody> getShipmentCourier(@Header("sangeetha_token") String token);

    @POST("api-order-shipment-sub-courier")
    @FormUrlEncoded
    Call<ResponseBody> getSubShipmentCourier(@Header("sangeetha_token") String token, @Field("courier_name") String courierName);

    @POST("api-update-order-status-otp")
    @FormUrlEncoded
    Call<CommonResponse> validateOTP(@Header("sangeetha_token") String token, @Field("order_id") String order_id, @Field("customer_otp") String customer_otp);

    @POST("api-update-order-delivery-login")
    @FormUrlEncoded
    Call<CommonResponse> sendDeliveryBoyDetails(@Header("sangeetha_token") String token, @Field("order_id") String order_id);

    @POST("api-update-order-status-otp-resend")
    @FormUrlEncoded
    Call<CommonResponse> resendOTP(@Header("sangeetha_token") String token, @Field("order_id") String order_id);

    @POST("api-order-employee-details")
    @FormUrlEncoded
    Call<CommonResponse> getOrderEmpDetails(@Header("sangeetha_token") String token, @Field("employee_id") String empId);

    @POST("api-update-order-status-delivery")
    @FormUrlEncoded
    Call<CommonResponse> orderDeliver(@Header("sangeetha_token") String token, @Field("order_id") String order_id
            , @Field("order_status") String order_status, @Field("payment_details") String payment_details
            , @Field("reference_number") String reference_number);

    @POST("api-update-order-status-shipped")
    @FormUrlEncoded
    Call<CommonResponse> updateOrderShipStatus(@Header("sangeetha_token") String token,
                                               @Field("order_status") String order_status,
                                               @Field("order_id") String order_id,
                                               @Field("shipping_method") String shipping_method,
                                               @Field("shipped_by") String shipped_by,
                                               @Field("promoter_brand") String promoter_brand,
                                               @Field("promoter_name") String promoter_name,
                                               @Field("promoter_mobile") String promoter_mobile,
                                               @Field("employee_id") String empId,
                                               @Field("reference_number") String reference_number);


    @Multipart
    @POST("api-update-order-status-shipped")
    Call<CommonResponse> submitOrderShipStatus(@Header("sangeetha_token") String token
            , @Part("order_status") RequestBody order_status
            , @Part("order_id") RequestBody order_id
            , @Part("shipping_method") RequestBody shipping_method
            , @Part("shipped_by") RequestBody shipped_by
            , @Part("promoter_brand") RequestBody promoter_brand
            , @Part("promoter_name") RequestBody promoter_name
            , @Part("promoter_mobile") RequestBody promoter_mobile
            , @Part("employee_id") RequestBody employee_id
            , @Part("reference_number") RequestBody reference_number
            , @Part MultipartBody.Part file
    );


    @POST("api-fetch-order-list")
    @FormUrlEncoded
    Call<OrderListResponse> getStoreOrdersList(@Header("sangeetha_token") String token, @Field("type") String type, @Field("store_code") String storeCode);


    @POST("api-fetch-order-details")
    @FormUrlEncoded
    Call<ResponseBody> getOrderDetails(@Header("sangeetha_token") String token, @Field("order_id") String order_id);


    @POST("api-fetch-order-details")
    @FormUrlEncoded
    Call<ResponseBody> orderDetails(@Header("sangeetha_token") String token, @Field("order_id") String order_id);

    @GET("api-branch-wise-details")
    Call<ResponseBody> getBranchWiseDetails(@Header("sangeetha_token") String token);

    @POST("api-mining-reason")
    @FormUrlEncoded
    Call<FetchReasonResponse> getReasonList(@Header("sangeetha_token") String token, @Field("category_id") String categoryId);

    @POST("api-mining-fetch")
    @FormUrlEncoded
    Call<FetchMiningResponse> fetchMiningDetails(@Header("sangeetha_token") String token, @Field("category_type") String categoryId);

    @POST("api-mining-update-status")
    @FormUrlEncoded
    Call<CommonResponse> updateCallBack(@Header("sangeetha_token") String token, @Field("customer_mobile") String cusMobile, @Field("invoice_id") String invoiceId,
                                        @Field("store_code") String StoreCode, @Field("store_id") String storeId, @Field("reason") String reason,
                                        @Field("callback_note") String notes, @Field("callback_date") String date, @Field("callback_time") String time);

    @POST("api-mining-update-status")
    @FormUrlEncoded
    Call<CommonResponse> updateNoResponse(@Header("sangeetha_token") String token, @Field("customer_mobile") String cusMobile, @Field("invoice_id") String invoiceId,
                                          @Field("store_code") String StoreCode, @Field("store_id") String storeId, @Field("reason") String reason,
                                          @Field("noresponse_note") String notes);

    @POST("api-mining-update-status")
    @FormUrlEncoded
    Call<CommonResponse> updateRequestReview(@Header("sangeetha_token") String token, @Field("customer_mobile") String cusMobile, @Field("invoice_id") String invoiceId,
                                             @Field("store_code") String StoreCode, @Field("store_id") String storeId, @Field("reason") String reason,
                                             @Field("review_note") String notes);

    @POST("api-mining-update-status")
    @FormUrlEncoded
    Call<CommonResponse> updateBankMoratarium(@Field("customer_mobile") String cusMobile, @Field("invoice_id") String invoiceId,
                                              @Field("store_code") String StoreCode, @Field("store_id") String storeId, @Field("reason") String reason,
                                              @Field("zestmoney_note") String notes);

    @POST("api-mining-update-status")
    @FormUrlEncoded
    Call<CommonResponse> updateDoNotDisturb(@Header("sangeetha_token") String token, @Field("customer_mobile") String cusMobile, @Field("invoice_id") String invoiceId,
                                            @Field("store_code") String StoreCode, @Field("store_id") String storeId, @Field("reason") String reason);


    @POST("user-form-submit")
    @FormUrlEncoded
    Call<CommonResponse> addAppointment(@Field("type") String type, @Field("name") String name,
                                        @Field("email") String email, @Field("mobile") String mobile, @Field("storeid") String storeid, @Field("model") String model,
                                        @Field("date") String date, @Field("time") String time, @Field("chattype") String chattype, @Field("language") String language
            , @Field("reason") String Reason);


    @POST("api-mining-update-status")
    @FormUrlEncoded
    Call<CommonResponse> updateNeedaPhone(@Header("sangeetha_token") String token, @Field("customer_mobile") String cusMobile, @Field("invoice_id") String invoiceId,
                                          @Field("store_code") String StoreCode, @Field("store_id") String storeId, @Field("reason") String reason,
                                          @Field("review_note") String notes, @Field("trade_make") String make, @Field("trade_model") String model, @Field("purchase_date") String purchaseDate, @Field("purchase_store_code") String storeCode);

    @GET("api-make")
    Call<MakeResponse> getAllMakes();

    @POST("api-model")
    @FormUrlEncoded
    Call<ModelResponse> getModelFromMake(@Field("make_id") String makeID);


    @Multipart
    @POST("upload-image-app")
    Call<CommonResponse> uploadImage(@Part("order_unique_id") RequestBody OrderId, @Part MultipartBody.Part image);

    @Multipart
    @POST("api-call-record-submit")
    Call<CommonResponse> uploadAudio(@Part("employee_id") RequestBody empID,
                                     @Part("store_id") RequestBody storeId,
                                     @Part MultipartBody.Part audioFile,
                                     @Part("mobile_number") RequestBody mobile);

    @GET("login/check_details")
    Call<CommonResponse> checkData();

}
