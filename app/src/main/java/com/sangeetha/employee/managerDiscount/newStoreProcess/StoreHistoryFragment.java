package com.sangeetha.employee.managerDiscount.newStoreProcess;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.sangeetha.employee.R;
import com.sangeetha.employee.managerDiscount.manager_pojo.CouponHistoryData;
import com.sangeetha.employee.managerDiscount.manager_pojo.CouponHistoryResponse;
import com.sangeetha.employee.utils.AppPreference;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link StoreHistoryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class StoreHistoryFragment extends Fragment implements DiscountHistoryView {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    AppPreference appPreference;
    private String mParam2;
    private RecyclerView storeHistory;
    private ManagerDiscountPresenter managerDiscountPresenter;
    private DiscountHistoryAdapter discountHistoryAdapter;
    private ArrayList<CouponHistoryData> couponHistoryData = new ArrayList<>();
    private ProgressBar progressBar;
    private FloatingActionButton sortBtn;
    private OnFragmentInteractionListener mListener;
    private View sheetView;
    private BottomSheetDialog mBottomSheetDialog;

    public StoreHistoryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment StoreHistoryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static StoreHistoryFragment newInstance(String param1, String param2) {
        StoreHistoryFragment fragment = new StoreHistoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public static boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
        //    Log.e("Network Testing", "***Available***");
            return true;
        } else {
           // Log.e("Network Testing", "***Not Available***");
            Toast.makeText(mContext, "Network Not Available", Toast.LENGTH_LONG).show();
            return false;
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_store_history, container, false);
        progressBar = view.findViewById(R.id.history_progressBar);
        storeHistory = view.findViewById(R.id.coupon_history_rv);
        appPreference=new AppPreference(getContext());
        storeHistory.setLayoutManager(new LinearLayoutManager(getActivity()));
        storeHistory.setHasFixedSize(false);
        managerDiscountPresenter = new ManagerDiscountPresenterImp(this);
        if (isNetworkAvailable(getActivity()))
            managerDiscountPresenter.couponHistory(appPreference.getUserToken());
        discountHistoryAdapter = new DiscountHistoryAdapter(couponHistoryData, getActivity());
        storeHistory.setAdapter(discountHistoryAdapter);
        sortBtn = view.findViewById(R.id.sort_f_btn);
        sortBtn.setVisibility(View.INVISIBLE);
        sortBtn.setOnClickListener(v -> {
            showSortBottomSheetDialog();
        });
        return view;
    }

    @Override
    public void setCouponHistory(CouponHistoryResponse couponHistory) {
        if (couponHistory != null) {
            if (couponHistory.isStatus() && couponHistory.getData().size() > 0) {
                couponHistoryData.clear();
                couponHistoryData.addAll(couponHistory.getData());
               // shortByDate(couponHistoryData);
                discountHistoryAdapter.notifyDataSetChanged();
                sortBtn.setVisibility(View.VISIBLE);
            } else {
                showMessage(couponHistory.getMessage());
            }
        }
        progressBar.setVisibility(View.GONE);

    }

    @Override
    public void showMessage(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    private void showSortBottomSheetDialog() {
        if (sheetView == null) {
            sheetView = getLayoutInflater().inflate(R.layout.store_coupon_sort,
                    null);
            mBottomSheetDialog = new BottomSheetDialog(getActivity());
            mBottomSheetDialog.setContentView(sheetView);
            mBottomSheetDialog.setCancelable(true);
        }
        mBottomSheetDialog.show();
        final RadioGroup timeRadioGroup = sheetView.findViewById(R.id.radio_group_for_sort);
        timeRadioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.sort_by_date:
                    if (isNetworkAvailable(getActivity()))
                        managerDiscountPresenter.couponHistory(appPreference.getUserToken());
                    break;
                case R.id.sort_by_waiting:
                    couponHistoryData.addAll(filterData("Waiting for Approval"));
                    discountHistoryAdapter.notifyDataSetChanged();
                    break;
                case R.id.sort_by_not_used:
                    couponHistoryData.addAll(filterData("NOT USED"));
                    discountHistoryAdapter.notifyDataSetChanged();
                    break;
                case R.id.sort_by_reject:
                    couponHistoryData.addAll(filterData("Rejected"));
                    discountHistoryAdapter.notifyDataSetChanged();
                    break;
                case R.id.sort_by_cancel:
                    couponHistoryData.addAll(filterData("Coupon Code Cancel"));
                    discountHistoryAdapter.notifyDataSetChanged();
                    break;

            }
            mBottomSheetDialog.dismiss();
        });


    }

    private ArrayList<CouponHistoryData> filterData(String filterString) {
        ArrayList<CouponHistoryData> filterData = new ArrayList<>();
        ArrayList<CouponHistoryData> data = new ArrayList<>();
        for (CouponHistoryData couponHistory : couponHistoryData) {
            if (couponHistory.getStatus().equalsIgnoreCase(filterString)) {
                filterData.add(couponHistory);
            } else {
                data.add(couponHistory);
            }
        }
       // shortByDate(filterData);

        filterData.addAll(data);
        couponHistoryData.clear();
        return filterData;
    }

    private void shortByDate(ArrayList<CouponHistoryData> filterData) {
        Collections.sort(filterData, new Comparator<CouponHistoryData>() {
            DateFormat f = new SimpleDateFormat("dd-MM-yyyy");

            @Override
            public int compare(CouponHistoryData o1, CouponHistoryData o2) {
                try {
                    return f.parse(o2.getCreated()).compareTo(f.parse(o1.getCreated()));
                } catch (ParseException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        });
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
