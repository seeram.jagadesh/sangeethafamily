package com.sangeetha.employee.empAudit.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StartAuditResponse implements Parcelable {

    public static final Creator<StartAuditResponse> CREATOR = new Creator<StartAuditResponse>() {
        @Override
        public StartAuditResponse createFromParcel(Parcel in) {
            return new StartAuditResponse(in);
        }

        @Override
        public StartAuditResponse[] newArray(int size) {
            return new StartAuditResponse[size];
        }
    };
    @SerializedName("store_code")
    private String storeCode;
    @SerializedName("start_time")
    private String startTime;
    @SerializedName("audit_id")
    private String auditId;
    @SerializedName("employee_details")
    private List<EmployeeDetailsItem> employeeDetails;
    @SerializedName("message")
    private String message;
    @SerializedName("status")
    private boolean status;

    protected StartAuditResponse(Parcel in) {
        storeCode = in.readString();
        startTime = in.readString();
        auditId = in.readString();
        employeeDetails = in.createTypedArrayList(EmployeeDetailsItem.CREATOR);
        message = in.readString();
        status = in.readByte() != 0;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getAuditId() {
        return auditId;
    }

    public void setAuditId(String auditId) {
        this.auditId = auditId;
    }

    public List<EmployeeDetailsItem> getEmployeeDetails() {
        return employeeDetails;
    }

    public void setEmployeeDetails(List<EmployeeDetailsItem> employeeDetails) {
        this.employeeDetails = employeeDetails;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "StartAuditResponse{" +
                        "store_code = '" + storeCode + '\'' +
                        ",start_time = '" + startTime + '\'' +
                        ",audit_id = '" + auditId + '\'' +
                        ",employee_details = '" + employeeDetails + '\'' +
                        ",message = '" + message + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(storeCode);
        dest.writeString(startTime);
        dest.writeString(auditId);
        dest.writeTypedList(employeeDetails);
        dest.writeString(message);
        dest.writeByte((byte) (status ? 1 : 0));
    }
}