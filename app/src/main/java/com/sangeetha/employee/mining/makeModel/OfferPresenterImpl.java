package com.sangeetha.employee.mining.makeModel;

import android.text.TextUtils;

import com.sangeetha.employee.managerDiscount.manager_pojo.MakeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelResponse;
import com.sangeetha.employee.mining.response.StoreResponse;


public class OfferPresenterImpl implements OfferPresenter, OfferInteractor.DiwaliOfferListener {

    private OfferView offerView;
    private OfferInteractor offerInteractor;

    public OfferPresenterImpl(OfferView offerView) {
        this.offerView = offerView;
        offerInteractor = new OfferInteractorImpl(this);
    }

    @Override
    public void getMake(String token) {
        if (offerView != null) {
            offerView.showProgress();
            offerInteractor.getMake(token);
        }

    }

    @Override
    public void getModel(String token,String makeId) {
        if (offerView != null) {
            if (TextUtils.isEmpty(makeId)) {
                offerView.showMessage("Please select model");
            } else {
                offerView.showProgress();
                offerInteractor.getModel(token,makeId);
            }
        }

    }

    @Override
    public void getStores(String token) {
        if (offerView != null) {
            offerView.showProgress();
            offerInteractor.getStoreList(token);
        }
    }


    @Override
    public void showMessage(String msg) {
        if (offerView != null) {
            offerView.hideProgress();
            offerView.showMessage(msg);
        }

    }

    @Override
    public void setMake(MakeResponse makeResponse) {
        if (offerView != null) {
            offerView.hideProgress();
            offerView.setMake(makeResponse);
        }
    }

    @Override
    public void setModel(ModelResponse modelResponse) {
        if (offerView != null) {
            offerView.hideProgress();
            offerView.setModel(modelResponse);
        }
    }

    @Override
    public void setStores(StoreResponse response) {
        if (offerView != null) {
            offerView.hideProgress();
            offerView.setStores(response);
        }
    }


}
