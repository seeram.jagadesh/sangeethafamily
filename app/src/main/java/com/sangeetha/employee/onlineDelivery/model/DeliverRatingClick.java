package com.sangeetha.employee.onlineDelivery.model;

public interface DeliverRatingClick {

    void getRating(String rateId,String amount);

    void getEmpInfo(String orderId,String amount);
}
