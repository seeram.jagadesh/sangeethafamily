package com.sangeetha.employee.calendarView

import java.util.*

interface WeekDaySubtitleInterpreter {
    fun getFormattedWeekDaySubtitle(date: Calendar): String
}
