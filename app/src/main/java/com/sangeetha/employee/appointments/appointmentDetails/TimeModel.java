package com.sangeetha.employee.appointments.appointmentDetails;

import com.google.gson.annotations.SerializedName;

public class TimeModel {

//     "time_slot": "3:00 PM",
//             "time_slot_seconds": 1641461400

    @SerializedName("time_slot")
    public String timeSlot;

    @SerializedName("time_slot_seconds")
    public String timeSlotSeconds;

    public boolean isSelected;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }


    @Override
    public String toString() {
        return "TimeModel{" +
                "timeSlot='" + timeSlot + '\'' +
                ", timeSlotSeconds='" + timeSlotSeconds + '\'' +
                ", isSelected=" + isSelected +
                '}';
    }

    public String getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(String timeSlot) {
        this.timeSlot = timeSlot;
    }

    public String getTimeSlotSeconds() {
        return timeSlotSeconds;
    }

    public void setTimeSlotSeconds(String timeSlotSeconds) {
        this.timeSlotSeconds = timeSlotSeconds;
    }
}
