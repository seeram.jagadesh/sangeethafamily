package com.sangeetha.employee.peopleTracking;

public interface TrackingPresenter {

    void getStoreList();

    void startTracking(String regId, String storeId, String imei, String type, String latValue, String lngValue);

    void stopTracking(String regId, String storeId, String imei, String type, String latValue, String lngValue, String trackingId);


}
