package com.sangeetha.employee.mining;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.sangeetha.employee.mining.response.CalendarResponse;


public class CalendarViewModel extends ViewModel {

    public CalendarViewModel() {

    }

    public LiveData<CalendarResponse> calendarTasks(String storeCode) {
        MutableLiveData<CalendarResponse> responseBodyMutableLiveData;
        CalendarRepository repository = new CalendarRepository();
        responseBodyMutableLiveData = repository.calendarInfo(storeCode);
        return responseBodyMutableLiveData;

    }


}