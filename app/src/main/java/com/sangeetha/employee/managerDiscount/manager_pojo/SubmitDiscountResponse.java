package com.sangeetha.employee.managerDiscount.manager_pojo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubmitDiscountResponse {

    @SerializedName("approver")
    private String approver;

    @SerializedName("approver_details")
    ApproverDetails approverDetails;

//    @SerializedName("approver_html")
//    private List<ApproverHtmlItem> approverHtml;
    @SerializedName("approver_html")
    private String approverHtml;

    @SerializedName("discount_value")
    private int discountValue;

    @SerializedName("final_amount")
    private int finalAmount;

    @SerializedName("discount_percentage")
    private String discountPercentage;

    @SerializedName("status")
    private boolean status;

    @SerializedName("message")
    private String message;

    public ApproverDetails getApproverDetails() {
        return approverDetails;
    }

    public void setApproverDetails(ApproverDetails approverDetails) {
        this.approverDetails = approverDetails;
    }

    public String getApprover() {
        return approver;
    }

    public void setApprover(String approver) {
        this.approver = approver;
    }

    public String getApproverHtml() {
        return approverHtml;
    }

    public void setApproverHtml(String approverHtml) {
        this.approverHtml = approverHtml;
    }

    public int getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(int discountValue) {
        this.discountValue = discountValue;
    }

    public int getFinalAmount() {
        return finalAmount;
    }

    public void setFinalAmount(int finalAmount) {
        this.finalAmount = finalAmount;
    }

    public String getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(String discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return
                "SubmitDiscountResponse{" +
                        "approver = '" + approver + '\'' +
                        ",approver_html = '" + approverHtml + '\'' +
                        ",discount_value = '" + discountValue + '\'' +
                        ",final_amount = '" + finalAmount + '\'' +
                        ",discount_percentage = '" + discountPercentage + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}