package com.sangeetha.employee.onlineDelivery;

import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.sangeetha.employee.R;
import com.sangeetha.employee.empLogin.EmployeeLogin;
import com.sangeetha.employee.employeeHome.EmployeeLanding;
import com.sangeetha.employee.onlineDelivery.fragment.DeliveredOrderFragment;
import com.sangeetha.employee.onlineDelivery.fragment.OrderHistoryFragment;
import com.sangeetha.employee.onlineDelivery.interfaces.IDataCallback;
import com.sangeetha.employee.onlineDelivery.interfaces.IFragmentListener;
import com.sangeetha.employee.onlineDelivery.interfaces.ISearch;
import com.sangeetha.employee.utils.AppPreference;
import com.sangeetha.employee.utils.LanguageUtil;
import com.sangeetha.employee.utils.MyContextWrapper;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class DeliveryOrdersActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener, SearchView.OnQueryTextListener, IFragmentListener {


    AppPreference appPreference;
    ViewPager viewPager;
    TabLayout tabLayout;

    private pagerAdapter adapter;


    ArrayList<ISearch> iSearch = new ArrayList<>();
    private MenuItem searchMenuItem;
    private String newText;
    ArrayList<String> listData = null;

    IDataCallback iDataCallback = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitivity_delivery_orders);
        Toolbar toolbar = findViewById(R.id.toolbar_custom);

        appPreference = new AppPreference(this);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.deliveryboy);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AppCompatImageView logout = findViewById(R.id.logout);
        viewPager = findViewById(R.id.viewpager);
        tabLayout = findViewById(R.id.tabs);
//        setupViewPager(viewPager);


        tabLayout.addTab(tabLayout.newTab().setText("Pending Orders"));
        tabLayout.addTab(tabLayout.newTab().setText("Delivered Orders"));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#ffffff"));
        tabLayout.setTabTextColors(Color.parseColor("#cdcdcd"), Color.parseColor("#ffffff"));
        adapter = new pagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount(), newText);

        tabLayout.setOnTabSelectedListener(this);

        viewPager.setAdapter(adapter);
        logout.setOnClickListener(v -> {
            logoutPopUp();
        });
    }

    private void moveToLoginScreen() {
        Intent loginIntent = new Intent(DeliveryOrdersActivity.this, EmployeeLogin.class);
        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
    }

    private void logoutPopUp() {
        final Dialog dialog = new Dialog(DeliveryOrdersActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.logout_pop_up);
        dialog.setCancelable(true);
        dialog.findViewById(R.id.yes_btn_logout_popup).setOnClickListener(v -> {
            dialog.dismiss();
            appPreference.clearAppPreference();
            moveToLoginScreen();
        });

        dialog.findViewById(R.id.no_btn_logout_popup).setOnClickListener(v -> dialog.dismiss());

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.search, menu);
//        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//        searchMenuItem = menu.findItem(R.id.action_search);
//        SearchView searchView = (SearchView) searchMenuItem.getActionView();
//
//        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
//        searchView.setSubmitButtonEnabled(true);
//        searchView.setOnQueryTextListener(this);
//        return super.onCreateOptionsMenu(menu);
//    }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new OrderHistoryFragment(), "Orders");
        adapter.addFrag(new DeliveredOrderFragment(), "Delivered Orders");

        viewPager.setAdapter(adapter);
    }

    static class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        private ViewPagerAdapter(FragmentManager manager) {
            super(manager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        }

        @NotNull
        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        private void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    static class pagerAdapter extends FragmentStatePagerAdapter {

        private String mSearchTerm;
        //integer to count number of tabs
        int tabCount;

        public pagerAdapter(FragmentManager fm, int tabCount, String searchTerm) {
            super(fm);
            //Initializing tab count
            this.tabCount = tabCount;
            this.mSearchTerm = searchTerm;
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return OrderHistoryFragment.newInstance(mSearchTerm);
                case 1:
                    return DeliveredOrderFragment.newInstance(mSearchTerm);
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return tabCount;
        }

        public void setTextQueryChanged(String newText) {
            Log.e("setTextQueryChanged",newText);
            Log.e("setTextQueryChanged",newText);
            mSearchTerm = newText;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        Locale languageType = LanguageUtil.getLanguageType(newBase);
        super.attachBaseContext(MyContextWrapper.wrap(newBase, languageType));
    }

    public void setiDataCallback(IDataCallback iDataCallback) {
        this.iDataCallback = iDataCallback;
        iDataCallback.onFragmentCreated(listData);
    }
    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        Log.e("onQueryTextChange",newText);
        Log.e("onQueryTextChange",newText);
        this.newText = newText;
        adapter.setTextQueryChanged(newText);
        for (ISearch iSearchLocal : this.iSearch)
            iSearchLocal.onTextQuery(newText);
        return true;
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void addiSearch(ISearch iSearch) {
        this.iSearch.add(iSearch);
    }

    @Override
    public void removeISearch(ISearch iSearch) {
        this.iSearch.remove(iSearch);
    }

}
