package com.sangeetha.employee.managerDiscount.manager_pojo;

import com.google.gson.annotations.SerializedName;

public class PaymentDataItem {

    @SerializedName("id")
    private String id;

    @SerializedName("payment_name")
    private String paymentName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPaymentName() {
        return paymentName;
    }

    public void setPaymentName(String paymentName) {
        this.paymentName = paymentName;
    }
}