package com.sangeetha.employee.mining.makeModel;

import com.sangeetha.employee.managerDiscount.manager_pojo.MakeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelResponse;
import com.sangeetha.employee.mining.response.StoreResponse;

public interface OfferView {

    void showMessage(String msg);

    void showProgress();

    void hideProgress();

    void setMake(MakeResponse managerResponse);

    void setModel(ModelResponse managerResponse);

    void setStores(StoreResponse managerResponse);


}
