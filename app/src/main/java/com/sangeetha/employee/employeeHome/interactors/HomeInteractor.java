package com.sangeetha.employee.employeeHome.interactors;

import org.json.JSONObject;

public interface HomeInteractor {

    void getPortalIcons(String token);

    interface HomeListener {
        void showMessage(String message);

        void setPortalIcons(JSONObject data);
    }
}
