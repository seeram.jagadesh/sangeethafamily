package com.sangeetha.employee.servify.model.data


import com.google.gson.annotations.SerializedName

data class ServifyCouponCode(
    @SerializedName("apx_coupon_code")
    val apxCouponCode: String,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Boolean
)