package com.sangeetha.employee.peopleTracking;


import com.sangeetha.employee.peopleTracking.model.StoreResponse;
import com.sangeetha.employee.peopleTracking.model.TrackingResponse;

public interface TrackingView {

    void setStoreList(StoreResponse storeList);

    void showMessage(String msg);

    void showProgress();

    void hideProgress();

    void setTrackingResponse(TrackingResponse trackingResponse);


}
