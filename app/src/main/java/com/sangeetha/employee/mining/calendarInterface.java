package com.sangeetha.employee.mining;


import com.sangeetha.employee.mining.response.CalendarResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface calendarInterface {


    @POST("api-mining-calendar")
    Call<CalendarResponse> myCalendar(@Header("sangeetha_token") String store_code);


}
