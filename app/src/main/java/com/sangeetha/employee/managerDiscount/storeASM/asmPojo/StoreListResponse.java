package com.sangeetha.employee.managerDiscount.storeASM.asmPojo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StoreListResponse {

    @SerializedName("data")
    private List<StoreListItem> storeList;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private boolean status;

    public List<StoreListItem> getStoreList() {
        return storeList;
    }

    public void setStoreList(List<StoreListItem> storeList) {
        this.storeList = storeList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "StoreListResponse{" +
                        "store_list = '" + storeList + '\'' +
                        ",message = '" + message + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}