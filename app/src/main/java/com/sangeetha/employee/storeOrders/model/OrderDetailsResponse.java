package com.sangeetha.employee.storeOrders.model;

import com.google.gson.annotations.SerializedName;

public class OrderDetailsResponse {

    @SerializedName("status")
    private boolean status;

    @SerializedName("data")
    private OrderDetailsData orderDetailsData;

    @SerializedName("message")
    private String message;


    @Override
    public String toString() {
        return "OrderDetailsResponse{" +
                "status=" + status +
                ", orderDetailsData=" + orderDetailsData +
                ", message='" + message + '\'' +
                '}';
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public OrderDetailsData getOrderDetailsData() {
        return orderDetailsData;
    }

    public void setOrderDetailsData(OrderDetailsData orderDetailsData) {
        this.orderDetailsData = orderDetailsData;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
