package com.sangeetha.employee.appointments.adapter;


import com.sangeetha.employee.appointments.appointmentDetails.TimeModel;

public interface TimeItemClick {

    void getClickedItem(TimeModel item);
}
