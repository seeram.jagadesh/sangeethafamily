package com.sangeetha.employee.callRecorder.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.WorkManager
import androidx.work.OneTimeWorkRequest
import androidx.work.PeriodicWorkRequest
import com.sangeetha.employee.callRecorder.services.ServiceWorker
import timber.log.Timber
import java.util.concurrent.TimeUnit

class ServiceRestartReceiver : BroadcastReceiver() {
//    const val TAG = ServiceRestartReceiver::class.java.simpleName;
    override fun onReceive(context: Context, intent: Intent) {
        Timber.d("onReceive called action = ${intent.action}")
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.action)) {
            val UNIQUE_WORK_NAME = "CUG StartMyServiceViaWorker"
            val workManager = WorkManager.getInstance(context)

            // As per Documentation: The minimum repeat interval that can be defined is 15 minutes
            // (same as the JobScheduler API), but in practice 15 doesn't work. Using 16 here
            val request = PeriodicWorkRequest.Builder(
                ServiceWorker::class.java,
                16,
                TimeUnit.MINUTES
            )
                .build()

            // to schedule a unique work, no matter how many times app is opened i.e. startServiceViaWorker gets called
            // do check for AutoStart permission
            workManager.enqueueUniquePeriodicWork(
                UNIQUE_WORK_NAME,
                ExistingPeriodicWorkPolicy.KEEP,
                request
            )
        } else {
            // We are starting MyService via a worker and not directly because since Android 7
            // (but officially since Lollipop!), any process called by a BroadcastReceiver
            // (only manifest-declared receiver) is run at low priority and hence eventually
            // killed by Android.
            val workManager = WorkManager.getInstance(context)
            val startServiceRequest = OneTimeWorkRequest.Builder(ServiceWorker::class.java)
                .build()
            workManager.enqueue(startServiceRequest)
        }
    }
}