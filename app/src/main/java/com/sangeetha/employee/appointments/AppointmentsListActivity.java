package com.sangeetha.employee.appointments;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sangeetha.employee.R;
import com.sangeetha.employee.SangeethaCareApiClient;
import com.sangeetha.employee.utils.AppPreference;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppointmentsListActivity extends AppCompatActivity {

    List<Datum> datumList = new ArrayList<>();
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    AppointmentRecycler adapter;
    AppPreference appPreference;
AppCompatButton btnSearch;
    AppCompatEditText edtAppointments;


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        appPreference = new AppPreference(this);
        recyclerView = findViewById(R.id.recyclerList);
        btnSearch = findViewById(R.id.btnSearch);
        edtAppointments = findViewById(R.id.edtAppointments);
        fetchData("all", "");

        btnSearch.setOnClickListener(v->{
            if(edtAppointments.getText().toString().equalsIgnoreCase("")){
                showMessage("Please enter appointment ID");
            }else{
                datumList.clear();
                fetchData("all",edtAppointments.getText().toString());
            }
        });
    }

    private void fetchData(String type, String appointmentId) {
        ListInterface listInterface =
                SangeethaCareApiClient.getClient().create(ListInterface.class);
        listInterface.getDataList(appPreference.getToken(), type, appointmentId).enqueue(new Callback<AppointmentsListResponse>() {
            @Override
            public void onResponse(Call<AppointmentsListResponse> call, Response<AppointmentsListResponse> response) {
                if (response.isSuccessful()) {
                    AppointmentsListResponse appointmentsListResponse = response.body();
                    assert appointmentsListResponse != null;
                    if (appointmentsListResponse.getStatus()) {
                        if (appointmentsListResponse.getData().getData().size() > 0) {
                            datumList.addAll(appointmentsListResponse.getData().getData());
                            adapter = new AppointmentRecycler(AppointmentsListActivity.this, datumList);
                            recyclerView.setAdapter(adapter);
                        } else {
                            showMessage("No data available");
                        }
                    } else {
                        showMessage(appointmentsListResponse.getMessage());
                    }
                } else {
                    showMessage("Error while getting list data");
                }

            }

            @Override
            public void onFailure(Call<AppointmentsListResponse> call, Throwable t) {
                showMessage("Failed while getting list date");
            }
        });
    }


    void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

}