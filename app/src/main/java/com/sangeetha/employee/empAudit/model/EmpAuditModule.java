package com.sangeetha.employee.empAudit.model;

import com.google.android.material.textfield.TextInputEditText;

import java.util.List;

public class EmpAuditModule {
    private TextInputEditText empIdEdit;
    private TextInputEditText empName;
    private List<TextInputEditText> empNumbers;


    public TextInputEditText getEmpIdEdit() {
        return empIdEdit;
    }

    public void setEmpIdEdit(TextInputEditText empIdEdit) {
        this.empIdEdit = empIdEdit;
    }

    public TextInputEditText getEmpName() {
        return empName;
    }

    public void setEmpName(TextInputEditText empName) {
        this.empName = empName;
    }

    public List<TextInputEditText> getEmpNumbers() {
        return empNumbers;
    }

    public void setEmpNumbers(List<TextInputEditText> empNumbers) {
        this.empNumbers = empNumbers;
    }
}
