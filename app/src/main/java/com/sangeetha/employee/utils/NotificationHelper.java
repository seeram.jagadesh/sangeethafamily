package com.sangeetha.employee.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.provider.Settings;

import androidx.core.app.NotificationCompat;

import com.sangeetha.employee.R;
import com.sangeetha.employee.SplashScreen;
import com.sangeetha.employee.managerDiscount.storeASM.ApprovalDetailActivity;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class NotificationHelper {
    public static final String NOTIFICATION_CHANNEL1 = "BANNER_NOTIFICATION";
    public static final String NOTIFICATION_CHANNEL2 = "TEXT_NOTIFICATION";
    public static final String NOTIFICATION_WITH_ACTION_CHANNEL = "ACTION_NOTIFICATION";
    private Context context;
    private NotificationManager notificationManager;
    AppPreference appPreference;

    public NotificationHelper(Context context) {
        this.context = context;
        appPreference=new AppPreference(context);
        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    public NotificationChannel getNotificationChannel(String channelID, String channelName,
                                                      int importance, String description) {
        if (Build.VERSION.SDK_INT >= 26) {
            NotificationChannel notificationChannel = new NotificationChannel(channelID,
                    channelName, importance);
            notificationChannel.setDescription(description);

            return notificationChannel;
        } else
            return null;
    }

    private Bitmap getBitmapFromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    public void createNotification(String title, String msg,
                                   NotificationChannel notificationChannel) {
        NotificationCompat.Builder builder;
        if (Build.VERSION.SDK_INT >= 26) {
            builder = new NotificationCompat.Builder(context, notificationChannel.getId());
        } else
            builder = new NotificationCompat.Builder(context);

        Intent resultIntent = new Intent(context, SplashScreen.class);
        // Create the TaskStackBuilder and add the intent, which inflates the back stack
        androidx.core.app.TaskStackBuilder stackBuilder = androidx.core.app.TaskStackBuilder.create(context);
        stackBuilder.addNextIntentWithParentStack(resultIntent);
        // Get the PendingIntent containing the entire back stack
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setSmallIcon(R.drawable.push_notification)
                .setContentTitle(title)
                .setContentText(msg)
                .setAutoCancel(true)
                .setContentIntent(resultPendingIntent)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(msg))

                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI);



        if (Build.VERSION.SDK_INT >= 26) {
            notificationManager.createNotificationChannel(notificationChannel);
        }

        notificationManager.notify(1, builder.build());
    }

    public void showASMNotification(String title, String msg, String refId, NotificationChannel notificationChannel) {
        NotificationCompat.Builder builder;
        int notificationID = NotificationID.getID();
        if (Build.VERSION.SDK_INT >= 26) {
            builder = new NotificationCompat.Builder(context, notificationChannel.getId());
        } else
            builder = new NotificationCompat.Builder(context);

        Intent activityIntent = new Intent(context, ApprovalDetailActivity.class);
        activityIntent.putExtra("reference_id", refId);
        // Create the TaskStackBuilder and add the intent, which inflates the back stack
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntentWithParentStack(activityIntent);
        // Get the PendingIntent containing the entire back stack
        PendingIntent activityPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setSmallIcon(R.drawable.push_notification)
                .setContentTitle(title)
                .setContentText(msg)
                .setContentIntent(activityPendingIntent)
                .setAutoCancel(true)
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setVibrate(new long[]{1000, 1000});
              /*  .setStyle(new NotificationCompat.DecoratedCustomViewStyle())

                .setCustomContentView(expandedView);*/
        // .setCustomBigContentView(expandedView);


        if (Build.VERSION.SDK_INT >= 26) {
            notificationManager.createNotificationChannel(notificationChannel);
        }
        notificationManager.notify(notificationID, builder.build());
    }


}
