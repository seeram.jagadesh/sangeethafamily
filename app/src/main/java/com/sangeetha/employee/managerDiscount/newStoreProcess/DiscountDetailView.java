package com.sangeetha.employee.managerDiscount.newStoreProcess;


import com.sangeetha.employee.managerDiscount.manager_pojo.DiscountDetailsResponse;

public interface DiscountDetailView {
    void showProgress();

    void hideProgress();

    void showCouponMessage(String msg);

    void setDiscountDetails(DiscountDetailsResponse discountDetails);

    void setDiscountInfo(DiscountDetailsResponse discountDetails);

    void couponCancel(boolean status, String msg);

}
