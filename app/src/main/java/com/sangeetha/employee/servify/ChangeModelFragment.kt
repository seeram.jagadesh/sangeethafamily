package com.sangeetha.employee.servify

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.sangeetha.employee.R
import com.sangeetha.employee.databinding.ChangeModelFragmentBinding
import com.sangeetha.employee.employee.servify.viewmodel.ServifyViewModel
import com.sangeetha.employee.managerDiscount.manager_pojo.DataItem
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelItem
import com.sangeetha.employee.servify.model.data.MakeData
import com.sangeetha.employee.servify.model.data.ModelData
import com.sangeetha.employee.servify.model.data.ProductTypeData
import com.sangeetha.employee.servify.model.data.ServifyHistoryData
import com.sangeetha.employee.utils.AppPreference

class ChangeModelFragment : Fragment(), AdapterView.OnItemSelectedListener {

    private lateinit var makeAdapter: ArrayAdapter<String>
    private lateinit var modelAdapter: ArrayAdapter<String>
    private lateinit var productTypeAdapter: ArrayAdapter<String>
    private lateinit var makeDataList: ArrayList<DataItem>
    private lateinit var modelDataList: ArrayList<ModelItem>
    private lateinit var productTypeDataList: ArrayList<ProductTypeData>
    private lateinit var makes: ArrayList<String>
    private lateinit var models: ArrayList<String>
    private lateinit var productType: ArrayList<String>
    private var selectedMake: String = ""
    private var selectedModel: String = ""
    private var selectedProductType: String = ""
    private var storeId = "560"
    private var refId = "560"
    var appPreference: AppPreference? = null
    private var userToken: String = ""
    private lateinit var viewModel: ServifyViewModel
    private lateinit var orderData: ServifyHistoryData

    companion object {
        fun newInstance() = ChangeModelFragment()
    }

    // private lateinit var viewModel: ChangeModelViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = ChangeModelFragmentBinding.inflate(inflater, container, false)
        orderData = requireArguments().getParcelable("orderData")!!
        storeId = orderData.storeId
        refId = orderData.id
        makeDataList = ArrayList()
        modelDataList = ArrayList()
        productTypeDataList = ArrayList()
        makes = ArrayList()
        models = ArrayList()
        productType = ArrayList()
        appPreference = AppPreference(activity)
        makes.add(0, "---- Select Make ----")
        models.add(0, "---- Select Model ----")
        productType.add(0, "---- Select Product Type ----")
        userToken = appPreference?.userToken.toString()
        makeAdapter = context?.let { ArrayAdapter(it, R.layout.custom_spinner_layout, makes) }!!
        modelAdapter = context?.let { ArrayAdapter(it, R.layout.custom_spinner_layout, models) }!!
        productTypeAdapter =
            context?.let { ArrayAdapter(it, R.layout.custom_spinner_layout, productType) }!!
        binding.servifyMakeSpinner.adapter = makeAdapter
        binding.servifyModelSpinner.adapter = modelAdapter
        binding.servifyProductTypeSpinner.adapter = productTypeAdapter
        binding.servifyMakeSpinner.onItemSelectedListener = this
        binding.servifyModelSpinner.onItemSelectedListener = this
        binding.servifyProductTypeSpinner.onItemSelectedListener = this
        binding.submitChange.setOnClickListener {
            if (selectedMake.equals("", true)) {
                showMessage("Select Make")
            } else if (selectedModel.equals("", true)) {
                showMessage("Select Model")
            } else if (selectedProductType.equals("", true)) {
                showMessage("Select Product Type")
            } else {
                viewModel.changeMakeModel(
                    userToken,
                    refId,
                    selectedMake,
                    selectedModel,
                    selectedProductType
                ).observe(viewLifecycleOwner, Observer {
                    if (it.status) {
                        Navigation.findNavController(binding.root).popBackStack()

                    }
                })
            }
            //Navigation.findNavController(rootView).popBackStack()
        }
        return binding.root
    }

    private fun showMessage(msg: String) {
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ServifyViewModel::class.java)
        viewModel.getProductType(userToken).observe(viewLifecycleOwner, Observer {
            if (it.status) {
                productTypeDataList.addAll(it.dataList)
                for (Product in productTypeDataList) {
                    productType.add(Product.productName)
                }
                productTypeAdapter.notifyDataSetChanged()
            }
        })

    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("Not yet implemented")
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        if (position > 0) {
            when (parent?.id) {
                R.id.servifyProductTypeSpinner -> {
                    selectedProductType = productTypeDataList[position - 1].id
                    setMake(productTypeDataList[position - 1].id)
                    makes.clear()
                    makes.add(0, "---- Select Make ----")
                    makeAdapter.notifyDataSetChanged()
                    models.clear()
                    models.add(0, "---- Select Model ----")
                    modelAdapter.notifyDataSetChanged()
                }
                R.id.servifyMakeSpinner -> {
                    selectedMake = makeDataList[position - 1].id
                    setModel(makeDataList[position - 1].id, selectedProductType)
                    models.clear()
                    models.add(0, "---- Select Model ----")
                    modelAdapter.notifyDataSetChanged()
                }
                R.id.servifyModelSpinner -> {
                    selectedModel = modelDataList[position - 1].id
                }
            }
        }
    }

    private fun setMake(productType: String) {
        makes.clear()
        makeDataList.clear()
        makes.add(0, "---- Select Make ----")
        viewModel.getMake(userToken, productType).observe(viewLifecycleOwner, Observer {
            if (it.isStatus) {
                makeDataList.addAll(it.data)
                for (makeData in makeDataList) {
                    makes.add(makeData.makeName)
                }
                makeAdapter.notifyDataSetChanged()
            }
        })
    }

    private fun setModel(makeId: String, productType: String) {
        models.clear()
        modelDataList.clear()
        models.add(0, "---- Select Model ----")
        viewModel.getModel(userToken, makeId, productType).observe(viewLifecycleOwner, Observer {
            if (it.isStatus) {
                modelDataList.addAll(it.data)
                for (model in it.data) {
                    models.add(model.modelName)
                }
                modelAdapter.notifyDataSetChanged()
            }
        })
    }

}
