package com.sangeetha.employee.managerDiscount.storeASM;


import android.app.Fragment;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatSpinner;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sangeetha.employee.R;
import com.sangeetha.employee.employeeHome.adapter.IconSection;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.ApproveDiscountResponse;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.DataItem;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.DiscountDropdownItem;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.DiscountTypeResponse;
import com.sangeetha.employee.utils.AppPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class ApproveDiscountFragment extends androidx.fragment.app.Fragment implements ApprovalDiscountView, AdapterView.OnItemSelectedListener {

    private AppCompatSpinner filterSpinner;
    private StoreASMPresenter storeASMPresenter;
    private ProgressBar progressBar;
    private ArrayList<DataItem> dataItems = new ArrayList<>();
    private ApproveDiscountAdapter approveDiscountAdapter;
    private AppPreference appPreference;
    private String empId;

    List<DiscountDropdownItem> dropdownItemList;

    public ApproveDiscountFragment() {
        // Required empty public constructor
    }


    private static boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            //     Log.e("Network Testing", "***Available***");
            return true;
        } else {
            //    Log.e("Network Testing", "***Not Available***");
            Toast.makeText(mContext, "No network available, please check your WiFi or EmpData connection", Toast.LENGTH_LONG).show();
            return false;
        }

    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);

    }

    @Override
    public void showMessage(String msg) {
        if (getActivity() != null)
            Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();

    }

    @Override
    public void setApproval(ApproveDiscountResponse approval) {

    }

    @Override
    public void setDiscountTypes(DiscountTypeResponse discountTypes) {

        JSONArray jsonArray = new JSONArray(discountTypes.getData());
        for (int f = 0; f < jsonArray.length(); f++) {
            try {
                DiscountDropdownItem discountDropdownItem = new DiscountDropdownItem();
                HashMap<String, String> dataHash = new HashMap<>();
                JSONObject data = new JSONObject(jsonArray.get(f).toString());
                Iterator<String> iter = data.keys();
                while (iter.hasNext()) {
                    String key = iter.next();
                    discountDropdownItem.setId(key);
                    try {
                        Object value = data.get(key);
                        discountDropdownItem.setName(value.toString());
                        dropdownItemList.add(discountDropdownItem);
                    } catch (JSONException e) {
                        Log.e("Error", e.getMessage());
                        showMessage("PI " + e.getMessage());
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        String[] iData = new String[dropdownItemList.size()];
        for (int f = 0; f < dropdownItemList.size(); f++) {
            iData[f] = dropdownItemList.get(f).getName();
        }
        ArrayAdapter data =
                new ArrayAdapter<>(requireContext(), R.layout.custom_spinner_layout, iData);
        filterSpinner.setAdapter(data);

    }

    @Override
    public void setDiscountStatus(ApproveDiscountResponse approveDiscountResponse) {

        dataItems.clear();
        if (approveDiscountResponse.isStatus()) {
            dataItems.addAll(approveDiscountResponse.getData());
            //shortByDate(dataItems);

        } else {
            showMessage(approveDiscountResponse.getMessage());
        }
        approveDiscountAdapter.notifyDataSetChanged();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_approve_discount, container, false);
        appPreference = new AppPreference(getContext());
        progressBar = view.findViewById(R.id.approve_progressBar);
        filterSpinner = view.findViewById(R.id.approve_spinner);

        filterSpinner.setOnItemSelectedListener(this);
        RecyclerView recyclerView = view.findViewById(R.id.approve_discount_rv);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(false);
        approveDiscountAdapter = new ApproveDiscountAdapter(dataItems, getContext());
        recyclerView.setAdapter(approveDiscountAdapter);
        storeASMPresenter = new StoreASMPresenterImpl(this);
        dropdownItemList = new ArrayList<>();

        String userToken = appPreference.getUserToken();

        storeASMPresenter.getDiscounType(userToken);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isNetworkAvailable(getContext())) {
            if (appPreference.getASMData() != null) {
                empId = appPreference.getASMData().getEmployeeId();
                storeASMPresenter.getWaitingApproval(empId);
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//        switch (position) {
//            case 0:
//                if (isNetworkAvailable(Objects.requireNonNull(getContext())))
//                    storeASMPresenter.getWaitingApproval(empId);
//                break;
//            case 1:
//                if (isNetworkAvailable(Objects.requireNonNull(getContext())))
//                    storeASMPresenter.getNotUsedApproval(empId);
//                break;
//            case 2:
//                if (isNetworkAvailable(Objects.requireNonNull(getContext())))
//                    storeASMPresenter.getUsedApproval(empId);
//                break;
//        }

        String dId = dropdownItemList.get(position).getId();
//        showMessage(dId);
        if (isNetworkAvailable(requireContext()))
            storeASMPresenter.getDiscountStatus(appPreference.getUserToken(), dId);
    }

    private void shortByDate(ArrayList<DataItem> filterData) {
        Collections.sort(filterData, new Comparator<DataItem>() {
            DateFormat f = new SimpleDateFormat("dd-MM-yyyy");

            @Override
            public int compare(DataItem o1, DataItem o2) {
                try {
                    return Objects.requireNonNull(f.parse(o2.getCreated())).compareTo(f.parse(o1.getCreated()));
                } catch (ParseException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        });
    }
}
