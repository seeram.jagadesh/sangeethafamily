package com.sangeetha.employee.onlineDelivery.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.sangeetha.employee.R;
import com.sangeetha.employee.onlineDelivery.DeliveryOrdersDetailsActivity;
import com.sangeetha.employee.onlineDelivery.model.DataItem;
import com.sangeetha.employee.onlineDelivery.model.DeliverRatingClick;
import com.sangeetha.employee.registrationForm.model.Data;

import java.util.ArrayList;
import java.util.List;


public class OrderHistoryAdapter extends RecyclerView.Adapter<OrderHistoryAdapter.MyViewHolder> implements Filterable {

    private List<DataItem> data;
    private List<DataItem> dataList;
    private Context context;
    private String value,SearchId="";
    DeliverRatingClick ratingClick;

    @Override
    public Filter getFilter() {
        return exampleFilter;
    }

    private Filter exampleFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<DataItem> filteredList = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(dataList);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (DataItem item : dataList) {
                    if (item.getOrderUniqueId().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            data.clear();
            data.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    static class MyViewHolder extends RecyclerView.ViewHolder {
        private AppCompatTextView name, orderNo, mobileNo, paymentMode, amount;
        private LinearLayout itemLayout;
        AppCompatButton btnReviewAdd;

        private MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            itemLayout = view.findViewById(R.id.itemLayout);
            orderNo = view.findViewById(R.id.order_no);
            mobileNo = view.findViewById(R.id.mobile_no);
            amount = view.findViewById(R.id.amount);
            btnReviewAdd = view.findViewById(R.id.btnReviewAdd);
            paymentMode = view.findViewById(R.id.payment_mode);

        }
    }


    public OrderHistoryAdapter(List<DataItem> data, String value, String searchData, DeliverRatingClick deliverRatingClick) {
        this.data = data;
        this.value = value;
        this.SearchId=searchData;
        this.ratingClick=deliverRatingClick;
        dataList = new ArrayList<>(data);

    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.online_order_history_item, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        holder.name.setText(data.get(position).getCustomerName());
        holder.orderNo.setText(data.get(position).getOrderUniqueId());
        holder.mobileNo.setText(data.get(position).getCustomerMobileNumber());
        holder.paymentMode.setText(data.get(position).getPaymentType());
        holder.amount.setText(String.format("₹ %s", data.get(position).getOrderAmount()));

        String status=data.get(position).getReview_added_status();
//        if(status.equalsIgnoreCase("0")){
//            holder.btnReviewAdd.setVisibility(View.VISIBLE);
//        }else{
//            holder.btnReviewAdd.setVisibility(View.GONE);
//        }

        holder.btnReviewAdd.setOnClickListener(v->{
            ratingClick.getRating(data.get(position).getOrderUniqueId(),data.get(position).getOrderAmount());
        });
        holder.itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, DeliveryOrdersDetailsActivity.class).putExtra("order_id", data.get(position).getOrderUniqueId()).putExtra("value", value));
            }
        });


    }


    @Override
    public int getItemCount() {
        return data.size();
    }


}