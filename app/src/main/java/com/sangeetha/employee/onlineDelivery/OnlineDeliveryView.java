package com.sangeetha.employee.onlineDelivery;


import com.sangeetha.employee.onlineDelivery.model.OrderListResponse;
import com.sangeetha.employee.onlineDelivery.model.orderDetails.OrderDetailsResponse;

public interface OnlineDeliveryView {

    void showMessage(String msg);

    void showProgress();

    void hideProgress();

    void getOrders(OrderListResponse orderListResponse);

    void getDeliveredOrders(OrderListResponse orderListResponse);

    void orderDetails(OrderDetailsResponse orderDetailsResponse);



}
