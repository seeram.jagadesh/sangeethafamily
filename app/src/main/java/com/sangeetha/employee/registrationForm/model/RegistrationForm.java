package com.sangeetha.employee.registrationForm.model;

import java.io.Serializable;

public class RegistrationForm implements Serializable {
    private String name, mob, email, dob, anniversary, imeiNum, invoiceNum,
            invoiceTotal, pwd, gender, make, model, alternateNum;


    public RegistrationForm(String name, String mob, String email, String dob, String anniversary,
                            String imeiNum, String invoiceNum, String invoiceTotal, String pwd
            , String alternateNum) {
        this.name = name;
        this.mob = mob;
        this.email = email;
        this.dob = dob;
        this.anniversary = anniversary;
        this.imeiNum = imeiNum;
        this.invoiceNum = invoiceNum;
        this.invoiceTotal = invoiceTotal;
        this.pwd = pwd;
        this.alternateNum = alternateNum;
    }

    public String getAlternateNum() {
        return alternateNum;
    }

    public void setAlternateNum(String alternateNum) {
        this.alternateNum = alternateNum;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMob() {
        return mob;
    }

    public void setMob(String mob) {
        this.mob = mob;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getAnniversary() {
        return anniversary;
    }

    public void setAnniversary(String anniversary) {
        this.anniversary = anniversary;
    }

    public String getImeiNum() {
        return imeiNum;
    }

    public void setImeiNum(String imeiNum) {
        this.imeiNum = imeiNum;
    }

    public String getInvoiceNum() {
        return invoiceNum;
    }

    public void setInvoiceNum(String invoiceNum) {
        this.invoiceNum = invoiceNum;
    }

    public String getInvoiceTotal() {
        return invoiceTotal;
    }

    public void setInvoiceTotal(String invoiceTotal) {
        this.invoiceTotal = invoiceTotal;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }
}
