package com.sangeetha.employee.mining.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sangeetha.employee.mining.model.FetchReasonModel;

import java.util.List;

public class FetchReasonResponse {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<FetchReasonModel> data;

    @Override
    public String toString() {
        return "FetchReasonResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<FetchReasonModel> getData() {
        return data;
    }

    public void setData(List<FetchReasonModel> data) {
        this.data = data;
    }
}
