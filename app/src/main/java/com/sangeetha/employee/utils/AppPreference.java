package com.sangeetha.employee.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.LocaleList;
import android.util.DisplayMetrics;

import com.google.gson.Gson;
import com.sangeetha.employee.empLogin.loginModel.ASMData;
import com.sangeetha.employee.empLogin.loginModel.EmpData;
import com.sangeetha.employee.empLogin.loginModel.OnlineDeliveryData;
import com.sangeetha.employee.empLogin.loginModel.StoreData;

import java.util.Locale;

public class AppPreference {
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;
    private static final String KEY_LANGUAGE = "language";
    private static Context mContext;
    private Context context;

    public AppPreference(Context cxt) {
        sharedPreferences = getAppPreference(context);
        this.context = cxt;
    }

    private static SharedPreferences getAppPreference(Context context) {
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(AppConstants.APP_PREFERENCE, Context.MODE_PRIVATE);
        }
        return sharedPreferences;
    }

    public boolean isUserLoggedIn() {
        return sharedPreferences.getBoolean("IS_USER_LOGGED_IN", false);
    }


    public void setUserLoggedIn(boolean userLoggedIn) {
        editor = sharedPreferences.edit();
        editor.putBoolean("IS_USER_LOGGED_IN", userLoggedIn);
        editor.apply();
    }

    public String getCouponCode() {
        return sharedPreferences.getString("coupon_code", "");
    }

    public void saveEmpData(EmpData loginData) {
        editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String loginDataFromGson = gson.toJson(loginData);
        editor.putString("EMP_DATA", loginDataFromGson);
        editor.apply();
    }


    public EmpData getEmpData() {
        return new Gson().fromJson(sharedPreferences.getString("EMP_DATA", null), EmpData.class);
    }

    public void saveStoreData(StoreData loginData) {
        editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String loginDataFromGson = gson.toJson(loginData);
        editor.putString("STORE_DATA", loginDataFromGson);
        editor.apply();
    }


    public StoreData getStoreData() {
        return new Gson().fromJson(sharedPreferences.getString("STORE_DATA", null), StoreData.class);
    }

    public void saveASMData(ASMData loginData) {
        editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String loginDataFromGson = gson.toJson(loginData);
        editor.putString("ASM_DATA", loginDataFromGson);
        editor.apply();
    }


    public ASMData getASMData() {
        return new Gson().fromJson(sharedPreferences.getString("ASM_DATA", null), ASMData.class);
    }


    public void saveOnlineDeliveryData(OnlineDeliveryData onlineDeliveryData) {
        editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String loginDataFromGson = gson.toJson(onlineDeliveryData);
        editor.putString("ONLINE_DELIVERY_DATA", loginDataFromGson);
        editor.apply();
    }

    public OnlineDeliveryData getOnlineDeliveryData() {
        return new Gson().fromJson(sharedPreferences.getString("ONLINE_DELIVERY_DATA", null), OnlineDeliveryData.class);
    }

    public void clearAppPreference() {
        editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

    public static void setLanguage(String language, Context con) {
        mContext = con;

        sharedPreferences = con.getSharedPreferences(AppConstants.APP_PREFERENCE, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(KEY_LANGUAGE, language);
        edit.apply();
        setLocale(language);
    }

    public static String getLanguage(Context context) {
        sharedPreferences = context.getSharedPreferences(AppConstants.APP_PREFERENCE, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_LANGUAGE, "en");
    }

    private static void setLocale(String localeName) {
        Locale myLocale = new Locale(localeName);
        Locale.setDefault(myLocale);
        Resources res = mContext.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration configuration = res.getConfiguration();

        // configuration.locale = myLocale;
        // res.updateConfiguration(configuration, dm);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            configuration.setLocale(myLocale);
            LocaleList localeList = new LocaleList(myLocale);
            LocaleList.setDefault(localeList);
            configuration.setLocales(localeList);
            mContext = mContext.createConfigurationContext(configuration);

        } else {
            configuration.setLocale(myLocale);
            mContext = mContext.createConfigurationContext(configuration);
            res.updateConfiguration(configuration, dm);

        }
    }

    public void saveToken(String token) {
        editor = sharedPreferences.edit();
        editor.putString("TOKEN", token);
        editor.apply();
    }

    public String getToken() {
        return sharedPreferences.getString("TOKEN", null);
    }

    public void setUserToken(String token) {
        editor = sharedPreferences.edit();
        editor.putString("uToken", token);
        editor.apply();
    }

    public String getUserToken() {
        return sharedPreferences.getString("uToken", null);
    }
    public void setTitle(String title) {
        editor = sharedPreferences.edit();
        editor.putString("Title", title);
        editor.apply();
    }

    public String getTitle() {
        return sharedPreferences.getString("Title", null);
    }

    public void clearToken() {
        editor = sharedPreferences.edit();
        editor.remove("TOKEN");
        editor.apply();
    }


    public long getOtpTimeRemaining() {
        return sharedPreferences.getLong("OTP_TIME_REMAINING", 0);
    }

    public void saveOtpDuration(long time) {
        editor = sharedPreferences.edit();
        editor.putLong("OTP_DURATION", time);
        editor.apply();
    }

    public long getOtpDuration() {
        return sharedPreferences.getLong("OTP_DURATION", 0);
    }

    public boolean isOtpTimerRunning() {
        return sharedPreferences.getBoolean("IS_OTP_TIMER_RUNNING", false);
    }

    public void setOtpTimerRunning(boolean status) {
        editor = sharedPreferences.edit();
        editor.putBoolean("IS_OTP_TIMER_RUNNING", status);
        editor.apply();
    }

    public void saveOtpTimeRemaining(long time) {
        editor = sharedPreferences.edit();
        editor.putLong("OTP_TIME_REMAINING", time);
        editor.apply();
    }

}
