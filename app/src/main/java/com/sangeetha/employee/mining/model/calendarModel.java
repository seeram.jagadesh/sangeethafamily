package com.sangeetha.employee.mining.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class calendarModel {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("start")
    @Expose
    private String startDate;
    @SerializedName("end")
    @Expose
    private String endDate;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
