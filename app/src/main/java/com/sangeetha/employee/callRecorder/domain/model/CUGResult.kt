package com.sangeetha.employee.callRecorder.domain.model

sealed class CUGResult<out T> {
    class Success<T>(val response: T, val responseCode: Int) : CUGResult<T>()
    class Error(val message: String) : CUGResult<Nothing>()
}