package com.sangeetha.employee.empAudit.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EmpCategoryResponse {

    @SerializedName("data")
    private List<EmpCategoryDataItem> empCategoryData;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private boolean status;

    public List<EmpCategoryDataItem> getEmpCategoryData() {
        return empCategoryData;
    }

    public void setEmpCategoryData(List<EmpCategoryDataItem> empCategoryData) {
        this.empCategoryData = empCategoryData;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "EmpCategoryResponse{" +
                        "empCategoryData = '" + empCategoryData + '\'' +
                        ",message = '" + message + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}