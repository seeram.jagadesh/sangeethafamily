package com.sangeetha.employee.callRecorder;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.sangeetha.employee.R;
import com.sangeetha.employee.utils.FileUtils;

import java.io.File;

import androidx.appcompat.app.AppCompatActivity;
import cafe.adriel.androidaudioconverter.AndroidAudioConverter;
import cafe.adriel.androidaudioconverter.callback.IConvertCallback;
import cafe.adriel.androidaudioconverter.model.AudioFormat;

public class AudioConverterMainActivity extends AppCompatActivity {

    private static final int PICKFILE_REQUEST_CODE = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_audio_converter_main);

//        if (getSupportActionBar() != null) {
//            getSupportActionBar().setBackgroundDrawable(
//                    new ColorDrawable(getResources().getColor(R.color.colorPrimaryDark)));
//        }

//        Util.requestPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
//        Util.requestPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    public void convertAudio(View v) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        startActivityForResult(intent, PICKFILE_REQUEST_CODE);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("MainActivity", "onActivityResult ");
        if (requestCode == PICKFILE_REQUEST_CODE) {
            Log.d("MainActivity", "onActivityResult result ok");
            if (resultCode == Activity.RESULT_OK) {
                /**
                 *  Update with a valid audio file!
                 *  Supported formats: {@link AndroidAudioConverter.AudioFormat}
                 */
                String fileUri = data.getDataString();
                Log.d("MainActivity", "onActivityResult Result_OK fileUri = " + fileUri);
                String filePath = FileUtils.getRealPath(this, data.getData());
                File wavFile = new File(filePath);
                Log.d("MainActivity", "onActivityResult Result_OK wavFile = " + filePath + "   File exists : " + wavFile.exists());
                IConvertCallback callback = new IConvertCallback() {
                    @Override
                    public void onSuccess(File convertedFile) {
                        Log.d("MainActivity", "onSuccess : filePath = " + convertedFile.getAbsolutePath());
                        Toast.makeText(getApplicationContext(), "SUCCESS: " + convertedFile.getPath(), Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Exception error) {
                        Log.e("MainActivity", "onFailure", error);
                        Toast.makeText(getApplicationContext(), "ERROR: " + error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                };
                Toast.makeText(this, "Converting audio file...", Toast.LENGTH_SHORT).show();
                AndroidAudioConverter.with(this)
                        .setFile(wavFile)
                        .setFormat(AudioFormat.MP3)
                        .setCallback(callback)
                        .convert();
            }
        }
    }
}