package com.sangeetha.employee.callRecorder.domain.room

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.sangeetha.employee.callRecorder.domain.room.model.CUGMessage

@Dao
interface CUGDao {
    @Query("SELECT * FROM CUGMessage")
    fun pagingSource(): PagingSource<Int, CUGMessage>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(cugMessage: CUGMessage)
}