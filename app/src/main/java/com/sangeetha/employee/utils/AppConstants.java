package com.sangeetha.employee.utils;

import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.widget.Toast;

import java.sql.Time;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import okhttp3.logging.HttpLoggingInterceptor;

import static android.content.Context.ACTIVITY_SERVICE;

/**
 * Created by skilledanswers on 3/5/18.
 */

public class AppConstants {

    public static final String TOPIC_NAME = "Sangeetha_Cashback_Redemption";

    public static final String GetInvoice = "https://exclusife.com/getinvoice";

    public static final String GENERAL_TOPIC_NAME = "General_notification";

    static final String APP_PREFERENCE = "SANGEETHA_CARE_PREFERENCE";

    public static final String Error_404 = "https://www.sangeethamobiles.com/assets/img/404-error.png";

    public static final String pinelabsPaymentMode = "1,3,4,7,10,11";

    //DP VALIDITY
    public static final String[] dpValidity = new String[]{
            "3 Months",
            "6 Months",
            "1 Year"
    };

    //PP VALIDITY
    public static final String[] ppValidity = new String[]{
            "3 Months"
    };
    //TP VALIDITY
    public static final String[] tpValidity = new String[]{
            "12 Months"
    };
    public static final String[] abValidity = new String[]{
            "12 Months"
    };

    public static final String[] cpValidity = new String[]{
            "SP-6M DP-12M AB-12M", "SP-6M DP-12M AB-24M"
    };
    public static final String[] spValidity = new String[]{
            "6 Months"
    };

    public static HttpLoggingInterceptor.Level LoggingCode = HttpLoggingInterceptor.Level.BODY;  //BODY -Prints Body Tag of APIs


    public static String zapStoreBackEndBaseUrl = "http://develop.sangeethamobiles.net/";
//    public static String zapStoreBackEndBaseUrl = "https://connect.sangeethamobiles.net/";


//    public static String  SangeethaInfoServer = "https://develop.sangeethamobiles.net/";
//    public static String developAPI = "https://api.sangeethamobiles.net/";
//    public static String claimAPIURL = "https://api.hideandshare.com/";

    public static String adminBackEndBaseUrl = "https://admin.sangeethamobiles.com/";
    public static String sangeetain = "https://www.sangeethamobiles.com/";

    public static final String VAS_TERMS_AND_CONDITIONS = "api-vas-terms-condition";

    public static final String onlineDelivery = "https://admin.sangeethamobiles.com/";

    private static String transText;


    private static String RecorderFilePath = "/Sangeetha-calls/";
    private static String RecorderFileName = "Sangeetha-recorder";

    public static final String TAG = "Sangeetha Mobiles";

    public static final String FILE_NAME_PATTERN = "^[\\d]{14}_[_\\d]*\\..+$";

    public static final int STATE_OUTGOING_NUMBER = 6;
    public static final int STATE_INCOMING_NUMBER = 1;
    public static final int STATE_CALL_START = 2;
    public static final int STATE_CALL_END = 3;
    public static final int RECORDING_ENABLED = 4;
    public static final int RECORDING_DISABLED = 5;
    private static Calendar cal = Calendar.getInstance();

    public static String getDate() {
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        int day = cal.get(Calendar.DATE);

        //   Log.d(TAG, "Date " + date);
        return day + "_" + month + "_" + year;
    }


    public static String getTIme() {
        String am_pm = "";
        int sec = cal.get(Calendar.SECOND);
        int min = cal.get(Calendar.MINUTE);
        int hr = cal.get(Calendar.HOUR);
        int amPm = cal.get(Calendar.AM_PM);
        if (amPm == 1)
            am_pm = "PM";
        else am_pm = "AM";

        //  Log.d(TAG, "Time :- " + time);
        return hr + ":" + min + ":" + sec + " " + am_pm;
    }

    public static String getDateTime() {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        int day = cal.get(Calendar.DATE);
        String date = day + "_" + month + "_" + year;
        String am_pm = "";
        int sec = cal.get(Calendar.SECOND);
        int min = cal.get(Calendar.MINUTE);
        int hr = cal.get(Calendar.HOUR);
        int amPm = cal.get(Calendar.AM_PM);
        if (amPm == 1)
            am_pm = "PM";
        else am_pm = "AM";

        String time = hr + ":" + min + ":" + sec + " " + am_pm;

        String DateTime = date + "_" + time;
        DateFormat readFormat = new SimpleDateFormat("dd_MM_yyyy_hh:mm:ss aa");
        DateFormat writeFormat = new SimpleDateFormat("dd-MM-yyyy_HH:mm:ss");
        Date date2 = null;
        try {
            date2 = readFormat.parse(DateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String formattedDate = "";
        formattedDate = writeFormat.format(date2);

        return formattedDate;
    }


    //coi1920for10  1222020
    public static String getAcademicYear(String currentYear) {
        String monthFormat = CalendarUtils.getYearInCustomFormat("MM", currentYear);
        String yearFormat = CalendarUtils.getYearInCustomFormat("yyyy", currentYear);
//        Log.e("Format", monthFormat);
//        Log.e("Format", yearFormat);
        String year = yearFormat.substring(yearFormat.length() - 4);
        String lastTwo = year.substring(year.length() - 2);
        int c = Integer.parseInt(lastTwo);
        int cc = 0;
        String getY = "";
        if (Integer.parseInt(monthFormat) > 3) {
            cc = c + 1;
            getY = String.valueOf(c) + cc;
        } else {
            cc = c - 1;
            getY = String.valueOf(cc) + c;
        }
        return getY;
    }


    public static Drawable setButtonbg(Drawable layout, int color, int mulcolor, float rad) {
        GradientDrawable changeBG = (GradientDrawable) layout.mutate();
        changeBG.setCornerRadius(rad);
        ColorFilter filter = new LightingColorFilter(color, mulcolor);
        changeBG.setColorFilter(filter);
        return changeBG;
    }


    public static Drawable setStroke(Drawable layout, int color) {
        GradientDrawable changeBG = (GradientDrawable) layout.mutate();
        changeBG.setCornerRadius(20);
        changeBG.setStroke(AppConstants.convertDpToPx(2), color);
        return changeBG;
    }


    private static int convertDpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static double mapValueFromRangeToRange(double value, double fromLow, double fromHigh, double toLow, double toHigh) {
        return toLow + ((value - fromLow) / (fromHigh - fromLow) * (toHigh - toLow));
    }

    public static double clamp(double value, double low, double high) {
        return Math.min(Math.max(value, low), high);
    }

    static ProgressDialog progressDialog;

    public static void showProgress(Context cxt, String msg) {
        if (progressDialog != null)
            progressDialog = null;

        progressDialog = new ProgressDialog(cxt);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage(msg);
        progressDialog.show();
    }

    public static void hideProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    public static void showMessage(Context cxt, String msg) {
        Toast.makeText(cxt, msg, Toast.LENGTH_SHORT).show();
    }

    public static String getTime(int hr, int min) {
        Time tme = new Time(hr, min, 0);//seconds by default set to zero
        Format formatter;
        formatter = new SimpleDateFormat("h:mm a");
        return formatter.format(tme);
    }

    public static boolean checkServiceRunning(Context cxt, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) cxt.getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
