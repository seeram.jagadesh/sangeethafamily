package com.sangeetha.employee.storeOrders.model;

import com.google.gson.annotations.SerializedName;

public class OrderShipment {

    @SerializedName("id")
    private String id;

    @SerializedName("order_id")
    private String orderId;

    @SerializedName("order_unique_id")
    private String order_unique_id;

    @SerializedName("shippment_type")
    private String shippment_type;


    @SerializedName("courier_name")
    private String courier_name;

    @SerializedName("shippment_person")
    private String shippment_person;

    @SerializedName("employee_id")
    private String employee_id;

    @SerializedName("employee_name")
    private String employee_name;

    @SerializedName("employee_mobile")
    private String employee_mobile;

    @SerializedName("brand")
    private String brand;

    @SerializedName("promoter_name")
    private String promoter_name;

    @SerializedName("promoter_mobile")
    private String promoter_mobile;

    @SerializedName("waybill_no")
    private String waybill_no;

    @SerializedName("awn_no")
    private String awn_no;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrder_unique_id() {
        return order_unique_id;
    }

    public void setOrder_unique_id(String order_unique_id) {
        this.order_unique_id = order_unique_id;
    }

    public String getShippment_type() {
        return shippment_type;
    }

    public void setShippment_type(String shippment_type) {
        this.shippment_type = shippment_type;
    }

    public String getCourier_name() {
        return courier_name;
    }

    public void setCourier_name(String courier_name) {
        this.courier_name = courier_name;
    }

    public String getShippment_person() {
        return shippment_person;
    }

    public void setShippment_person(String shippment_person) {
        this.shippment_person = shippment_person;
    }

    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }

    public String getEmployee_name() {
        return employee_name;
    }

    public void setEmployee_name(String employee_name) {
        this.employee_name = employee_name;
    }

    public String getEmployee_mobile() {
        return employee_mobile;
    }

    public void setEmployee_mobile(String employee_mobile) {
        this.employee_mobile = employee_mobile;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getPromoter_name() {
        return promoter_name;
    }

    public void setPromoter_name(String promoter_name) {
        this.promoter_name = promoter_name;
    }

    public String getPromoter_mobile() {
        return promoter_mobile;
    }

    public void setPromoter_mobile(String promoter_mobile) {
        this.promoter_mobile = promoter_mobile;
    }

    public String getWaybill_no() {
        return waybill_no;
    }

    public void setWaybill_no(String waybill_no) {
        this.waybill_no = waybill_no;
    }

    public String getAwn_no() {
        return awn_no;
    }

    public void setAwn_no(String awn_no) {
        this.awn_no = awn_no;
    }
}

