package com.sangeetha.employee.mining.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sangeetha.employee.mining.model.calendarModel;

import java.util.List;

public class StoreModel {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("store_code")
    @Expose
    private String store_code;
    @SerializedName("store_name")
    @Expose
    private String store_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStore_code() {
        return store_code;
    }

    public void setStore_code(String store_code) {
        this.store_code = store_code;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }
}
