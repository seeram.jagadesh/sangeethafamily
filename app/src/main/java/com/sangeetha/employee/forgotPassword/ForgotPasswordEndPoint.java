package com.sangeetha.employee.forgotPassword;

import com.sangeetha.employee.CommonResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Jasmini on 22/5/18.
 */

public interface ForgotPasswordEndPoint {


    @POST("api-generate-otp")
    @FormUrlEncoded
    Call<GenerateOtpResponse> generateOtp(@Field("customerNumber") String mob, @Field("customerEmail") String email);

    @POST("api-employee-verified")
    @FormUrlEncoded
    Call<CommonResponse> verifyOtp(@Field("employee_id") String mob, @Field("otp") String otp);

    @POST("api-employee-forgot")
    @FormUrlEncoded
    Call<CommonResponse> forgotPassword(@Field("employee_id") String mob);

    @POST("api-employee-change")
    @FormUrlEncoded
    Call<CommonResponse> changePwd(@Field("employee_id") String mob, @Field("new_password") String pwd);


}
