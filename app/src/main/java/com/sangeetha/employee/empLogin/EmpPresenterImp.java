package com.sangeetha.employee.empLogin;

import android.text.TextUtils;

import com.sangeetha.employee.empLogin.loginModel.ASMData;
import com.sangeetha.employee.empLogin.loginModel.EmpData;
import com.sangeetha.employee.empLogin.loginModel.OnlineDeliveryData;
import com.sangeetha.employee.empLogin.loginModel.StoreData;


public class EmpPresenterImp implements EmpPresenter, EmpInteractor.LoginProgressListener {
    private EmpLoginView empLoginView;
    private EmpInteractor empInteractor;

    EmpPresenterImp(EmpLoginView empLoginView) {
        this.empLoginView = empLoginView;
        this.empInteractor = new EmpInteractorImp(this);
    }

    @Override
    public void showMessage(String message) {
        if (empLoginView != null) {
            empLoginView.hideProgress();
            empLoginView.showMessage(message);
        }
    }

    @Override
    public void setCallRecordingEnable(Boolean isEnable) {
        if (empLoginView != null) {
            empLoginView.setCallRecordingEnable(isEnable);
        }
    }

    @Override
    public void setToken(String token) {
        if (empLoginView != null) {
            empLoginView.setToken(token);
        }
    }

    @Override
    public void loginSuccesses(EmpData data) {
        if (empLoginView != null) {
            empLoginView.hideProgress();
            empLoginView.loginSuccesses(data);
        }
    }

    @Override
    public void delLogin(EmpData data) {
        if (empLoginView != null) {
            empLoginView.hideProgress();
            empLoginView.deliveryBoyLogin(data);
        }
    }

    @Override
    public void setStoreData(StoreData storeData) {
        if (empLoginView != null) {
            empLoginView.hideProgress();
            empLoginView.setStoreData(storeData);
        }
    }

    @Override
    public void setASMData(ASMData asmData) {
        if (empLoginView != null) {
            empLoginView.hideProgress();
            empLoginView.setASMData(asmData);
        }
    }

    @Override
    public void setOnlineDeliveryData(OnlineDeliveryData onlineDeliveryData) {
        if (empLoginView != null) {
            empLoginView.hideProgress();
            empLoginView.setOnlineDeliveryData(onlineDeliveryData);
        }
    }

    @Override
    public void login(String empId, String pass, String IMEI) {
        if (empLoginView != null) {
            if (empId.isEmpty()) {
                empLoginView.onIdError("Please Enter Emp Id");
            } else if (TextUtils.isEmpty(pass)) {
                empLoginView.onPassError("Please Enter password");
            }
//            else if (TextUtils.isEmpty(IMEI)) {
//                empLoginView.showMessage("Please give IMEI permission");
//            }
            else {
                empLoginView.showProgress();
                empInteractor.login(empId, pass, IMEI);
            }
        }

    }

    @Override
    public void delLogin(String id, String pass) {
        if (empLoginView != null) {
            if (id.isEmpty()) {
                empLoginView.onIdError("Please Enter Emp Id");
            } else if (TextUtils.isEmpty(pass)) {
                empLoginView.onPassError("Please Enter password");
            }
//            else if (TextUtils.isEmpty(IMEI)) {
//                empLoginView.showMessage("Please give IMEI permission");
//            }
            else {
                empLoginView.showProgress();
                empInteractor.delLogin(id, pass);
            }
        }
    }
}
