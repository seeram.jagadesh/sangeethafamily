package com.sangeetha.employee.managerDiscount.storeASM;

import android.util.Log;

import androidx.annotation.NonNull;

import com.sangeetha.employee.CommonResponse;
import com.sangeetha.employee.RetrofitServiceGenerator;
import com.sangeetha.employee.checkStock.StockCheckResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.MakeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ProductTypeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.SubmitDiscountResponse;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.ApproveDiscountResponse;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.DiscountTypeResponse;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.PhoneDetailsResponse;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.RejectReasonResponse;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.StoreListResponse;
import com.sangeetha.employee.peopleTracking.model.StoreResponse;
import com.sangeetha.employee.utils.AppConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StoreASMInteractorImp implements StoreASMInteractor {
    private StoreASMEndPoint storeASMEndPoint;
    private StoreASMListener storeASMListener;

    public StoreASMInteractorImp(StoreASMListener storeASMListener) {
        this.storeASMListener = storeASMListener;
        storeASMEndPoint = RetrofitServiceGenerator.getClient(AppConstants.zapStoreBackEndBaseUrl).create(StoreASMEndPoint.class);
    }

    @Override
    public void getStoreList() {

    }

    @Override
    public void getStores(String token) {
        Log.e("store token",token);
        Log.e("store token",token);
        storeASMEndPoint.getStores(token).enqueue(new Callback<StoreResponse>() {
            @Override
            public void onResponse(Call<StoreResponse> call, Response<StoreResponse> response) {
                if (response.isSuccessful()) {
                    storeASMListener.setStores(response.body());
                } else {
                    storeASMListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(Call<StoreResponse> call, Throwable t) {
                storeASMListener.showMessage(t.getMessage());
            }
        });
    }

    @Override
    public void getMake(String userToken, String productId) {
        storeASMEndPoint.getMake(userToken,productId).enqueue(new Callback<MakeResponse>() {
            @Override
            public void onResponse(Call<MakeResponse> call, Response<MakeResponse> response) {
                if (response.isSuccessful()) {
                    storeASMListener.setMake(response.body());
                } else {
                    storeASMListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(Call<MakeResponse> call, Throwable t) {
                storeASMListener.showMessage(t.getMessage());
            }
        });
    }

    @Override
    public void getModel(String userToken, String productId, String makeId) {
        storeASMEndPoint.getModel(userToken,productId,makeId).enqueue(new Callback<ModelResponse>() {
            @Override
            public void onResponse(Call<ModelResponse> call, Response<ModelResponse> response) {
                if (response.isSuccessful()) {
                    storeASMListener.setModel(response.body());
                } else {
                    storeASMListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(Call<ModelResponse> call, Throwable t) {
                storeASMListener.showMessage(t.getMessage());
            }
        });
    }

    @Override
    public void getStocks(String token, String storeId, String productId, String makeId, String modelId) {
        Log.e("msg",token +" " + storeId+" " + productId+" " +makeId+" " + modelId);

        storeASMEndPoint.checkStock(token,productId,makeId,modelId,storeId).enqueue(new Callback<StockCheckResponse>() {
            @Override
            public void onResponse(Call<StockCheckResponse> call, Response<StockCheckResponse> response) {
                if (response.isSuccessful()) {
                    StockCheckResponse stockCheckResponse=response.body();
//                    if(stockCheckResponse.isStatus()) {
                        storeASMListener.setCheckStock(stockCheckResponse);
//                    }else{
//                        storeASMListener.showMessage(stockCheckResponse.getMessage());
//                    }
                } else {
                    storeASMListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(Call<StockCheckResponse> call, Throwable t) {
                storeASMListener.showMessage(t.getMessage());

            }
        });
    }

    @Override
    public void getProductType(String token, String paymentId) {
        storeASMEndPoint.getProductTypesList(token).enqueue(new Callback<ProductTypeResponse>() {
            @Override
            public void onResponse(@NonNull Call<ProductTypeResponse> call, @NonNull Response<ProductTypeResponse> response) {
                if (response.isSuccessful()) {
                    storeASMListener.setProductTypes(response.body());
                } else {
                    storeASMListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(@NonNull Call<ProductTypeResponse> call, @NonNull Throwable t) {
                storeASMListener.showMessage(t.getMessage());
            }
        });

    }
    @Override
    public void getWaitingApproval(String empId) {
        storeASMEndPoint.waitingApproval(empId).enqueue(new Callback<ApproveDiscountResponse>() {
            @Override
            public void onResponse(Call<ApproveDiscountResponse> call, Response<ApproveDiscountResponse> response) {
                if (response.isSuccessful()) {
                    storeASMListener.setApproval(response.body());
                } else {
                    storeASMListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(Call<ApproveDiscountResponse> call, Throwable t) {
                storeASMListener.showMessage(t.getMessage());

            }
        });

    }

    @Override
    public void getDiscountTypes(String token) {
        storeASMEndPoint.getDiscountTypeList(token).enqueue(new Callback<DiscountTypeResponse>() {
            @Override
            public void onResponse(Call<DiscountTypeResponse> call, Response<DiscountTypeResponse> response) {
                if (response.isSuccessful()) {
                    storeASMListener.setDiscountTypes(response.body());
                } else {
                    storeASMListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(Call<DiscountTypeResponse> call, Throwable t) {
                storeASMListener.showMessage(t.getMessage());

            }
        });

    }

    @Override
    public void getDiscountStatus(String token, String id) {
        storeASMEndPoint.getApprovalType(token, id).enqueue(new Callback<ApproveDiscountResponse>() {
            @Override
            public void onResponse(Call<ApproveDiscountResponse> call, Response<ApproveDiscountResponse> response) {
                if (response.isSuccessful()) {
                    storeASMListener.setDiscountStatus(response.body());
                } else {
                    storeASMListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(Call<ApproveDiscountResponse> call, Throwable t) {
                storeASMListener.showMessage(t.getMessage());

            }
        });

    }

    @Override
    public void getNotUsedApproval(String empId) {
        storeASMEndPoint.notUsedApproval(empId).enqueue(new Callback<ApproveDiscountResponse>() {
            @Override
            public void onResponse(Call<ApproveDiscountResponse> call, Response<ApproveDiscountResponse> response) {
                if (response.isSuccessful()) {
                    storeASMListener.setApproval(response.body());
                } else {
                    storeASMListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(Call<ApproveDiscountResponse> call, Throwable t) {
                storeASMListener.showMessage(t.getMessage());
            }
        });

    }

    @Override
    public void getUsedApproval(String empId) {
        storeASMEndPoint.usedApproval(empId).enqueue(new Callback<ApproveDiscountResponse>() {
            @Override
            public void onResponse(Call<ApproveDiscountResponse> call, Response<ApproveDiscountResponse> response) {
                if (response.isSuccessful()) {
                    storeASMListener.setApproval(response.body());
                } else {
                    storeASMListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(Call<ApproveDiscountResponse> call, Throwable t) {
                storeASMListener.showMessage(t.getMessage());
            }
        });

    }

    @Override
    public void discountApprove(String refId, String sourceFrom) {
        storeASMEndPoint.discountApprove(refId, sourceFrom).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String res = response.body().string();
                        JSONObject responseJson = new JSONObject(res);
                        if (responseJson.optBoolean("status")) {
                            storeASMListener.showMessage(responseJson.optString("message"));
                            storeASMListener.setDiscountApproval(responseJson.optString("apx_coupon_code"));
                        } else {
                            storeASMListener.showMessage(responseJson.optString("message"));
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    storeASMListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                storeASMListener.showMessage(t.getMessage());
            }
        });

    }

    @Override
    public void discountReject(String refId, String sourceFrom, String reason, String note) {

        storeASMEndPoint.discountReject(refId, sourceFrom, reason, note).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String res = response.body().string();
                        JSONObject responseJson = new JSONObject(res);
                        if (responseJson.optBoolean("status")) {
                            storeASMListener.setDiscountReject(responseJson.optString("message"));
                        } else {
                            storeASMListener.showMessage(responseJson.optString("message"));
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    storeASMListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                storeASMListener.showMessage(t.getMessage());
            }
        });

    }

    @Override
    public void getStoreList(String asmId) {
        storeASMEndPoint.getStoreList(asmId).enqueue(new Callback<StoreListResponse>() {
            @Override
            public void onResponse(Call<StoreListResponse> call, Response<StoreListResponse> response) {
                if (response.isSuccessful()) {
                    storeASMListener.setStoreList(response.body());
                } else {
                    storeASMListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(Call<StoreListResponse> call, Throwable t) {
                storeASMListener.showMessage(t.getMessage());
            }
        });

    }

    @Override
    public void getRejectionList(String token) {
        storeASMEndPoint.getRejectionList(token).enqueue(new Callback<RejectReasonResponse>() {
            @Override
            public void onResponse(Call<RejectReasonResponse> call, Response<RejectReasonResponse> response) {
                if (response.isSuccessful()) {
                    storeASMListener.setRejectionList(response.body());
                } else {
                    storeASMListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(Call<RejectReasonResponse> call, Throwable t) {
                storeASMListener.showMessage(t.getMessage());
            }
        });

    }

    @Override
    public void checkBargain(String regIs, String totalAmount, String empId) {
        storeASMEndPoint.checkBargain(regIs, totalAmount, empId).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    String res = null;
                    try {
                        res = response.body().string();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    storeASMListener.setDiscountCheck(res);

                } else {
                    storeASMListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                storeASMListener.showMessage(t.getMessage());
            }
        });

    }

    @Override
    public void submitBargain(String refId, String totalAmount, String empId, String bargainOption, String accessoryValue, String vasValue, String accessoryProduct, String vasProduct) {
        storeASMEndPoint.submitBargain(refId, totalAmount, empId, bargainOption, accessoryValue, vasValue, accessoryProduct, vasProduct).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String res = response.body().string();
                        storeASMListener.setSubmitBargain(res);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    storeASMListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                storeASMListener.showMessage(t.getMessage());
            }
        });

    }

    @Override
    public void getPhoneDetailByRefId(String token, String refId) {
        storeASMEndPoint.phoneDetailByRefId(token, refId).enqueue(new Callback<PhoneDetailsResponse>() {
            @Override
            public void onResponse(Call<PhoneDetailsResponse> call, Response<PhoneDetailsResponse> response) {
                if (response.isSuccessful()) {
                    storeASMListener.setPhoneDetail(response.body());
                } else {
                    storeASMListener.showMessage("Server returned an error");
                }
            }

            @Override
            public void onFailure(Call<PhoneDetailsResponse> call, Throwable t) {
                storeASMListener.showMessage(t.getMessage());

            }
        });
    }

    @Override
    public void checkEligibleException(String token) {

        storeASMEndPoint.checkExceptionEligible(token).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(@NonNull Call<CommonResponse> call, @NonNull Response<CommonResponse> response) {

                assert response.body() != null;
                if (response.isSuccessful()) {
                    storeASMListener.getCheckEligibleException(response.body());
                } else {
                    storeASMListener.showMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                storeASMListener.showMessage(t.getMessage());

            }
        });
    }

    @Override
    public void salesCheckCalculate(String token, String paymentMode, String productType, String makeId, String modelId, String discountValue,String storeId) {

        storeASMEndPoint.salesCheckCalculate(token, paymentMode, productType, makeId, modelId, discountValue,storeId).enqueue(new Callback<SubmitDiscountResponse>() {
            @Override
            public void onResponse(@NonNull Call<SubmitDiscountResponse> call, @NonNull Response<SubmitDiscountResponse> response) {
                assert response.body() != null;
                if (response.isSuccessful()) {
                    storeASMListener.getSalesCheckCalculate(response.body());
                } else {
                    storeASMListener.showMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(@NonNull Call<SubmitDiscountResponse> call, @NonNull Throwable t) {
                storeASMListener.showMessage(t.getMessage());
            }
        });
    }

    @Override
    public void sendExceptionRequest(String token, String paymentMode, String productType, String makeId,
                                     String modelId, String discountValue, String storeId, String totalAmount,
                                     String customerName, String customerMobile, String customerEmail,
                                     String customerMobileVerified, String employeeId) {
        storeASMEndPoint.exceptionSendRequest(token, paymentMode, productType, makeId, modelId, discountValue,
                storeId, totalAmount, customerName, customerMobile, customerEmail, customerMobileVerified,
                employeeId).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(@NonNull Call<CommonResponse> call,@NonNull  Response<CommonResponse> response) {
                assert response.body() != null;
                if (response.isSuccessful()) {
                    storeASMListener.sendExceptionRequest(response.body());
                } else {
                    storeASMListener.showMessage(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(@NonNull Call<CommonResponse> call,@NonNull  Throwable t) {
                storeASMListener.showMessage(t.getMessage());
            }
        });
    }
}
