package com.sangeetha.employee.onlineDelivery.interfaces;

public interface IFragmentListener {

    void addiSearch(ISearch iSearch);

    void removeISearch(ISearch iSearch);
}