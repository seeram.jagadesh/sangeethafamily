package com.sangeetha.employee.callRecorder.domain.model

import com.google.gson.annotations.SerializedName

data class MetaDataResponse(
    @SerializedName("status")
    val status: String,
    @SerializedName("interval_second")
    val interval_second: String,
    @SerializedName("message")
    val message: String,
)