package com.sangeetha.employee.mining.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryModel {

//     "id": "24",
//             "category_name": "Request Review/ Rating Feb Data",
//             "category_description": "Calling to check on how Ur phone is since u bought it recently.",
//             "content": "Hi sir/Madam. I am Calling from Sangeetha <branch name> regarding your recent purchase. \n1. This is a regular call to check how your phone is working. \n2. How are you doing? Hope your family and friends are all safe sir. Hope you are wearing the mask at all times. \n3. Just wanted to tell you that if you have bought on EMI bank is giving a moratorium, which means you can extend the loan period, more information is available about this on our Instagram account or facebook. This does not mean waiver but only extension of tenure.\n4. We are giving 5% extra discount to anybody from the medical fraternity as our gratitude to them once our stores reopen. So please share the message. \n5. Please give us a review/rating based on your previous visit to the store. You will get a link after this call, just click and rate and you will get a huge amount of Rs.500 just by giving a simple review. You can use this money to buy anything with us.\n\nFor more details log on to our Instagram/facebook account.\n",
//             "status": "1",
//             "created": "2020-03-26 12:45:21",
//             "modified": "2020-04-05 20:50:38"

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("category_description")
    @Expose
    private String categoryDescription;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("modified")
    @Expose
    private String modified;

    @Override
    public String toString() {
        return "CategoryModel{" +
                "id='" + id + '\'' +
                ", categoryName='" + categoryName + '\'' +
                ", categoryDescription='" + categoryDescription + '\'' +
                ", content='" + content + '\'' +
                ", status='" + status + '\'' +
                ", created='" + created + '\'' +
                ", modified='" + modified + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryDescription() {
        return categoryDescription;
    }

    public void setCategoryDescription(String categoryDescription) {
        this.categoryDescription = categoryDescription;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }
}
