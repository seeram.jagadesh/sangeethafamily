package com.sangeetha.employee.managerDiscount.storeASM;


public interface StoreASMPresenter {

    void getStoreList();

    void getMake(String userToken, String productId);

    void getModel(String userToken, String productId, String makeId);

    void getStores(String token);

    void stockCheck(String token,String storeId,String productType, String makeId, String modelId);

    void getWaitingApproval(String empId);

    void getProductType(String token, String paymentId);

    void getDiscounType(String token);

    void getNotUsedApproval(String empId);

    void getUsedApproval(String empId);

    void getDiscountStatus(String token, String empId);

    void discountApprove(String refId, String sourceFrom);

    void discountReject(String token, String refId, String reason, String note);

    void getStoreList(String asmId);

    void getRejectionList(String token);

    void checkBargain(String regIs, String totalAmount, String empId);

    void submitBargain(String refId, String totalAmount, String empId, String bargainOption, String accessoryValue, String vasValue, String accessoryProduct, String vasProduct);

    void getPhoneDetailByRefId(String token, String refId);

    void checkEligibleException(String token);

    void salesCheckCalculate(String token, String paymentMode, String productType, String makeId, String modelId, String discountValue,String storeId);

    void sendExceptionRequest(String token, String paymentMode, String productType, String makeId,
                              String modelId, String discountValue, String storeId, String totalAmount,
                              String customerName, String customerMobile, String customerEmail,
                              String customerMobileVerified, String employeeId);
}
