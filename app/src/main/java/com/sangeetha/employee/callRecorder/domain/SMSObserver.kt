package com.sangeetha.employee.callRecorder.domain

import android.annotation.SuppressLint
import android.content.ContentResolver
import android.database.ContentObserver
import android.database.Cursor
import android.net.Uri
import android.os.Handler
import java.util.*

class SMSObserver(
    private val contentResolver: ContentResolver,
    private val sharedPreferences: SharedPreferencesSmsStorage,
    handler: Handler,
    private val smsListener: (CugSMS) -> Unit
) : ContentObserver(handler) {

    companion object {
        private val SMS_URI = Uri.parse("content://sms/")
        private val SMS_SENT_URI = Uri.parse("content://sms/sent")
        private val SMS_INBOX_URI = Uri.parse("content://sms/inbox")

        private const val ADDRESS_COLUMN_NAME = "address";
        private const val DATE_COLUMN_NAME = "date";
        private const val BODY_COLUMN_NAME = "body";
        private const val TYPE_COLUMN_NAME = "type";
        private const val ID_COLUMN_NAME = "_id";

        private const val PROTOCOL_COLUMN_NAME = "protocol"
        private const val SMS_ORDER = "date DESC"

        private const val SMS_MAX_AGE_MILLIS = 5000;
    }

    @SuppressLint("Recycle", "Range")
    override fun onChange(selfChange: Boolean) {
        super.onChange(selfChange)
        contentResolver.query(SMS_URI, null, null, null, null)?.let { protocolCursor ->
            processCursor {
                if (protocolCursor.moveToFirst()) {
                    val protocol: String? =
                        protocolCursor.getString(protocolCursor.getColumnIndex(PROTOCOL_COLUMN_NAME))
                    getDetailedCursor(protocol)?.let { cursor ->
                        processCursor {
                            parseSms(cursor)?.let { sms ->
                                if (shouldParseSms(sms.id, Date(sms.date.toLong()))) {
                                    updateLastSmsParsed(sms.id)
                                    smsListener(sms)
                                }
                            }
                            cursor
                        }
                    }
                }
                protocolCursor
            }
        }
    }

    private fun getDetailedCursor(protocol: String?): Cursor? = if (protocol == null) {
        contentResolver.query(SMS_SENT_URI, null, null, null, SMS_ORDER)
    } else {
        contentResolver.query(SMS_INBOX_URI, null, null, null, SMS_ORDER)
    }

    @SuppressLint("Range")
    private fun parseSms(cursor: Cursor): CugSMS? = if (cursor.isOkay()) {
        CugSMS(
            id = cursor.getInt(cursor.getColumnIndex(ID_COLUMN_NAME)),
            message = cursor.getString(cursor.getColumnIndex(BODY_COLUMN_NAME)),
            address = cursor.getString(cursor.getColumnIndex(ADDRESS_COLUMN_NAME)),
            type = cursor.getString(cursor.getColumnIndex(TYPE_COLUMN_NAME)),
            date = cursor.getString(cursor.getColumnIndex(DATE_COLUMN_NAME))
        )
    } else {
        null
    }

    private fun Cursor.isOkay(): Boolean = count > 0 && moveToNext()

    private fun shouldParseSms(smsId: Int, smsDate: Date): Boolean {
        val isFirstSmsParsed = isFirstSmsParsed()
        val isOld = isOld(smsDate)
        val shouldParseId = shouldParseSmsId(smsId)
        return isFirstSmsParsed && !isOld || !isFirstSmsParsed && shouldParseId
    }

    private fun isOld(smsDate: Date): Boolean {
        val now = Date()
        return now.time - smsDate.time > SMS_MAX_AGE_MILLIS
    }

    private fun shouldParseSmsId(smsId: Int): Boolean {
        if (sharedPreferences.isFirstSmsIntercepted()) {
            return false
        }
        val lastSmsIdIntercepted: Int = sharedPreferences.getLastSmsIntercepted()
        return smsId > lastSmsIdIntercepted
    }

    private fun isFirstSmsParsed(): Boolean {
        return sharedPreferences.isFirstSmsIntercepted()
    }

    private fun updateLastSmsParsed(smsId: Int) {
        sharedPreferences.updateLastSmsIntercepted(smsId)
    }

    private fun processCursor(block: () -> Cursor) {
        var cursor: Cursor? = null
        try {
            cursor = block()
        } finally {
            cursor?.close()
        }
    }
}