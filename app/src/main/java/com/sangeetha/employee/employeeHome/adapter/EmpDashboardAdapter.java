package com.sangeetha.employee.employeeHome.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.sangeetha.employee.AdapterCallback;
import com.sangeetha.employee.R;


public class EmpDashboardAdapter extends RecyclerView.Adapter<EmpDashboardAdapter.EmpDashboardItem> {

    private int[] icons;
    private String[] titles;
    private AdapterCallback adapterCallback;

    public EmpDashboardAdapter(int[] icons, String[] titles, AdapterCallback adapterCallback) {
        this.icons = icons;
        this.titles = titles;
        this.adapterCallback = adapterCallback;
    }

    @NonNull
    @Override
    public EmpDashboardItem onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.employee_dashboard_item, parent, false);

        return new EmpDashboardItem(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EmpDashboardItem holder, int position) {
        /*if (position == 1){
            holder.cardView.setVisibility(View.GONE);
        }else {
            holder.cardView.setVisibility(View.VISIBLE);
        }*/
        holder.imageView.setImageResource(icons[position]);
        holder.textView.setText(titles[position]);

    }

    @Override
    public int getItemCount() {
        return titles.length;
    }

    class EmpDashboardItem extends RecyclerView.ViewHolder {
        AppCompatImageView imageView;
        AppCompatTextView textView;
        LinearLayout cardView;

        EmpDashboardItem(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.item_icon);
            textView = itemView.findViewById(R.id.item_text);
            cardView = itemView.findViewById(R.id.dashboard_card);
            cardView.setOnClickListener(v -> {
                adapterCallback.onClickCallback(getAdapterPosition());
            });
        }
    }
}
