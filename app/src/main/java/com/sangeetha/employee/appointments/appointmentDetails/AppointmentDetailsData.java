package com.sangeetha.employee.appointments.appointmentDetails;

import com.google.gson.annotations.SerializedName;

public class AppointmentDetailsData {

    @SerializedName("appointmentDetails")
    public AppointmentDetails appointmentDetails;

    @Override
    public String toString() {
        return "AppointmentDetailsData{" +
                "appointmentDetails=" + appointmentDetails +
                '}';
    }

    public AppointmentDetails getAppointmentDetails() {
        return appointmentDetails;
    }

    public void setAppointmentDetails(AppointmentDetails appointmentDetails) {
        this.appointmentDetails = appointmentDetails;
    }
}
