package com.sangeetha.employee.managerDiscount.manager_pojo;

import com.google.gson.annotations.SerializedName;

public class FindEmployeeResponse {

    @SerializedName("employee_name")
    private String employeeName;

    @SerializedName("status")
    private boolean status;

    @SerializedName("message")
    private String message;

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return
                "FindEmployeeResponse{" +
                        "employee_name = '" + employeeName + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}