package com.sangeetha.employee.checkStock;

import com.google.gson.annotations.SerializedName;

public class StockCheckResponse {

    @SerializedName("status")
    public boolean status;

    @SerializedName("data")
    public StockCheckData stockCheckData;

    @SerializedName("message")
    public String message;

    @Override
    public String toString() {
        return "StockCheckResponse{" +
                "status=" + status +
                ", stockCheckData=" + stockCheckData +
                ", message='" + message + '\'' +
                '}';
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public StockCheckData getStockCheckData() {
        return stockCheckData;
    }

    public void setStockCheckData(StockCheckData stockCheckData) {
        this.stockCheckData = stockCheckData;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
