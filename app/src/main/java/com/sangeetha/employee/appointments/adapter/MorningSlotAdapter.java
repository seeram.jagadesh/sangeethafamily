package com.sangeetha.employee.appointments.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;


import com.sangeetha.employee.R;
import com.sangeetha.employee.appointments.appointmentDetails.TimeModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;


public class MorningSlotAdapter extends RecyclerView.Adapter<MorningSlotAdapter.StoreListViewHolder> {

    private Context mContext;

    List<TimeModel> itemModelList;
    private int lastSelectedPosition = -1;
    private TimeItemClick storeListClick;

    public MorningSlotAdapter(Context context, TimeItemClick storeClick, List<TimeModel> dataModel) {
        mContext = context;
        itemModelList=dataModel;
//        mStoreList = searchStores;
        storeListClick = storeClick;
    }

    @NotNull
    @Override
    public StoreListViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.single_item_timer, parent, false);
        return new StoreListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(StoreListViewHolder holder, int position) {
//        holder.checkStore.setVisibility(View.VISIBLE);
//        SearchItemModel myStores = mStoreList.get(position);
//        holder.mAddress.setText(myStores.getStoreAddress());
//        holder.checkStore.setChecked(position == lastSelectedPosition);

        holder.txtItem.setText(itemModelList.get(position).getTimeSlot());

        if(lastSelectedPosition==position) {
            if (itemModelList.get(position).isSelected()) {
                itemModelList.get(position).setSelected(false);
                holder.mainItem.setBackground(mContext.getResources().getDrawable(R.drawable.btn_bg));
            } else {
                holder.mainItem.setBackground(mContext.getResources().getDrawable(R.drawable.orangebtn_bg));
                itemModelList.get(position).setSelected(true);
            }
        }else{
            holder.mainItem.setBackground(mContext.getResources().getDrawable(R.drawable.btn_bg));
            itemModelList.get(position).setSelected(false);
        }
//        if(lastSelectedPosition==position){
//            holder.mainItem.setBackground(mContext.getResources().getDrawable(R.drawable.btn_bg));
//        }else{
//            holder.mainItem.setBackground(mContext.getResources().getDrawable(R.drawable.orangebtn_bg));
//        }
    }

    @Override
    public int getItemCount() {
        return itemModelList.size();
    }

    class StoreListViewHolder extends RecyclerView.ViewHolder {

        Context mContext;
        ImageView imgHome;
        AppCompatTextView txtItem;
        CardView mainItem;

        StoreListViewHolder(View itemView) {
            super(itemView);

            txtItem=itemView.findViewById(R.id.txtItemView);
            mainItem=itemView.findViewById(R.id.myCardView);

            mContext = itemView.getContext();
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastSelectedPosition = getAdapterPosition();
                    if(!itemModelList.get(getAdapterPosition()).isSelected()) {
                        storeListClick.getClickedItem(itemModelList.get(getAdapterPosition()));
                    }
//                    storeListClick.getBookingStore(mStoreList.get(getAdapterPosition()));
                    notifyDataSetChanged();
                }
            };
            itemView.setOnClickListener(clickListener);

        }

    }
}