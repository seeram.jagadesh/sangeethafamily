package com.sangeetha.employee.appointments.appointmentDetails;

import com.google.gson.annotations.SerializedName;

public class AppointmentDetailsResponse {

    @SerializedName("status")
    public boolean status;

    @SerializedName("message")
    public String message;

    @SerializedName("refresh")
    public String refresh;


    @SerializedName("data")
    public AppointmentDetailsData appointmentDetails;


    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRefresh() {
        return refresh;
    }

    public void setRefresh(String refresh) {
        this.refresh = refresh;
    }

    public AppointmentDetailsData getAppointmentDetails() {
        return appointmentDetails;
    }

    public void setAppointmentDetails(AppointmentDetailsData appointmentDetails) {
        this.appointmentDetails = appointmentDetails;
    }

    @Override
    public String toString() {
        return "AppointmentDetailsResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", refresh='" + refresh + '\'' +
                ", appointmentDetails=" + appointmentDetails +
                '}';
    }
}
