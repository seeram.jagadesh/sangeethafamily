package com.sangeetha.employee.storeOrders.model;

import com.google.gson.annotations.SerializedName;

public class OrderSummary {

    @SerializedName("id")
    private String id;

    @SerializedName("imei_no")
    private String imei_no;

    @SerializedName("imei_request")
    private String imei_request;

    @SerializedName("store_pickup_status")
    private String store_pickup_status;

    @SerializedName("order_store_status")
    private String order_store_status;

    @SerializedName("shipping_address_id")
    private String shipping_address_id;

    @SerializedName("delivery_type")
    private String delivery_type;

    @SerializedName("billing_country")
    private String billing_country;

    @SerializedName("billing_state")
    private String billing_state;

    @SerializedName("billing_pincode")
    private String billing_pincode;

    @SerializedName("billing_city")
    private String billing_city;

    @SerializedName("billing_landmark")
    private String billing_landmark;

    @SerializedName("billing_address")
    private String billing_address;

    @SerializedName("billing_mobile_number")
    private String billing_mobile_number;

    @SerializedName("billing_email")
    private String billing_email;

    @SerializedName("billing_name")
    private String billing_name;

    @SerializedName("shipping_country")
    private String shipping_country;

    @SerializedName("shipping_state")
    private String shipping_state;

    @SerializedName("shipping_pincode")
    private String shipping_pincode;

    @SerializedName("shipping_city")
    private String shipping_city;

    @SerializedName("shipping_landmark")
    private String shipping_landmark;

    @SerializedName("shipping_address")
    private String shipping_address;

    @SerializedName("shipping_mobile_number")
    private String shipping_mobile_number;

    @SerializedName("shipping_email")
    private String shipping_email;

    @SerializedName("shipping_name")
    private String shipping_name;

    @SerializedName("order_unique_id")
    private String order_unique_id;

    @SerializedName("payment_option")
    private String payment_option;

    @SerializedName("payment_gateway_name")
    private String payment_gateway_name;

    @SerializedName("user_id")
    private String user_id;

    @SerializedName("total_price")
    private String total_price;

    @SerializedName("order_status")
    private String order_status;

    @SerializedName("status")
    private String status;

    @SerializedName("allocated_store")
    private String allocated_store;

    @SerializedName("is_app")
    private String is_app;

    @SerializedName("created_at")
    private String created_at;

    @SerializedName("transaction_id")
    private String transaction_id;

    @SerializedName("payment_transaction_id")
    private String payment_transaction_id;

    @SerializedName("payment_status")
    private String payment_status;

    @SerializedName("customer_name")
    private String customer_name;

    @SerializedName("customer_mobile")
    private String customer_mobile;

    @SerializedName("customer_email")
    private String customer_email;

    @Override
    public String toString() {
        return "OrderSummary{" +
                "id='" + id + '\'' +
                ", imei_no='" + imei_no + '\'' +
                ", imei_request='" + imei_request + '\'' +
                ", store_pickup_status='" + store_pickup_status + '\'' +
                ", order_store_status='" + order_store_status + '\'' +
                ", shipping_address_id='" + shipping_address_id + '\'' +
                ", delivery_type='" + delivery_type + '\'' +
                ", billing_country='" + billing_country + '\'' +
                ", billing_state='" + billing_state + '\'' +
                ", billing_pincode='" + billing_pincode + '\'' +
                ", billing_city='" + billing_city + '\'' +
                ", billing_landmark='" + billing_landmark + '\'' +
                ", billing_address='" + billing_address + '\'' +
                ", billing_mobile_number='" + billing_mobile_number + '\'' +
                ", billing_email='" + billing_email + '\'' +
                ", billing_name='" + billing_name + '\'' +
                ", shipping_country='" + shipping_country + '\'' +
                ", shipping_state='" + shipping_state + '\'' +
                ", shipping_pincode='" + shipping_pincode + '\'' +
                ", shipping_city='" + shipping_city + '\'' +
                ", shipping_landmark='" + shipping_landmark + '\'' +
                ", shipping_address='" + shipping_address + '\'' +
                ", shipping_mobile_number='" + shipping_mobile_number + '\'' +
                ", shipping_email='" + shipping_email + '\'' +
                ", shipping_name='" + shipping_name + '\'' +
                ", order_unique_id='" + order_unique_id + '\'' +
                ", payment_option='" + payment_option + '\'' +
                ", payment_gateway_name='" + payment_gateway_name + '\'' +
                ", user_id='" + user_id + '\'' +
                ", total_price='" + total_price + '\'' +
                ", order_status='" + order_status + '\'' +
                ", status='" + status + '\'' +
                ", allocated_store='" + allocated_store + '\'' +
                ", is_app='" + is_app + '\'' +
                ", created_at='" + created_at + '\'' +
                ", transaction_id='" + transaction_id + '\'' +
                ", payment_transaction_id='" + payment_transaction_id + '\'' +
                ", payment_status='" + payment_status + '\'' +
                ", customer_name='" + customer_name + '\'' +
                ", customer_mobile='" + customer_mobile + '\'' +
                ", customer_email='" + customer_email + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImei_no() {
        return imei_no;
    }

    public void setImei_no(String imei_no) {
        this.imei_no = imei_no;
    }

    public String getImei_request() {
        return imei_request;
    }

    public void setImei_request(String imei_request) {
        this.imei_request = imei_request;
    }

    public String getStore_pickup_status() {
        return store_pickup_status;
    }

    public void setStore_pickup_status(String store_pickup_status) {
        this.store_pickup_status = store_pickup_status;
    }

    public String getOrder_store_status() {
        return order_store_status;
    }

    public void setOrder_store_status(String order_store_status) {
        this.order_store_status = order_store_status;
    }

    public String getShipping_address_id() {
        return shipping_address_id;
    }

    public void setShipping_address_id(String shipping_address_id) {
        this.shipping_address_id = shipping_address_id;
    }

    public String getDelivery_type() {
        return delivery_type;
    }

    public void setDelivery_type(String delivery_type) {
        this.delivery_type = delivery_type;
    }

    public String getBilling_country() {
        return billing_country;
    }

    public void setBilling_country(String billing_country) {
        this.billing_country = billing_country;
    }

    public String getBilling_state() {
        return billing_state;
    }

    public void setBilling_state(String billing_state) {
        this.billing_state = billing_state;
    }

    public String getBilling_pincode() {
        return billing_pincode;
    }

    public void setBilling_pincode(String billing_pincode) {
        this.billing_pincode = billing_pincode;
    }

    public String getBilling_city() {
        return billing_city;
    }

    public void setBilling_city(String billing_city) {
        this.billing_city = billing_city;
    }

    public String getBilling_landmark() {
        return billing_landmark;
    }

    public void setBilling_landmark(String billing_landmark) {
        this.billing_landmark = billing_landmark;
    }

    public String getBilling_address() {
        return billing_address;
    }

    public void setBilling_address(String billing_address) {
        this.billing_address = billing_address;
    }

    public String getBilling_mobile_number() {
        return billing_mobile_number;
    }

    public void setBilling_mobile_number(String billing_mobile_number) {
        this.billing_mobile_number = billing_mobile_number;
    }

    public String getBilling_email() {
        return billing_email;
    }

    public void setBilling_email(String billing_email) {
        this.billing_email = billing_email;
    }

    public String getBilling_name() {
        return billing_name;
    }

    public void setBilling_name(String billing_name) {
        this.billing_name = billing_name;
    }

    public String getShipping_country() {
        return shipping_country;
    }

    public void setShipping_country(String shipping_country) {
        this.shipping_country = shipping_country;
    }

    public String getShipping_state() {
        return shipping_state;
    }

    public void setShipping_state(String shipping_state) {
        this.shipping_state = shipping_state;
    }

    public String getShipping_pincode() {
        return shipping_pincode;
    }

    public void setShipping_pincode(String shipping_pincode) {
        this.shipping_pincode = shipping_pincode;
    }

    public String getShipping_city() {
        return shipping_city;
    }

    public void setShipping_city(String shipping_city) {
        this.shipping_city = shipping_city;
    }

    public String getShipping_landmark() {
        return shipping_landmark;
    }

    public void setShipping_landmark(String shipping_landmark) {
        this.shipping_landmark = shipping_landmark;
    }

    public String getShipping_address() {
        return shipping_address;
    }

    public void setShipping_address(String shipping_address) {
        this.shipping_address = shipping_address;
    }

    public String getShipping_mobile_number() {
        return shipping_mobile_number;
    }

    public void setShipping_mobile_number(String shipping_mobile_number) {
        this.shipping_mobile_number = shipping_mobile_number;
    }

    public String getShipping_email() {
        return shipping_email;
    }

    public void setShipping_email(String shipping_email) {
        this.shipping_email = shipping_email;
    }

    public String getShipping_name() {
        return shipping_name;
    }

    public void setShipping_name(String shipping_name) {
        this.shipping_name = shipping_name;
    }

    public String getCustomer_email() {
        return customer_email;
    }

    public void setCustomer_email(String customer_email) {
        this.customer_email = customer_email;
    }

    public String getOrder_unique_id() {
        return order_unique_id;
    }

    public void setOrder_unique_id(String order_unique_id) {
        this.order_unique_id = order_unique_id;
    }

    public String getPayment_option() {
        return payment_option;
    }

    public void setPayment_option(String payment_option) {
        this.payment_option = payment_option;
    }

    public String getPayment_gateway_name() {
        return payment_gateway_name;
    }

    public void setPayment_gateway_name(String payment_gateway_name) {
        this.payment_gateway_name = payment_gateway_name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTotal_price() {
        return total_price;
    }

    public void setTotal_price(String total_price) {
        this.total_price = total_price;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAllocated_store() {
        return allocated_store;
    }

    public void setAllocated_store(String allocated_store) {
        this.allocated_store = allocated_store;
    }

    public String getIs_app() {
        return is_app;
    }

    public void setIs_app(String is_app) {
        this.is_app = is_app;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getPayment_transaction_id() {
        return payment_transaction_id;
    }

    public void setPayment_transaction_id(String payment_transaction_id) {
        this.payment_transaction_id = payment_transaction_id;
    }

    public String getPayment_status() {
        return payment_status;
    }

    public void setPayment_status(String payment_status) {
        this.payment_status = payment_status;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getCustomer_mobile() {
        return customer_mobile;
    }

    public void setCustomer_mobile(String customer_mobile) {
        this.customer_mobile = customer_mobile;
    }
}
