package com.sangeetha.employee.forgotPassword;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.sangeetha.employee.CommonResponse;
import com.sangeetha.employee.R;
import com.sangeetha.employee.SangeethaCareApiClient;
import com.sangeetha.employee.empLogin.EmployeeLogin;
import com.sangeetha.employee.utils.LanguageUtil;
import com.sangeetha.employee.utils.MyContextWrapper;
import com.sangeetha.employee.utils.ValidationUtils;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePwdActivity extends AppCompatActivity {
    private TextInputLayout newPwdTxtIP, confirmPwdTxtIP;
    private TextInputEditText newPwdEd, confirmPwdEd;

    private ConstraintLayout view;
    private String mobNum;
    private ProgressDialog otpGenerateProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pwd);

        mobNum = getIntent().getStringExtra("MOB_NUM_CHANGE_PWD");

        initChangePwdView();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        Locale languageType = LanguageUtil.getLanguageType(newBase);
        super.attachBaseContext(MyContextWrapper.wrap(newBase, languageType));
    }
    private void initChangePwdView() {
        view = findViewById(R.id.change_pwd_screen_id);

        newPwdTxtIP = findViewById(R.id.new_pwd_txt_ip);
        confirmPwdTxtIP = findViewById(R.id.confirm_pwd_txt_ip);

        newPwdEd = findViewById(R.id.new_pwd_ed);
        confirmPwdEd = findViewById(R.id.confirm_pwd_ed);

        textChangeListener();

        findViewById(R.id.submit_btn_change_pwd).setOnClickListener(v -> {
            if (ValidationUtils.isThereInternet(ChangePwdActivity.this)) {
                doValidationAndChangePwd();
            } else {
                showErrorSnack("No internet connection!");
            }
        });
    }

    private void showErrorSnack(String msg) {
        Snackbar snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    private void showError(TextInputLayout view, TextInputEditText edView, String msg) {
        view.setErrorEnabled(true);
        view.setError(msg);
        edView.setFocusable(true);
    }

    private void doValidationAndChangePwd() {
        String pwd = newPwdEd.getText().toString();
        String confirmPwd = confirmPwdEd.getText().toString();

        String msg;

        if (TextUtils.isEmpty(pwd)) {
            msg = "Please provide password";
            showErrorSnack(msg);
            showError(newPwdTxtIP, newPwdEd, msg);
        } else if (pwd.length() < 5) {
            msg = "Password must be minimum 5 characters long";
            showErrorSnack(msg);
            showError(newPwdTxtIP, newPwdEd, msg);
        } else if (TextUtils.isEmpty(confirmPwd)) {
            msg = "Please provide Confirm password";
            showErrorSnack(msg);
            showError(confirmPwdTxtIP, confirmPwdEd, msg);
        } else if (confirmPwd.length() < 5) {
            msg = "Password doesn't match!";
            showErrorSnack(msg);
            showError(confirmPwdTxtIP, confirmPwdEd, msg);
        } else if (!pwd.equals(confirmPwd)) {
            msg = "Password doesn't match!";
            showErrorSnack(msg);
            showError(newPwdTxtIP, newPwdEd, msg);
        } else {
            changePwd(pwd);
        }
    }

    private void changePwd(String pwd) {
        showProgress("Changing password...");

        ForgotPasswordEndPoint apiCallable = SangeethaCareApiClient.getClient().create(ForgotPasswordEndPoint.class);
        apiCallable.changePwd(mobNum, pwd).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getStatus()) {
                            hideProgress();
                            Toast.makeText(ChangePwdActivity.this, "Password changed successfully! Please login to continue.", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(ChangePwdActivity.this, EmployeeLogin.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            //overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                        } else {
                            hideProgress();
                            Toast.makeText(ChangePwdActivity.this, "Unable to change password, Please try later", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        hideProgress();
                        Toast.makeText(ChangePwdActivity.this, "Unable to change password, Please try later", Toast.LENGTH_SHORT).show();

                    }
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                hideProgress();
                Toast.makeText(ChangePwdActivity.this, "Unable to change password, Please try later", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showProgress(String msg) {
        if (otpGenerateProgress != null)
            otpGenerateProgress = null;

        otpGenerateProgress = new ProgressDialog(ChangePwdActivity.this);
        otpGenerateProgress.setCancelable(false);
        otpGenerateProgress.setCanceledOnTouchOutside(false);
        otpGenerateProgress.setMessage(msg);
        otpGenerateProgress.show();
    }

    private void hideProgress() {
        if (otpGenerateProgress != null)
            otpGenerateProgress.dismiss();
    }

    private void textChangeListener() {
        newPwdEd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                newPwdTxtIP.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        confirmPwdEd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                confirmPwdTxtIP.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}
