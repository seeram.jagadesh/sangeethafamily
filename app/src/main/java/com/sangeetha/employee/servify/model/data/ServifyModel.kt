package com.sangeetha.employee.servify.model.data


import com.google.gson.annotations.SerializedName

data class ServifyModel(
    @SerializedName("message")
    val message: String,
    @SerializedName("data")
    val modelData: List<ModelData>,
    @SerializedName("status")
    val status: Boolean
)