package com.sangeetha.employee.appointments.appointmentDetails;

import com.google.gson.annotations.SerializedName;

public class AppointmentDescription {

    @SerializedName("appointment_status_1")
    public String appointment_status_1;

    @SerializedName("invoice_no")
    public String invoiceNumber;

    @SerializedName("invoice_date")
    public String invoiceDate;

    @Override
    public String toString() {
        return "AppointmentDescription{" +
                "appointment_status_1='" + appointment_status_1 + '\'' +
                ", invoiceNumber='" + invoiceNumber + '\'' +
                ", invoiceDate='" + invoiceDate + '\'' +
                '}';
    }

    public String getAppointment_status_1() {
        return appointment_status_1;
    }

    public void setAppointment_status_1(String appointment_status_1) {
        this.appointment_status_1 = appointment_status_1;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }
}
