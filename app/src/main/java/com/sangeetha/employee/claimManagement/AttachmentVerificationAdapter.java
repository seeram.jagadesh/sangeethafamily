package com.sangeetha.employee.claimManagement;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.sangeetha.employee.R;

public class AttachmentVerificationAdapter extends RecyclerView.Adapter {


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.attachment_verification, parent, false);
        return new AttachmentItem(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 4;
    }

    static class AttachmentItem extends RecyclerView.ViewHolder {
        AppCompatImageView attachmentImage;
        AppCompatTextView attachmentText;
        AppCompatImageButton attachmentDownload;

        public AttachmentItem(@NonNull View itemView) {
            super(itemView);
            attachmentImage = itemView.findViewById(R.id.attachment_image);
            attachmentText = itemView.findViewById(R.id.attachment_text);
            attachmentDownload = itemView.findViewById(R.id.attachment_download);
        }
    }
}
