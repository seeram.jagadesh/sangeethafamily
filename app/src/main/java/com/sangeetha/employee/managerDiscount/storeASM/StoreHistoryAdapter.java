package com.sangeetha.employee.managerDiscount.storeASM;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.sangeetha.employee.R;
import com.sangeetha.employee.managerDiscount.storeASM.asmPojo.StoreListItem;
import com.sangeetha.employee.utils.RecyclerViewOnClick;

import java.util.ArrayList;

public class StoreHistoryAdapter extends RecyclerView.Adapter<StoreHistoryAdapter.HistoryItem> {
    ArrayList<StoreListItem> storeList;
    private RecyclerViewOnClick recyclerViewOnClick;

    public StoreHistoryAdapter(ArrayList<StoreListItem> storeList, RecyclerViewOnClick recyclerViewOnClick) {
        this.storeList = storeList;
        this.recyclerViewOnClick = recyclerViewOnClick;
    }

    @NonNull
    @Override
    public HistoryItem onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.store_list_item, parent, false);

        return new HistoryItem(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryItem holder, int position) {
        holder.storeCode.setText(storeList.get(position).getStoreCode());
        holder.storeName.setText(storeList.get(position).getStoreName());
        holder.storeCugNo.setText(storeList.get(position).getStoreMobile());
        holder.storeAddress.setText(storeList.get(position).getStoreAddress());

        holder.storeCugNo.setOnClickListener(v -> {
            recyclerViewOnClick.onClick(storeList.get(position).getStoreMobile());
        });

    }

    @Override
    public int getItemCount() {
        return storeList.size();
    }

    class HistoryItem extends RecyclerView.ViewHolder {
        AppCompatTextView storeCode, storeName, storeAddress, storeCugNo;

        public HistoryItem(@NonNull View itemView) {
            super(itemView);
            storeCode = itemView.findViewById(R.id.storeCode);
            storeName = itemView.findViewById(R.id.storeName);
            storeAddress = itemView.findViewById(R.id.storeAddress);
            storeCugNo = itemView.findViewById(R.id.storeCugNo);
        }
    }
}
