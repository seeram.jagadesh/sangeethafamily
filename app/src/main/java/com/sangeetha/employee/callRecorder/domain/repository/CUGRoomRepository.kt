package com.sangeetha.employee.callRecorder.domain.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.sangeetha.employee.callRecorder.domain.room.CUGDao
import com.sangeetha.employee.callRecorder.domain.room.model.CUGMessage
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class CUGRoomRepository @Inject constructor(
    private val dao: CUGDao,
) {

    suspend fun addCUGMessage(cugMessage: CUGMessage) {
        dao.insert(cugMessage)
    }

    fun getMessages(): Flow<PagingData<CUGMessage>> = Pager(
        PagingConfig(
            pageSize = 10,
            enablePlaceholders = true
        )
    ) { dao.pagingSource() }.flow
}