package com.sangeetha.employee.cashify

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.google.android.material.navigation.NavigationView
import com.sangeetha.employee.R
import com.sangeetha.employee.cashify.dummy.DummyContent

class CashifyDashboardActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener,
        CashifyHistoryFragment.OnListFragmentInteractionListener, VoucherHistoryFragment.OnListFragmentInteractionListener {

    private var toolbar: Toolbar? = null
    private var drawerLayout: DrawerLayout? = null
    private var navigationView: NavigationView? = null
    var navController: NavController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cashify_dashboard)
        bindViews()
        setUpToolbar()
        setUpNavigation()
        navigationView!!.setNavigationItemSelectedListener(this)

    }

    private fun bindViews() {
        toolbar = findViewById(R.id.cashify_toolbar)
        drawerLayout = findViewById(R.id.cashify_drawer_layout)
        navController = Navigation.findNavController(this, R.id.cashify_main)
        navigationView = findViewById(R.id.cashify_nav_view)

    }

    private fun setUpToolbar() {
        setSupportActionBar(toolbar)
        val supportActionBar = supportActionBar
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true)
            supportActionBar.setDisplayShowHomeEnabled(true)
        }
    }

    private fun setUpNavigation() {
        NavigationUI.setupActionBarWithNavController(this, navController!!, drawerLayout)
        NavigationUI.setupWithNavController(navigationView!!, navController!!)
    }

    override fun onSupportNavigateUp(): Boolean {
        return NavigationUI.navigateUp(navController!!, drawerLayout)
    }

    override fun onBackPressed() {
        if (drawerLayout!!.isDrawerOpen(GravityCompat.START)) {
            drawerLayout!!.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_cashify_dash -> navController!!.navigate(R.id.action_global_cashifyDashboardFragment)
            R.id.nav_cashify_processing -> navController!!.navigate(R.id.action_global_processFragment)
            R.id.nav_cashify_history -> navController!!.navigate(R.id.action_global_cashifyHistoryFragment)
            R.id.nav_cashify_report -> navController!!.navigate(R.id.action_global_reportFragment)
            R.id.nav_cashify_voucher -> navController!!.navigate(R.id.action_global_voucherFragment)
            R.id.nav_cashify_voucher_code -> navController!!.navigate(R.id.action_global_voucherHistoryFragment)
            R.id.nav_cashify_voucher_report -> navController!!.navigate(R.id.action_global_voucherReportFragment)

        }
        drawerLayout!!.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onListFragmentInteraction(item: DummyContent.DummyItem?) {
        TODO("Not yet implemented")
    }


}
