package com.sangeetha.employee.appointments;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.sangeetha.employee.R;
import com.sangeetha.employee.appointments.appointmentDetails.AppointmentsDetailsActivity;

import java.util.List;

public class AppointmentRecycler extends RecyclerView.Adapter<AppointmentRecycler.examViewHolder> {
    List<Datum> datumList;
    Context mContext;

    public AppointmentRecycler(Context listActivity, List<Datum> datumList) {
        this.datumList = datumList;
        this.mContext = listActivity;
    }

    @NonNull
    @Override
    public examViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.appointment_view, parent, false);
        return new examViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull examViewHolder holder, int position) {
        Datum adapter = datumList.get(position);

        holder.txtAppointmentId.setText(adapter.getAppointmentId());
        holder.txtAppointmentType.setText(adapter.getAppointmentType());
        holder.txtCustomerMobile.setText(adapter.getCustomerMobile());
        holder.txtCustomerName.setText(adapter.getCustomerName());
        holder.txtModelName.setText(adapter.getModelName());
        holder.txtStatus.setText(adapter.getStatus());

        holder.txtAppointmentId.setOnClickListener(v -> {
            mContext.startActivity(new Intent(mContext, AppointmentsDetailsActivity.class)
                    .putExtra("id", adapter.getAppointmentId()));
        });
    }

    @Override
    public int getItemCount() {
        return datumList.size();
    }

    public class examViewHolder extends RecyclerView.ViewHolder {
        AppCompatTextView txtAppointmentId, txtAppointmentType, txtCustomerName, txtCustomerMobile,
                txtModelName, txtStatus;

        public examViewHolder(@NonNull View itemView) {
            super(itemView);

            txtAppointmentId = itemView.findViewById(R.id.txtAppointmentId);
            txtAppointmentType = itemView.findViewById(R.id.txtAppointmentType);
            txtCustomerName = itemView.findViewById(R.id.txtCustomerName);
            txtCustomerMobile = itemView.findViewById(R.id.txtCustomerMobile);
            txtModelName = itemView.findViewById(R.id.txtModelName);
            txtStatus = itemView.findViewById(R.id.txtStatus);


        }
    }
}
