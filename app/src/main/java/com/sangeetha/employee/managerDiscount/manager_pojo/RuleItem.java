package com.sangeetha.employee.managerDiscount.manager_pojo;

import com.google.gson.annotations.SerializedName;

public class RuleItem {

    @SerializedName("rule_id")
    private String ruleId;

    @SerializedName("low_percentage")
    private String lowPercentage;

    @SerializedName("approval")
    private String approval;

    @SerializedName("high_percentage")
    private String highPercentage;

    public String getRuleId() {
        return ruleId;
    }

    public void setRuleId(String ruleId) {
        this.ruleId = ruleId;
    }

    public String getLowPercentage() {
        return lowPercentage;
    }

    public void setLowPercentage(String lowPercentage) {
        this.lowPercentage = lowPercentage;
    }

    public String getApproval() {
        return approval;
    }

    public void setApproval(String approval) {
        this.approval = approval;
    }

    public String getHighPercentage() {
        return highPercentage;
    }

    public void setHighPercentage(String highPercentage) {
        this.highPercentage = highPercentage;
    }

    @Override
    public String toString() {
        return
                "RuleItem{" +
                        "rule_id = '" + ruleId + '\'' +
                        ",low_percentage = '" + lowPercentage + '\'' +
                        ",approval = '" + approval + '\'' +
                        ",high_percentage = '" + highPercentage + '\'' +
                        "}";
    }
}