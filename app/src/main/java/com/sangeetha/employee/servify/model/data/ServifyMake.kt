package com.sangeetha.employee.servify.model.data


import com.google.gson.annotations.SerializedName

data class ServifyMake(
    @SerializedName("data")
    val makeData: List<MakeData>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Boolean
)