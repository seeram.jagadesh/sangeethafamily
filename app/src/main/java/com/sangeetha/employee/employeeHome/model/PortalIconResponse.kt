package com.sangeetha.employee.employeeHome.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import org.json.JSONObject

class PortalIconResponse {

    @SerializedName("status")
    @Expose
    var status: Boolean = false

    @SerializedName("data")
    @Expose
    var data: JSONObject? = null

    @SerializedName("message")
    @Expose
    var message: String? = null

}