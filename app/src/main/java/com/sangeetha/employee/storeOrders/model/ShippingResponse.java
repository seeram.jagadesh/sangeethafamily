package com.sangeetha.employee.storeOrders.model;

import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

public class ShippingResponse {

    @SerializedName("status")
    private boolean status;

    @SerializedName("data")
    private JSONObject data;

    @SerializedName("message")
    private String message;

    @Override
    public String toString() {
        return "ShippingResponse{" +
                "status=" + status +
                ", data=" + data +
                ", message='" + message + '\'' +
                '}';
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public JSONObject getData() {
        return data;
    }

    public void setData(JSONObject data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
