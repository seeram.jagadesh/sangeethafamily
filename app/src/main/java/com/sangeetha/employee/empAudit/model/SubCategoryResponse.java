package com.sangeetha.employee.empAudit.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubCategoryResponse {

    @SerializedName("data")
    private List<SubCategoryDataItem> subCategoryData;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private boolean status;

    public List<SubCategoryDataItem> getSubCategoryData() {
        return subCategoryData;
    }

    public void setSubCategoryData(List<SubCategoryDataItem> subCategoryData) {
        this.subCategoryData = subCategoryData;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "SubCategoryResponse{" +
                        "subCategoryData = '" + subCategoryData + '\'' +
                        ",message = '" + message + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}