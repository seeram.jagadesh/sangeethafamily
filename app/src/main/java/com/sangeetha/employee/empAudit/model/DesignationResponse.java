package com.sangeetha.employee.empAudit.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DesignationResponse {

    @SerializedName("designations")
    private List<DesignationsItem> designations;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private boolean status;

    public List<DesignationsItem> getDesignations() {
        return designations;
    }

    public void setDesignations(List<DesignationsItem> designations) {
        this.designations = designations;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "DesignationResponse{" +
                        "designations = '" + designations + '\'' +
                        ",message = '" + message + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}