package com.sangeetha.employee.mining;

import androidx.lifecycle.MutableLiveData;

import com.sangeetha.employee.SangeethaCareApiClient;
import com.sangeetha.employee.mining.response.CalendarResponse;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

class CalendarRepository {


    CalendarRepository() {
    }

    MutableLiveData<CalendarResponse> calendarInfo(String storeCode) {
        calendarInterface apiInterface = SangeethaCareApiClient.getClient().create(calendarInterface.class);
        final MutableLiveData<CalendarResponse> commonResponseMutableLiveData = new MutableLiveData<>();
        apiInterface.myCalendar(storeCode).enqueue(new Callback<CalendarResponse>() {

            @Override
            public void onResponse(@NotNull Call<CalendarResponse> call, @NotNull Response<CalendarResponse> response) {
                if (response.isSuccessful()) {
                    commonResponseMutableLiveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(@NotNull Call<CalendarResponse> call, @NotNull Throwable t) {
                Timber.e(t);
            }
        });

        return commonResponseMutableLiveData;
    }


}
