package com.sangeetha.employee.onlineDelivery.interfaces;

public interface ISearch {

    void onTextQuery(String text);
}