package com.sangeetha.employee.empLogin.loginModel;

import com.google.gson.annotations.SerializedName;

public class StoreData {

    @SerializedName("store_id")
    private String storeId;

    @SerializedName("store_code")
    private String storeCode;

    @SerializedName("user_id")
    private String userId;

    @SerializedName("email")
    private String email;

    @SerializedName("username")
    private String username;

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return
                "StoreData{" +
                        "store_id = '" + storeId + '\'' +
                        ",store_code = '" + storeCode + '\'' +
                        ",user_id = '" + userId + '\'' +
                        ",email = '" + email + '\'' +
                        ",username = '" + username + '\'' +
                        "}";
    }
}