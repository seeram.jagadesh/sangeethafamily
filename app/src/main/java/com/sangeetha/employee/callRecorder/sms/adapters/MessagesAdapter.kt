package com.sangeetha.employee.callRecorder.sms.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.sangeetha.employee.callRecorder.domain.room.model.CUGMessage
import com.sangeetha.employee.databinding.ItemCugMessageBinding

class MessagesAdapter(private val clickListener: (cugMessage: CUGMessage) -> Unit) :
    PagingDataAdapter<CUGMessage, MessagesAdapter.BookmarkListViewHolder>(DIFF_CALLBACK) {

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<CUGMessage>() {
            override fun areItemsTheSame(oldItem: CUGMessage, newItem: CUGMessage): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: CUGMessage, newItem: CUGMessage): Boolean {
                return oldItem == newItem
            }
        }
    }

    override fun onBindViewHolder(holder: BookmarkListViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookmarkListViewHolder =
        BookmarkListViewHolder(ItemCugMessageBinding.inflate(LayoutInflater.from(parent.context)))

    inner class BookmarkListViewHolder(private val binding: ItemCugMessageBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(cugMessage: CUGMessage) {
            binding.textMessage.text = cugMessage.message
            binding.root.setOnClickListener {
                getItem(layoutPosition)?.let { selectedBookmark ->
                    clickListener(selectedBookmark)
                }
            }
        }
    }
}
