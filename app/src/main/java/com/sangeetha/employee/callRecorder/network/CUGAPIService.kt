package com.sangeetha.employee.callRecorder.network

import com.sangeetha.employee.callRecorder.domain.model.AudioFileUploadResponse
import com.sangeetha.employee.callRecorder.domain.model.LoginResponse
import com.sangeetha.employee.callRecorder.domain.model.MetaDataResponse
import com.sangeetha.employee.callRecorder.domain.model.StoreResponse
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface CUGAPIService {
    @POST("api/login/")
    @Headers("Cache-Control:no-cache, no-store")
    @FormUrlEncoded
    suspend fun login(
        @Field("username") userName: String?,
        @Field("password") password: String?,
        @Field("device_id") deviceId: String?
    ): Response<LoginResponse>

    @POST("api-employee-record")
    @Headers("Cache-Control:no-cache, no-store")
    @FormUrlEncoded
    fun smsPost(
        @Header("Authorization") token: String,
        @Field("type") callType: String,
        @Field("data") callList: JSONArray,
        @Field("info") info: JSONObject
    ): Call<ResponseBody>

    @POST("api-employee-record")
    @Headers("Cache-Control:no-cache, no-store")
    @FormUrlEncoded
    fun postCallData(
        @Header("Authorization") token: String,
        @Field("type") callType: String,
        @Field("data") callList: JSONArray,
        @Field("info") info: JSONObject
    ): Call<ResponseBody>

    @POST("api-employee-record")
    @Headers("Cache-Control:no-cache, no-store")
    @FormUrlEncoded
    fun postLocation(
        @Header("Authorization") token: String,
        @Field("type") callType: String,
        @Field("data") locationList: JSONArray,
        @Field("info") info: JSONObject
    ): Call<ResponseBody>

    @POST("api-employee-record")
    @Headers("Cache-Control:no-cache, no-store")
    @FormUrlEncoded
    fun postCheckIn(
        @Header("Authorization") token: String,
        @Field("type") callType: String,
        @Field("data") locationList: JSONArray,
        @Field("info") info: JSONObject?
    ): Call<ResponseBody>

    @GET("api-interval-time-set")
    fun getInfo(): Call<MetaDataResponse>

    @GET("api-fetch-store")
    fun getStores(@Header("sangeetha_token") token: String): Call<StoreResponse>

    @POST("api-employee-record")
    @Headers("Cache-Control:no-cache, no-store")
    @FormUrlEncoded
    fun postConnectivity(
        @Header("Authorization") token: String,
        @Field("type") callType: String,
        @Field("data") locationList: JSONArray,
        @Field("info") info: JSONObject?
    ): Call<ResponseBody>

    @Multipart
    @POST("api-employee-call-record")
    @Headers("Cache-Control:no-cache, no-store")
    fun uploadAudioFile(
        @Header("Authorization") token: String,
        @Part("purpose") purpose: String,
        @Part userProfileImage: MultipartBody.Part?
    ): Call<AudioFileUploadResponse>
}