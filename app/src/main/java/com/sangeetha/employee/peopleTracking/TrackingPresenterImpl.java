package com.sangeetha.employee.peopleTracking;

import android.text.TextUtils;

import com.sangeetha.employee.peopleTracking.model.StoreResponse;
import com.sangeetha.employee.peopleTracking.model.TrackingResponse;

public class TrackingPresenterImpl implements TrackingPresenter, TrackingInteractor.TrackingListener {

    private TrackingView trackingView;
    private TrackingInteractorImpl trackingListener;

    TrackingPresenterImpl(TrackingView trackingView) {
        this.trackingView = trackingView;
        trackingListener = new TrackingInteractorImpl(this);

    }

    @Override
    public void getStoreList() {
        if (trackingView != null) {
            trackingView.showProgress();
            trackingListener.getStoreList();
        }

    }

    @Override
    public void startTracking(String regId, String storeId, String imei, String type, String latValue, String lngValue) {
        if (trackingView != null) {
            if (TextUtils.isEmpty(regId)) {
                trackingView.showMessage("Please login");
            } else if (TextUtils.isEmpty(storeId)) {
                trackingView.showMessage("Please select store");
            }
//            else if (TextUtils.isEmpty(imei)) {
//                trackingView.showMessage("Empty IMEI");
//            }
            else if (TextUtils.isEmpty(latValue) && TextUtils.isEmpty(lngValue)) {
                trackingView.showMessage("Location is Empty");
            } else {
                trackingView.showProgress();
                trackingListener.startTracking(regId, storeId, imei, type, latValue, lngValue);
            }
        }

    }

    @Override
    public void stopTracking(String regId, String storeId, String imei, String type, String latValue, String lngValue, String trackingId) {
        if (trackingView != null) {
            if (TextUtils.isEmpty(regId)) {
                trackingView.showMessage("Please login");
            } else if (TextUtils.isEmpty(storeId)) {
                trackingView.showMessage("Please select store");
            } else if (TextUtils.isEmpty(imei)) {
                trackingView.showMessage("Empty IMEI");
            } else if (TextUtils.isEmpty(latValue) && TextUtils.isEmpty(lngValue)) {
                trackingView.showMessage("Location is Empty");
            } else if (TextUtils.isEmpty(trackingId)) {
                trackingView.showMessage("Tracking ID is Empty");
            } else {
                trackingView.showProgress();
                trackingListener.stopTracking(regId, storeId, imei, type, latValue, lngValue, trackingId);
            }
        }

    }

    @Override
    public void setMessage(String msg) {
        if (trackingView != null) {
            trackingView.hideProgress();
            trackingView.showMessage(msg);
        }

    }

    @Override
    public void setStoreList(StoreResponse storeList) {
        if (trackingView != null) {
            trackingView.hideProgress();
            trackingView.setStoreList(storeList);
        }

    }

    @Override
    public void setTracking(TrackingResponse trackingResponse) {
        if (trackingView != null) {
            trackingView.hideProgress();
            trackingView.setTrackingResponse(trackingResponse);
        }

    }
}
