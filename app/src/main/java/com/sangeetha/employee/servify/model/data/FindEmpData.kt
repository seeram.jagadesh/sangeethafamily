package com.sangeetha.employee.servify.model.data


import com.google.gson.annotations.SerializedName

data class FindEmpData(
    @SerializedName("employee_name")
    val employeeName: String,
    @SerializedName("status")
    val status: Boolean
)