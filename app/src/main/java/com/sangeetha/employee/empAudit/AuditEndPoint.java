package com.sangeetha.employee.empAudit;

import com.sangeetha.employee.empAudit.model.AuditReportResponse;
import com.sangeetha.employee.empAudit.model.BrandResponse;
import com.sangeetha.employee.empAudit.model.DesignationResponse;
import com.sangeetha.employee.empAudit.model.EmpCategoryResponse;
import com.sangeetha.employee.empAudit.model.EmpStatusResponse;
import com.sangeetha.employee.empAudit.model.SearchEmpResponse;
import com.sangeetha.employee.empAudit.model.StartAuditResponse;
import com.sangeetha.employee.empAudit.model.StoreResponse;
import com.sangeetha.employee.empAudit.model.SubCategoryResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface AuditEndPoint {

    @GET("api-fetch-store")
    Call<StoreResponse> getStores(@Header("sangeetha_token") String token);

    @GET("api-fetch-brand")
    Call<BrandResponse> getBrands(@Header("sangeetha_token") String token);

    @FormUrlEncoded
    @POST("api-search-employee")
    Call<SearchEmpResponse> getEmployeeDetail(@Header("sangeetha_token") String token,@Field("employee_id") String employeeID);

    @FormUrlEncoded
    @POST("api-audit-start")
    Call<StartAuditResponse> startAudit(@Header("sangeetha_token") String token,@Field("employee_id") String employee_id,
                                        @Field("store_code") String store_code,
                                        @Field("lat_long") String lat_long);

    @FormUrlEncoded
    @POST("api-audit-submit")
    Call<AuditReportResponse> submitAudit(@Header("sangeetha_token") String token,@Field("audit_id") String audit_id,
                                          @Field("employee_id") String employee_id,
                                          @Field("store_code") String store_code,
                                          @Field("emp_detail") String emp_detail,
                                          @Field("pro_detail") String pro_detail);

    @GET("api-employee-status")
    Call<EmpStatusResponse> getEmpStatus(@Header("sangeetha_token") String token);

    @GET("api-get-designation")
    Call<DesignationResponse> getDesignation(@Header("sangeetha_token") String token);

    @GET("api-fetch-category")
    Call<EmpCategoryResponse> getEmpCategory(@Header("sangeetha_token") String token);

    @FormUrlEncoded
    @POST("api-fetch-sub-category")
    Call<SubCategoryResponse> getSubCategory(@Header("sangeetha_token") String token,@Field("category_id") String categoryId);

}
