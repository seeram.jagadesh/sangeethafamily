package com.sangeetha.employee.onlineDelivery.model.orderDetails;


import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;


public class UserAddress{

	@SerializedName("pincode")
	private String pincode;

	@SerializedName("address")
	private String address;

	@SerializedName("city")
	private String city;

	@SerializedName("state")
	private String state;

	public void setPincode(String pincode){
		this.pincode = pincode;
	}

	public String getPincode(){
		return pincode;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	public void setState(String state){
		this.state = state;
	}

	public String getState(){
		return state;
	}

	@NotNull
	@Override
 	public String toString(){
		return 
			"UserAddress{" + 
			"pincode = '" + pincode + '\'' + 
			",address = '" + address + '\'' + 
			",city = '" + city + '\'' + 
			",state = '" + state + '\'' + 
			"}";
		}
}