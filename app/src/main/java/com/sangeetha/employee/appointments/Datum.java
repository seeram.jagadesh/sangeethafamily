
package com.sangeetha.employee.appointments;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Datum {

    @SerializedName("actions")
    private String mActions;
    @SerializedName("appointment_id")
    private String mAppointmentId;
    @SerializedName("appointment_type")
    private String mAppointmentType;
    @SerializedName("customer_mobile")
    private String mCustomerMobile;
    @SerializedName("customer_name")
    private String mCustomerName;
    @SerializedName("id")
    private String mId;
    @SerializedName("model_name")
    private String mModelName;
    @SerializedName("status")
    private String mStatus;

    public String getActions() {
        return mActions;
    }

    public void setActions(String actions) {
        mActions = actions;
    }

    public String getAppointmentId() {
        return mAppointmentId;
    }

    public void setAppointmentId(String appointmentId) {
        mAppointmentId = appointmentId;
    }

    public String getAppointmentType() {
        return mAppointmentType;
    }

    public void setAppointmentType(String appointmentType) {
        mAppointmentType = appointmentType;
    }

    public String getCustomerMobile() {
        return mCustomerMobile;
    }

    public void setCustomerMobile(String customerMobile) {
        mCustomerMobile = customerMobile;
    }

    public String getCustomerName() {
        return mCustomerName;
    }

    public void setCustomerName(String customerName) {
        mCustomerName = customerName;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getModelName() {
        return mModelName;
    }

    public void setModelName(String modelName) {
        mModelName = modelName;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

}
