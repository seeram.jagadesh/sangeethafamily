package com.sangeetha.employee.empAudit.model;

import com.google.gson.annotations.SerializedName;

public class EmpCategoryDataItem {

    @SerializedName("category_name")
    private String categoryName;

    @SerializedName("category_id")
    private String categoryId;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public String toString() {
        return
                "EmpCategoryDataItem{" +
                        "category_name = '" + categoryName + '\'' +
                        ",category_id = '" + categoryId + '\'' +
                        "}";
    }
}