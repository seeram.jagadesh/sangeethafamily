package com.sangeetha.employee.servify.model.data


import com.google.gson.annotations.SerializedName

data class MakeData(
    @SerializedName("id")
    val id: String,
    @SerializedName("make_name")
    val makeName: String
)