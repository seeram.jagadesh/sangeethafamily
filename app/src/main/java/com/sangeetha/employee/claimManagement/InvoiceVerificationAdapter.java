package com.sangeetha.employee.claimManagement;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.sangeetha.employee.R;

public class InvoiceVerificationAdapter extends RecyclerView.Adapter<InvoiceVerificationAdapter.InvoiceItem> {


    @NonNull
    @Override
    public InvoiceItem onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.invoice_verification, parent, false);

        return new InvoiceItem(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InvoiceItem holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    static class InvoiceItem extends RecyclerView.ViewHolder {

        AppCompatTextView invoiceKey, invoiceValue;
        RadioGroup invoiceVerification;

        InvoiceItem(@NonNull View itemView) {
            super(itemView);
            invoiceKey = itemView.findViewById(R.id.invoice_key);
            invoiceValue = itemView.findViewById(R.id.invoice_value);
            invoiceVerification = itemView.findViewById(R.id.verification);
        }
    }
}
