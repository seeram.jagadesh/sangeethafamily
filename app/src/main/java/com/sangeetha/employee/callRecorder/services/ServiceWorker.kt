package com.sangeetha.employee.callRecorder.services

import android.content.Context
import androidx.work.WorkerParameters
import timber.log.Timber
import android.content.Intent
import androidx.core.content.ContextCompat
import androidx.work.Worker
import com.sangeetha.employee.callRecorder.extensions.isServiceRunning

class ServiceWorker(
    private val context: Context,
    params: WorkerParameters
) : Worker(context, params) {

    override fun doWork(): Result {
        val isServiceRunning = context.isServiceRunning(CallRecordService::class.java)
        Timber.d("doWork called for: " + this.id + "  is service running = " + isServiceRunning)

//        Timber.d("Service Running: " + CallRecordService.isServiceRunning)
        if (!isServiceRunning) {
            Timber.d("starting service from doWork")
            val intent = Intent(context, CallRecordService::class.java)
            ContextCompat.startForegroundService(context, intent)
        }
        return Result.success()
    }

    override fun onStopped() {
        Timber.d("onStopped called for: " + this.id)
        super.onStopped()
    }
}