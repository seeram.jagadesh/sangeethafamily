package com.sangeetha.employee.empAudit;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;
import com.sangeetha.employee.R;
import com.sangeetha.employee.empAudit.model.DropdownDetailsItem;
import com.sangeetha.employee.empAudit.model.EmployeeDetailsItem;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AuditFormAdapter extends RecyclerView.Adapter<AuditFormAdapter.EmpViewHolder> {
    private ArrayList<String> empStatus;
    private Context context;
    private List<EmployeeDetailsItem> employeeDetails;

    AuditFormAdapter(List<EmployeeDetailsItem> employeeDetails, List<DropdownDetailsItem> dropdownDetails) {
        this.employeeDetails = employeeDetails;
        empStatus = new ArrayList<String>();
        empStatus.add(0,"Select");
        for (int i = 0; i < dropdownDetails.size(); i++) {
            empStatus.add(dropdownDetails.get(i).getStatusName());
        }
    }

    @NonNull
    @Override
    public EmpViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        context = parent.getContext();

        View view = inflater.inflate(R.layout.emp_audit_form, parent, false);
        return new EmpViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull EmpViewHolder holder, int position) {


        holder.empID.setText(employeeDetails.get(position).getEmployeeId());
        holder.empName.setText(employeeDetails.get(position).getEmployeeName());
        holder.empDesignation.setText(employeeDetails.get(position).getDesignation());
        holder.mobileNumber.setText(employeeDetails.get(position).getMobile());
        holder.empBrand.setText(employeeDetails.get(position).getDepartment());
        holder.empInfo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                employeeDetails.get(position).setAdditionalInfo(s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        holder.updateSpinner.setAdapter(new ArrayAdapter<>(context, R.layout.custom_spinner_layout, empStatus));
        holder.updateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                Log.e("update",empStatus.get(pos));
                employeeDetails.get(position).setUpdate(empStatus.get(pos));
                // Log.e("spinner update ", context.getResources().getStringArray(R.array.update_spinner)[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        holder.moreNumber.setOnClickListener(v -> {
            if (!holder.mobileNumber.getText().toString().isEmpty()) {
                String[] numbers = holder.mobileNumber.getText().toString().split(",");
                if (numbers.length < 5) {
                    employeeDetails.get(position).getNumbers().add(holder.mobileNumber.getText().toString());
                    addNumber(holder.mobileNumber, context, employeeDetails.get(position).getNumbers());
                } else {
                    Toast.makeText(context, "Only 5 numbers.", Toast.LENGTH_LONG).show();
                }
            }


        });

    }


    @Override
    public int getItemCount() {
        return employeeDetails.size();
    }

    private void addNumber(AppCompatTextView mobileNumber, Context context, ArrayList<String> mNumbers) {
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(context);
        View mView = layoutInflaterAndroid.inflate(R.layout.add_no, null);
        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(context);
        alertDialogBuilderUserInput.setView(mView);
        final TextInputEditText userInputDialogEditText = mView.findViewById(R.id.more_no);
        alertDialogBuilderUserInput
                .setCancelable(false)
                .setPositiveButton("Done", (dialogBox, id) -> {
                    // ToDo get user input here

                    if (mobileNumber.getText().toString().isEmpty()) {
                        mobileNumber.setText(userInputDialogEditText.getText());

                    } else {
                        String numbers = mobileNumber.getText().toString() + ", " + userInputDialogEditText.getText();
                        mobileNumber.setText(numbers);
                    }
                    mobileNumber.setSelected(true);
                    String num = mobileNumber.getText().toString();
                    String[] nums = num.split(",");
                    mNumbers.addAll(Arrays.asList(nums));

                })

                .setNegativeButton("Cancel",
                        (dialogBox, id) -> dialogBox.cancel());

        AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
        alertDialogAndroid.show();
    }

    public List<EmployeeDetailsItem> getEmpDetails() {
        return this.employeeDetails;

    }

    class EmpViewHolder extends RecyclerView.ViewHolder {
        private LinearLayoutCompat numberLayout;
        private AppCompatTextView empID, empName, empDesignation, empBrand;
        private TextInputEditText empInfo;
        private AppCompatImageButton moreNumber;
        private AppCompatTextView mobileNumber;
        private AppCompatSpinner updateSpinner;

        public EmpViewHolder(@NonNull View itemView) {
            super(itemView);
            empID = itemView.findViewById(R.id.emp_id);
            empName = itemView.findViewById(R.id.emp_name);
            empDesignation = itemView.findViewById(R.id.emp_designation);
            empBrand = itemView.findViewById(R.id.emp_brand);
            empInfo = itemView.findViewById(R.id.emp_info);
            numberLayout = itemView.findViewById(R.id.layout_no);
            moreNumber = itemView.findViewById(R.id.add_number);
            mobileNumber = itemView.findViewById(R.id.emp_mobile);
            updateSpinner = itemView.findViewById(R.id.emp_update);
        }

    }


}
