package com.sangeetha.employee.employee.servify.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.sangeetha.employee.managerDiscount.manager_pojo.MakeResponse
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelResponse
import com.sangeetha.employee.servify.model.data.*
import com.sangeetha.employee.servify.model.remote.ServifyRepository

class ServifyViewModel() : ViewModel() {
    val servifyRepository = ServifyRepository()

    fun checkReferenceCode(token: String, refId: String): LiveData<CheckReferenceCodeData> {
        return servifyRepository.checkReferenceCode(token, refId)
    }

    fun getMake(token: String, productType: String): LiveData<MakeResponse> {
        return servifyRepository.getServifyMakeNew(token, productType)
    }

    fun getModel(token: String, make: String, productType: String): LiveData<ModelResponse> {
        return servifyRepository.getServifyModelNew(token, make, productType)
    }


    fun findEmp(token: String, empId: String): LiveData<FindEmpData> {
        return servifyRepository.findEmployee(token, empId)
    }

    fun getCoupon(
        storeId: String,
        referenceCode: String,
        amount: String,
        tradeMode: String,
        imei: String,
        tradeStatus: String,
        customerName: String,
        customerMobile: String,
        customerEmail: String,
        make: String,
        model: String,
        empId: String,
        empName: String,
        agree: String,
        productType: String,
        retailerAmount: String,
        offerAmount : String,
        offerValue : String,
        sangOfferAmount: String,
        sangOfferValue : String
    ): LiveData<ServifyCouponCode> {
        return servifyRepository.getCoupon(
            storeId,
            referenceCode,
            amount,
            tradeMode,
            imei,
            tradeStatus,
            customerName,
            customerMobile,
            customerEmail,
            make,
            model,
            empId,
            empName,
            agree,
            productType,
            retailerAmount,
            offerAmount,offerValue,sangOfferAmount,sangOfferValue
        )
    }

    fun getHistory(token: String): LiveData<ServifyHistory> {
        return servifyRepository.getServifyHistory(token)
    }

    fun getProductType(token: String): LiveData<ProductTypeResponse> {
        return servifyRepository.getProducTypeList(token)
    }

    fun getCouponDetails(storeId: String, refId: String): LiveData<ServifyHistoryData> {
        return servifyRepository.getServifyDetails(storeId, refId)
    }

    fun getDetails(storeId: String, refId: String): LiveData<ServifyHistoryResponseData> {
        return servifyRepository.getServifyItemDetails(storeId, refId)
    }

    fun changeMakeModel(
        storeId: String,
        refId: String,
        make: String,
        model: String,
        productType: String
    ): LiveData<ResponseMessage> {
        return servifyRepository.changeMakeModel(storeId, refId, make, model, productType)
    }

    fun cancelCoupon(storeId: String, refId: String): LiveData<ResponseMessage> {
        return servifyRepository.cancelCoupon(storeId, refId)
    }


}
