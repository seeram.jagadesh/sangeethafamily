package com.sangeetha.employee.managerDiscount.manager_pojo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ModelDiscountResponse {

    @SerializedName("rule")
    private List<RuleItem> rule;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private boolean status;

    @SerializedName("manage_id")
    private String manageId;

    @SerializedName("type_discount")

    private String typeDiscount;

    public List<RuleItem> getRule() {
        return rule;
    }

    public void setRule(List<RuleItem> rule) {
        this.rule = rule;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getManageId() {
        return manageId;
    }

    public void setManageId(String manageId) {
        this.manageId = manageId;
    }

    public String getTypeDiscount() {
        return typeDiscount;
    }

    public void setTypeDiscount(String typeDiscount) {
        this.typeDiscount = typeDiscount;
    }

    @Override
    public String toString() {
        return
                "ModelDiscountResponse{" +
                        "rule = '" + rule + '\'' +
                        ",message = '" + message + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}