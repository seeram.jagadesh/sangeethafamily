package com.sangeetha.employee.mining.makeModel;

import com.sangeetha.employee.managerDiscount.manager_pojo.MakeResponse;
import com.sangeetha.employee.managerDiscount.manager_pojo.ModelResponse;
import com.sangeetha.employee.mining.response.StoreResponse;


public interface OfferInteractor {
    void getMake(String token);

    void getModel(String token, String makeId);

    void getStoreList(String token);


    interface DiwaliOfferListener {
        void showMessage(String msg);

        void setMake(MakeResponse managerResponse);

        void setModel(ModelResponse managerResponse);

        void setStores(StoreResponse response);
    }
}
