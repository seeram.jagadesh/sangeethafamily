package com.sangeetha.employee.servify.model.data


import com.google.gson.annotations.SerializedName

data class ServifyHistory(
        @SerializedName("message")
    val message: String,
        @SerializedName("data")
    val servifyHistoryData: List<ServifyHistoryData>,
        @SerializedName("status")
    val status: Boolean
)